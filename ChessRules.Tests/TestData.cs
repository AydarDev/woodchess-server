namespace ChessRules.Tests;

public class TestData
{
    internal static readonly string WhiteFen = "rn1qkb1r/pppb1pPp/4pn1p/3p4/Q1PPP3/2N2N2/PP3P1P/R1B1KB1R w KQkq - 0 1";
    internal static readonly string BlackFen = "rn1qkb1r/pppb1pPp/4pn2/3p4/Q1PPP3/2N2N2/PP3PpP/R1B1KB1R b KQkq - 0 1";
    internal static readonly string WhiteDualMoves = "r6r/pp2kp1p/1qpbpn1p/2n4b/Q1N1P1R1/2BP1N2/PP3P1P/4KBR1 w - - 0 1";
    internal static readonly string BlackDualMoves = "r1B4r/pp2kp1p/1qpb1n1p/2n1p2b/Q1N1P1R1/2BP1N2/PP3P1P/4KBR1 b - - 0 1";
    internal static readonly string CastlingWhite = "r3k2r/pppqbppp/2np1n2/1B2p3/4P1b1/2NPBN2/PPPQ1PPP/R3K2R w KQkq - 0 1";
    internal static readonly string CastlingBlack = "r3k2r/pppqbppp/2np1n2/1B2p3/4P1b1/2NPBN2/PPPQ1PPP/R3K2R b KQkq - 0 1";
    internal static readonly string PromotionBlackStart = "2r5/1P4P1/4k3/8/3K4/8/1p3p2/2N5 b - - 0 1";
    internal static readonly string SimpleDualMoves = "8/5kB1/RB6/1N3K2/n7/5N2/1B6/R5R1 w - - 0 1";
    internal static readonly string DualMovesWithWholeFromSquare = "8/b3bk2/2n3n1/2P1P3/2n3n1/b2Kb3/8/8 b - - 0 1";
    internal static readonly string DualPawnMoves = "rnbkqbnr/pppPpppp/3P4/4p3/3PP3/8/PPP2PPP/RNBQKBNR w KQkq - 0 1";
}