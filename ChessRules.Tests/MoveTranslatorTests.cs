﻿using Xunit;
using static ChessRules.Tests.TestData;

namespace ChessRules.Tests
{
    public class MoveTranslatorTests
    {
        [InlineData("Ne5", "Nf3e5")]
        [InlineData("N:d5", "Nc3d5")]
        [InlineData("Q:d7+", "Qa4d7")]
        [InlineData("Ba4", "")]
        [InlineData("Nc6", "")]
        [Theory]
        public void GetWhiteFigureMove(string inputMove, string result)
        {
            Chess chess = new Chess(WhiteFen);
            Assert.Equal(result, MoveTranslator.GetTranslator(chess).TranslateMove(inputMove));
        }

        [InlineData("Ba4", "")]
        [InlineData("Nc6", "")]
        [Theory]
        public void SendBlackMoveWhenWhiteFigureMove(string move, string result)
        {
            Chess chess = new Chess(WhiteFen);

            Assert.Equal(result, MoveTranslator.GetTranslator(chess).TranslateMove(move));
        }

        [InlineData("Ba4", "bd7a4")]
        [InlineData("Nc6", "nb8c6" )]
        [InlineData("Bb4", "bf8b4")]
        [InlineData("B:g7", "bf8g7")]
        [Theory]
        public void NormalBlackFigureMove(string inputMove, string expectedResult)
        {
            Chess chess = new Chess(BlackFen);
           Assert.Equal(expectedResult, MoveTranslator.GetTranslator(chess).TranslateMove(inputMove));
        }

        [InlineData("Q:d7", "")]
        [InlineData("Kd1", "")]
        [Theory]
        public void SendWhiteMoveWhenBlackFigureMove(string input, string expected)
        {
            Chess chess = new Chess(BlackFen);
            Assert.Equal(expected, MoveTranslator.GetTranslator(chess).TranslateMove(input));
            
        }

        [InlineData ("Nf:e5", "Nf3e5")]
        [InlineData ("N3:e5", "Nf3e5")]
        [InlineData ("Nc:e5", "Nc4e5")]
        [InlineData ("N4:e5", "Nc4e5")]
        [InlineData ("Nfd2",  "Nf3d2")]
        [InlineData ("N3d2",  "Nf3d2")]
        [Theory]
        public void ResolveKnightDualMovesForWhite(string move, string result)
        {
            Chess chess = new Chess(WhiteDualMoves);
            Assert.Equal(result, MoveTranslator.GetTranslator(chess).TranslateMove(move));
        }
        
        [InlineData ("R4g2",  "Rg4g2")]
        [InlineData ("R1g2",  "Rg1g2")]
        [Theory]
        public void ResolveRookDualMovesForWhite(string move, string result)
        {
            Chess chess = new Chess(WhiteDualMoves);
            
            Assert.Equal(result, MoveTranslator.GetTranslator(chess).TranslateMove(move));
        }

        [InlineData ("Nc:e4", "nc5e4")]
        [InlineData ("N5:e4", "nc5e4")]
        [InlineData ("Nf:e4", "nf6e4")]
        [InlineData ("N6:d7", "nf6d7")]
        [InlineData ("Nf:d7", "nf6d7")]
        [Theory]
        public void ResolveKnightDualMovesForBlack(string move, string result)
        {
            Chess chess = new Chess(BlackDualMoves);
            Assert.Equal(result, MoveTranslator.GetTranslator(chess).TranslateMove(move));
        }
        
        [InlineData ("Rh:c8", "rh8c8")]
        [InlineData ("Ra:c8", "ra8c8")]
        [InlineData ("Rab8", "ra8b8")]
        [InlineData ("Rhb8", "")]
        [Theory]
        public void ResolveRookDualMovesForBlack(string move, string result)
        {
            Chess chess = new Chess(BlackDualMoves);
            Assert.Equal(result, MoveTranslator.GetTranslator(chess).TranslateMove(move));
        }

        [InlineData("0-0", "Ke1g1")]
        [InlineData("0-0-0", "Ke1c1")]
        [Theory]
        public void MakeCastlingWhite(string inputMove, string outputMove)
        {
            Chess chess = new Chess(CastlingWhite);
            Assert.Equal(outputMove,  MoveTranslator.GetTranslator(chess).TranslateMove(inputMove));
        }
        
        [InlineData("0-0", "ke8g8")]
        [InlineData("0-0-0", "ke8c8")]
        [Theory]
        public void MakeCastlingBlack(string inputMove, string outputMove)
        {
            Chess chess = new Chess(CastlingBlack);
            Assert.Equal(outputMove,  MoveTranslator.GetTranslator(chess).TranslateMove(inputMove));
        }

        [InlineData("e5", "Pe4e5")]
        [InlineData("h4", "Ph2h4")]
        [InlineData("c5", "Pc4c5")]
        [InlineData("h5", "")]
        [InlineData("cd5", "Pc4d5")]
        [InlineData("ed5", "Pe4d5")]
        [InlineData("exd5", "Pe4d5")]
        [InlineData("ed", "Pe4d5")]
        [InlineData("g8Q", "Pg7g8Q")]
        [InlineData("ghR", "Pg7h8R")]
        [InlineData("c4c5", "Pc4c5")]
        [InlineData("g8=Q", "Pg7g8Q")]
        [InlineData("gxh8=N", "Pg7h8N")]
        [Theory]
        public void MakeWhitePawnMoves(string inputMove, string result)
        {
            Chess chess = new Chess(WhiteFen);
            Assert.Equal(result, MoveTranslator.GetTranslator(chess).TranslateMove(inputMove));
        }

        [InlineData("dc", "pd5c4")]
        [InlineData("h5", "ph7h5")]
        [InlineData("c5", "pc7c5")]
        [Theory]
        public void MakeBlackPawnMoves(string move, string result)
        {
            Chess chess = new Chess(BlackFen);
            Assert.Equal(result, MoveTranslator.GetTranslator(chess).TranslateMove(move));

        }

        [InlineData("f1=Q", "pf2f1q")]
        [InlineData("f1R", "pf2f1r")]
        [InlineData("f1Q", "pf2f1q")]
        [InlineData("f2f1N", "pf2f1n")]
        [InlineData("bxc1Q", "pb2c1q")]
        [Theory]
        public void MakeBlackPromotionMoves(string move, string result)
        {
            Chess chess = new Chess(PromotionBlackStart);
            Assert.Equal(result, MoveTranslator.GetTranslator(chess).TranslateMove(move));
        }

        [InlineData("jfd")]
        [InlineData("fdwe")]
        [InlineData("Rddd2")]
        [Theory]
        public void NotValidMoveFormatThrowsException(string move)
        {
            Chess chess = new Chess();
            Assert.Throws<FormatException>(() => MoveTranslator.GetTranslator(chess).TranslateMove(move));
        }

        [InlineData("Ph2h4", "Ph2h4")]
        [InlineData("Rh1g1", "Rh1g1")]
        [InlineData("Pg7h8Q", "Pg7h8Q")]
        [InlineData("Ke1g1", "Ke1g1")]
        [Theory]
        public void ReturnMoveAsIsIfMatchFullMoveFormatWhite(string move, string result)
        {
            Chess chess = new Chess(WhiteFen);
            Assert.Equal(result, MoveTranslator.GetTranslator(chess).TranslateMove(move));
        }

        [InlineData("nf6e4", "nf6e4")]
        [InlineData("pc7c5", "pc7c5")]
        [InlineData("pd5c4", "pd5c4")]
        [InlineData("ke8e7", "ke8e7")]
        [Theory]
        public void ReturnMoveAsIsIfMatchFullMoveFormatBlack(string move, string result)
        {
            Chess chess = new Chess(BlackFen);
            Assert.Equal(result, MoveTranslator.GetTranslator(chess).TranslateMove(move));
        }

        [InlineData("pf2f1q", "pf2f1q")]
        [InlineData("pf2f1r", "pf2f1r")]
        [InlineData("pf2f1n", "pf2f1n")]
        [InlineData("pb2c1q", "pb2c1q")]
        [Theory]
        public void ReturnMoveAsIsForPromotionMoves(string move, string result)
        {
            Chess chess = new Chess(PromotionBlackStart);
            Assert.Equal(result, MoveTranslator.GetTranslator(chess).TranslateMove(move));
        }

        [InlineData("f3e5", "Nf3e5")]
        [InlineData("a4d7", "Qa4d7")]
        [InlineData("c4c5", "Pc4c5")]
        
        [Theory]
        public void TranslateMoveFromTwoSquaresFormat(string move, string result)
        {
            var chess = new Chess(WhiteFen);
            Assert.Equal(result, MoveTranslator.GetTranslator(chess).TranslateMove(move));
        }
    }
}