using System.Runtime.InteropServices;
using Xunit;
using static ChessRules.Tests.TestData;

namespace ChessRules.Tests;

public class NotationTranslatorTests
{
    [InlineData("Ke1g1", "0-0")]
    [InlineData("Ke1c1", "0-0-0")]
    [InlineData("ke8g8", "0-0")]
    [InlineData("ke8c8", "0-0-0")]
    [InlineData("e1g1", "0-0")]
    [InlineData("e1c1", "0-0-0")]
    [InlineData("e8g8", "0-0")]
    [InlineData("e8c8", "0-0-0")]
    [Theory]
    public void TestWhiteCastlingReturnsCorrectValues(string inputMove, string result)
    {
        Assert.Equal(result, NotationTranslator.GetShortNotation(inputMove, new Chess()));
    }

    [InlineData("Qa4d7", "Qxd7+")]
    [InlineData("Nc3d5", "Nxd5")]
    [InlineData("Ra1b1", "Rb1")]
    [InlineData("Bf1d3", "Bd3")]
    [InlineData("Bc1e3", "Be3")]
    [InlineData("Nf3e5", "Ne5")]
    [Theory]
    public void TestSimpleWhiteFigureMoves(string inputMove, string result)
    {
        var chess = new Chess(WhiteFen);
        Assert.Equal(result, NotationTranslator.GetShortNotation(inputMove, chess));
    }

    [InlineData("nb8c6", "Nc6")]
    [InlineData("bd7a4", "Bxa4")]
    [InlineData("bd7c6", "Bc6")]
    [InlineData("bf8d6", "Bd6")]
    [InlineData("rh8g8", "Rg8")]
    [InlineData("nf6e4", "Nxe4")]
    [Theory]
    public void TestSimpleBlackFigureMoves(string inputMove, string result)
    {
        var chess = new Chess(BlackFen);
        Assert.Equal(result, NotationTranslator.GetShortNotation(inputMove, chess));
    }
    
    
    [InlineData("a4d7", "Qxd7+")]
    [InlineData("c3d5", "Nxd5")]
    [InlineData("a1b1", "Rb1")]
    [InlineData("f1d3", "Bd3")]
    [InlineData("c1e3", "Be3")]
    [InlineData("f3e5", "Ne5")]
    [Theory]
    public void TestWhiteFigureMovesWithoutFigure(string inputMove, string result)
    {
        var chess = new Chess(WhiteFen);
        Assert.Equal(result, NotationTranslator.GetShortNotation(inputMove, chess));
    }
    
    [InlineData("b8c6", "Nc6")]
    [InlineData("d7a4", "Bxa4")]
    [InlineData("d7c6", "Bc6")]
    [InlineData("f8d6", "Bd6")]
    [InlineData("h8g8", "Rg8")]
    [InlineData("f6e4", "Nxe4")]
    [Theory]
    public void TestBlackFigureMovesWithoutFigure(string inputMove, string result)
    {
        var chess = new Chess(BlackFen);
        Assert.Equal(result, NotationTranslator.GetShortNotation(inputMove, chess));
    }


    [InlineData("Pe4e5", "e5")]
    [InlineData("Pb2b4", "b4")]
    [InlineData("Pc4d5", "cd")]
    [InlineData("Pg7g8Q", "g8=Q")]
    [InlineData("Pg7f8R", "gf=R+")]
    [Theory]
    public void TestWhitePawnMoves(string inputMove, string result)
    {
        var chess = new Chess(WhiteFen);
        Assert.Equal(result, NotationTranslator.GetShortNotation(inputMove, chess));
    }

    [InlineData("ph7h5", "h5")]
    [InlineData("pb7b5", "b5")]
    [InlineData("pd5e4", "de")]
    [InlineData("pg2g1q", "g1=Q")]
    [InlineData("pg2h1r", "gh=R")]
    [InlineData("pg2f1q", "gf=Q+")]
    [Theory]
    public void TestBlackPawnMoves(string inputMove, string result)
    {
        var chess = new Chess(BlackFen);
        Assert.Equal(result, NotationTranslator.GetShortNotation(inputMove, chess));
    }
    
    [InlineData("e4e5", "e5")]
    [InlineData("b2b4", "b4")]
    [InlineData("c4d5", "cd")]
    [InlineData("g7g8Q", "g8=Q")]
    [InlineData("g7f8R", "gf=R+")]
    [Theory]
    public void TestWhitePawnMovesWithoutPawnSign(string inputMove, string result)
    {
        var chess = new Chess(WhiteFen);
        Assert.Equal(result, NotationTranslator.GetShortNotation(inputMove, chess));
    }
    
    [InlineData("h7h5", "h5")]
    [InlineData("b7b5", "b5")]
    [InlineData("d5e4", "de")]
    [InlineData("g2g1q", "g1=Q")]
    [InlineData("g2h1r", "gh=R")]
    [InlineData("g2f1q", "gf=Q+")]
    [Theory]
    public void TestBlackPawnMovesWithoutPawnSign(string inputMove, string result)
    {
        var chess = new Chess(BlackFen);
        Assert.Equal(result, NotationTranslator.GetShortNotation(inputMove, chess));
    }

    [InlineData("a1d1", "Rad1")]
    [InlineData("Ra6a4", "R6xa4")]
    [InlineData("Nf3d4", "Nfd4")]
    [InlineData("g7d4", "Bgd4")]
    [InlineData("Bb6d4", "B6d4")]
    [Theory]
    public void TestSimpleDualMoves(string inputMove, string result)
    {
        var chess = new Chess(SimpleDualMoves);
        Assert.Equal(result, NotationTranslator.GetShortNotation(inputMove, chess));
    }

    
    [InlineData("g4e5", "Ng4xe5+")]
    [InlineData("nc6e5", "Nc6xe5+")]
    [InlineData("e3c5", "Be3xc5")]
    [InlineData("ba7c5", "Ba7xc5")]
    [Theory]
    public void TestDualMovesWithWholeSquare(string inputMove, string result)
    {
        var chess = new Chess(DualMovesWithWholeFromSquare);
        Assert.Equal(result, NotationTranslator.GetShortNotation(inputMove, chess));
    }

    [InlineData("Pd4e5", "dxe5")]
    [InlineData("d6e7", "dxe7+")]
    [InlineData("Pd7e8Q", "de=Q+")]
    [InlineData("d7e8q", "de=Q+")]
    [Theory]
    public void TestDualPawnMoves(string inputMove, string result)
    {
        var chess = new Chess(DualPawnMoves);
        Assert.Equal(result, NotationTranslator.GetShortNotation(inputMove, chess));
    }

    [InlineData("Qxd7+", "Qxd7+")]
    [InlineData("Nxd5", "Nxd5")]
    [InlineData("Rb1", "Rb1")]
    [InlineData("e5", "e5")]
    [InlineData("b4", "b4")]
    [InlineData("cd", "cd")]
    [InlineData("g8=Q", "g8=Q")]
    [InlineData("0-0", "0-0")]
    [Theory]
    public void MoveInShortNotationReturnsWithoutChanges(string inputMove, string result)
    {
        Assert.Equal(result, NotationTranslator.GetShortNotation(inputMove, new Chess()));
    }

    [Fact]
    public void CheckmateMoveByWhite()
    {
        var fen = "r1bqkb1r/pppp1ppp/2n2n2/4p2Q/2B1P3/8/PPPP1PPP/RNB1K1NR w KQkq - 0 1";
        
        Assert.Equal("Qxf7#", NotationTranslator.GetShortNotation("Qh5f7", fen));
    }

    [Fact]
    public void CheckmateMoveByBlack()
    {
        var fen = "rnb1k1nr/pppp1ppp/5q2/2b1p3/2B1P3/2N4P/PPPP1PP1/R1BQK1NR b KQkq - 0 1";
        
        Assert.Equal("Qxf2#", NotationTranslator.GetShortNotation("qf6f2", fen));
    }
}