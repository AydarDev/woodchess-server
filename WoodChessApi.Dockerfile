﻿FROM mcr.microsoft.com/dotnet/sdk:7.0 AS build-env
WORKDIR /app

COPY WoodChessApi/ ./WoodChessApi
COPY WoodChessModels/ ./WoodChessModels
COPY ChessRules/ ./ChessRules

RUN dotnet restore WoodChessApi/WoodChessApi.csproj

COPY . ./
RUN dotnet publish WoodChessApi/WoodChessApi.csproj -c Release -o out

FROM mcr.microsoft.com/dotnet/aspnet:7.0
WORKDIR /app
COPY --from=build-env /app/out .
ENTRYPOINT ["dotnet", "WoodChessApi.dll"]