﻿using ChessBlazor.BoardComponents;
using ChessBlazor.Shared;
using ChessRules;
using Microsoft.JSInterop;
using WebSocketModels.Boards;

namespace ChessBlazor.Main;

public class BlazorBoard : BaseBoard<Square, Figure>
{
    private Board board;
    private static Action<string, string> _jsAction;
    private bool isPromote;
    private string onPromotionMove;
    
    public BlazorBoard(Board board)
    {
        this.board = board;
        this.BoardConsumer = board.boardConsumer;
        _jsAction = DropObject;
        BoardState = BoardState.GetPlayBoard(board.SideColor);
        Chess = new Chess(board.Fen);
    }
    
    public override void SetUpBoardImpl()
    {
        
    }
    
    public async void DropObject(string figureSquare, string toSquare)
    {
        Console.WriteLine($"[Blazor] Drop figure {figureSquare} to {toSquare}");
        string move = $"{figureSquare}{toSquare}";
        string figureAt = figureSquare[0].ToString();
        if(IsValidMove(move))
        {
            Console.WriteLine($"[Blazor] Move {move} is valid");
            if (IsPromotionMove(figureAt, toSquare))
            {
                Console.WriteLine($"Promotion move {move}. IsPromote={isPromote}");
                ShowPromotionFigures(move);
            }
            else
                MakeMove(move);
        }
        else
        {
            Console.WriteLine($"[Blazor] Move {move} is not valid");
            await board.JsRuntime.InvokeVoidAsync("resetPosition", $"#{move.Substring(0, 3)}");
        }
    }
    public override void PickFigure(Figure figure)
    {
        
    }

    public override bool IsValidPick(Figure figure)
    {
        return true;
    }

    public override void ShowSquare(int x, int y, bool marked = false)
    {
        
    }

    public override void MakeCaptions()
    {
        
    }

    public override void ShowPromotionFigures(string move)
    {
        onPromotionMove = move;
        isPromote = true;
        PromotionWizard.InitPromotion(move);   
        //UpdateBoard();
    }

    public override void HidePromotionFigures()
    {
        isPromote = false;
        //UpdateBoard();
    }

    public override void ResetObject(Figure figure)
    {
        
    }


    public override async void MakeMove(string move)
    {
        Console.WriteLine($"[Blazor] Trying to invoke action with move: {move}");
        if (Chess != null && IsValidMove(move))
        {
            Console.WriteLine($"[Blazor] Move {move} is valid.\nFen: {Chess.Fen}");
            BoardConsumer?.SendMove(move);
        }
        else
        {
            Console.WriteLine($"Move {move} is not valid");
            await board.JsRuntime.InvokeVoidAsync("resetPosition", $"#{move.Substring(0, 3)}");
        }
    }
    
    [JSInvokable]
    public static void MakeMoveJs(string figureSquare, string toSquare)
    {
        _jsAction?.Invoke(figureSquare, toSquare);
    }

    internal string CoordsToSquare(int x, int y)
    {
        return BoardState.CoordsToSquare((x, y)) ?? "";
    }

    internal void CompletePromotion(char figure)
    {
        if(isPromote)
            MakeMove(onPromotionMove + figure);
        isPromote = false;
        onPromotionMove = "";
        //UpdateBoard();
    }
}