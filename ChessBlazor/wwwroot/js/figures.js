﻿function dragAndDrop(className) {
    const position = {x: 0, y: 0}
    interact(className).draggable({
        listeners: {
            start(event) {
                position.x = 0
                position.y = 0
                event.target.style.zIndex = 999;
                console.log(event.type, event.target)
                console.log("Start position x: " + position.x + " position y: " + position.y)
            },
            move(event) {
                position.x += event.dx
                position.y += event.dy

                event.target.style.left =  position.x + 'px'
                event.target.style.top = position.y + 'px'
                    // transform =
                    // `translate(${position.x}px, ${position.y}px)`
            }, drop() {
               
            }
        }, modifiers: [
            interact.modifiers.restrictRect({
                restriction: '.board',
                endOnly: true
            })]
    })
}

function setDropzone(className) {
    interact(className)
        .dropzone({
            ondrop: function (event) {
                console.log(event.relatedTarget.id
                    + ' was dropped into '
                    + event.target.id)

                event.relatedTarget.style.zIndex = 99
                var figureSquare = event.relatedTarget.id
                var toSquare = event.target.id
                DotNet.invokeMethodAsync('ChessBlazor', 'MakeMoveJs', figureSquare, toSquare)
                event.stopImmediatePropagation()
            }
        })
}

window.resetPosition = (figureId) => {
    console.log('Reset position for ' + figureId );
    $(figureId).css({top: 0, left: 0});
}


/*
function resetPosition(figure) {
    figure.style('top', '0px')
    figure.style('left', '0px')
}


window.setDraggable = () => {
    $('.figura').draggable({
        dragStart: function () {
            $(this).css("z-index", 9999);
        }        
    });
}

window.setDroppable = () => {
    $('.square').droppable({
        drop: function (event, ui) {
            var figureSquare = ui.draggable.attr('id');            
            var toCoord = $(this).attr('id');
            console.log("Move " + figureSquare + toCoord );
            setDefaultOrderLayer(figureSquare);
            DotNet.invokeMethodAsync('ChessBlazor', 'MakeMoveJs', figureSquare, toCoord );
        }
    });
}

window.resetPosition = (figureId) => {
    $(figureId).css({top: 0, left: 0});
}

/!*window.updateId = (oldId, newId) => {
    //console.log("Change id from " + oldId + " to " + newId);
    $("#" + oldId).attr("id", newId);
}*!/

window.setDefaultOrderLayer = (id) => {
    //console.log("Set order by default for " + id );
    $("#" + id).css("z-index", 387);
}*/
