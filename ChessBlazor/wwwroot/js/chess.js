﻿var map = [];
var divSquare = '<div id="s$coord" class="square $color"></div>'
var divFigure = '<div id="f$coord" class="figureDiv"><img class="figure" src="$image" /></div>'

var chessImage = {};

var source = '/Files/Figures/';
chessImage['K'] = source + 'WhiteKing.png';
chessImage['Q'] = source + 'WhiteQueen.png';
chessImage['R'] = source + 'WhiteRook.png';
chessImage['B'] = source + 'WhiteBishop.png';
chessImage['N'] = source + 'WhiteKnight.png';
chessImage['P'] = source + 'WhitePawn.png';
chessImage['k'] = source + 'BlackKing.png';
chessImage['q'] = source + 'BlackQueen.png';
chessImage['r'] = source + 'BlackRook.png';
chessImage['b'] = source + 'BlackBishop.png';
chessImage['n'] = source + 'BlackKnight.png';
chessImage['p'] = source + 'BlackPawn.png';
chessImage['1'] = source + 'Empty.png';


$(function () {
    start();
});

function start() {
    addSquares();
    showFigures('rnbqkbnrpppppppp11111111111111111111111111111111PPPPPPPPRNBQKBNR');
}

function addSquares() {
    $('.board').html('');
    for (var coord = 0; coord < 64; coord++)
        $('.board').append(divSquare
            .replace('$coord', coord)
            .replace('$color', isBlackSquareAt(coord) ? 'black' : 'white'));
    setDroppable();
}


function setDroppable() {
    $('.square').droppable({
        drop: function (event, ui) {
            var fromCoord = ui.draggable.attr('id').substring(1);
            var toCoord = $(this).attr('id').substring(1);
            moveFigure(fromCoord, toCoord);
        }
    });
}

function showFigures(figures) {
    for (var coord = 0; coord < 64; coord++)
        showFigureAt(coord, figures.charAt(coord));
}

function showFigureAt(coord, figure) {
    map[coord] = figure;

    if (figure == '1')
        setEmptyAt(coord);
    else {
        $('#s' + coord).html(divFigure
            .replace('$coord', coord)
            .replace('$image', chessImage[figure]));
    }

    setDraggable('#f' + coord);
}

function setEmptyAt(coord) {
    $('#s' + coord).html('');
}

function setDraggable(figure) {
    $(figure).draggable();
}

function moveFigure(fromCoord, toCoord) {
    figure = map[fromCoord];
    console.log('move ' + figure + ' from ' + fromCoord + ' to ' + toCoord);
    setEmptyAt(fromCoord, '1');
    showFigureAt(toCoord, figure);
}


function isBlackSquareAt(coord) {
    return (parseInt(coord / 8) + (coord % 8)) % 2;
}