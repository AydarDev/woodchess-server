﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Xunit;
using WoodChessV1.Models;
using static WoodChess.Tests.DataHelper;
using System.Collections.Concurrent;
using WoodChessV1.Exceptions;
using WoodChessV1.Models.NullClasses;

namespace WoodChess.Tests
{
    public class ChallengeManagerTests
    {
        private ChallengeInMemoryStorage _chInMemoryStorage;

        public ChallengeManagerTests()
        {
            _chInMemoryStorage = new ChallengeInMemoryStorage();
        }

        [Fact]
        public void CreateChallengeCorrectly()
        {
            _chInMemoryStorage.CreateChallenge(TestPlayers[0], NullPlayer.GetInstance(), new TimeControl{TimeRange = 10, TimeIncrement = 0}, 0);


            Assert.NotEmpty(_chInMemoryStorage.GetAllChallenges());
            Assert.Equal(1, _chInMemoryStorage.GetAllChallenges().First().FromPlayer.Id);

            DropChallengeData();
        }

        [Fact]
        public void CreateChallengeThrowsExceptionIfAlreadyExists()
        {
            _chInMemoryStorage.CreateChallenge(TestPlayers[0], NullPlayer.GetInstance(), new TimeControl(10, 5), 0);

            Assert.Throws<ChessException>(() => _chInMemoryStorage.CreateChallenge(TestPlayers[0], NullPlayer.GetInstance(),  new TimeControl(10, 5), 0));
            
            DropChallengeData();
        }

        [Fact]
        public void GetAllChallengesReturnsList()
        {
            FillChallengeData();

            var challenges = _chInMemoryStorage.GetAllChallenges();

            Assert.Equal(3, challenges.Count);

            DropChallengeData();
        }

        [Fact]
        public void GetChallengeByIdReturnsCorrectChallenge()
        {
            FillChallengeData();

            var challenge = _chInMemoryStorage.Get(new Guid("33333333-3333-3333-3333-333333333333"));

            Assert.True(!challenge.IsNull);
            Assert.Equal(3, challenge.TimeControl.TimeIncrement);

            DropChallengeData();
        }


        [Fact]
        public void GetChallengeByIdReturnsNullChallengeWhenNotExists()
        {
            FillChallengeData();

            var challenge = _chInMemoryStorage.Get(new Guid("33333333-3333-3333-0000-333333333333"));

            Assert.IsType<NullChallenge>(challenge);
            Assert.True(challenge.IsNull);
            
            DropChallengeData();
        }

        [Fact]
        public void GetMyChallengesReturnsCorrectList()
        {
            FillChallengeData();
            const int playerId = 2;

            var myChallenges = _chInMemoryStorage.GetPlayerChallenges(playerId);

            Assert.Single(myChallenges);
            Assert.Equal(playerId, myChallenges[0].FromPlayer.Id);

            DropChallengeData();
        }

        [Fact]
        public void GetPublicChallengesReturnsList()
        {
            FillChallengeData();

            var challenges = _chInMemoryStorage.GetPublicChallenges();

            Assert.Equal(2, challenges.Count);
            Assert.True(challenges.First().ToPlayer.IsNull);

            DropChallengeData();
        }

        [Fact]
        public void GetPrivateChallengesReturnsList()
        {
            FillChallengeData();

            const int playerId = 4;

            var challenges = _chInMemoryStorage.GetPrivateChallenges(playerId);

            Assert.Single(challenges);
            Assert.NotNull(challenges.First().ToPlayer);

            DropChallengeData();
        }

        [Fact]
        public void RemoveChallengeCorrectly()
        {
            FillChallengeData();

            var existingChallengeId = new Guid("33333333-3333-3333-3333-333333333333");
            var nonExistingChallengeId = new Guid("33333333-3333-3333-0000-333333333333");;

            var result1 = _chInMemoryStorage.Remove(existingChallengeId);
            var result2 = _chInMemoryStorage.Remove(nonExistingChallengeId);

            Assert.True(result1);
            Assert.False(result2);
            Assert.Equal(2, _chInMemoryStorage.GetAllChallenges().Count);
            
            DropChallengeData();
        }

        [Fact]
        public void RemoveCurrentChallengesCorrectly()
        {
            FillChallengeData();
            var player = TestPlayers[1];

            _chInMemoryStorage.RemoveCurrentChallenges(player.Id);

            var allChallenges = _chInMemoryStorage.GetAllChallenges();
            var playerChallenges = allChallenges.
                Where(ch => ch.FromPlayer.Id.Equals(player.Id) || ch.ToPlayer.Id.Equals(player.Id)).ToList();

            Assert.Empty(playerChallenges);
            Assert.Equal(2, allChallenges.Count);
        }


        private void FillChallengeData()
        {
            var tempDict = new ConcurrentDictionary<Guid, Challenge>();

            foreach (var challenge in GetTestChallenges()) tempDict.TryAdd(challenge.Id, challenge);

            _chInMemoryStorage.GetType().GetField("_currentChallenges", BindingFlags.NonPublic | BindingFlags.Instance)?.SetValue(_chInMemoryStorage, tempDict);
        }

        private void DropChallengeData()
        {
            var emptyDict = new ConcurrentDictionary<Guid, Challenge>();
            _chInMemoryStorage.GetType().GetField("_currentChallenges", BindingFlags.NonPublic | BindingFlags.Instance)?.SetValue(_chInMemoryStorage, emptyDict);
        }

    }
}
