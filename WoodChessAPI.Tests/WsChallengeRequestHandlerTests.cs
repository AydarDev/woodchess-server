﻿using ChessRules;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Moq;
using System;
using System.Collections.Generic;
using System.Net.WebSockets;
using System.Reflection;
using System.Threading.Tasks;
using AutoMapper;
using WebSocketModels.Enums;
using WebSocketModels.Models;
using WebSocketModels.WsMessages.WsRequests;
using WebSocketModels.WsMessages.WsResponses;
using WoodChessV1.ChessClocks;
using WoodChessV1.ChessEngine;
using WoodChessV1.Exceptions;
using WoodChessV1.Models;
using WoodChessV1.Repository.Abstract;
using WoodChessV1.Services.GroupServices;
using WoodChessV1.WebSocketClasses.WebSocketManager;
using WoodChessV1.WebSocketClasses.WsRequestHandlers;
using WoodChessV1.WebSocketClasses.WsRequestHandlers.Abstract;
using Xunit;
using static WoodChess.Tests.DataHelper;


namespace WoodChess.Tests
{
    public class WsChallengeRequestHandlerTests
    {
        private readonly Mock<IChessLogic> _chessLogic = new();
        private readonly Mock<IChallengeStorage> _challengeManager = new();
        private readonly Mock<IUnitOfWork> _repo = new();
        private readonly Mock<IGroupService> _groupManager = new();
        private readonly Mock<IWebSocketHandler> _messageHandler = new();
        private readonly Mock<IChessClockManager> _chessClockManager = new();
        private readonly Mock<IServiceScopeFactory> _scopeFactory = new();
        private readonly Mock<ILogger<WsChallengeRequestHandler>> _logger = new();
        private readonly Mock<IMapper> _mapper = new();
        private readonly WebSocket _webSocket = new ClientWebSocket();
        private readonly Mock<IServiceProvider> _serviceProvider = new();
        private readonly Mock<IServiceScope> _scope = new();

        private WsChallengeRequestHandler _challengeHandler;
        private readonly Challenge _challenge;
        private readonly Player _player;

        public WsChallengeRequestHandlerTests()
        {
            _player = TestPlayers[0];
            _challenge = new Challenge
            {
                Id = new Guid("11111111-1111-1111-1111-111111111111"),
                FromPlayer = _player,
                Color = 0,
                TimeControl = new TimeControl(10, 5),
                ToPlayer = NullPlayer.GetInstance()
            };


            _scopeFactory.Setup(s => s.CreateScope()).Returns(_scope.Object);
            _scope.Setup(s => s.ServiceProvider).Returns(_serviceProvider.Object);
            _serviceProvider.Setup(s => s.GetService(typeof(IUnitOfWork))).Returns(_repo.Object);
        }

        [Fact]
        public async Task CreateChallengeInvokeCorrectMethods()
        {
            _challengeManager.Setup(ch => ch.GetPlayerChallenges(_player.Id)).Returns(new List<Challenge>());
            _repo.Setup(r => r.Players.Get(TestPlayers[0].Id)).Returns(TestPlayers[0]);
            _repo.Setup(r => r.Players.Get(0)).Returns(NullPlayer.GetInstance());
            _repo.Setup(r => r.Games.GetCurrentGame(TestPlayers[0].Id)).Returns(NullGame.GetInstance());
            _challengeManager.Setup(ch =>
                    ch.CreateChallenge(It.IsAny<Player>(), It.IsAny<Player>(), It.IsAny<TimeControl>(), 0))
                .Returns(_challenge);
            _challengeManager.Setup(ch => ch.Set(_challenge.Id, _challenge)).Returns(true);
            _challengeManager.Setup(ch => ch.GetPublicChallenges()).Returns(GetTestChallenges());
            _challengeHandler = new WsChallengeRequestHandler(_scopeFactory.Object, _messageHandler.Object,
                _chessLogic.Object,
                _challengeManager.Object, _chessClockManager.Object, _groupManager.Object, _logger.Object,
                _mapper.Object);

            var request = new ChallengeRequest
            {
                ChallengeRequestType = ChallengeRequestType.CreateChallenge,
                FromPlayerId = _player.Id,
                TimeControl = new TimeControlDto { TimeRange = 10, TimeIncrement = 5 },
                SideColor = SideColor.None
            };

            await _challengeHandler.CreateChallenge(request);

            _challengeManager.Verify();
            _messageHandler.Verify(m => m.SendMessageAsync(_webSocket,
                It.Is<DataResponse>(dr =>
                    dr.DataResponseType == DataResponseType.System && dr.Data == "Challenge created!")));
            _messageHandler.Verify(m =>
                m.SendMessageToAllAsync(It.IsAny<ChallengeResponse>()));
        }


        [Fact]
        public async Task CreateChallengeForMySelfSendsErrorMessage()
        {
            _repo.Setup(r => r.Players.Get(1)).Returns(TestPlayers[0]);

            _challengeHandler = new WsChallengeRequestHandler(_scopeFactory.Object, _messageHandler.Object,
                _chessLogic.Object,
                _challengeManager.Object, _chessClockManager.Object, _groupManager.Object, _logger.Object,
                _mapper.Object);

            var request = new ChallengeRequest
            {
                FromPlayerId = _player.Id,
                ChallengeRequestType = ChallengeRequestType.CreateChallenge,
                TimeControl = new TimeControlDto { TimeRange = 10, TimeIncrement = 5 },
                SideColor = SideColor.None,
                ToPlayerId = 1
            };

            await _challengeHandler.CreateChallenge(request);

            _messageHandler.Verify(m => m.SendMessageAsync(_player.Id,
                It.Is<DataResponse>(dr =>
                    dr.DataResponseType == DataResponseType.Exception &&
                    dr.Data == "You cannot make challenge to yourself")));
        }

        [Fact]
        public async Task CreateChallengeSendsErrorWhenAlreadyExist()
        {
            _repo.Setup(r => r.Players.Get(_player.Id)).Returns(TestPlayers[0]);
            _repo.Setup(r => r.Players.Get(0)).Returns(NullPlayer.GetInstance());
            _repo.Setup(r => r.Games.GetCurrentGame(_player.Id)).Returns(NullGame.GetInstance());

            _challengeManager
                .Setup(ch => ch.CreateChallenge(It.IsAny<Player>(), It.IsAny<Player>(), It.IsAny<TimeControl>(),
                    It.IsAny<SideColor>()))
                .Throws(new ChessException("Challenge with these parameters already exists"));

            _challengeHandler = new WsChallengeRequestHandler(_scopeFactory.Object, _messageHandler.Object,
                _chessLogic.Object,
                _challengeManager.Object, _chessClockManager.Object, _groupManager.Object, _logger.Object,
                _mapper.Object);

            var request = new ChallengeRequest
            {
                ChallengeRequestType = ChallengeRequestType.CreateChallenge,
                FromPlayerId = _player.Id,
                TimeControl = new TimeControlDto { TimeRange = 10, TimeIncrement = 5 },
                SideColor = SideColor.None
            };

            await _challengeHandler.CreateChallenge(request);

            _messageHandler.Verify(m => m.SendMessageAsync(_player.Id,
                It.Is<DataResponse>(dr => dr.DataResponseType == DataResponseType.Exception
                                          && dr.Data ==
                                          "Challenge with these parameters already exists")));
        }

        [Fact]
        public async Task CreatePrivateChallengeInvokeCorrectMethods()
        {
            var toPlayerId = 2;
            var fromPlayer = TestPlayers[0];
            var toPlayer = TestPlayers[1];

            var challenge = new Challenge
            {
                Id = new Guid("11111111-1111-1111-1111-111111111111"),
                FromPlayer = fromPlayer,
                ToPlayer = toPlayer,
                TimeControl = new TimeControl(15, 0),
                Color = SideColor.None
            };

            _repo.Setup(r => r.Players.Get(fromPlayer.Id)).Returns(fromPlayer);
            _repo.Setup(r => r.Players.Get(toPlayer.Id)).Returns(toPlayer);
            _repo.Setup(r => r.Games.GetCurrentGame(fromPlayer.Id)).Returns(NullGame.GetInstance());
            _repo.Setup(r => r.Games.GetCurrentGame(toPlayer.Id)).Returns(NullGame.GetInstance());
            _challengeManager.Setup(ch => ch.CreateChallenge(fromPlayer, toPlayer, It.IsAny<TimeControl>(), 0))
                .Returns(challenge);

            _challengeHandler = new WsChallengeRequestHandler(_scopeFactory.Object, _messageHandler.Object,
                _chessLogic.Object,
                _challengeManager.Object, _chessClockManager.Object, _groupManager.Object, _logger.Object,
                _mapper.Object);

            var request = new ChallengeRequest
            {
                FromPlayerId = _player.Id,
                ChallengeRequestType = ChallengeRequestType.CreateChallenge,
                TimeControl = new TimeControlDto { TimeRange = 10, TimeIncrement = 5 },
                SideColor = SideColor.None,
                ToPlayerId = toPlayerId
            };

            await _challengeHandler.CreateChallenge(request);

            _messageHandler.Verify(m => m.SendMessageAsync(_webSocket,
                It.Is<DataResponse>(dr =>
                    dr.DataResponseType == DataResponseType.System && dr.Data == "Challenge created!")));
            _messageHandler.Verify(m => m.SendMessageToGroupAsync(It.IsAny<Guid>(),
                It.IsAny<ChallengeResponse>()));
            _repo.Verify();
            _challengeManager.Verify();
        }


        [Fact]
        public async Task CreatePrivateChallengeSendsErrorWhenPlayerIsBusy()
        {
            var toPlayerId = 2;
            var fromPlayer = TestPlayers[0];
            var toPlayer = TestPlayers[1];
            var gameOfToPlayer = TestGames[0];

            var challenge = new Challenge
            {
                Id = new Guid("11111111-1111-1111-1111-111111111111"),
                FromPlayer = fromPlayer,
                ToPlayer = toPlayer,
                TimeControl = new TimeControl(15, 0),
                Color = SideColor.None
            };

            _repo.Setup(r => r.Players.Get(fromPlayer.Id)).Returns(fromPlayer);
            _repo.Setup(r => r.Players.Get(toPlayer.Id)).Returns(toPlayer);
            _repo.Setup(r => r.Games.GetCurrentGame(fromPlayer.Id)).Returns(NullGame.GetInstance());
            _repo.Setup(r => r.Games.GetCurrentGame(toPlayer.Id)).Returns(gameOfToPlayer);
            _challengeManager.Setup(ch => ch.CreateChallenge(fromPlayer, toPlayer, It.IsAny<TimeControl>(), 0))
                .Returns(challenge);

            _challengeHandler = new WsChallengeRequestHandler(_scopeFactory.Object, _messageHandler.Object,
                _chessLogic.Object,
                _challengeManager.Object, _chessClockManager.Object, _groupManager.Object, _logger.Object,
                _mapper.Object);

            var request = new ChallengeRequest
            {
                FromPlayerId = _player.Id,
                ChallengeRequestType = ChallengeRequestType.CreateChallenge,
                TimeControl = new TimeControlDto { TimeRange = 10, TimeIncrement = 5 },
                SideColor = SideColor.None,
                ToPlayerId = toPlayerId
            };

            await _challengeHandler.CreateChallenge(request);

            _messageHandler.Verify(m =>
                m.SendMessageAsync(fromPlayer.Id,
                    It.Is<DataResponse>(dr => dr.DataResponseType == DataResponseType.Exception
                                              && dr.Data == "Player Player2 is playing now")));
        }

        [Fact]
        public async Task JoinGameWsInvokeCorrectMethods()
        {
            var startFen = "rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1";
            //var fromPlayer = TestPlayers[1];
            var joinPlayer = TestPlayers[1];

            var challId = new Guid("11111111-1111-1111-1111-111111111111");
            var game = TestGames[1];
            var challenges = GetTestChallenges();
            var challenge = GetTestChallenges()[0];
            var request = new ChallengeRequest
            {
                FromPlayerId = _player.Id,
                ChallengeId = challId
            };

            _chessLogic.Setup(c => c.StartGame(game.Id, startFen));
            _challengeManager.Setup(c => c.GetPublicChallenges()).Returns(challenges);
            _challengeManager.Setup(c => c.Get(It.IsAny<Guid>())).Returns(challenge);
            _repo.Setup(r => r.Games.GetCurrentGame(It.IsAny<long>())).Returns(NullGame.GetInstance());
            _repo.Setup(r => r.Players.Get(_player.Id)).Returns(_player);
            _repo.Setup(r => r.Players.Get(joinPlayer.Id)).Returns(joinPlayer);
            _chessClockManager.Setup(cl => cl.SetUpClock(It.IsAny<Challenge>(), It.IsAny<Game>())).Returns(game);

            _challengeHandler = new WsChallengeRequestHandler(_scopeFactory.Object, _messageHandler.Object,
                _chessLogic.Object,
                _challengeManager.Object, _chessClockManager.Object, _groupManager.Object, _logger.Object,
                _mapper.Object);

            var challGroupsField = _challengeHandler.GetType()
                .GetProperty("ChallengeGroups", BindingFlags.Static | BindingFlags.NonPublic)
                ?.GetValue(_challengeManager);
            challGroupsField?.GetType().GetMethod("TryAdd")
                ?.Invoke(challGroupsField, new object[] { 1, Guid.NewGuid() });


            await _challengeHandler.AcceptChallenge(request);

            _chessLogic.Verify(c => c.StartGame(It.IsAny<long>(), startFen));
            _messageHandler.Verify(m => m.SendMessageToAllAsync(It.IsAny<ChallengeResponse>()));
            _messageHandler.Verify(m => m.SendMessageToGroupAsync(It.IsAny<Guid>(), It.IsAny<ChallengeResponse>()));
        }


        [Fact]
        public async Task RemoveChallengeInvokesCorrectMethods()
        {
            var challenge = GetTestChallenges()[0];
            var challenges = GetTestChallenges();
            _challengeManager.Setup(ch => ch.Remove(challenge.Id)).Returns(true);
            _challengeManager.Setup(ch => ch.GetPublicChallenges()).Returns(challenges);
            _challengeManager.Setup(ch => ch.Get(challenge.Id)).Returns(challenge);
            _repo.Setup(r => r.Players.Get(challenge.FromPlayer.Id)).Returns(challenge.FromPlayer);

            var request = new ChallengeRequest
            {
                FromPlayerId = challenge.FromPlayer.Id,
                ChallengeRequestType = ChallengeRequestType.RemoveChallenge,
                ChallengeId = challenge.Id
            };

            _challengeHandler = new WsChallengeRequestHandler(_scopeFactory.Object, _messageHandler.Object,
                _chessLogic.Object,
                _challengeManager.Object, _chessClockManager.Object, _groupManager.Object, _logger.Object,
                _mapper.Object);

            await _challengeHandler.RemoveChallenge(request);

            _messageHandler.Verify(m => m.SendMessageToAllAsync(It.IsAny<ChallengeResponse>()));
            _messageHandler.Verify(m => m.SendMessageAsync(challenge.FromPlayer.Id,
                It.Is<DataResponse>(dr =>
                    dr.DataResponseType == DataResponseType.System && dr.Data == "Challenge removed.")));
        }


        [Fact]
        public async Task RemovePrivateChallengeInvokesCorrectMethods()
        {
            var fromPlayer = TestPlayers[3];

            var challenge = GetTestChallenges()[2];
            _challengeManager.Setup(ch => ch.Remove(challenge.Id)).Returns(true);
            _challengeManager.Setup(ch => ch.Get(challenge.Id)).Returns(challenge);
            _repo.Setup(r => r.Players.Get(fromPlayer.Id)).Returns(fromPlayer);

            var request = new ChallengeRequest
            {
                ChallengeRequestType = ChallengeRequestType.RemoveChallenge,
                FromPlayerId = fromPlayer.Id,
                ChallengeId = challenge.Id
            };

            _challengeHandler = new WsChallengeRequestHandler(_scopeFactory.Object, _messageHandler.Object,
                _chessLogic.Object,
                _challengeManager.Object, _chessClockManager.Object, _groupManager.Object, _logger.Object,
                _mapper.Object);

            var challGroupsField = _challengeHandler.GetType()
                .GetProperty("ChallengeGroups", BindingFlags.Static | BindingFlags.NonPublic)
                ?.GetValue(_challengeManager);
            challGroupsField?.GetType().GetMethod("TryAdd")
                ?.Invoke(challGroupsField, new object[] { 3, Guid.NewGuid() });

            await _challengeHandler.RemoveChallenge(request);

            _messageHandler.Verify(m => m.SendMessageToGroupAsync(It.IsAny<Guid>(), It.Is<ChallengeResponse>(
                cr => cr.ChallengeResponseType == ChallengeResponseType.ChallengeRemove)));
            _messageHandler.Verify(m => m.SendMessageAsync(fromPlayer.Id,
                It.Is<DataResponse>(dr =>
                    dr.DataResponseType == DataResponseType.System && dr.Data == "Challenge removed.")));
        }

        [Fact]
        public async Task RemoveChallengeNotByOwnerSendsErrorMessage()
        {
            var fromPlayer = TestPlayers[0];
            var challenge = GetTestChallenges()[2];
            _challengeManager.Setup(ch => ch.Remove(challenge.Id)).Returns(true);
            _challengeManager.Setup(ch => ch.Get(challenge.Id)).Returns(challenge);
            _repo.Setup(r => r.Players.Get(fromPlayer.Id)).Returns(fromPlayer);

            var request = new ChallengeRequest
            {
                ChallengeRequestType = ChallengeRequestType.RemoveChallenge,
                FromPlayerId = fromPlayer.Id,
                ChallengeId = challenge.Id
            };

            _challengeHandler = new WsChallengeRequestHandler(_scopeFactory.Object, _messageHandler.Object,
                _chessLogic.Object,
                _challengeManager.Object, _chessClockManager.Object, _groupManager.Object, _logger.Object,
                _mapper.Object);

            await _challengeHandler.RemoveChallenge(request);

            _messageHandler.Verify(m => m.SendMessageAsync(_webSocket,
                It.Is<DataResponse>(dr => dr.DataResponseType == DataResponseType.Exception
                                          && dr.Data == "Error. Not owner of challenge.")));
        }

        [Fact]
        public async Task RemoveNonExistingChallengeSendsErrorMessage()
        {
            const int fromPlayerId = 4;
            Guid challengeId = new Guid("11111111-1111-1111-1111-111111111111");
            
            _challengeManager.Setup(ch => ch.Get(challengeId)).Returns(NullChallenge.GetInstance());

            var request = new ChallengeRequest
            {
                ChallengeRequestType = ChallengeRequestType.RemoveChallenge,
                FromPlayerId = fromPlayerId,
                ChallengeId = challengeId
            };

            _challengeHandler = new WsChallengeRequestHandler(_scopeFactory.Object, _messageHandler.Object,
                _chessLogic.Object,
                _challengeManager.Object, _chessClockManager.Object, _groupManager.Object, _logger.Object,
                _mapper.Object);

            await _challengeHandler.RemoveChallenge(request);

            _messageHandler.Verify(m => m.SendMessageAsync(_webSocket, It.Is<DataResponse>(
                dr => dr.DataResponseType == DataResponseType.Exception
                      && dr.Data == "Challenge is not exists.")));
        }

        [Fact]
        public async void DeclineChallengeInvokesCorrectMethods()
        {
            var fromPlayer = TestPlayers[3];
            var toPlayer = TestPlayers[0];
            var challenge = GetTestChallenges()[2];
            _challengeManager.Setup(ch => ch.Remove(challenge.Id)).Returns(true);
            _challengeManager.Setup(ch => ch.Get(challenge.Id)).Returns(challenge);
            _repo.Setup(r => r.Players.Get(It.IsAny<long>())).Returns(fromPlayer);

            var request = new ChallengeRequest
            {
                FromPlayerId = toPlayer.Id,
                ChallengeRequestType = ChallengeRequestType.DeclineChallenge,
                ChallengeId = challenge.Id
            };

            _challengeHandler = new WsChallengeRequestHandler(_scopeFactory.Object, _messageHandler.Object,
                _chessLogic.Object,
                _challengeManager.Object, _chessClockManager.Object, _groupManager.Object, _logger.Object,
                _mapper.Object);

            await _challengeHandler.DeclineChallenge(request);

            _challengeManager.Verify(ch => ch.Remove(challenge.Id));
            _messageHandler.Verify(m => m.SendMessageAsync(fromPlayer.Id, It.Is<ChallengeResponse>(
                cr => cr.ChallengeResponseType == ChallengeResponseType.ChallengeDecline &&
                      cr.ChallengeId == challenge.Id)));
        }

        [Fact]
        public async void DeclineNonExistingChallengeSendsError()
        {
            var toPlayerId = 1;
            var challengeId = new Guid("11111111-1111-1111-1111-111111111111");
            
            _challengeManager.Setup(ch => ch.Get(challengeId)).Returns(NullChallenge.GetInstance());

            var request = new ChallengeRequest
            {
                ChallengeRequestType = ChallengeRequestType.DeclineChallenge,
                FromPlayerId = toPlayerId,
                ChallengeId = challengeId
            };

            _challengeHandler = new WsChallengeRequestHandler(_scopeFactory.Object, _messageHandler.Object,
                _chessLogic.Object,
                _challengeManager.Object, _chessClockManager.Object, _groupManager.Object, _logger.Object,
                _mapper.Object);

            await _challengeHandler.DeclineChallenge(request);

            _messageHandler.Verify(m => m.SendMessageAsync(_webSocket,
                It.Is<DataResponse>(dr => dr.DataResponseType == DataResponseType.Exception
                                          && dr.Data == "Challenge is not exists.")));
        }
        
       [Fact]
       public async void DeclineChallengeNotByOwnerSendsError()
       {
          const int fromPlayerId = 4;

          var challenge = GetTestChallenges()[2];
          //_challengeManager.Setup(ch => ch.Remove(challenge.Id)).Returns(true);
          _challengeManager.Setup(ch => ch.Get(challenge.Id)).Returns(challenge);

          var request = new ChallengeRequest
          {
              ChallengeRequestType = ChallengeRequestType.DeclineChallenge,
              FromPlayerId = fromPlayerId,
              ChallengeId = challenge.Id
          };
       
          _challengeHandler = new WsChallengeRequestHandler(_scopeFactory.Object, _messageHandler.Object, _chessLogic.Object, 
              _challengeManager.Object, _chessClockManager.Object, _groupManager.Object, _logger.Object, _mapper.Object );
       
          await _challengeHandler.DeclineChallenge(request);

          _messageHandler.Verify(m => m.SendMessageAsync(_webSocket, It.Is<DataResponse>(dr =>
              dr.DataResponseType == DataResponseType.Exception
              && dr.Data == "Error. Not owner of challenge.")));
       }
    }
}