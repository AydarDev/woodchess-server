﻿/*using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Threading.Tasks;
using Moq;
using WoodChessModels.Enums;
using WoodChessAPI.Models;
using WoodChessAPI.Repository.Abstract;
using WoodChessAPI.Repository.Concrete;
using Xunit;
using static WoodChessAPI.Tests.DataHelper;

namespace WoodChessAPI.Tests
{
    [Collection("DbIntegration")]
    public class DbIntegrationTests : IClassFixture<DatabaseFixture>
    {
        private readonly DatabaseFixture _dbFixture;
        private readonly IUnitOfWork _repo;
        private ChessContext Context => _dbFixture._context;
        private readonly Mock<IGamesStorage> _gameStorage = new();

        public DbIntegrationTests()
        {
            _dbFixture = new DatabaseFixture();
            _repo = new UnitOfWork(_dbFixture._context, _gameStorage.Object);
            PrepareDataSet();
        }

        [Fact]
        [Trait("Category", "DB Integration Tests")]
        public void AddPlayerToRepo()
        {
            var player = _repo.Players.GetById(1);
            Assert.Equal(1600, player?.GetRating(RatingType.Blitz).Value);
        }


        [Fact]
        [Trait("Category", "DB Integration Tests")]
        public async Task UpdatePlayerFromRepo()
        {
            //PrepareDataSet();
            var player = _repo.Players.GetById(1);
            player.GetRating(RatingType.Blitz).Value += 200;
            _repo.Players.Update(player);
            await _repo.Commit();

            player = _repo.Players.GetById(1);
            Assert.Equal(1800, player.GetRating(RatingType.Blitz).Value);
        }

        [Fact]
        [Trait("Category", "DB Integration Tests")]
        public void GetAllPlayersFromRepo()
        {
            //PrepareDataSet();
            var players = _repo.Players.GetAllPlayers();
            Assert.Equal(4, players.Count);
        }


        [Fact]
        [Trait("Category", "DB Integration Tests")]
        public void GetPlayerWithEmptyGuidReturnsNullPlayer()
        {
            PrepareDataSet();

            Player player = repo.players.GetById(Guid.Empty);

            Assert.True(player.isNull);
            Assert.IsType<NullPlayer>(player);
        }

        [Fact]
        [Trait("Category", "DB Integration Tests")]
        public void GetNonExistingPlayerReturnsNullPlayer()
        {
            PrepareDataSet();

            Player player = repo.players.GetPlayerByName("Micky");

            Assert.True(player.isNull);
        }

        [Fact]
        [Trait("Category", "DB Integration Tests")]
        public void AddGameToRepo()
        {
            PrepareDataSet();

            Game game = _context.games.Find(Guid.Parse("55555555-5555-5555-5555-555555555555"));

            Assert.Equal("r1bqkbnr/pppp1ppp/2n5/1B2p3/4P3/5N2/PPPP1PPP/RNBQK2R b KQkq - 0 3", game.Fen);
        }

        [Fact]
        [Trait("Category", "DB Integration Tests")]
        public async Task UpdateGameFromRepo()
        {
            PrepareDataSet();

            Game game = _context.games.Find(Guid.Parse("77777777-7777-7777-7777-777777777777"));

            string newFen = "r1bqkbnr/pppp34pp/2n5/1B2p3/4P3/5N2/PPPP1PPP/RNBQK2R b KQkq - 0 4";

            game.Fen = newFen;

            repo.games.Update(game);
            await repo.Commit();

            game = _context.games.Find(Guid.Parse("77777777-7777-7777-7777-777777777777"));

            Assert.Equal(newFen, game.Fen);
        }


        [Fact]
        [Trait("Category", "DB Integration Tests")]
        public void GetNonExistingGameReturnsNullGame()
        {
            PrepareDataSet();

            Game nullGame = repo.games.GetById(Guid.Parse("33333333-3333-3333-3333-333333333333"));

            Assert.True(nullGame.isNull);
        }

        [Fact]
        [Trait("Category", "DB Integration Tests")]
        public void GetGameFetchesChildEntitiesCorrectly()
        {
            PrepareDataSet();

            Game game = repo.games.GetById(Guid.Parse("55555555-5555-5555-5555-555555555555"));

            Assert.Equal("Player1", game.w_player.name);
            Assert.Equal("Player2", game.b_player.name);
            Assert.Equal("11111111-1111-1111-1111-111111111111", game.w_player_id.ToString());
            Assert.Equal("22222222-2222-2222-2222-222222222222", game.b_player_id.ToString());
            Assert.Equal("Bf1b5", game.lastMove.value);
            Assert.Equal(10, game.timeControl.time_range);
        }


        [Fact]
        [Trait("Category", "DB Integration Tests")]
        public void GetMyGameReturnsCorrectGame()
        {
            PrepareDataSet();

            Game myGame = repo.games.GetMyCurrentGame(Guid.Parse("11111111-1111-1111-1111-111111111111"));

            Assert.Equal(Guid.Parse("55555555-5555-5555-5555-555555555555"), myGame.Id);
            Assert.Equal("r1bqkbnr/pppp1ppp/2n5/1B2p3/4P3/5N2/PPPP1PPP/RNBQK2R b KQkq - 0 3", myGame.Fen);
        }

        [Fact]
        [Trait("Category", "DB Integration Tests")]
        public void GetMyCurrentGameFetchesChildEntitiesCorrectly()
        {
            PrepareDataSet();

            Game game = repo.games.GetMyCurrentGame(Guid.Parse("22222222-2222-2222-2222-222222222222"));

            Assert.Equal("Player1", game.w_player.name);
            Assert.Equal("Player2", game.b_player.name);
            Assert.Equal("11111111-1111-1111-1111-111111111111", game.w_player_id.ToString());
            Assert.Equal("22222222-2222-2222-2222-222222222222", game.b_player_id.ToString());
            Assert.Equal("Bf1b5", game.lastMove.value);
            Assert.Equal(10, game.timeControl.time_range);
        }

        [Fact]
        [Trait("Category", "DB Integration Tests")]
        public void GetMovesReturnsListOfMoves()
        {
            PrepareDataSet();

            Guid gameId = Guid.Parse("55555555-5555-5555-5555-555555555555");
            List<Move> moves = repo.moves.GetMoves(gameId);

            Assert.Equal(4, moves.Count);
            Assert.Equal("Pe2e4", moves[0].value);
        }

        [Fact]
        [Trait("Category", "DB Integration Tests")]
        public void GetMovesReturnsEmptyListWhenNoMoves()
        {
            PrepareDataSet();

            List<Move> moves = repo.moves.GetMoves(Guid.Parse("00000000-0000-0000-0000-000000000000"));

            Assert.Empty(moves);
        }

        [Fact]
        [Trait("Category", "DB Integration Tests")]
        public void GetLastMoveReturnsCorrectMove()
        {
            PrepareDataSet();

            Guid gameId = Guid.Parse("55555555-5555-5555-5555-555555555555");
            Move move = repo.moves.GetLastMove(gameId);

            Assert.Equal(4, move.ply);
            Assert.Equal("nb8c3", move.value);
        }

        [Fact]
        [Trait("Category", "DB Integration Tests")]
        public void GetLastMoveOfNonExistingGameReturnsNullMove()
        {
            PrepareDataSet();

            Move move = repo.moves.GetLastMove(Guid.Parse("00000000-0000-0000-0000-000000000000"));

            Assert.IsType<NullMove>(move);
            Assert.Equal(Guid.Empty, move.game_id);
        }

        [Fact]
        [Trait("Category", "DB Integration Tests")]
        public void GetCurrentPlyReturnCorrectValue()
        {
            PrepareDataSet();

            int currPly = repo.moves.GetCurrentPly(Guid.Parse("55555555-5555-5555-5555-555555555555"));

            Assert.Equal(5, currPly);
        }

        [Fact]
        [Trait("Category", "DB Integration Tests")]
        public void GetCurrentPlyReturnsOneWhenNoMoves()
        {
            PrepareDataSet();

            int currPly = repo.moves.GetCurrentPly(Guid.Parse("77777777-7777-7777-7777-777777777777"));

            Assert.Equal(1, currPly);
        }

        [Fact]
        [Trait("Category", "DB Integration Tests")]
        public async Task AddMoveCorrectly()
        {
            PrepareDataSet();

            Guid gameId = Guid.Parse("55555555-5555-5555-5555-555555555555");

            Move newMove = new Move
            {
                game_id = gameId,
                player_id = Guid.Parse("33333333-3333-3333-3333-333333333333"),
                num = 3,
                ply = 5,
                value = "Bf1b5",
                fen = "rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1"
            };

            repo.moves.Add(newMove);
            await repo.Commit();

            List<Move> moves = repo.moves.GetMoves(gameId);

            Assert.Equal(5, moves.Count);
        }

        [Fact]
        [Trait("Category", "DB Integration Tests")]
        public async Task UpdateMoveCorrectly()
        {
            PrepareDataSet();

            Move move = repo.Moves.GetLastMove(Guid.Parse("55555555-5555-5555-5555-555555555555"));

            move.draw_flag = DrawFlag.OFFER;
            move.value = "Qe3e2";
            repo.moves.Update(move);
            await repo.Commit();

            move = repo.moves.GetLastMove(Guid.Parse("55555555-5555-5555-5555-555555555555"));

            Assert.Equal("Qe3e2", move.value);
            Assert.Equal(DrawFlag.Offer, move.draw_flag);
        }

        private void PrepareDataSet()
        {
            TruncateTable("Players", "Games", "Moves");

            AddPlayers();
            AddGames();
            AddMoves();

            Context.SaveChanges();
        }

        private void AddMoves()
        {
            Context.Moves.AddRange(TestMoves);
            //Context.SaveChanges();           
        }

        private void AddGames()
        {
            var games = TestGames;

            Context.Games.AddRange(games);
            //Context.SaveChanges();
        }

        private void AddPlayers()
        {
            var players = TestPlayers;

            Context.Players.AddRange(players);
            //Context.SaveChanges();
*/
            /*foreach (var player in players)
                _context.Entry(player).State = EntityState.Detached;*/
        /*
        }

        private void TruncateTable(params string[] tableNames)
        {
            foreach(string tableName in tableNames)
                Context.Database.ExecuteSqlRaw($"truncate {tableName} cascade;");
        }
    }
}
*/
