﻿using Microsoft.Extensions.DependencyInjection;
using Moq;
using System;
using System.Threading.Tasks;
using WoodChessV1.ChessClocks;
using static WoodChess.Tests.DataHelper;
using Xunit;
using WoodChessV1.WebSocketClasses.WsRequestHandlers;
using WoodChessV1.WebSocketClasses.WsRequestHandlers.Abstract;

namespace WoodChess.Tests
{
    public class ChessClockManagerTests
    {
        private readonly ChessClockManager _clockManager;
        private readonly Mock<IServiceScopeFactory> _scopeFactory = new();
        private readonly Mock<IServiceScope> _serviceScope = new();
        private readonly Mock<IServiceProvider> _serviceProvider = new();
        private readonly Mock<IWsGameRequestHandler> _wsGameRequestHandler = new();


        public ChessClockManagerTests()
        {
            _serviceProvider.Setup(s => s.GetService(typeof(IWsGameRequestHandler))).Returns(_wsGameRequestHandler.Object);
            _serviceScope.Setup(s => s.ServiceProvider).Returns(_serviceProvider.Object);
            _scopeFactory.Setup(s => s.CreateScope()).Returns(_serviceScope.Object);

            _clockManager = new ChessClockManager(_scopeFactory.Object);
        }

        [Fact]
        public async Task FlagDropInvokesCorrectMethod()
        {
            var game = TestGames[1];
            var clock = new ChessClock(game);

            await _clockManager.FlagDrop(clock);

            _wsGameRequestHandler.Verify(g => g.InitTimeOver(clock));
        }


        [Fact]
        public void GetClockReturnsNewClockIfNotExists()
        {
            var game = TestGames[1];

            var clock = _clockManager.GetClock(game);

            Assert.Equal(15, clock.TimeRange.TotalMinutes);
            Assert.True(clock.IsGameRun);
        }

        [Fact]
        public void GetClockReturnsClockFromDictionary()
        {
            var game = TestGames[1];

            var inputClock = new ChessClock(game);
            _clockManager.Create(game.Id, inputClock);

            var clock = _clockManager.GetClock(game);

            Assert.Equal(clock, inputClock);
        }

        [Fact]
        public void SetupClockReturnsTimeStampedGame()
        {
            var challenge = GetTestChallenges()[0];
            var game = TestGames[1];

            game = _clockManager.SetUpClock(challenge, game);

            Assert.True(_clockManager.GetClock(game).IsGameRun);
            Assert.Equal(15, _clockManager.GetClock(game).TimeRange.TotalMinutes);
            Assert.NotNull(game.TimeStamp);
        }
    }
}
