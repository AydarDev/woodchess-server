/*
using System;
using Xunit;
using WoodChessAPI.WebSocketManager;
using Moq;
using Microsoft.AspNetCore.Http;
using System.Net.WebSockets;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using WoodChessAPI.ChessManagers;
using Microsoft.Extensions.DependencyInjection;
using System.Collections.Generic;
using System.Reflection;
using WoodChessAPI.Models;
using WoodChessAPI.Repository;
using Microsoft.EntityFrameworkCore;
using WoodChessAPI.Repository.Abstract;
using WoodChessAPI.WebSocketClasses.WebSocketManager;

namespace WoodChessAPI.Tests
{
    public class ChessMessageHandlerTests
    {
        const string testMessage = "R0lGODlhAQABAIAAAAAAAAAAACH5BAAAAAAALAAAAAABAAEAAAICTAEAOw==";
        byte[] buffer;

        ChessMessageHandler chessMessageHandler;

        ConnectionManager connectionManager;
        Mock<IGameGroupManager> gameGroupManager = new Mock<IGameGroupManager>(); 

        Mock<ILogger<ChessMessageHandler>> logger = new Mock<ILogger<ChessMessageHandler>>();
        Mock<WebSocket> webSocket = new Mock<WebSocket>();

        Mock<IServiceScopeFactory> serviceScopeFactory = new Mock<IServiceScopeFactory>();
        Mock<IServiceScope> serviceScope = new Mock<IServiceScope>();
        Mock<ChContext> chContext = new Mock<ChContext>();
        Mock<IServiceProvider> serviceProvider = new Mock<IServiceProvider>();
        Mock<IUnitOfWork> repo = new Mock<IUnitOfWork>();


        Game game = new Game 
        {
            Id = Guid.Parse("11111111-2222-2222-3333-444444444445"),
            w_player_id = Guid.Parse("11111111-2222-2222-3333-444444444444"),
            b_player_id = Guid.Parse("55555555-6666-6666-7777-888888888888"),
            fen = "rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1",
            status = "PLAY" 
        };


        public ChessMessageHandlerTests()
        {
            buffer = Convert.FromBase64String(testMessage);

            connectionManager = new ConnectionManager();
            chessMessageHandler = new ChessMessageHandler(connectionManager, gameGroupManager.Object, serviceScopeFactory.Object, logger.Object);

            serviceProvider.Setup(x => x.GetService(typeof(ChContext))).Returns(chContext.Object);
            serviceScope.Setup(sc => sc.ServiceProvider).Returns(serviceProvider.Object);
            serviceScopeFactory.Setup(scopeFact => scopeFact.CreateScope()).Returns(serviceScope.Object);

            Mock<DbSet<Game>> dbSetGames = DbContextMock.GetQueryableDbSetMock<Game>(new List<Game> { game });

            chContext.Setup(x => x.Set<Game>()).Returns(dbSetGames.Object);
        }

        [Fact]
        public async void ReceiveAsyncThrowsNotImplementedException()
        {
            Task TestReceiveAsync = chessMessageHandler.ReceiveAsync(webSocket.Object, new WebSocketReceiveResult(2048, WebSocketMessageType.Text, true), buffer);
            
            await Assert.ThrowsAsync<NotImplementedException>(() => TestReceiveAsync);
        }


    }
}
*/
