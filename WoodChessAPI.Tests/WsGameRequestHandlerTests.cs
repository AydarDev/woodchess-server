﻿using Moq;
using System;
using System.Net.WebSockets;
using WoodChessV1.ChessClocks;
using WoodChessV1.ChessEngine;
using WoodChessV1.Models;
using static WoodChess.Tests.DataHelper;
using Xunit;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using WebSocketModels.Enums;
using WebSocketModels.WsMessages.WsRequests;
using WebSocketModels.WsMessages.WsResponses;
using WoodChessV1.Repository.Abstract;
using WoodChessV1.Services.GroupServices;
using WoodChessV1.Tools;
using WoodChessV1.WebSocketClasses.WebSocketManager;
using WoodChessV1.WebSocketClasses.WsRequestHandlers;
using WoodChessV1.WebSocketClasses.WsRequestHandlers.Abstract;

namespace WoodChess.Tests
{
    public class WsGameRequestHandlerTests
    {
        private WsGameRequestHandler _gameRequestHandler;
        private readonly Mock<IWebSocketHandler> _webSocketHandler = new();
        private readonly Mock<IChessLogic> _chessLogic = new();
        private readonly Mock<IUnitOfWork> _repo = new();
        private readonly Mock<IChessClockManager> _clockManager = new();
        private readonly Mock<IGroupService> _groupManager = new();
        private readonly Mock<IServiceScopeFactory> _scopeFactory = new();
        private readonly Mock<IServiceProvider> _serviceProvider = new();
        private readonly Mock<IServiceScope> _scope = new();
        private readonly Mock<IMapper> _mapper = new();
        private readonly Mock<ILogger<WsGameRequestHandler>> _logger = new();
        private readonly Mock<IRatingHelper> _ratingHelper = new();
        private readonly WebSocket _webSocket = new ClientWebSocket();

        public WsGameRequestHandlerTests()
        {
            _scopeFactory.Setup(s => s.CreateScope()).Returns(_scope.Object);
            _scope.Setup(s => s.ServiceProvider).Returns(_serviceProvider.Object);
            _serviceProvider.Setup(s => s.GetService(typeof(IUnitOfWork))).Returns(_repo.Object);
            _serviceProvider.Setup(s => s.GetService(typeof(IRatingHelper))).Returns(_ratingHelper.Object);
        }

        [Fact]
        public async Task WsMakeMoveInvokesCorrectMethods()
        {
            var game = TestGames[1];
            var player = TestPlayers[0];
            var chessClock = new ChessClock(game);
            var gameRequest = new GameRequest
            {
                FromPlayerId = player.Id,
                GameRequestType = GameRequestType.MakeMove,
                GameId = game.Id,
                Move = "Pe2e4"
            };

            _chessLogic.Setup(ch => ch.MakeMove(It.IsAny<Game>(), It.IsAny<string>())).Returns(TestGames[1]);
            _clockManager.Setup(c => c.GetClock(It.IsAny<Game>())).Returns(chessClock);
            _repo.Setup(r => r.Moves.GetCurrentPly(game.Id)).Returns(5);
            _repo.Setup(r => r.Games.Get(game.Id)).Returns(game);
            _repo.Setup(r => r.Players.Get(player.Id)).Returns(player);

           
            _gameRequestHandler = new WsGameRequestHandler(_webSocketHandler.Object,
                _clockManager.Object, _groupManager.Object, _scopeFactory.Object, _logger.Object, _mapper.Object);

            await _gameRequestHandler.MakeMove(_webSocket, gameRequest);

            _webSocketHandler.Verify(ch => ch.SendMessageToGroupAsync(It.IsAny<Guid>(),
                It.Is<GameResponse>(d => d.GameResponseType == GameResponseType.Move)));
            _repo.Verify(r => r.Moves.Add(It.IsAny<Move>()));
            _repo.Verify(r => r.Games.Update(It.IsAny<Game>()));
            _repo.Verify(r => r.Commit());
        }


        [Fact]
        public async Task WsMakeMoveValidationOfNotOwnerPlayer()
        {
            var game = TestGames[1];
            var player = TestPlayers[2];
            var chessClock = new ChessClock(game);

            var gameRequest = new GameRequest
            {
                FromPlayerId = player.Id,
                GameRequestType = GameRequestType.MakeMove,
                GameId = game.Id,
                Move = "Pe2e4"
            };

            _chessLogic.Setup(ch => ch.MakeMove(It.IsAny<Game>(), It.IsAny<string>())).Returns(TestGames[0]);
            _clockManager.Setup(c => c.GetClock(It.IsAny<Game>())).Returns(chessClock);
            _repo.Setup(r => r.Moves.GetCurrentPly(game.Id)).Returns(5);
            _repo.Setup(r => r.Games.Get(game.Id)).Returns(game);
            _repo.Setup(r => r.Players.Get(player.Id)).Returns(player);

            _gameRequestHandler = new WsGameRequestHandler(_webSocketHandler.Object,
                _clockManager.Object, _groupManager.Object, _scopeFactory.Object, _logger.Object, _mapper.Object);

            await _gameRequestHandler.MakeMove(_webSocket, gameRequest);

            _webSocketHandler.Verify(ch => ch.SendMessageAsync(_webSocket, 
                It.Is<DataResponse>(d => d.DataResponseType == DataResponseType.Exception && d.Data == "Not game owner!")));
        }
        
        [Fact]
        public async Task WsMakeMoveValidationOfWrongTurn()
        {
            var game = TestGames[1];
            var player = TestPlayers[0];
            var chessClock = new ChessClock(game);

            var gameRequest = new GameRequest
            {
                GameRequestType = GameRequestType.MakeMove,
                FromPlayerId = player.Id,
                GameId = game.Id,
                Move = "pe7e5"
            };

            _chessLogic.Setup(ch => ch.MakeMove(It.IsAny<Game>(), It.IsAny<string>())).Returns(TestGames[0]);
            _clockManager.Setup(c => c.GetClock(It.IsAny<Game>())).Returns(chessClock);
            _repo.Setup(r => r.Moves.GetCurrentPly(game.Id)).Returns(6);
            _repo.Setup(r => r.Games.Get(game.Id)).Returns(game);
            _repo.Setup(r => r.Players.Get(player.Id)).Returns(player);

            _gameRequestHandler = new WsGameRequestHandler(_webSocketHandler.Object,
                _clockManager.Object, _groupManager.Object, _scopeFactory.Object, _logger.Object, _mapper.Object);

            await _gameRequestHandler.MakeMove(_webSocket, gameRequest);

            _webSocketHandler.Verify(ch => ch.SendMessageAsync(_webSocket, 
                It.Is<DataResponse>(d => d.DataResponseType == DataResponseType.Exception && d.Data == "Not your turn!")));
        }

        [Fact]
        public async Task WsMakeMoveValidationOfNotPlayingGame()
        {
            var game = GetDoneGame();
            var player = TestPlayers[1];
            var chessClock = new ChessClock(game);
            var gameRequest = new GameRequest
            {
                GameRequestType = GameRequestType.MakeMove,
                FromPlayerId = player.Id,
                GameId = game.Id,
                Move = "ng8f6"
            };

            _chessLogic.Setup(ch => ch.MakeMove(It.IsAny<Game>(), It.IsAny<string>())).Returns(TestGames[0]);
            _clockManager.Setup(c => c.GetClock(It.IsAny<Game>())).Returns(chessClock);
            _repo.Setup(r => r.Moves.GetCurrentPly(game.Id)).Returns(6);
            _repo.Setup(r => r.Games.Get(game.Id)).Returns(game);
            _repo.Setup(r => r.Players.Get(player.Id)).Returns(player);

            _gameRequestHandler = new WsGameRequestHandler(_webSocketHandler.Object,
                _clockManager.Object, _groupManager.Object, _scopeFactory.Object, _logger.Object, _mapper.Object);

            await _gameRequestHandler.MakeMove(_webSocket, gameRequest);

            _webSocketHandler.Verify(ch => ch.SendMessageAsync(_webSocket, 
                It.Is<DataResponse>(d => d.DataResponseType == DataResponseType.Exception && d.Data == "Game has been finished.")));
        }
        
        [Fact]
        public async Task WsMakeMoveFinalizeGameWhenDone()
        {
            var game = GetPreCheckmateWhiteGame();
            var doneGame = GetDoneGame();
            var player = TestPlayers[0];
            var chessClock = new ChessClock(game);
            
            var gameRequest = new GameRequest
            {
                GameRequestType = GameRequestType.MakeMove,
                FromPlayerId = player.Id,
                GameId = game.Id,
                Move = "Qh5f7"
            };
            
            _chessLogic.Setup(ch => ch.MakeMove(It.IsAny<Game>(), It.IsAny<string>())).Returns(doneGame);
            _clockManager.Setup(c => c.GetClock(It.IsAny<Game>())).Returns(chessClock);
            _repo.Setup(r => r.Moves.GetCurrentPly(game.Id)).Returns(5);
            _repo.Setup(r => r.Games.Get(game.Id)).Returns(game);
            _repo.Setup(r => r.Games.Get(doneGame.Id)).Returns(doneGame);
            _repo.Setup(r => r.Players.Get(player.Id)).Returns(player);

            _gameRequestHandler = new WsGameRequestHandler(_webSocketHandler.Object,
                _clockManager.Object, _groupManager.Object, _scopeFactory.Object, _logger.Object, _mapper.Object);          

            await _gameRequestHandler.MakeMove(_webSocket, gameRequest);

            _webSocketHandler.Verify(ch => ch.SendMessageToGroupAsync(It.IsAny<Guid>(),
                It.Is<GameResponse>(d => d.GameResponseType == GameResponseType.Move)));
            
            _webSocketHandler.Verify(ch => ch.SendMessageToGroupAsync(It.IsAny<Guid>(), 
                It.Is<GameResponse>(d => d.GameResponseType == GameResponseType.Game)));

            _repo.Verify(r => r.Moves.Add(It.IsAny<Move>()));
            _repo.Verify(r => r.Games.Update(It.IsAny<Game>()));
            _repo.Verify(r => r.UpdatePlayersRatings(It.IsAny<Game>()));
            _repo.Verify(r => r.Commit());

        }


        [Fact]
        public async Task TimeOverInvokesCorrectMethods()
        {
            var game = TestGames[1];
            var chessClock = new ChessClock(game);

            _clockManager.Setup(cl => cl.Remove(It.IsAny<long>())).Returns(true);
            _clockManager.Setup(c => c.GetClock(It.IsAny<Game>())).Returns(chessClock);
            _repo.Setup(r => r.Games.Get(It.IsAny<long>())).Returns(game);
            _repo.Setup(r => r.Moves.GetLastMove(It.IsAny<long>())).Returns(TestMoves[0]);
            _serviceProvider.Setup(s => s.GetService(typeof(IUnitOfWork))).Returns(_repo.Object);
            _scope.Setup(s => s.ServiceProvider).Returns(_serviceProvider.Object);
            _scopeFactory.Setup(s => s.CreateScope()).Returns(_scope.Object);

            _gameRequestHandler = new WsGameRequestHandler(_webSocketHandler.Object,
                _clockManager.Object, _groupManager.Object, _scopeFactory.Object, _logger.Object, _mapper.Object);

            await _gameRequestHandler.InitTimeOver(chessClock);

            _webSocketHandler.Verify(ch => ch.SendMessageToGroupAsync(It.IsAny<Guid>(),
                It.Is<GameResponse>(r => r.GameResponseType == GameResponseType.Game)));
            _repo.Verify(r => r.Games.Update(It.IsAny<Game>()));
            _repo.Verify(r => r.UpdatePlayersRatings(It.IsAny<Game>()));
            _repo.Verify(r => r.Commit());
            Assert.Equal("DONE", game.Status);
            Assert.Equal("BLACK_WIN_TIME_EXPIRED", game.Result);
        }

        [Fact]
        public async Task TimeOverSendErrorMessageWhenGameDome()
        {
            var doneGame = GetDoneGame();
            var clock = new ChessClock(doneGame);

            _repo.Setup(r => r.Games.Get(It.IsAny<long>())).Returns(doneGame);
            _repo.Setup(r => r.Moves.GetLastMove(It.IsAny<long>())).Returns(TestMoves[0]);
            _serviceProvider.Setup(s => s.GetService(typeof(IUnitOfWork))).Returns(_repo.Object);
            _scope.Setup(s => s.ServiceProvider).Returns(_serviceProvider.Object);
            _scopeFactory.Setup(s => s.CreateScope()).Returns(_scope.Object);

            _gameRequestHandler = new WsGameRequestHandler(_webSocketHandler.Object,
                _clockManager.Object, _groupManager.Object, _scopeFactory.Object, _logger.Object, _mapper.Object);

            await _gameRequestHandler.InitTimeOver(clock);

            _webSocketHandler.Verify(c => c.SendMessageToGroupAsync(It.IsAny<Guid>(), 
                It.Is<DataResponse>(d => d.DataResponseType == DataResponseType.System && d.Data == $"Time over. But game is not playing. Game {doneGame.Id}")));
            
        }

        [Fact]
        public async void ResignGameInvokesCorrectMethods()
        {
            var game = TestGames[0];
            var player = TestPlayers[1];
            var chessClock = new ChessClock(game);
            var gameRequest = new GameRequest
            {
                GameRequestType = GameRequestType.ResignGame,
                FromPlayerId = player.Id,
                GameId = game.Id
            };

            _repo.Setup(r => r.Games.Get(game.Id)).Returns(game);
            _repo.Setup(r => r.Players.Get(player.Id)).Returns(player);
            _clockManager.Setup(cl => cl.GetClock(game)).Returns(chessClock);
            _repo.Setup(r => r.Moves.GetLastMove(It.IsAny<long>())).Returns(TestMoves[0]);

            _gameRequestHandler = new WsGameRequestHandler(_webSocketHandler.Object,
                _clockManager.Object, _groupManager.Object, _scopeFactory.Object, _logger.Object, _mapper.Object);

            await _gameRequestHandler.ResignGame(_webSocket, gameRequest);

            Assert.Equal("DONE", game.Status);
            Assert.Equal("WHITE_WIN_BY_RESIGN", game.Result);

            _chessLogic.Verify(ch => ch.FinishGame(game.Id));
            _webSocketHandler.Verify(ch => ch.SendMessageToGroupAsync(It.IsAny<Guid>(),
                It.Is<GameResponse>(r => r.GameResponseType == GameResponseType.Game)));
            _repo.Verify(r => r.UpdatePlayersRatings(It.IsAny<Game>()));
        }

        [Fact]
        public async void ResignGameSendErrorMessageWhenNotOwner()
        {
            var game = TestGames[1];
            var player = TestPlayers[2];
            var chessClock = new ChessClock(game);

            var gameRequest = new GameRequest
            {
                GameRequestType = GameRequestType.ResignGame,
                FromPlayerId = player.Id,
                GameId = game.Id
            };

            _repo.Setup(r => r.Games.Get(game.Id)).Returns(game);
            _repo.Setup(r => r.Players.Get(player.Id)).Returns(player);
            _clockManager.Setup(cl => cl.GetClock(game)).Returns(chessClock);

            _gameRequestHandler = new WsGameRequestHandler(_webSocketHandler.Object,
                _clockManager.Object, _groupManager.Object, _scopeFactory.Object, _logger.Object, _mapper.Object);

            await _gameRequestHandler.ResignGame(_webSocket, gameRequest);

            _webSocketHandler.Verify(c => c.SendMessageAsync(_webSocket, 
                It.Is<DataResponse>(d => d.DataResponseType == DataResponseType.Exception && d.Data == $"{player.Name} not owner in {game}!")));
        }


        [Fact]
        public async void AcceptDrawInvokesCorrectMethods()
        {
            var game = GetDrawOfferedGame();
            var player = game.WhitePlayer;
            var chessClock = new ChessClock(game);
            
            var gameRequest = new GameRequest
            {
                FromPlayerId = player.Id,
                GameRequestType = GameRequestType.AcceptDraw,
                GameId = game.Id
            };

            _repo.Setup(r => r.Games.Get(game.Id)).Returns(game);
            _repo.Setup(r => r.Players.Get(player.Id)).Returns(player);
            _repo.Setup(r => r.Moves.GetLastMove(game.Id)).Returns(game.LastMove);
            _clockManager.Setup(cl => cl.GetClock(game)).Returns(chessClock);

            _gameRequestHandler = new WsGameRequestHandler(_webSocketHandler.Object,
                _clockManager.Object, _groupManager.Object, _scopeFactory.Object, _logger.Object, _mapper.Object);

            await _gameRequestHandler.AcceptDraw(_webSocket, gameRequest);

            Assert.Equal("DONE", game.Status);
            Assert.Equal("DRAW_BY_AGREE", game.Result);
            _webSocketHandler.Verify(ch => ch.SendMessageToGroupAsync(It.IsAny<Guid>(), 
                It.Is<GameResponse>(gr => gr.GameResponseType == GameResponseType.Game)));
            _repo.Verify(r => r.UpdatePlayersRatings(It.IsAny<Game>()));
        }

        [Fact]
        public async void AcceptDrawSendErrorMessageWhenNotOwner()
        {
            var game = GetDrawOfferedGame();
            var player = TestPlayers[2];
            var chessClock = new ChessClock(game);

            var gameRequest = new GameRequest
            {
                FromPlayerId = player.Id,
                GameRequestType = GameRequestType.AcceptDraw,
                GameId = game.Id
            };

            _repo.Setup(r => r.Games.Get(game.Id)).Returns(game);
            _repo.Setup(r => r.Players.Get(player.Id)).Returns(player);
            _repo.Setup(r => r.Moves.GetLastMove(game.Id)).Returns(game.LastMove);
            _clockManager.Setup(cl => cl.GetClock(game)).Returns(chessClock);

            _gameRequestHandler = new WsGameRequestHandler(_webSocketHandler.Object,
                _clockManager.Object, _groupManager.Object, _scopeFactory.Object, _logger.Object, _mapper.Object);

            await _gameRequestHandler.AcceptDraw(_webSocket, gameRequest);

            _webSocketHandler.Verify(c => c.SendMessageAsync(_webSocket,
                It.Is<DataResponse>(dr => dr.DataResponseType == DataResponseType.Exception &&
                                          dr.Data == $"{player.Name} not owner in {game}!")));
        }
        
        [Fact]
        public async void AcceptDrawReceiveErrorWhenNoDrawOffered()
        {
            var game = TestGames[1];
            var player = TestPlayers[0];
            var chessClock = new ChessClock(game);

            var gameRequest = new GameRequest
            {
                FromPlayerId = player.Id,
                GameRequestType = GameRequestType.AcceptDraw,
                GameId = game.Id
            };

            _repo.Setup(r => r.Games.Get(game.Id)).Returns(game);
            _repo.Setup(r => r.Players.Get(player.Id)).Returns(player);
            _repo.Setup(r => r.Moves.GetLastMove(game.Id)).Returns(game.LastMove);
            _clockManager.Setup(cl => cl.GetClock(game)).Returns(chessClock);

            _gameRequestHandler = new WsGameRequestHandler(_webSocketHandler.Object,
                _clockManager.Object, _groupManager.Object, _scopeFactory.Object, _logger.Object, _mapper.Object);

            await _gameRequestHandler.AcceptDraw(_webSocket, gameRequest);

            _webSocketHandler.Verify(ch => ch.SendMessageAsync(_webSocket,
            It.Is<DataResponse>(dr => dr.DataResponseType == DataResponseType.Exception
            && dr.Data == $"Draw was not offered in {game}")));
        }


        [Fact]
        public async void DeclineDrawInvokesCorrectMethods()
        {
            var game = GetDrawOfferedGame();
            var player = game.WhitePlayer;
            var chessClock = new ChessClock(game);

            var gameRequest = new GameRequest
            {
                GameRequestType = GameRequestType.DeclineDraw,
                FromPlayerId = player.Id,
                GameId = game.Id
            };
           
            _repo.Setup(r => r.Games.Get(game.Id)).Returns(game);
            _repo.Setup(r => r.Moves.GetLastMove(game.Id)).Returns(game.LastMove);
            _repo.Setup(r => r.Players.Get(player.Id)).Returns(player);
            _clockManager.Setup(cl => cl.GetClock(game)).Returns(chessClock);

            _gameRequestHandler = new WsGameRequestHandler(_webSocketHandler.Object,
                _clockManager.Object, _groupManager.Object, _scopeFactory.Object, _logger.Object, _mapper.Object);

            await _gameRequestHandler.DeclineDraw(_webSocket, gameRequest);

            Assert.Equal(DrawFlag.Decline, game.DrawInfo.DrawFlag);
            Assert.Equal("PLAY", game.Status);
            _webSocketHandler.Verify(ch => ch.SendMessageToGroupAsync(It.IsAny<Guid>(),
                It.Is<GameResponse>(gr => gr.GameResponseType == GameResponseType.Draw)));
        }

        [Fact]
        public async void DeclineDrawReceiveErrorWhenNoDrawOffered()
        {
            var game = TestGames[1];
            var player = TestPlayers[0];
            var chessClock = new ChessClock(game);
            
            var gameRequest = new GameRequest
            {
                GameRequestType = GameRequestType.DeclineDraw,
                FromPlayerId = player.Id,
                GameId = game.Id
            };

            _repo.Setup(r => r.Games.Get(game.Id)).Returns(game);
            _repo.Setup(r => r.Players.Get(player.Id)).Returns(player);
            _repo.Setup(r => r.Moves.GetLastMove(game.Id)).Returns(game.LastMove);
            _clockManager.Setup(cl => cl.GetClock(game)).Returns(chessClock);

            _gameRequestHandler = new WsGameRequestHandler(_webSocketHandler.Object,
                _clockManager.Object, _groupManager.Object, _scopeFactory.Object, _logger.Object, _mapper.Object);

            await _gameRequestHandler.DeclineDraw(_webSocket, gameRequest);

            _webSocketHandler.Verify(ch => ch.SendMessageAsync(_webSocket, 
                It.Is<DataResponse>(dr => dr.DataResponseType == DataResponseType.Exception
                && dr.Data == $"Draw was not offered in {game}")));
        }

        [Fact]
        public async void DeclineDrawSendErrorMessageWhenNotOwner()
        {
            var game = GetDrawOfferedGame();
            var player = TestPlayers[2];
            var chessClock = new ChessClock(game);

            var gameRequest = new GameRequest
            {
                GameRequestType = GameRequestType.DeclineDraw,
                FromPlayerId = player.Id,
                GameId = game.Id
            };

            _repo.Setup(r => r.Games.Get(game.Id)).Returns(game);
            _repo.Setup(r => r.Players.Get(player.Id)).Returns(player);
            _repo.Setup(r => r.Moves.GetLastMove(game.Id)).Returns(game.LastMove);
            _clockManager.Setup(cl => cl.GetClock(game)).Returns(chessClock);

            _gameRequestHandler = new WsGameRequestHandler(_webSocketHandler.Object,
                _clockManager.Object, _groupManager.Object, _scopeFactory.Object, _logger.Object, _mapper.Object);

            await _gameRequestHandler.DeclineDraw(_webSocket, gameRequest);

            _webSocketHandler.Verify(c => c.SendMessageAsync(_webSocket,
                It.Is<DataResponse>(dr => dr.DataResponseType == DataResponseType.Exception
                && dr.Data == $"{player.Name} not owner in {game}!")));
        }
    }
}
