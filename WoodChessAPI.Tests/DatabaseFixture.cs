﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Text;
using WoodChessV1.Repository;
using WoodChessV1.Repository.Concrete;
using WoodChessV1.Repository.Contexts;

namespace WoodChess.Tests
{
    public class DatabaseFixture : IDisposable
    {
        internal ChessContext _context;

        public DatabaseFixture()
        {
            DbContextOptionsBuilder<ChessContext> optionsBuilder = new DbContextOptionsBuilder<ChessContext>();

            IConfigurationRoot configuration = new ConfigurationBuilder()
           .SetBasePath(AppDomain.CurrentDomain.BaseDirectory)
           .AddJsonFile("appsettings.json")
           .Build();

            optionsBuilder.UseNpgsql(configuration.GetConnectionString("ChessDbTest")).EnableSensitiveDataLogging();

            _context = new ChessContext(optionsBuilder.Options);
            _context.Database.EnsureCreated();
        }


        public void Dispose()
        {
            _context.Database.EnsureDeleted();
        }
    }
}
