﻿using System;
using System.Collections.Concurrent;
using System.Reflection;
using WoodChessV1.ChessEngine;
using Xunit;
using ChessRules;
using static WoodChess.Tests.DataHelper;
using WoodChessV1.Models;
using WoodChessV1.Exceptions;

namespace WoodChess.Tests
{
    public class ChessLogicTests
    {
        private readonly ChessLogic _chessLogic;
        private const string NewChessFen = "rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1";

        /*
        Mock<IServiceScopeFactory> scopeFactory = new Mock<IServiceScopeFactory>();
        Mock<IChessClockManager> clockManager = new Mock<IChessClockManager>();
        Mock<IGroupManager> groupManager = new Mock<IGroupManager>();
        Mock<ILogger<ChessLogic>> logger = new Mock<ILogger<ChessLogic>>();
        Mock<IUnitOfWork> unitOfWork = new Mock<IUnitOfWork>();*/


        public ChessLogicTests()
        {
            _chessLogic = new ChessLogic();
        }

        [Fact]
        public void StartGameCreateNewBoard()
        {

            var game = TestGames[1];
            _chessLogic.StartGame(game.Id);
            
            var newChess = _chessLogic.GetChess(game);
                        
            Assert.Equal(NewChessFen, newChess.Fen);
            
            ClearChessBoards();
        }

        [Fact]
        public void MakeMoveReturnsChangedGameOnCorrectMove()
        {
            var game = TestGames[0];
            AddGameToChessBoards(game);

            game = _chessLogic.MakeMove(game, "pd7d6");

            Assert.Equal("r1bqkbnr/ppp2ppp/2np4/1B2p3/4P3/5N2/PPPP1PPP/RNBQK2R w KQkq - 0 4", game.Fen);
            Assert.Equal("PLAY", game.Status);
            
            ClearChessBoards();
        }


        [Fact]
        public void MakeWrongMoveThrowsException()
        {

            Game game1 = TestGames[0];
            Game game2 = TestGames[1];

            AddGameToChessBoards(game1);
            AddGameToChessBoards(game2);

            Assert.Throws<ChessException>(() => _chessLogic.MakeMove(game1, "bf8g7"));
            Assert.Throws<ChessException>(() => _chessLogic.MakeMove(game2, "Ke1e2"));
            
            ClearChessBoards();
        }


        [Fact]
        public void MoveReturnsGameWithCorrectResultsWhenWhiteCheckMate()
        {
            Game game = GetPreCheckmateWhiteGame();
            AddGameToChessBoards(game);

            game = _chessLogic.MakeMove(game, "Qh5f7");

            Chess chess = GetChessBoards()[game.Id];

            Assert.True(chess.IsCheckmate);
            Assert.Equal("DONE", game.Status);
            Assert.Equal("WHITE_WIN_CHECKMATE", game.Result);

            ClearChessBoards();
        }


        [Fact]
        public void MoveReturnsGameWithCorrectResultsWhenBlackCheckMate()
        {
            Game game = GetPreCheckMateBlackGame();
            AddGameToChessBoards(game);

            game = _chessLogic.MakeMove(game, "qd8h4");

            Chess chess = GetChessBoards()[game.Id];

            Assert.True(chess.IsCheckmate);
            Assert.Equal("DONE", game.Status);
            Assert.Equal("BLACK_WIN_CHECKMATE", game.Result);

            ClearChessBoards();
        }


        [Fact]
        public void MoveReturnsGameWithResultsWhenStaleMate()
        {
            Game game = GetPreStaleMateGame();
            AddGameToChessBoards(game);

            game = _chessLogic.MakeMove(game, "Qc5d6");
            Chess chess = GetChessBoards()[game.Id];

            Assert.True(chess.IsStalemate);
            Assert.Equal("DONE", game.Status);
            Assert.Equal("DRAW_STALEMATE", game.Result);

            ClearChessBoards();
        }

        [Fact]
        public void GameChessRestoreIfAbsentInChessBoards()
        {
            Game game = TestGames[1];

            game = _chessLogic.MakeMove(game, "Pc2c4");

            GetChessBoards().TryGetValue(game.Id, out var chess);

            Assert.NotNull(chess);
            Assert.Equal("rnbqkbnr/pppppppp/8/8/2P5/8/PP1PPPPP/RNBQKBNR b KQkq c3 0 1", chess.Fen);
            Assert.Equal("PLAY", game.Status);
        }

        [Fact]
        public void FinishGameDeleteBoard()
        {
            Game game = TestGames[0];
            AddGameToChessBoards(game);

            _chessLogic.FinishGame(game.Id);

            var testBoards = GetChessBoards();

            Assert.False(testBoards.TryGetValue(game.Id, out _));

            ClearChessBoards();
        }

        private void AddGameToChessBoards(Game game)
        {
            var chessRepo = GetChessRepo();

            var testBoards = GetChessBoards();
            testBoards.TryAdd(game.Id, new Chess(game.Fen));
            chessRepo.GetType().GetField("chessBoards", BindingFlags.NonPublic | BindingFlags.Instance)?.SetValue(chessRepo, testBoards);
        }

        private ConcurrentDictionary<long, Chess> GetChessBoards()
        {
            var chessRepo = GetChessRepo();

            return (ConcurrentDictionary<long, Chess>)chessRepo.GetType().
               GetField("_chessBoards", BindingFlags.NonPublic | BindingFlags.Instance)
               ?.GetValue(chessRepo);
        }

        private void ClearChessBoards()
        {
            ConcurrentDictionary<Guid, Chess> emptyDic = new ConcurrentDictionary<Guid, Chess>();

            var chessRepo = GetChessRepo();

            chessRepo.GetType().GetField("chessBoards", BindingFlags.NonPublic | BindingFlags.Instance)?
                .SetValue(chessRepo, emptyDic);
        }

        private ChessRepository GetChessRepo()
        {
            return (ChessRepository)_chessLogic.GetType().GetField("_chessRepo", BindingFlags.NonPublic | BindingFlags.Instance)?.GetValue(_chessLogic);
        }
    }
}
