using ChessRules;
using WebSocketModels.Enums;
using WebSocketModels.Models.Ratings;
using WoodChessV1.RatingCalculator;
using Xunit;

namespace WoodChess.Tests;

public class RatingCalcTest
{
    private readonly WoodChessV1.RatingCalculator.RatingCalculator _ratingCalculator = WoodChessV1.RatingCalculator.RatingCalculator.GetInstance();
    private readonly InputRating _inputRating1 = new InputRating(1655.5, 1855.5);
    private readonly InputRating _inputRating2 = new InputRating(1530.5, 2050.6);
    private readonly InputRating _inputRating3 = new InputRating(1600.0, 1890.5);
    private readonly InputRating _inputRating4 = new InputRating(1730.0, 1730.0);
    private readonly InputRating _inputRating5 = new InputRating(2450.5, 2620.5);


    [Fact]
    public void WhiteWinRatingChangeTest1()
    {
        var ratingDeviationList = _ratingCalculator.GetRatingDeviation(_inputRating1, GameResult.WhiteWin);

        var expectedWhiteDeviation1 = new RatingDeviationDto(15);
        var expectedBlackDeviation1 = new RatingDeviationDto(-15);

        Assert.Equal(expectedWhiteDeviation1, ratingDeviationList[SideColor.White]);
        Assert.Equal(expectedBlackDeviation1, ratingDeviationList[SideColor.Black]);
    }

    [Fact]
    public void WhiteWinRatingChangeTest2()
    {
        var ratingDeviationList = _ratingCalculator.GetRatingDeviation(_inputRating2, GameResult.WhiteWin);

        var expectedWhiteDeviation1 = new RatingDeviationDto(19);
        var expectedBlackDeviation1 = new RatingDeviationDto(-19);

        Assert.Equal(expectedWhiteDeviation1, ratingDeviationList[SideColor.White]);
        Assert.Equal(expectedBlackDeviation1, ratingDeviationList[SideColor.Black]);
    }

    [Fact]
    public void WhiteWinRatingChangeTest3()
    {
        var ratingDeviationList = _ratingCalculator.GetRatingDeviation(_inputRating3, GameResult.WhiteWin);

        var expectedWhiteDeviation1 = new RatingDeviationDto(17);
        var expectedBlackDeviation1 = new RatingDeviationDto(-17);

        Assert.Equal(expectedWhiteDeviation1, ratingDeviationList[SideColor.White]);
        Assert.Equal(expectedBlackDeviation1, ratingDeviationList[SideColor.Black]);
    }
[Fact]
    public void WhiteWinRatingChangeTest4()
    {
        var ratingDeviationList = _ratingCalculator.GetRatingDeviation(_inputRating4, GameResult.WhiteWin);

        var expectedWhiteDeviation1 = new RatingDeviationDto(10);
        var expectedBlackDeviation1 = new RatingDeviationDto(-10);

        Assert.Equal(expectedWhiteDeviation1, ratingDeviationList[SideColor.White]);
        Assert.Equal(expectedBlackDeviation1, ratingDeviationList[SideColor.Black]);
    }

    [Fact]
    public void WhiteWinRatingChangeTest5()
    {
        var ratingDeviationList = _ratingCalculator.GetRatingDeviation(_inputRating5, GameResult.WhiteWin);

        var expectedWhiteDeviation1 = new RatingDeviationDto(15);
        var expectedBlackDeviation1 = new RatingDeviationDto(-15);

        Assert.Equal(expectedWhiteDeviation1, ratingDeviationList[SideColor.White]);
        Assert.Equal(expectedBlackDeviation1, ratingDeviationList[SideColor.Black]);
    }

    [Fact]
    public void BlackWinRatingChangeTest1()
    {
        var ratingDeviationList =  _ratingCalculator.GetRatingDeviation(_inputRating1, GameResult.BlackWin);

        var expectedWhiteDeviation1 = new RatingDeviationDto(-5);
        var expectedBlackDeviation1 = new RatingDeviationDto(5);

        Assert.Equal(expectedWhiteDeviation1, ratingDeviationList[SideColor.White]);
        Assert.Equal(expectedBlackDeviation1, ratingDeviationList[SideColor.Black]);
    }


    [Fact]
    public void BlackWinRatingChangeTest2()
    {
        var ratingDeviationList = _ratingCalculator.GetRatingDeviation(_inputRating2, GameResult.BlackWin);

        var expectedWhiteDeviation1 = new RatingDeviationDto(-1);
        var expectedBlackDeviation1 = new RatingDeviationDto(1);

        Assert.Equal(expectedWhiteDeviation1, ratingDeviationList[SideColor.White]);
        Assert.Equal(expectedBlackDeviation1, ratingDeviationList[SideColor.Black]);
    }

    [Fact]
    public void BlackWinRatingChangeTest3()
    {
        var ratingDeviationList = _ratingCalculator.GetRatingDeviation(_inputRating3, GameResult.BlackWin);

        var expectedWhiteDeviation1 = new RatingDeviationDto(-3);
        var expectedBlackDeviation1 = new RatingDeviationDto(3);

        Assert.Equal(expectedWhiteDeviation1, ratingDeviationList[SideColor.White]);
        Assert.Equal(expectedBlackDeviation1, ratingDeviationList[SideColor.Black]);
    }

    [Fact]
    public void BlackWinRatingChangeTest4()
    {
        var ratingDeviationList = _ratingCalculator.GetRatingDeviation(_inputRating4, GameResult.BlackWin);

        var expectedWhiteDeviation1 = new RatingDeviationDto(-10.00);
        var expectedBlackDeviation1 = new RatingDeviationDto(10.00);

        Assert.Equal(expectedWhiteDeviation1, ratingDeviationList[SideColor.White]);
        Assert.Equal(expectedBlackDeviation1, ratingDeviationList[SideColor.Black]);
    }

    [Fact]
    public void BlackWinRatingChangeTest5()
    {
        var ratingDeviationList = _ratingCalculator.GetRatingDeviation(_inputRating5, GameResult.BlackWin);

        var expectedWhiteDeviation1 = new RatingDeviationDto(-5);
        var expectedBlackDeviation1 = new RatingDeviationDto(5);

        Assert.Equal(expectedWhiteDeviation1, ratingDeviationList[SideColor.White]);
        Assert.Equal(expectedBlackDeviation1, ratingDeviationList[SideColor.Black]);
    }


    [Fact]
    public void DrawRatingChangeTest1()
    {
        var ratingDeviationList = _ratingCalculator.GetRatingDeviation(_inputRating1, GameResult.Draw);

        var expectedWhiteDeviation1 = new RatingDeviationDto(5);
        var expectedBlackDeviation1 = new RatingDeviationDto(-5);

        Assert.Equal(expectedWhiteDeviation1, ratingDeviationList[SideColor.White]);
        Assert.Equal(expectedBlackDeviation1, ratingDeviationList[SideColor.Black]);
    }


    [Fact]
    public void DrawRatingChangeTest2()
    {
        var ratingDeviationList = _ratingCalculator.GetRatingDeviation(_inputRating2, GameResult.Draw);

        var expectedWhiteDeviation1 = new RatingDeviationDto(9);
        var expectedBlackDeviation1 = new RatingDeviationDto(-9);

        Assert.Equal(expectedWhiteDeviation1, ratingDeviationList[SideColor.White]);
        Assert.Equal(expectedBlackDeviation1, ratingDeviationList[SideColor.Black]);
    }

    [Fact]
    public void DrawRatingChangeTest3()
    {
        var ratingDeviationList = _ratingCalculator.GetRatingDeviation(_inputRating3, GameResult.Draw);

        var expectedWhiteDeviation1 = new RatingDeviationDto(7);
        var expectedBlackDeviation1 = new RatingDeviationDto(-7);

        Assert.Equal(expectedWhiteDeviation1, ratingDeviationList[SideColor.White]);
        Assert.Equal(expectedBlackDeviation1, ratingDeviationList[SideColor.Black]);
    }

    [Fact]
    public void DrawRatingChangeTest4()
    {
        var ratingDeviationList = _ratingCalculator.GetRatingDeviation(_inputRating4, GameResult.Draw);

        var expectedWhiteDeviation1 = new RatingDeviationDto(0);
        var expectedBlackDeviation1 = new RatingDeviationDto(0);

        Assert.Equal(expectedWhiteDeviation1, ratingDeviationList[SideColor.White]);
        Assert.Equal(expectedBlackDeviation1, ratingDeviationList[SideColor.Black]);
    }

    [Fact]
    public void DrawRatingChangeTest5()
    {
        var ratingDeviationList = _ratingCalculator.GetRatingDeviation(_inputRating3, GameResult.Draw);

        var expectedWhiteDeviation1 = new RatingDeviationDto(7);
        var expectedBlackDeviation1 = new RatingDeviationDto(-7);

        Assert.Equal(expectedWhiteDeviation1, ratingDeviationList[SideColor.White]);
        Assert.Equal(expectedBlackDeviation1, ratingDeviationList[SideColor.Black]);
    }


    [Fact]
    public void CancelRatingChangeTest()
    {
        var ratingDeviationList = _ratingCalculator.GetRatingDeviation(_inputRating1, GameResult.Cancel);

        var expectedDeviationWhite = new RatingDeviationDto(0);
        var expectedDeviationBlack = new RatingDeviationDto(0);

        Assert.Equal(expectedDeviationWhite, ratingDeviationList[SideColor.White]);
        Assert.Equal(expectedDeviationBlack, ratingDeviationList[SideColor.Black]);
    }
}
