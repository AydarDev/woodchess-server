﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ChessRules;
using WebSocketModels.Enums;
using WoodChessV1.Models;
using WoodChessV1.Models.NullClasses;
using WoodChessV1.Models.Ratings;

namespace WoodChess.Tests
{
    internal class DataHelper
    {
        internal static List<Player> TestPlayers { get; } =
            new()
            {
                new Player
                {
                    Id = 1,
                    Name = "Player1",
                    Ratings = new List<Rating>
                    {
                        new() { RatingType = RatingType.Blitz, Value = 1600, IsGame = true }
                    }
                },

                new Player
                {
                    Id = 2,
                    Name = "Player2",
                    Ratings = new List<Rating>
                    {
                        new() { RatingType = RatingType.Blitz, Value = 1600, IsGame = true }
                    }
                },

                new Player
                {
                    Id = 3,
                    Name = "Player3",
                    Ratings = new List<Rating>
                    {
                        new() { RatingType = RatingType.Blitz, Value = 1600, IsGame = true }
                    }
                },


                new Player
                {
                    Id = 4,
                    Name = "Player4",
                    Ratings = new List<Rating>
                    {
                        new() { RatingType = RatingType.Blitz, Value = 1600, IsGame = true }
                    }
                }
            };


        internal static List<Challenge> GetTestChallenges()
        {
            var challenges = new List<Challenge>
            {
                new()
                {
                    Id = new Guid("11111111-1111-1111-1111-111111111111"),
                    FromPlayer = TestPlayers[1],
                    TimeControl = new TimeControl { TimeRange = 15, TimeIncrement = 0 },
                    Color = 0,
                    ToPlayer = NullPlayer.GetInstance()
                },

                new()
                {
                    Id = new Guid("22222222-2222-2222-2222-222222222222"),
                    FromPlayer = TestPlayers[2],
                    TimeControl = new TimeControl(10, 5),
                    Color = 0,
                    ToPlayer = NullPlayer.GetInstance()
                },

                new()
                {
                    Id = new Guid("33333333-3333-3333-3333-333333333333"),
                    FromPlayer = TestPlayers[3],
                    ToPlayer = TestPlayers[0],
                    TimeControl = new TimeControl(5, 3),
                    Color = 0
                }
            };

            return challenges;
        }

        internal static List<Game> TestGames =>
            new()
            {
                new Game
                {
                    Id = 5,
                    GamePlayers = new List<GamePlayer>
                    {
                        new() { Color = SideColor.White, Player = TestPlayers[0], PlayerId = TestPlayers[0].Id },
                        new() { Color = SideColor.Black, Player = TestPlayers[1], PlayerId = TestPlayers[1].Id },
                    },
                    Players = new List<Player>
                    {
                        TestPlayers[0], TestPlayers[1]
                    },
                    TimeControl = new TimeControl(10, 5),
                    TimeStamp = "2020-09-20, 00:00:00",
                    Fen = "r1bqkbnr/pppp1ppp/2n5/1B2p3/4P3/5N2/PPPP1PPP/RNBQK2R b KQkq - 0 3",
                    Status = "PLAY",
                    LastMove = new Move
                    {
                        GameId = 5,
                        PlayerId = 1,
                        Num = 3,
                        Ply = 5,
                        Value = "Bf1b5",
                        TimeStamp = "09/28/20 1:01:00.256",
                        WhiteTime = "08:00:00",
                        BlackTime = "05:07:25"
                    },
                    DrawInfo = NullDrawInfo.GetInstance()
                },


                new Game
                {
                    Id = 1,
                    GamePlayers = new List<GamePlayer>
                    {
                        new() { Color = SideColor.White, Player = TestPlayers[0], PlayerId = TestPlayers[0].Id },
                        new() { Color = SideColor.Black, Player = TestPlayers[1], PlayerId = TestPlayers[1].Id }
                    },
                    Players = new List<Player>
                    {
                        TestPlayers[0], TestPlayers[1]
                    },
                    TimeControl = new TimeControl(15, 0),
                    Fen = "rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1",
                    Status = "PLAY",
                    LastMove = NullMove.GetInstance(),
                    DrawInfo = NullDrawInfo.GetInstance()
                },
            };


        internal static Game GetPreCheckmateWhiteGame()
        {
            return new Game
            {
                Id = 98,
                GamePlayers = new List<GamePlayer>
                {
                    new() { Color = SideColor.White, Player = TestPlayers[0], PlayerId = TestPlayers[0].Id },
                    new() { Color = SideColor.Black, Player = TestPlayers[1], PlayerId = TestPlayers[1].Id },
                },
                Players = new List<Player> { TestPlayers[0], TestPlayers[1] },
                TimeControl = new TimeControl(15, 0),
                Fen = "r1bqkbnr/ppp2ppp/2np4/4p2Q/2B1P3/8/PPPP1PPP/RNB1K1NR w KQkq - 0 4", //Qh5f7 mate
                Status = "PLAY",
                DrawInfo = NullDrawInfo.GetInstance()
            };
        }

        internal static Game GetPreCheckMateBlackGame()
        {
            return new Game
            {
                Id = 99,
                GamePlayers = new List<GamePlayer>
                {
                    new() { Color = SideColor.White, Player = TestPlayers[0], PlayerId = TestPlayers[0].Id },
                    new() { Color = SideColor.Black, Player = TestPlayers[1], PlayerId = TestPlayers[1].Id },
                },
                Players = new List<Player> { TestPlayers[0], TestPlayers[1] },
                Fen = "rnbqkbnr/pppp1ppp/8/4p3/6P1/5P2/PPPPP2P/RNBQKBNR b KQkq - 0 2", //qd8h4 mate
                Status = "PLAY",
                DrawInfo = NullDrawInfo.GetInstance()
            };
        }

        internal static Game GetPreStaleMateGame()
        {
            return new Game
            {
                Id = 100,
                GamePlayers = new List<GamePlayer>
                {
                    new() { Color = SideColor.White, Player = TestPlayers[0], PlayerId = TestPlayers[0].Id },
                    new() { Color = SideColor.Black, Player = TestPlayers[1], PlayerId = TestPlayers[1].Id },
                },
                Players = new List<Player> { TestPlayers[0], TestPlayers[1] },
                Fen = "k7/8/1K6/2Q5/8/8/8/8 w - - 0 50", //Qc5d6 stalemate
                Status = "PLAY",
                DrawInfo = NullDrawInfo.GetInstance()
            };
        }

        internal static Game GetDoneGame()
        {
            return new Game
            {
                Id = 101,
                GamePlayers = new List<GamePlayer>
                {
                    new() { Color = SideColor.White, Player = TestPlayers[0], PlayerId = TestPlayers[0].Id },
                    new() { Color = SideColor.Black, Player = TestPlayers[1], PlayerId = TestPlayers[1].Id },
                },
                Players = new List<Player> { TestPlayers[0], TestPlayers[1] },
                TimeControl = new TimeControl(15, 0),
                Fen = "r1bqkbnr/ppp2Qpp/2np4/4p3/2B1P3/8/PPPP1PPP/RNB1K1NR b KQkq - 0 6",
                Status = "DONE",
                Result = "WHITE_WIN_CHECKMATE",
                DrawInfo = NullDrawInfo.GetInstance()
            };
        }

        internal static Game GetDrawOfferedGame()
        {
            var game = new Game
            {
                Id = 100,
                GamePlayers = new List<GamePlayer>
                {
                    new() { Color = SideColor.White, Player = TestPlayers[0], PlayerId = TestPlayers[0].Id },
                    new() { Color = SideColor.Black, Player = TestPlayers[1], PlayerId = TestPlayers[1].Id },
                },
                Players = new List<Player> { TestPlayers[0], TestPlayers[1] },
                TimeControl = new TimeControl { TimeRange = 15, TimeIncrement = 0 },
                Fen = "r1bqkbnr/ppp2Qpp/2np4/4p3/2B1P3/8/PPPP1PPP/RNB1K1NR b KQkq - 0 6",
                Status = "PLAY",
                DrawInfo = new DrawInfo { DrawFlag = DrawFlag.Offer, FromPlayerId = TestPlayers[1].Id }
            };

            game.LastMove = new Move
            {
                Game = game,
                Player = game.WhitePlayer,
                Ply = 11,
                Num = 6,
                Value = "Nb1c3",
                TimeStamp = "09/28/20 1:01:00.256",
                WhiteTime = "08:00:00",
                BlackTime = "05:07:25",
                Fen = "r1bqkbnr/ppp2Qpp/2np4/4p3/2B1P3/8/PPPP1PPP/RNB1K1NR b KQkq - 0 6"
            };

            return game;
        }

        internal static List<Move> TestMoves { get; } =
            new()
            {
                new Move
                {
                    GameId = 1,
                    PlayerId = 1,
                    Ply = 1,
                    Num = 1,
                    Value = "Pe2e4",
                    Fen = "rnbqkbnr/pppppppp/8/8/4P3/8/PPPP1PPP/RNBQKBNR w KQkq - 0 1"
                },

                new Move
                {
                    GameId = 1,
                    PlayerId = 2,
                    Ply = 2,
                    Num = 1,
                    Value = "pe7e5",
                    Fen = "rnbqkbnr/pppp1ppp/8/4p3/4P3/8/PPPP1PPP/RNBQKBNR w KQkq - 0 2"
                },

                new Move
                {
                    GameId = 1,
                    PlayerId = 1,
                    Ply = 3,
                    Num = 2,
                    Value = "Ng1f3",
                    Fen = "rnbqkbnr/pppp1ppp/8/4p3/4P3/5N2/PPPP1PPP/RNBQKB1R w KQkq - 0 2"
                },

                new Move
                {
                    GameId = 1,
                    PlayerId = 2,
                    Ply = 4,
                    Num = 2,
                    Value = "nb8c3",
                    Fen = "r1bqkbnr/pppp1ppp/2n5/4p3/4P3/5N2/PPPP1PPP/RNBQKB1R w KQkq - 0 3"
                }
            };
    }
}