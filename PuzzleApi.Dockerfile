﻿FROM mcr.microsoft.com/dotnet/sdk:7.0 AS build-env

WORKDIR /src
COPY PuzzleApi/ ./PuzzleApi/
COPY GameInstruments/ ./GameInstruments/
COPY WoodChessModels/ ./WoodChessModels/
COPY ChessRules/ ./ChessRules/
RUN dotnet restore PuzzleApi/PuzzleApi.csproj

RUN dotnet publish PuzzleApi/PuzzleApi.csproj -c Release -o /app/out

FROM mcr.microsoft.com/dotnet/aspnet:7.0
WORKDIR /app
COPY --from=build-env /app/out .
ENTRYPOINT ["dotnet", "PuzzleApi.dll"]
