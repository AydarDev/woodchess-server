namespace GameInstruments.NotationBuilders
{
    public class NotationBuilder : BaseNotationBuilder
    {
        public override string BuildNotation(string currNotation, string move, int moveNum = 0)
        {
            Builder.Clear();
            Builder.Append(currNotation);
            
            if (moveNum % 2 == 1)
                Builder.Append($"{(moveNum + 1) / 2}. ");
            else if (string.IsNullOrEmpty(currNotation) && moveNum % 2 == 0)
                Builder.Append($"{(moveNum + 1) / 2}... ");

            Builder.Append(move).Append(' ');

            return Builder.ToString();
        }
    }
}