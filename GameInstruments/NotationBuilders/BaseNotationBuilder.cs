using System.Text;

namespace GameInstruments.NotationBuilders
{
    public abstract class BaseNotationBuilder
    {
        protected readonly StringBuilder Builder;
        
        public BaseNotationBuilder()
        {
            Builder = new StringBuilder();
        }
        public abstract string BuildNotation(string currNotation, string move, int moveNum = 0);
    }
}