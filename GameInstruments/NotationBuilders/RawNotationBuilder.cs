using System.Text;

namespace GameInstruments.NotationBuilders
{
    public class RawNotationBuilder : BaseNotationBuilder
    {
        
        public override string BuildNotation(string currNotation, string rawMove, int moveNum = 0)
        {
            Builder.Clear();
            Builder.Append(currNotation)
                .Append(rawMove).Append(' ');
            
            return Builder.ToString();
        }
    }
}