﻿using System.Collections.Generic;
using System.Linq;
using ChessRules;

namespace GameInstruments.Models
{
    public class GameInfo
    {
        private const string StartFen = "rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1";
        public Dictionary<string, string> GameParams { get; internal set; } = new Dictionary<string, string>();
        public SideColor PlayerColor { get; set; }
        public string Line { get; set; }
        public string InitialFen { get; set; }
        public List<Ply> GameTree { get; set; } = new List<Ply>();
        public virtual bool IsNull => false;
        public bool HasPreMove { get; internal set; }

        internal GameInfo() { }

        public override string ToString()
        {
            return GameParams.TryGetValue("Variant", out var variant) ? variant : "GameView";
        }
        
        public int GetKey()
        {
            if (!GameTree.Any()) return base.GetHashCode();
            
            var result = 1;
            var baseMove = !HasPreMove ? GameTree[0] : GameTree[1];
            baseMove.Move.ToCharArray().ToList().ForEach(x => result = x * result);
            result *= !HasPreMove ? GameTree.Count : GameTree.Count - 1;

            return result;

        }
    }
    public class NullGameInfo : GameInfo
    {
        public override bool IsNull => true;
    }
}
