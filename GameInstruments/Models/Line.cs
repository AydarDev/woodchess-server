﻿using System;

namespace GameInstruments.Models
{
    public class Line
    {
        public int LineLevel { get; internal set; }
        public Guid LineId { get; internal set; }
        public int LineOrder { get; internal set; }
        public Line ParentLine { get; internal set; }
        public Ply RootPly { get; internal set; }
        public virtual bool IsNull => false;
    }
    public class NullLine : Line
    {
        public override bool IsNull => true;
        private static NullLine _nullLine;
        
        private NullLine(){}

        public static NullLine GetInstance()
        {
            return _nullLine ??= new NullLine();
        }
    }
}