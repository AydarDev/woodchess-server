﻿using System.Collections.Generic;
using ChessRules;

namespace GameInstruments.Models
{
    public class Ply
    {
        public Ply()
        {
        }

        public Ply(string rawMove, int moveNum, int plyNum, string move, SideColor moveColor, Line line, string fen, Ply previousPly, bool resolvedSelf)
        {
            RawMove = rawMove;
            MoveNum = moveNum;
            PlyNum = plyNum;
            Move = move;
            MoveColor = moveColor;
            Line = line;
            Fen = fen;
            PreviousPly = previousPly;
            ResolvedSelf = resolvedSelf;
        }

        public static Ply Clone(Ply basePly)
        {
            return new Ply(basePly.RawMove, basePly.MoveNum, basePly.PlyNum, basePly.Move, basePly.MoveColor,
                basePly.Line, basePly.Fen, basePly.PreviousPly, basePly.ResolvedSelf);
        }

        public string RawMove { get; internal set; }
        public int MoveNum { get; internal set; }
        public int PlyNum { get; internal set; }
        public string Move { get; internal set; }
        public SideColor MoveColor { get; internal set; }
        public Line Line { get; internal set; }
        public string Fen { get; internal set; }
        public Ply PreviousPly { get; internal set; }
        public virtual Ply RootPly => Line.RootPly;
        public List<Ply> NextPlies { get; } = new List<Ply>();
        public bool ResolvedSelf { get; set; }

        public bool Resolved
        {
            get
            {
                var isResolved = ResolvedSelf;
                foreach (Ply nextPly in NextPlies)
                    isResolved &= nextPly.Resolved;

                return isResolved;
            }
        }

        public virtual bool IsNull => false;

        public override bool Equals(object obj)
        {
            if (obj is Ply plyObj)
            {
                return Fen == plyObj.Fen && Move == plyObj.Move && PlyNum == plyObj.PlyNum;
            }
            return base.Equals(obj);
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public override string ToString() => RawMove;
    }
    
    public class NullPly : Ply
    {
        public override bool IsNull => true;
        private static NullPly _nullPly;
        public override Ply RootPly => GetInstance();
        
        private NullPly(){}
        public static NullPly GetInstance() => _nullPly ??= new NullPly();
    }
}