﻿namespace GameInstruments.Models
{
    public class GuessMoveResponse
    {
        public ResultType Result { get; set; }
        public Ply GuessPly { get; set; }
        public Ply ResponsePly { get; set; }
        public string Message { get; set; }

        public string Move => ResponsePly.Move;
    }

    public enum ResultType
    {
        PuzzleSolved,
        LineSolved,
        MoveSolved,
        Fail
    }
}