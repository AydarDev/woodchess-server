﻿using System.Linq;
using GameInstruments.Models;
using WebSocketModels.Enums;
using WebSocketModels.Models;
using WebSocketModels.Models.Puzzles;
using static GameInstruments.Models.ResultType;

namespace GameInstruments
{
    public class PuzzleEngine
    {
        public GameInfo GameInfo { get; internal set; }
        public string CurrentFen => CurrentOpponentPly.Fen;
        public string InitialFen => GameInfo.InitialFen;

        internal Ply BasePly { get; set; }
        private Ply CurrBaseBly { get; set; } = NullPly.GetInstance();
        public Ply CurrentPlyToGuess { get; set; } = NullPly.GetInstance();
        public Ply CurrentOpponentPly { get; set; }

        string MoveToGuess => CurrentPlyToGuess.Move;

        public GameInfo LoadPuzzle(PuzzleDto puzzle)
        {
            var gameParser = new GameParser();
            if(puzzle.DataType == DataType.Pgn)
                LoadPuzzle(gameParser.ParsePgn(puzzle.Data, puzzle.HasPreMove));
            else if (puzzle.DataType == DataType.Csv)
                LoadPuzzle(gameParser.ParseCsv(puzzle.Data, puzzle.HasPreMove));
            return GameInfo;
        }
        
        public GameInfo LoadPuzzle(string pgn, bool hasPreMove = false)
        {
            GameParser gameParser = new GameParser();
            LoadPuzzle(gameParser.ParsePgn(pgn, hasPreMove));
            return GameInfo;
        }

        public GameInfo LoadPuzzleFromCsv(string csv, bool hasPreMove = false)
        {
            GameParser gameParser = new GameParser();
            LoadPuzzle(gameParser.ParseCsv(csv, hasPreMove));
            return GameInfo;
        }

        public void LoadPuzzle(GameInfo gameInfo)
        {
            GameInfo = gameInfo;
            CurrentPlyToGuess = !GameInfo.HasPreMove ? GameInfo.GameTree[0] : GameInfo.GameTree[1];
            BasePly = NullPly.GetInstance();
            CurrentOpponentPly = GetNextOpponentPly();
        }

        private Ply GetNextOpponentPly()
        {
            Ply nextPly = null;
            if (!CurrentPlyToGuess.Resolved)
                nextPly = CurrentPlyToGuess.NextPlies
                    .OrderByDescending(ply => ply.Line.LineOrder)
                    .FirstOrDefault(ply => !ply.Resolved);

            return nextPly ?? NullPly.GetInstance();
        }

        public GuessMoveResponse GuessMove(string move)
        {
            if (move.Equals(MoveToGuess))
            {
                var plyToGuess = CurrentPlyToGuess;
                CurrentPlyToGuess.ResolvedSelf = true;
                CurrentOpponentPly.ResolvedSelf = true;
                CurrentOpponentPly = GetNextOpponentPly();
                CurrBaseBly = Ply.Clone(CurrentPlyToGuess);
                CurrentPlyToGuess = NullPly.GetInstance();
                
                if (!CurrentOpponentPly.IsNull && CurrentOpponentPly.NextPlies.Any())
                    CurrentPlyToGuess = CurrentOpponentPly.NextPlies[0];

                if (!CurrentPlyToGuess.IsNull)
                    return new GuessMoveResponse
                    {
                        Result = MoveSolved, 
                        Message = "Correct move!", 
                        GuessPly = plyToGuess, 
                        ResponsePly = CurrentOpponentPly
                    };
                else
                {
                    CurrentOpponentPly.ResolvedSelf = true;
                    BasePly = GetNextBasePly();
                    if (BasePly.IsNull)
                        return new GuessMoveResponse
                        {
                            Result = PuzzleSolved, Message = "Puzzle solved! Congratulations!",
                            GuessPly = plyToGuess,
                            ResponsePly = CurrentOpponentPly
                        };
                    CurrentPlyToGuess = BasePly;
                    CurrentOpponentPly = GetNextOpponentPly();

                    CurrentPlyToGuess = CurrentOpponentPly.NextPlies[0];
                    return new GuessMoveResponse
                    {
                        Result = LineSolved, Message = "Line solved. Back to parent line.",
                        GuessPly = plyToGuess,
                        ResponsePly = CurrentOpponentPly
                    };
                }
            }

            return new GuessMoveResponse {Result = Fail, Message = "Wrong move.", ResponsePly = NullPly.GetInstance()};
        }

        private Ply GetNextBasePly()
        {
            Ply nextBasePly;
            do
            {
                nextBasePly = CurrBaseBly.RootPly;
                if (!nextBasePly.Resolved)
                    return nextBasePly;
            } while (!nextBasePly.Resolved && !nextBasePly.RootPly.IsNull);

            return NullPly.GetInstance();
        }

        public int GetSolutionKey(GameInfo info)
        {
            return SolutionKeyProvider.GetSolutionKey(info);
        }
    }
}