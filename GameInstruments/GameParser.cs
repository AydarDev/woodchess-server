﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using ChessRules;
using GameInstruments.Models;
using GameInstruments.TreeBuilders;
using GameInstruments.Utils;

namespace GameInstruments
{
    public class GameParser
    {
        private string _pgn;
        private TreeBuilder _treeBuilder;
        private GameInfo _gameInfo;

        public StreamReader Reader { get; set; }
        private readonly StringBuilder _lineBuilder = new StringBuilder();
        
        private static readonly Regex PgnParamPattern = new Regex("\\[(.+)\\s\"(.+)\"\\]");

        public GameInfo ParsePgn(string pgn, bool hasPreMove = false)
        {
            try
            {
                _pgn = pgn;
                _gameInfo = new GameInfo {HasPreMove = hasPreMove };
                ExtractGameParamsWithRemove();
                ExtractGameTree();
                GetPlayerColor();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return new NullGameInfo();
            }

            return _gameInfo ?? new NullGameInfo();
        }

        public GameInfo ParseCsv(string csv, bool hasPreMove = false)
        {
            try
            {
                var csvParts = csv.Split(',');
                var fen = csvParts[1];
                var csvLine = csvParts[2];
                var rating = csvParts[3];

                _gameInfo = new GameInfo
                {
                    HasPreMove = hasPreMove,
                    InitialFen = fen,
                    Line = csvLine
                };
                _treeBuilder = new CsvTreeBuilder().Init(fen, csvLine);
                _gameInfo.GameTree = _treeBuilder.CreateGameTree();
                GetPlayerColor();
                if(_gameInfo.HasPreMove)
                    _gameInfo.GameTree[0].ResolvedSelf = true;
                
                return _gameInfo;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return new NullGameInfo();
            }
        }

        private void GetPlayerColor()
        {
            var chess = new Chess(_gameInfo.InitialFen);
            _gameInfo.PlayerColor = !_gameInfo.HasPreMove ? chess.MoveColor : chess.MoveColor.Flip();
        }

        private void ExtractGameTree()
        {
            _gameInfo.GameParams.TryGetValue("FEN", out string fen);
            _gameInfo.InitialFen = fen;
            string line = ExtractGameLine();
            
            _gameInfo.Line = line;
            _treeBuilder = new PgnTreeBuilder().Init(fen, line);
            _gameInfo.GameTree = _treeBuilder.CreateGameTree();
            if (_gameInfo.HasPreMove)
                _gameInfo.GameTree[0].ResolvedSelf = true;
        }

        private string ExtractGameLine()
        {
            string line = _pgn.Trim(' ').TrimStart('\n', '\r');
            return line;
        }

        private void ExtractGameParamsWithRemove()
        {
            Dictionary<string, string> resultParams = new Dictionary<string, string>();
            foreach (Match match in PgnParamPattern.Matches(_pgn))
            {
                resultParams.Add(match.Groups[1].Value, match.Groups[2].Value);
                _pgn = _pgn.RemoveSubstring(match.Value).Trim('\n');
            }

            _gameInfo.GameParams = resultParams;
        }
        
        private Dictionary<string,string> ExtractGameParams(string pgn)
        {
            Dictionary<string, string> resultParams = new Dictionary<string, string>();
            foreach (Match match in PgnParamPattern.Matches(pgn))
                resultParams.Add(match.Groups[1].Value, match.Groups[2].Value);

            return resultParams;
        }
        
        public List<string> GetPgnList(string inputFile)
        {
            using StreamReader reader = new StreamReader(inputFile);
            List<string> pgns = new List<string>();
            
            string nextLine;
            StringBuilder pgnBuilder = new StringBuilder();
            while (!reader.EndOfStream)
            {
                nextLine = reader.ReadLine();
                pgnBuilder.AppendLine(nextLine);
                if (string.IsNullOrEmpty(nextLine) || PgnParamPattern.IsMatch(nextLine)) continue;
                pgns.Add(pgnBuilder.ToString());
                pgnBuilder.Clear();
            }

            return pgns;
        }

        public string GetNextPgn()
        {
            bool isLineCollected = false;
            _lineBuilder.Clear();
            if (Reader == null) return "";
            string nextLine = Reader.ReadLine();
            if (string.IsNullOrEmpty(nextLine))
                return string.Empty;
            _lineBuilder.AppendLine(nextLine);
            while (true)
            {
                if (!string.IsNullOrEmpty(nextLine) && !PgnParamPattern.IsMatch(nextLine))
                    isLineCollected = true;
                nextLine = Reader.ReadLine();
                _lineBuilder.AppendLine(nextLine);
                if(string.IsNullOrEmpty(nextLine) && isLineCollected)
                    break;
            } 

            var result = _lineBuilder.ToString();
            _lineBuilder.Clear();

            return result;
        }

        public string GetNameFromStandardPuzzlePgn(string pgn)
        {
            var gameParams = ExtractGameParams(pgn);
            var nameBuilder = new StringBuilder();
            
            if(!IsUndefinedParam(gameParams["Event"]))
                nameBuilder.Append(gameParams["Event"]);
            if (!IsUndefinedParam(gameParams["White"]))
                nameBuilder.Append(", ").Append(gameParams["White"]);
            if(!IsUndefinedParam(gameParams["Date"]) && gameParams["Date"].Length >= 4)
                nameBuilder.Append(", ").Append(gameParams["Date"].Substring(0,4));

            return nameBuilder.ToString();
        }

        public int GetSolutionKey(string pgn, bool hasPreMove)
        {
            GameInfo info = ParsePgn(pgn, hasPreMove);
            return SolutionKeyProvider.GetSolutionKey(info);
        }

        private static bool IsUndefinedParam(string param)
        {
            return "?".Equals(param);
        }
    }
}