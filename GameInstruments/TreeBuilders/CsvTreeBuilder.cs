﻿using System;
using System.Collections.Generic;
using System.Linq;
using ChessRules;
using GameInstruments.Models;
using GameInstruments.Utils;

namespace GameInstruments.TreeBuilders
{
    public class CsvTreeBuilder : TreeBuilder
    {
        public override List<Ply> CreateGameTree()
        {
            var moves = GameLine.Trim(' ').Split(' ');
            foreach (var move in moves)
            {
                CreatePly(move);
            }

            return GamePlies;
        }

        private void CreatePly(string move)
        {
            try
            {
                Ply = new Ply
                {
                    RawMove = move,
                    MoveNum = Chess.MoveNumber,
                    MoveColor = Chess.MoveColor,
                    PlyNum = GetPlyNum()
                };

                ContinueCurrLine();
                Ply.Move = MoveTranslator.GetTranslator(Chess).TranslateMove(move);
                Ply.Fen = MakeMoveForward();
                Ply.PreviousPly = PreviousPly;
                
                if (!GamePlies.Any())
                    CurrLine.RootPly = Ply;
                GamePlies.Add(Ply);
                LinkWithPreviousPly();
            }
            catch (FormatException)
            {
                throw new FormatException($"Format exception on move {move}");
            }
        }

        protected override int GetPlyNum()
        {
            var moveNum = Chess.MoveNumber;
            if (Chess.MoveColor == SideColor.Black)
                return (moveNum - 1) * 2 + 2;

            return (moveNum - 1) * 2 + 1;
        }
    }
}