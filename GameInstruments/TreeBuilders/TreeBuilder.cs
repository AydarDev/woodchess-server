﻿using System;
using System.Collections.Generic;
using System.Linq;
using ChessRules;
using GameInstruments.Models;
using WebSocketModels.Enums;

namespace GameInstruments.TreeBuilders
{
    public abstract class TreeBuilder
    {
        protected string GameLine;
        protected string InitialFen;
        internal Chess Chess;
        protected Ply Ply;
        protected Line CurrLine;
        protected readonly List<Ply> GamePlies = new List<Ply>();
        protected bool IsNeedToRestore;
        private int _currLineLevel;
        private int _lineOrderCounter;
        public virtual bool IsNull => false;
        
        protected Ply PreviousPly =>  GamePlies.Where(ply => ply.Line == Ply.Line).OrderBy(p => p.PlyNum).LastOrDefault() ??
                            GamePlies.Where(ply => ply.Line == Ply.Line.ParentLine)
                                .Where(p => p.MoveColor != Ply.MoveColor)
                                .OrderBy(p => p.PlyNum).LastOrDefault() ??
                            NullPly.GetInstance();

        public static TreeBuilder GetTreeBuilder(DataType dataType)
        {
            switch (dataType)
            {
                case DataType.Pgn:
                    return new PgnTreeBuilder();
                case DataType.Csv:
                    return new CsvTreeBuilder();
                default:
                    return new NullTreeBuilder();
            }
        }
        
        
        public TreeBuilder Init(string initialFen, string gameLine)
        {
            GameLine = gameLine;
            InitialFen = initialFen;
            Chess = new Chess(InitialFen);
            CurrLine = new Line {LineLevel = 0, LineId = Guid.NewGuid(), ParentLine = NullLine.GetInstance(), LineOrder = 0};
            return this;
        }
        
        public abstract List<Ply> CreateGameTree();
        protected abstract int GetPlyNum();
        
        protected string MakeMoveForward()
        {
            Chess = Chess.Move(Ply.Move);

            return Chess.Fen;
        }
        
        protected void OpenNewLine()
        {
            Line parentLine = CurrLine;
            IsNeedToRestore = true;
            Ply.Line = new Line
            {
                LineId = Guid.NewGuid(), 
                LineLevel = ++_currLineLevel, 
                ParentLine = parentLine,
                LineOrder = ++_lineOrderCounter
            };
            CurrLine = Ply.Line;
        }

        protected void ContinueCurrLine()
        {
            Ply.Line = CurrLine;
        }
        
        protected void CloseCurrentLine()
        {
            _currLineLevel--;
            IsNeedToRestore = true;
            CurrLine = CurrLine.ParentLine;
        }
        
        protected void LinkWithPreviousPly()
        {
            if (!Ply.PreviousPly.IsNull)
                Ply.PreviousPly.NextPlies.Add(Ply);
        }
    }

    public class NullTreeBuilder : TreeBuilder
    {
        public override bool IsNull => true;

        public override List<Ply> CreateGameTree()
        {
            throw new NotImplementedException();
        }

        protected override int GetPlyNum()
        {
            throw new NotImplementedException();
        }
    }
}