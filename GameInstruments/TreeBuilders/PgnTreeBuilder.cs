﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using ChessRules;
using GameInstruments.Models;
using GameInstruments.Utils;

namespace GameInstruments.TreeBuilders
{
    public class PgnTreeBuilder : TreeBuilder
    {
        private static readonly Regex RawMovePattern = new Regex(
            @"(((\(\s?)?)(\d+)(\.{1,3})\s?)?(([NBRQK]?[:x]?[a-h][1-8]=?[NBRQ]?)|(([PNBRQK]?)([a-h][1-8]?|[a-h]?[1-8])[:x]?([a-h][1-8]?)=?([NBRQ]?)[+#xX]?)|(O(-O){1,2}|0(-0){1,2}))[xX#+]?((\s?\))?)([!?]?[!?]?)");

        private static readonly Regex WhiteMoveSign = new Regex(@"^\.$");
        private static readonly Regex BlackMoveSign = new Regex(@"^\.{2,3}$");
        private Match _match;
        

        private bool IsNeedToOpenNewLine => !string.IsNullOrEmpty(_match.Groups[2].Value);
        private bool IsNeedToCloseCurrLine => !string.IsNullOrEmpty(_match.Groups[16].Value);
        string MoveNumText => _match.Groups[4].Value;
        string ColorDescriptor => _match.Groups[5].Value;
        string MoveValue => _match.Groups[6].Value;
        
        private int _currMoveNum;

        public PgnTreeBuilder() : base()
        {
        }

        public override List<Ply> CreateGameTree()
        {
            foreach (Match match in RawMovePattern.Matches(GameLine))
                CreatePly(match);

            return GamePlies;
        }

        private void CreatePly(Match match)
        {
            try
            {
                _match = match;

                Ply = new Ply
                {
                    RawMove = match.Value,
                    MoveNum = string.IsNullOrEmpty(MoveNumText) ? _currMoveNum : GetMoveNum(),
                    MoveColor = GetToMoveColor(),
                    PlyNum = GetPlyNum()
                };

                if (IsNeedToOpenNewLine)
                    OpenNewLine();
                else
                    ContinueCurrLine();
                Ply.PreviousPly = PreviousPly;
                if (IsNeedToOpenNewLine)
                    CurrLine.RootPly = PreviousPly;

                if (IsNeedToRestore)
                    RestoreChess();
                Ply.Move = MoveTranslator.GetTranslator(Chess).TranslateMove(MoveValue);
                Ply.Fen = MakeMoveForward();

                if (!GamePlies.Any())
                    CurrLine.RootPly = Ply;
                GamePlies.Add(Ply);
                LinkWithPreviousPly();

                if (IsNeedToCloseCurrLine)
                    CloseCurrentLine();
            }
            catch (FormatException)
            {
                throw new FormatException($"Format exception on move {MoveValue}");
            }
        }
        
        private void RestoreChess()
        {
            if (Ply.PreviousPly.IsNull)
                Chess = new Chess(InitialFen);
            Chess = new Chess(Ply.PreviousPly.Fen);
        }
        
        protected override int GetPlyNum()
        {
            int resultPlyNum = _currMoveNum * 2;
            if (GetToMoveColor() == SideColor.White)
                resultPlyNum--;
            return resultPlyNum;
        }
        
        private int GetMoveNum()
        {
            if (int.TryParse(MoveNumText, out int x))
                _currMoveNum = x;
            return x;
        }

        private SideColor GetToMoveColor()
        {
            return WhiteMoveSign.IsMatch(ColorDescriptor) ? SideColor.White : SideColor.Black;
        }
    }
}