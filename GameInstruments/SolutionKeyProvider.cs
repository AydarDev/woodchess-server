﻿using System.Linq;
using GameInstruments.Models;

namespace GameInstruments
{
    public static class SolutionKeyProvider
    {
        public static int GetSolutionKey(GameInfo info)
        {
            int key = -1;
            if (info.GameTree.Any())
                key = info.GetKey();

            return key;
        }
    }
}