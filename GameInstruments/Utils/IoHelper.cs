﻿using System.IO;

namespace GameInstruments.Utils
{
    public static class IoHelper
    {
        public static string GetPgnFromFile(string fileName)
        {
            string result = "";
            using StreamReader reader = new StreamReader(fileName);
            result = reader.ReadToEnd();
            return result;
        }
    }
}
