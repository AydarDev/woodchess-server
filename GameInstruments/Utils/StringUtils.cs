﻿using System.Text.RegularExpressions;

namespace GameInstruments.Utils
{
    static class StringUtils
    {
        internal static string RemoveFirstSubstring(this string input, string subString)
        {
            var regex = new Regex($"(^{subString})");
            
            return regex.Replace(input, "");
        }
        
        internal static string RemoveSubstring(this string input, string pattern)
        {
            return input.Replace(pattern, "");
        }
    }
}
