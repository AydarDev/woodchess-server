using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace ChessRules;
public static class NotationTranslator
{
    private static readonly List<char> Figures = new() { 'N', 'B', 'R', 'Q', 'K', 'n', 'b', 'r', 'q', 'k' };
    private static readonly List<char> Pawns = new() { 'P', 'p' };
    private static readonly Regex KingCastling = new Regex("^[Kk]?e[18]g[18]");
    private static readonly Regex QueenCastling = new Regex("^[Kk]?e[18]c[18]");

    public static string GetShortNotation(string move, string fen)
    {
        return GetShortNotation(move, new Chess(fen));
    }
    
    public static string GetShortNotation(string move, Chess chess)
    {
        try
        {
            var figure = !string.IsNullOrEmpty(move.Figure()) ? move.Figure()[0]
                : chess.GetFigureAt(move.FromSquare());

            return IsCastling(move) ? GetCastlingMove(move)
                : IsFigureMove(figure) ? GetFigureMove(figure, move, chess)
                : IsPawnMove(figure) ? GetPawnMove(move, chess)
                : move;
        }
        catch (Exception e)
        {
            Console.WriteLine(e);
            return move;
        }
    }

    private static bool IsCastling(string move) => QueenCastling.IsMatch(move) || KingCastling.IsMatch(move);
    private static bool IsFigureMove(char figure) => Figures.Contains(figure);
    private static bool IsPawnMove(char figure) => Pawns.Contains(figure);
    
    private static string GetCastlingMove(string move)
    {
        return KingCastling.IsMatch(move) ? "0-0" :
            QueenCastling.IsMatch(move) ? "0-0-0" : string.Empty;
    }

    private static string GetFigureMove(char figure, string move, Chess chess)
    {
        return new StringBuilder(figure.ToString().ToUpper())
            .ResolveMoveDuality(figure, move, chess)
            .AddCaptureSymbol(move, chess)
            .Append(move.ToSquare())
            .AddChecksMatesSymbols(move, chess)
            .ToString();
    }

    private static string GetPawnMove(string move, Chess chess)
    {
        return move.FromSquareFile() == move.ToSquareFile() ? 
            BuildForwardPawnMove(move, chess) : BuildCrossPawnMove(move, chess);
    }

    private static string BuildForwardPawnMove(string move, Chess chess)
    {
        var builder = new StringBuilder(move.ToSquare())
            .AddPromotionSymbol(move)
            .AddChecksMatesSymbols(move, chess);

        return builder.ToString();
    }

    private static string BuildCrossPawnMove(string move, Chess chess)
    {
        var builder = new StringBuilder(move.FromSquareFile()).Append(move.ToSquareFile());
        
        builder.ResolvePawnMoveDuality(move, chess)
            .AddPromotionSymbol(move)
            .AddChecksMatesSymbols(move, chess);

        return builder.ToString();
    }
}