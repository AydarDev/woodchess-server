﻿using System.Collections.Generic;

namespace ChessRules;
internal readonly struct Square
{
    public static Square None = new Square(-1, -1);

    public int X { get; }
    public int Y { get; }

    public Square(int x, int y)
    {
        X = x;
        Y = y;
    }

    public Square(string name)
    {
        if (name.Length == 2 &&
            name[0] >= 'a' && name[0] <= 'h' &&
            name[1] >= '1' && name[1] <= '8')
        {
            X = name[0] - 'a';
            Y = name[1] - '1';
        }
        else
        {
            this = None;
        }

    }

    public string Name
    {
        get
        {
            if (OnBoard())
                return (char)('a' + X) + (Y + 1).ToString();
            
            return "-";
        }
    }

    public bool OnBoard()
    {
        return X is >= 0 and < 8 &&
               Y is >= 0 and < 8;
    }

    public static IEnumerable<Square> YieldBoardSquares()
    {
        for (int y = 0; y < 8; y++)
        for (int x = 0; x < 8; x++)
            yield return new Square(x, y);
    }

    public static bool operator ==(Square a, Square b)
    {
        return a.X == b.X && a.Y == b.Y;
    }

    public static bool operator !=(Square a, Square b)
    {
        return !(a == b);
    }

    public override bool Equals(object obj)
    {
        return obj is Square other && Equals(other);
    }

    public override int GetHashCode()
    {
        unchecked
        {
            return (X * 397) ^ Y;
        }
    }
    private bool Equals(Square other)
    {
        return X == other.X && Y == other.Y;
    }
}