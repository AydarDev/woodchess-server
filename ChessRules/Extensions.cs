﻿using System;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace ChessRules;

internal static class Extensions
{
    private static readonly Regex FullMovePattern = new("^([PNBRQKpnbrqk]?)(([a-h])([1-8]))(([a-h])([1-8]))([NnBbRrQq]?)");
    internal static string Figure(this string move) => FullMovePattern.Match(move).Groups[1].Value;
    internal static string FromSquare(this string move) => FullMovePattern.Match(move).Groups[2].Value;
    internal static string FromSquareFile(this string move) => FullMovePattern.Match(move).Groups[3].Value;
    private static string FromSquareRank(this string move) => FullMovePattern.Match(move).Groups[4].Value;
    internal static string ToSquare(this string move) => FullMovePattern.Match(move).Groups[5].Value;
    internal static string ToSquareFile(this string move) => FullMovePattern.Match(move).Groups[6].Value;
    private static string ToSquareRank(this string move) => FullMovePattern.Match(move).Groups[7].Value;
    internal static string PromotionFigure(this string move) => FullMovePattern.Match(move).Groups[8].Value;
    internal static StringBuilder AddChecksMatesSymbols(this StringBuilder builder, string move, Chess chess)
    {
        chess = chess.Move(move);
        if (chess.IsCheckmate) 
            builder.Append("#");
        else if (chess.IsCheck)
            builder.Append("+");

        return builder;
    }

    internal static StringBuilder AddPromotionSymbol(this StringBuilder builder, string move)
    {
        if(!string.IsNullOrEmpty(move.PromotionFigure()))
            builder.Append('=').Append(move.PromotionFigure().ToUpper());

        return builder;
    }

    internal static StringBuilder AddCaptureSymbol(this StringBuilder builder, string move, Chess chess)
    {
        var isCapture = chess.GetFigureAt(move.ToSquare()) != '.';
        if (isCapture) builder.Append("x");

        return builder;
    }

    internal static StringBuilder ResolveMoveDuality(this StringBuilder builder, char figure, string move, Chess chess)
    {
        var validMoves = chess
            .YieldValidMoves()
            .Where(MatchByFigure(figure))
            .Where(MatchByToSquare(move)).ToArray();

        var count = validMoves.Length;
        if (count == 1) return builder;
        if (count == 2)
        {
            if (validMoves[0].FromSquareRank() == validMoves[1].FromSquareRank())
                builder.Append(move.FromSquareFile());
            else if (validMoves[0].FromSquareFile() == validMoves[1].FromSquareFile())
                builder.Append(move.FromSquareRank());
            else
                builder.Append(move.FromSquareFile());
        }
        else if (count > 2)
        {
            if (validMoves.Where(MatchBySourceFile(move)).Count() == 1)
                builder.Append(move.FromSquareFile());
            else if (validMoves.Where(MatchBySourceRank(move)).Count() == 1)
                builder.Append(move.FromSquareRank());
            else
                builder.Append(move.FromSquare());
        }
        return builder;
    }

    internal static StringBuilder ResolvePawnMoveDuality(this StringBuilder builder, string move, Chess chess)
    {
        var matchedMoves = 
            chess.YieldValidMoves()
                .Where(m => m[0] == 'P' || m[0] == 'p')
                .Where(MatchBySourceFile(move))
                .Where(MatchByTargetFile(move))
                .Where(MatchByPromotion(move));

        if (matchedMoves.Count() > 1)
        {
            builder.Insert(1, 'x');
            builder.Append(move.ToSquareRank());
        }
        return builder;
    }
    
    private static Func<string, bool> MatchByFigure(char figure) => 
        move => move.Figure()[0]  == figure;

    private static Func<string, bool> MatchByToSquare(string inputMove) =>
        move => move.ToSquare() == inputMove.ToSquare();

    private static Func<string, bool> MatchBySourceFile(string inputMove) =>
        move => move.FromSquareFile() == inputMove.FromSquareFile();

    private static Func<string, bool> MatchBySourceRank(string inputMove) =>
        move => move.FromSquareRank() == inputMove.FromSquareRank();

    private static Func<string, bool> MatchByTargetFile(string inputMove) =>
        move => move.ToSquareFile() == inputMove.ToSquareFile();

    private static Func<string, bool> MatchByPromotion(string inputMove) =>
        move => move.PromotionFigure() == inputMove.PromotionFigure();
}