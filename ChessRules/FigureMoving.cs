﻿using System;

namespace ChessRules
{
    class FigureMoving
    {
        public Figure Figure { get; }
        public Square From { get; }
        public Square To { get; }
        private Figure Promotion { get; }

        public static readonly FigureMoving None = new FigureMoving();

        private FigureMoving()
        {
            Figure = Figure.None;
            From = Square.None;
            To = Square.None;
            Promotion = Figure.None;
        }

        public FigureMoving(FigureOnSquare fs, Square to, Figure promotion = Figure.None)
        {
            Figure = fs.Figure;
            From = fs.Square;
            To = to;
            Promotion = promotion;
        }

        public FigureMoving(string move)        //Qd6d7,  Pe2e4,   Pa7a8Q
        {
            try
            {
                Figure = (Figure)move[0];
                From = new Square(move.Substring(1, 2));
                To = new Square(move.Substring(3, 2));

                if (move.Length == 6)
                    Promotion = (Figure)move[5];
                else
                    Promotion = Figure.None;
            } catch (ArgumentOutOfRangeException)
            {
                Console.WriteLine("Wrong Move");
            }
        }

        public override string ToString()
        {
            return (char)Figure + From.Name + To.Name + 
                (Promotion == Figure.None ? string.Empty : ((char)Promotion).ToString());
        }

        public int DeltaX => To.X - From.X;
        public int DeltaY => To.Y - From.Y;

        public int AbsDeltaX => Math.Abs(DeltaX);
        public int AbsDeltaY => Math.Abs(DeltaY);
        public int SignX => Math.Sign(DeltaX);
        public int SignY => Math.Sign(DeltaY);
        public Figure PlacedFigure => Promotion == Figure.None ? Figure : Promotion;
    }
}
