namespace ChessRules;

public static class Constants
{
    internal const string A1 = "a1";
    internal const string B1 = "b1";
    internal const string C1 = "c1";
    internal const string D1 = "d1";
    internal const string E1 = "e1";
    internal const string F1 = "f1";
    internal const string G1 = "g1";
    internal const string H1 = "h1";
    internal const string A8 = "a8";
    internal const string B8 = "b8";
    internal const string C8 = "c8";
    internal const string D8 = "d8";
    internal const string E8 = "e8";
    internal const string F8 = "f8";
    internal const string G8 = "g8";
    internal const string H8 = "h8";
    internal const string WhiteKing = "K";
    internal const string WhiteQueen = "Q";
    internal const string BlackKing = "k";
    internal const string BlackQueen = "q";
    internal const string White = "w";
    internal const string Black = "b";
}