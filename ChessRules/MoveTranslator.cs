﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace ChessRules;

public class MoveTranslator
{
    private static Chess _chess;
    private string _inputMove;
    private string _figure;
    private string _fromSquareSign;
    private string _toSquareSign;
    private string _promotion;


    private static readonly Regex FullMovePattern = new Regex("^[PpNnBbRrQqKk][a-h][1-8][a-h][1-8][NnBbRrQq]?$");
    private static readonly Regex CastlingMovePattern = new Regex("(0-){1,2}0|(O-){1,2}O");

    private static readonly Regex OneSquareMovePattern =
        new Regex("^([NBRQK]?)[:x]?([a-h][1-8])=?([NBRQ]?)[xX#+]?$");

    private static readonly Regex TwoSquareMovePattern =
        new Regex("^([PNBRQK]?)([a-h][1-8]?|[a-h]?[1-8])[:x]?([a-h][1-8]?)=?([NBRQnbrq]?)[+#xX]?$");

    private static readonly Regex StrictTwoSquaresPattern =
        new Regex(@"^([a-h][1-8])([a-h][1-8])([NBRQ]?)[+#xX]?$");

    private static readonly Regex PawnMoveFilter = new Regex("^[a-h].*");


    private static readonly Dictionary<string, Dictionary<SideColor, string>> CastlingMoves =
        new Dictionary<string, Dictionary<SideColor, string>>
        {
            {
                "0-0", new Dictionary<SideColor, string>
                {
                    {SideColor.White, "Ke1g1"},
                    {SideColor.Black, "ke8g8"}
                }
            },
            {
                "0-0-0", new Dictionary<SideColor, string>
                {
                    {SideColor.White, "Ke1c1"},
                    {SideColor.Black, "ke8c8"}
                }
            },
            {
                "O-O", new Dictionary<SideColor, string>
                {
                    {SideColor.White, "Ke1g1"},
                    {SideColor.Black, "ke8g8"}
                }
            },
            {
                "O-O-O", new Dictionary<SideColor, string>
                {
                    {SideColor.White, "Ke1c1"},
                    {SideColor.Black, "ke8c8"}
                }
            }
        };

    private static readonly Dictionary<SideColor, string> PawnChars = new Dictionary<SideColor, string>
    {
        {SideColor.White, "P"},
        {SideColor.Black, "p"}
    };

    private bool IsFullMove => FullMovePattern.IsMatch(_inputMove);
    private bool IsCastling => CastlingMovePattern.IsMatch(_inputMove);
    private bool IsPawnMove => PawnMoveFilter.IsMatch(_inputMove);
    private bool IsTwoSquareMove => TwoSquareMovePattern.IsMatch(_inputMove);
    private bool IsStrictTwoSquares => StrictTwoSquaresPattern.IsMatch(_inputMove);

    private static MoveTranslator _translator;

    public static MoveTranslator GetTranslator(Chess chess)
    {
        var translator = _translator;
        if (translator == null)
            return (_translator = new MoveTranslator(chess));

        _chess = chess;
        return translator;
    }

    private MoveTranslator(Chess chess)
    {
        _chess = chess;
    }

    public string TranslateMove(string move)
    {
        _inputMove = move;
        if (!ValidMoveFormat())
            throw new FormatException("Wrong move format");

        return IsFullMove ? move : IsStrictTwoSquares ? GetTwoSquaresMove() :
            IsCastling ? GetCastlingMove() : GetOrdinaryMove() ?? string.Empty;
    }

    private string GetTwoSquaresMove()
    {
        var match = StrictTwoSquaresPattern.Match(_inputMove);
        _fromSquareSign = match.Groups[1].Value;
        _toSquareSign = match.Groups[2].Value;
        _promotion = match.Groups[3].Value;

        return _chess.YieldValidMoves()
            .Where(MatchBySquares)
            .Where(MatchByPromotion)
            .FirstOrDefault();
    }

    private string GetOrdinaryMove()
    {
        ParseInputMove();

        return _chess
            .YieldValidMoves()
            .Where(MatchByFigure)
            .Where(MatchBySquares)
            .Where(MatchByPromotion)
            .FirstOrDefault();
    }

    private void ParseInputMove()
    {
        Match match = IsTwoSquareMove
            ? TwoSquareMovePattern.Match(_inputMove)
            : OneSquareMovePattern.Match(_inputMove);

        _figure = IsPawnMove ? PawnChars[_chess.MoveColor] : match.Groups[1].Value;
        if (IsTwoSquareMove)
        {
            _fromSquareSign = match.Groups[2].Value;
            _toSquareSign = match.Groups[3].Value;
            _promotion = match.Groups[4].Value;
        }
        else
        {
            _toSquareSign = match.Groups[2].Value;
            _promotion = match.Groups[3].Value;
        }
    }

    private bool MatchBySquares(string move)
    {
        bool isMatch = move.ToSquare().Contains(_toSquareSign);
        if (IsTwoSquareMove)
            isMatch &= move.FromSquare().Contains(_fromSquareSign);

        return isMatch;
    }

    private bool MatchByPromotion(string move)
    {
        return move.PromotionFigure().ToLower().Equals(_promotion.ToLower());
    }


    private string GetCastlingMove()
    {
        string moveCandidate = CastlingMoves[_inputMove][_chess.MoveColor];
        return _chess.IsValidMove(moveCandidate) ? moveCandidate : string.Empty;
    }

    private bool MatchByFigure(string moveCandidate)
    {
        return _chess.MoveColor == SideColor.Black
            ? moveCandidate[0].ToString() == _figure.ToLower()
            : moveCandidate[0].ToString() == _figure;
    }

    private bool ValidMoveFormat()
    {
        return IsFullMove || IsCastling || TwoSquareMovePattern.IsMatch(_inputMove) ||
               OneSquareMovePattern.IsMatch(_inputMove);
    }
}