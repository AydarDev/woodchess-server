﻿using System.Collections.Generic;
using static ChessRules.Constants;

namespace ChessRules;
class Board
{
    public string Fen { get; protected set; }
    protected readonly Figure[,] Figures;
    public SideColor MoveColor { get; protected set; }
    public bool CanCastleA1 { get; protected set; }  // Q
    public bool CanCastleA8 { get; protected set; }  // K
    public bool CanCastleH1 { get; protected set; }  //q
    public bool CanCastleH8 { get; protected set; }  // k
    public Square EnPassant { get; protected set; }
    protected int DrawNumber { get; private set; }
    public int MoveNumber { get; protected set; }

    public Board (string fen)
    {  
        Fen = fen;
        Figures = new Figure[8, 8];
        Init();
    }

    public Board Move(FigureMoving fm)
    {
        return new NextBoard(Fen, fm);
    }

    public IEnumerable<FigureOnSquare> YieldMyFiguresOnSquare()
    {
        foreach (Square square in Square.YieldBoardSquares())
            if (Equals(GetFigureAt(square).GetColor(), MoveColor))
                yield return new FigureOnSquare(GetFigureAt(square), square);
    }

    public Figure GetFigureAt(Square square)
    {
        if (square.OnBoard())
            return Figures[square.X, square.Y];
            
        return Figure.None;
    }

    public bool IsCheck()
    {
        return IsCheckAfter(FigureMoving.None);
    }
        
    internal bool IsCheckAfter(FigureMoving fm)
    {
        Board after = Move(fm);
        return after.CanEatKing();
    }

    private bool CanEatKing()
    {
        Square badKing = FindBadKing();
        Moves moves = new Moves(this);
        foreach (FigureOnSquare fs in YieldMyFiguresOnSquare())
            if (moves.CanMove(new FigureMoving(fs, badKing)))
                return true;
        return false;
    }

    private Square FindBadKing()
    {
        Figure badKing = MoveColor == SideColor.White ? Figure.BlackKing : Figure.WhiteKing;
        foreach (Square square in Square.YieldBoardSquares())
            if (GetFigureAt(square) == badKing)
                return square;
        return Square.None;
    }

    private void Init()
    {
        // rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1
        // 0                                           1 2    3 4 5

        string[] parts = Fen.Split();
        InitFigures(parts[0]);
        InitMoveColor(parts[1]);
        InitCastleFlags(parts[2]);
        InitEnPassant(parts[3]);
        InitDrawNumber(parts[4]);
        InitMoveNumber(parts[5]);
            
    }


    private void InitFigures(string v)
    {
        // 8 -> 71 -> 611 -> ...

        for (int j = 8; j > 1; j--)
            v = v.Replace(j.ToString(), (j - 1).ToString() + "1");
        v = v.Replace('1', (char)Figure.None);
        string[] lines = v.Split('/');

        for (int y = 7; y >= 0; y--)
        for (int x = 0; x <= 7; x++)
            Figures[x, y] = (Figure)lines[7 - y][x];

    }

    private void InitMoveColor(string v)
    {
        MoveColor = v == Black ? SideColor.Black : SideColor.White;
    }


    private void InitCastleFlags(string v)
    {
        CanCastleA1 = v.Contains(WhiteQueen);
        CanCastleH1 = v.Contains(WhiteKing);
        CanCastleA8 = v.Contains(BlackQueen);
        CanCastleH8 = v.Contains(BlackKing);
    }

    private void InitEnPassant(string v)
    {
        EnPassant = new Square(v);
    }

    private void InitDrawNumber(string v)
    {
        DrawNumber = int.Parse(v);
    }

    private void InitMoveNumber(string v)
    {
        MoveNumber = int.Parse(v);
    }

}