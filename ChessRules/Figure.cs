﻿using System.Collections.Generic;

namespace ChessRules;

internal enum Figure
{
    None = '.',
    WhiteKing = 'K',
    WhiteQueen = 'Q',
    WhiteRook = 'R',
    WhiteBishop = 'B',
    WhiteKnight = 'N',
    WhitePawn = 'P',

    BlackKing = 'k',
    BlackQueen = 'q',
    BlackRook = 'r',
    BlackBishop = 'b',
    BlackKnight = 'n',
    BlackPawn = 'p',
}

static class FigureMethods
{
    private const string White = "White";
    private const string Black = "Black";
    public static SideColor GetColor(this Figure figure)
    {
        if (figure.ToString().Contains(White))
            return SideColor.White;
        if (figure.ToString().Contains(Black))
            return SideColor.Black;
            
        return SideColor.None;
    }
    public static IEnumerable<Figure> YieldPromotions(this Figure figure, Square to)
    {
        if (figure == Figure.WhitePawn && to.Y == 7)
        {
            yield return Figure.WhiteQueen;
            yield return Figure.WhiteRook;
            yield return Figure.WhiteBishop;
            yield return Figure.WhiteKnight;
        }
        else if (figure == Figure.BlackPawn && to.Y == 0)
        {
            yield return Figure.BlackQueen;
            yield return Figure.BlackRook;
            yield return Figure.BlackBishop;
            yield return Figure.BlackKnight;
        }
        else
            yield return Figure.None;
    }
}