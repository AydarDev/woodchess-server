﻿using System.Collections.Generic;
using System.Linq;

namespace ChessRules
{
    public class Chess
    {
        const string InitialFen = "rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1";
        public string Fen => _board.Fen;

        public int MoveNumber => _board.MoveNumber;

        public SideColor MoveColor => _board.MoveColor;

        public bool IsCheck { get; private set; }
        public bool IsCheckmate { get; private set; }
        public bool IsStalemate { get; private set; }

        private readonly Board _board;
        private readonly Moves _moves;


        public Chess(string fen = InitialFen)
        {
            if (string.IsNullOrEmpty(fen))
                fen = InitialFen;

            _board = new Board(fen);
            _moves = new Moves(_board);
        }

        Chess(Board board)
        {
            this._board = board;
            _moves = new Moves(board);
            SetCheckFlags();
        }

        private void SetCheckFlags()
        {
            IsCheck = _board.IsCheck();
            IsCheckmate = false;
            IsStalemate = false;

            if (!YieldValidMoves().Any())
            {
                if (IsCheck)
                    IsCheckmate = true;
                else
                    IsStalemate = true;    
            }
        }

        public bool IsValidMove(string move)
        {
            FigureMoving fm = new FigureMoving(move);
            if (!_moves.CanMove(fm))
                return false;
            if (_board.IsCheckAfter(fm))
                return false;
            return true;
        }

        public Chess Move(string move)
        {
            move = MoveTranslator.GetTranslator(this).TranslateMove(move);
            if (!IsValidMove(move))
                return this;
            FigureMoving fm = new FigureMoving(move);
            Board nextBoard = _board.Move(fm);
            Chess nextChess = new Chess(nextBoard);
            return nextChess;
        }

        public char GetFigureAt(int x, int y)
        {
            Square square = new Square(x, y);
            Figure figure = _board.GetFigureAt(square);
            return figure == Figure.None ? '.' : (char)figure;
        }

        public char GetFigureAt(string xy)
        {
            Square square = new Square(xy);
            Figure figure = _board.GetFigureAt(square);
            return figure == Figure.None ? '.' : (char)figure;
        }

        public IEnumerable<string> YieldValidMoves()
        {
            foreach (FigureOnSquare fs in _board.YieldMyFiguresOnSquare())
                foreach (Square to in Square.YieldBoardSquares())
                    foreach (Figure promotion in fs.Figure.YieldPromotions(to))
                    {
                        FigureMoving fm = new FigureMoving(fs, to, promotion);
                        if (_moves.CanMove(fm))
                            if (!_board.IsCheckAfter(fm))
                                yield return fm.ToString();
                    }
        }
    }
}
