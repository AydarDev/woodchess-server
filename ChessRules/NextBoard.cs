﻿using System.Text;
using static ChessRules.Constants;

namespace ChessRules;

class NextBoard : Board
{
    private readonly FigureMoving _fm;

    public NextBoard(string fen, FigureMoving fm) : base(fen)
    {
        _fm = fm;
        MoveFigures();
        DropEnPassant();
        SetEnPassant();
        MoveCastleRook();
        UpdateCastleFlags();
        UpdateMoveNumber();
        UpdateMoveColor();
        GenerateFen();
    }

    private void SetFigureAt(Square square, Figure figure)
    {
        if (square.OnBoard())
            Figures[square.X, square.Y] = figure;
    }


    private void UpdateMoveColor()
    {
        MoveColor = MoveColor.Flip();
    }

    private void UpdateMoveNumber()
    {
        if (MoveColor == SideColor.Black)
            MoveNumber++;
    }

    private void MoveFigures()
    {
        SetFigureAt(_fm.From, Figure.None);
        SetFigureAt(_fm.To, _fm.PlacedFigure);
    }

    private void DropEnPassant()
    {
        if (_fm.To == EnPassant && _fm.Figure is Figure.WhitePawn or Figure.BlackPawn)
                SetFigureAt(new Square(_fm.To.X, _fm.From.Y), Figure.None);
    }

    private void SetEnPassant()
    {
        EnPassant = Square.None;
        if (_fm.Figure == Figure.WhitePawn && _fm.From.Y == 1 && _fm.To.Y == 3)
                EnPassant = new Square(_fm.From.X, 2);
        if (_fm.Figure == Figure.BlackPawn && _fm.From.Y == 6 && _fm.To.Y == 4)
                EnPassant = new Square(_fm.From.X, 5);
    }

    private void MoveCastleRook()
    {
        if (_fm.Figure == Figure.WhiteKing && _fm.From.Name == E1)
        {
            if (_fm.To.Name == G1)
            {
                SetFigureAt(new Square(H1), Figure.None);
                SetFigureAt(new Square(F1), Figure.WhiteRook);
            }
            else if (_fm.To.Name == C1)
            {
                SetFigureAt(new Square(A1), Figure.None);
                SetFigureAt(new Square(D1), Figure.WhiteRook);
            }
        }

        else if (_fm.Figure == Figure.BlackKing && _fm.From.Name == E8)
        {
            if (_fm.To.Name == G8)
            {
                SetFigureAt(new Square(H8), Figure.None);
                SetFigureAt(new Square(F8), Figure.BlackRook);
            }
            else if (_fm.To.Name == C8)
            {
                SetFigureAt(new Square(A8), Figure.None);
                SetFigureAt(new Square(D8), Figure.BlackRook);
            }
        }
    }

    private void UpdateCastleFlags()
    {
        switch (_fm.Figure)
        {
            case Figure.WhiteKing:
                CanCastleA1 = false;
                CanCastleH1 = false;
                break;

            case Figure.WhiteRook:
                if (_fm.From.Name == A1)
                    CanCastleA1 = false;
                else if (_fm.From.Name == H1)
                    CanCastleH1 = false;
                break;

            case Figure.BlackKing:
                CanCastleA8 = false;
                CanCastleH8 = false;
                break;

            case Figure.BlackRook:
                if (_fm.From.Name == A8)
                    CanCastleA8 = false;
                else if (_fm.From.Name == H8)
                    CanCastleH8 = false;
                break;
        }
    }

    private void GenerateFen()
    {
        Fen = FenFigures() + " " +
              FenMoveColor() + " " +
              FenCastleFlags() + " " +
              FenEnPassant() + " " +
              FenDrawNumber() + " " +
              FenMoveNumber();
    }

    private string FenFigures()
    {
        StringBuilder sb = new StringBuilder();
        for (int y = 7; y >= 0; y--)
        {
            for (int x = 0; x <= 7; x++)
                sb.Append(Figures[x, y] == Figure.None ? '1' : (char)Figures[x, y]);
            if (y > 0)
                sb.Append("/");
        }

        string eight = "11111111";
        for (int j = 8; j > 0; j--)
        {
            sb.Replace(eight, j.ToString());
            eight = eight.Remove(j - 1, 1);
        }

        return sb.ToString();
    }

    private string FenMoveColor()
    {
        return MoveColor == SideColor.White ? White : Black;
    }

    private string FenCastleFlags()
    {
        string flags =
            (CanCastleH1 ? WhiteKing : string.Empty) +
            (CanCastleA1 ? WhiteQueen : string.Empty) +
            (CanCastleH8 ? BlackKing : string.Empty) +
            (CanCastleA8 ? BlackQueen : string.Empty);

        return string.IsNullOrEmpty(flags) ? "-" : flags;
    }

    private string FenEnPassant()
    {
        return EnPassant.Name;
    }

    private string FenDrawNumber()
    {
        return DrawNumber.ToString();
    }

    private string FenMoveNumber()
    {
        return MoveNumber.ToString();
    }
}