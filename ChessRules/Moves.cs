﻿using static ChessRules.Constants;

namespace ChessRules;
class Moves
{
    private const string Ke1f1 = "Ke1f1";
    private const string Ke1g1 = "Ke1g1";
    private const string Ke1d1 = "Ke1d1";
    private const string Ke1c1 = "Ke1c1";
    private const string Ke8f8 = "ke8f8";
    private const string Ke8g8 = "ke8g8";
    private const string Ke8d8 = "ke8d8";
    private const string Ke8c8 = "ke8c8";

    private FigureMoving _fm;
    private readonly Board _board;

    public Moves(Board board)
    {
        _board = board;
    }

    public bool CanMove(FigureMoving fm)
    {
        _fm = fm;
        return
            CanMoveFrom() &&
            CanMoveTo() &&
            CanFigureMove();
    }


    private bool CanMoveFrom()
    {
        return _fm.From.OnBoard() &&
               _fm.Figure.GetColor() == _board.MoveColor;
    }

    private bool CanMoveTo()
    {
        return _fm.To.OnBoard() &&
               _board.GetFigureAt(_fm.To).GetColor() != _board.MoveColor;
    }

    private bool CanFigureMove()
    {
        switch (_fm.Figure)
        {
            case Figure.WhiteKing:
            case Figure.BlackKing:
                return CanKingMove() ||
                       CanKingCastle();

            case Figure.WhiteQueen:
            case Figure.BlackQueen:
                return CanStraightMove();

            case Figure.WhiteRook:
            case Figure.BlackRook:
                return (_fm.SignX == 0 || _fm.SignY == 0) &
                       CanStraightMove();

            case Figure.WhiteBishop:
            case Figure.BlackBishop:
                return (_fm.SignX != 0 && _fm.SignY != 0) &
                       CanStraightMove();

            case Figure.WhiteKnight:
            case Figure.BlackKnight:
                return CanKnightMove();

            case Figure.WhitePawn:
            case Figure.BlackPawn:
                return CanPawnMove();

            default:
                return false;
        }
    }


    private bool CanKingMove()
    {
        return _fm.AbsDeltaX <= 1 &&
               _fm.AbsDeltaY <= 1;
    }


    private bool CanKingCastle()
    {
        if (_fm.Figure == Figure.WhiteKing)
        {
            if (_fm.From.Name == E1 && _fm.To.Name == G1 && _board.CanCastleH1)
                return _board.GetFigureAt(new Square(H1)) == Figure.WhiteRook
                       && _board.GetFigureAt(new Square(F1)) == Figure.None
                       && _board.GetFigureAt(new Square(G1)) == Figure.None
                       && !_board.IsCheck() && !_board.IsCheckAfter(new FigureMoving(Ke1f1))
                       && !_board.IsCheckAfter(new FigureMoving(Ke1g1));

            if (_fm.From.Name == E1 && _fm.To.Name == C1)
                return _board.CanCastleA1
                       && _board.GetFigureAt(new Square(A1)) == Figure.WhiteRook
                       && _board.GetFigureAt(new Square(B1)) == Figure.None
                       && _board.GetFigureAt(new Square(C1)) == Figure.None
                       && _board.GetFigureAt(new Square(D1)) == Figure.None
                       && !_board.IsCheck()
                       && !_board.IsCheckAfter(new FigureMoving(Ke1d1))
                       && !_board.IsCheckAfter(new FigureMoving(Ke1c1));
        }

        else if (_fm.Figure == Figure.BlackKing)
        {
            if (_fm.From.Name == E8 && _fm.To.Name == G8)
                return _board.CanCastleH8
                       && _board.GetFigureAt(new Square(H8)) == Figure.BlackRook
                       && _board.GetFigureAt(new Square(G8)) == Figure.None
                       && _board.GetFigureAt(new Square(F8)) == Figure.None
                       && !_board.IsCheck()
                       && !_board.IsCheckAfter(new FigureMoving(Ke8f8))
                       && !_board.IsCheckAfter(new FigureMoving(Ke8g8));

            if (_fm.From.Name == E8 && _fm.To.Name == C8)
                return _board.CanCastleA8
                       && _board.GetFigureAt(new Square(A8)) == Figure.BlackRook
                       && _board.GetFigureAt(new Square(B8)) == Figure.None
                       && _board.GetFigureAt(new Square(C8)) == Figure.None
                       && _board.GetFigureAt(new Square(D8)) == Figure.None
                       && !_board.IsCheck()
                       && !_board.IsCheckAfter(new FigureMoving(Ke8d8))
                       && !_board.IsCheckAfter(new FigureMoving(Ke8c8));
        }

        return false;
    }

    private bool CanStraightMove()
    {
        Square at = _fm.From;
        do
        {
            at = new Square(at.X + _fm.SignX, at.Y + _fm.SignY);
            if (at == _fm.To)
                return true;
        } while (at.OnBoard() &
                 _board.GetFigureAt(at) == Figure.None);

        return false;
    }

    private bool CanKnightMove()
    {
        return _fm.AbsDeltaX == 2 && _fm.AbsDeltaY == 1 || _fm.AbsDeltaX == 1 && _fm.AbsDeltaY == 2;
    }

    private bool CanPawnMove()
    {
        if (_fm.From.Y is < 1 or > 6)
            return false;
        int stepY = _fm.Figure.GetColor() == SideColor.White ? +1 : -1;
        return CanPawnGo(stepY) ||
               CanPawnJump(stepY) ||
               CanPawnEat(stepY) ||
               CanPawnEnPassant(stepY);
    }


    private bool CanPawnGo(int stepY)
    {
        if (_board.GetFigureAt(_fm.To) == Figure.None)
            if (_fm.DeltaX == 0)
                if (_fm.DeltaY == stepY)
                    return true;
        return false;
    }

    private bool CanPawnJump(int stepY)
    {
        int startY = _fm.Figure.GetColor() == SideColor.White ? 1 : 6;

        if (_board.GetFigureAt(_fm.To) == Figure.None)
            if (_board.GetFigureAt(new Square(_fm.From.X, _fm.From.Y + stepY)) == Figure.None)
                if (_fm.From.Y == startY)
                    if (_fm.DeltaX == 0)
                        if (_fm.DeltaY == 2 * stepY)
                            return true;

        return false;
    }

    private bool CanPawnEat(int stepY)
    {
        if (_board.GetFigureAt(_fm.To) != Figure.None)
            if (_fm.AbsDeltaX == 1)
                if (_fm.DeltaY == stepY)
                    return true;

        return false;
    }

    private bool CanPawnEnPassant(int stepY)
    {
        if (_fm.To == _board.EnPassant)
            if (_board.GetFigureAt(_fm.To) == Figure.None)
                if (_fm.DeltaY == stepY)
                    if (_fm.AbsDeltaX == 1)
                        if (stepY == +1 && _fm.From.Y == 4 ||
                            stepY == -1 && _fm.From.Y == 3)
                            return true;

        return false;
    }
}