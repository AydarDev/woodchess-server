﻿namespace ChessRules;

public enum SideColor
{
    None,
    White,
    Black
}

public static class ColorMethods
{
    public static SideColor Flip (this SideColor color)
    {
        if (color == SideColor.Black)
            color = SideColor.White;
        else if (color == SideColor.White)
            color = SideColor.Black;
        return color;
    }
}