﻿namespace PuzzleApi.Models
{
    public enum SolveResult
    {
        Solve, Fail
    }
}