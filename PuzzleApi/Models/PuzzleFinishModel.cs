﻿using System;

namespace PuzzleApi.Models
{
    [Serializable]
    public class PuzzleFinishModel
    {
        public long PlayerId { get; set; }
        public int SolutionKey { get; set; }
        public bool IsResolved { get; set; }
        public string PuzzleId { get; set; }
    }
}