﻿using System.Collections.Generic;
using System.Text.Json.Serialization;
using Newtonsoft.Json.Converters;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using WebSocketModels.Enums;
using WebSocketModels.Models.Puzzles;

namespace PuzzleApi.Models
{
    public class Puzzle
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }
        public string Name { get; set; }
        
        [Newtonsoft.Json.JsonConverter(typeof(StringEnumConverter))]
        [BsonRepresentation(BsonType.String)]
        public PuzzleType PuzzleType { get; set; }
        
        [Newtonsoft.Json.JsonConverter(typeof(StringEnumConverter))]
        [BsonRepresentation(BsonType.String)]
        public DataType DataType { get; set; }
        public string Data { get; set; }
        public double Rating { get; set; }
        public List<string> Tags { get; init; } = new();
        
        [JsonIgnore]
        public HashSet<long> CurrentOf { get; set; } = new();
        
        [JsonIgnore]
        public HashSet<SolvedBy> SolvedBy { get; set; } = new();
        
        [JsonIgnore]
        public HashSet<long> LikedBy { get; set; }  = new();
        
        [JsonIgnore]
        public int SolutionKey { get; set; }
        public bool HasPreMove { get; init; }
    }
}