﻿using System;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;

namespace PuzzleApi.Models
{
    public class PuzzleDbSettings : IPuzzleDbSettings
    {
        public string PuzzlesCollectionName { get; }
        public string ConnectionString { get; }
        public string DatabaseName { get; }

        public PuzzleDbSettings(IWebHostEnvironment env, IConfiguration configuration)
        {
            ConnectionString = env.IsDevelopment()
                ? configuration["PuzzleDbSettings:ConnectionString"]
                : Environment.GetEnvironmentVariable("MONGO_CONNECTION");

            Console.WriteLine($"Get connection string {ConnectionString}");
            DatabaseName = configuration["PuzzleDbSettings:DatabaseName"];
            PuzzlesCollectionName = configuration["PuzzleDbSettings:PuzzlesCollectionName"];
        }

        public PuzzleDbSettings(string connectionString, string dbName, string collectionName)
        {
            ConnectionString = connectionString;
            DatabaseName = dbName;
            PuzzlesCollectionName = collectionName;
        }
    }

    public interface IPuzzleDbSettings
    {
        string PuzzlesCollectionName { get; }
        string ConnectionString { get; }
        string DatabaseName { get; }
    }
}