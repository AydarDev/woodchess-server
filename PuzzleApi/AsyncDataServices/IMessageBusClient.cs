﻿using WebSocketModels.Models.Events;

namespace PuzzleApi.AsyncDataServices;

public interface IMessageBusClient
{
    void SendPuzzleDoneMessage(PuzzleDoneDto puzzleSolvedDto);
}