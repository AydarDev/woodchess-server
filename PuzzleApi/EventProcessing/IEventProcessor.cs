﻿
namespace PuzzleApi.EventProcessing
{
    public interface IEventProcessor
    {
        void ProcessEvent(string message);
    }
}