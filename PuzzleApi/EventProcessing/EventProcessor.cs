﻿using System;
using Newtonsoft.Json;
using PuzzleApi.Services;
using WebSocketModels.Enums;
using WebSocketModels.Models.Events;

namespace PuzzleApi.EventProcessing
{
    public class EventProcessor : IEventProcessor
    {
        private readonly IPuzzleService _puzzleService;

        public EventProcessor(IPuzzleService puzzleService)
        {
            _puzzleService = puzzleService;
        }
        public void ProcessEvent(string message)
        {
            var eventType = DetermineEventType(message);
            switch (eventType)
            {
                case(EventType.RatingChanged):
                    MakeRatingsUpdate(message);
                    break;
                case(EventType.PuzzleDone):
                    break;
            }
        }

        private void MakeRatingsUpdate(string message)
        {
            var receivedEvent = JsonConvert.DeserializeObject<RatingChangedDto>(message);
            Console.WriteLine($"--> Rating changed with event {message}");
            var puzzle = _puzzleService.Get(receivedEvent.PuzzleId);
            puzzle.Rating = receivedEvent.NewRating;
            _puzzleService.Update(puzzle.Id, puzzle);
            Console.WriteLine($"--> Updating rating of puzzle {puzzle.Id}, new value {puzzle.Rating}");
        }

        private EventType DetermineEventType(string message)
        {
            try
            {
                var genericEvent = JsonConvert.DeserializeObject<GenericEventDto>(message);
                return genericEvent.EventType;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return EventType.Undetermined;
            }
            
        }
    }
}