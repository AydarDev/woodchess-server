﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading.Tasks;
using PuzzleApi.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using PuzzleApi.AsyncDataServices;
using PuzzleApi.Services.Grpc;
using WebSocketModels.Enums;
using WebSocketModels.Models.Events;
using Puzzle = PuzzleApi.Models.Puzzle;
using PuzzleFinishModel = PuzzleApi.Models.PuzzleFinishModel;

namespace PuzzleApi.Controllers
{
    [Route("puzzle_api/{controller}/{action}")]
    [ApiController]
    public class PuzzlesController : Controller
    {
        private readonly IPuzzleService _puzzleService;
        private readonly IMessageBusClient _messageBusClient;
        private readonly IPlayerClient _playerClient;
        private readonly ILogger<PuzzlesController> _logger;

        public PuzzlesController(IPuzzleService puzzleService, IMessageBusClient messageBusClient, IPlayerClient playerClient,
            ILogger<PuzzlesController> logger)
        {
            _puzzleService = puzzleService;
            _messageBusClient = messageBusClient;
            _playerClient = playerClient;
            _logger = logger;
        }

        [HttpGet]
        public ActionResult Home()
        {
            return Ok("Hello");
        }
        
        [HttpGet]
        public ActionResult<List<Puzzle>> GetAll() =>
            _puzzleService.Get();
        
        [Route("{id}")]
        [HttpGet(Name = "GetPuzzle")]
        public ActionResult<Puzzle> Get(string id)
        {
            var puzzle = _puzzleService.Get(id);
            if (puzzle == null)
                return NotFound();

            return puzzle;
        }

        [Route("{type}/{playerId}")]
        [HttpGet]
        public async Task<Puzzle> GetNext(PuzzleType type, long playerId)
        {
            var sw = new Stopwatch();
            sw.Start();
            var puzzle = await _puzzleService.GetCurrentPuzzle(type, playerId);
            
            _logger.LogDebug("After GetCurrentPuzzle elapsed time {}", sw.Elapsed);
            if (puzzle != null)
                return puzzle;
            
            var player = await _playerClient.GetPlayerAsync(playerId);
            _logger.LogDebug("After GetPlayerAsync elapsed time {}", sw.Elapsed);
            
            if(!player.IsNull)
                puzzle = await _puzzleService.GetNextRandomPuzzle(type, player);
                
            puzzle ??= await _puzzleService.GetNextRandomPuzzle(type, playerId);
            
            _logger.LogDebug("After GetNextRandomPuzzle elapsed time {}", sw.Elapsed);
            if (puzzle != null)
            {
                _logger.LogInformation("return Puzzle {} to Player with Id {}", puzzle.Name, playerId );
                if(!puzzle.CurrentOf.Contains(playerId))
                    _puzzleService.MakePuzzleCurrent(puzzle, playerId);
            }
            
            _logger.LogDebug("After MakePuzzleCurrent elapsed time {}", sw.Elapsed);
            
            return puzzle;
        }


        [Route("{id}")]
        [HttpGet]
        public ActionResult<Puzzle> GetNext(long id)
        {
            var puzzle = _puzzleService.GetNextForPlayer(id);
            if (puzzle == null)
                return NotFound();

            return puzzle;
        }

        [HttpPost]
        public ActionResult<Puzzle> Create(Puzzle puzzle)
        {
            _puzzleService.Create(puzzle);

            return puzzle;
        }

        [HttpPost]
        public IActionResult Update([FromHeader] string id, [FromBody] Puzzle inPuzzle)
        {
            var puzzle = _puzzleService.Get(id);

            if (puzzle == null)
                return NotFound();

            _puzzleService.Update(id, inPuzzle);

            return NoContent();
        }

        [HttpPost]
        public IActionResult Finish([FromBody] PuzzleFinishModel model)
        {
            if (_puzzleService.Finish(model))
            {
                var puzzle = _puzzleService.Get(model.PuzzleId);
                try
                {
                    _messageBusClient.SendPuzzleDoneMessage(new PuzzleDoneDto
                    {
                        PlayerId = model.PlayerId,
                        PuzzleId = model.PuzzleId,
                        PuzzleRating = puzzle.Rating,
                        IsResolved = model.IsResolved
                    });

                    return Ok();
                }
                catch (Exception e)
                {
                    Console.WriteLine($"--> Couldn't send message async: {e.Message}");
                }
            }

            return Unauthorized();
        }

        [HttpGet]
        public async Task<Puzzle> GetCsvPuzzle()
        {
            return await _puzzleService.GetCsvPuzzle();
        }
    }
}