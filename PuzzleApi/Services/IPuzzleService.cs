﻿using System.Collections.Generic;
using System.Threading.Tasks;
using GrpcService;
using PuzzleApi.Models;
using WebSocketModels.Enums;
using WebSocketModels.Models.Puzzles;
using PuzzleFinishModel = PuzzleApi.Models.PuzzleFinishModel;

namespace PuzzleApi.Services
{
    public interface IPuzzleService
    {
        List<Puzzle> Get();
        Puzzle Get(string id);
        Puzzle GetNextForPlayer(long playerId);
        Puzzle Get(PuzzleType type, long id);
        Puzzle Create(Puzzle puzzle);
        void Update(string id, Puzzle updPuzzle);
        void Remove(Puzzle remPuzzle);
        void Remove(string id);
        bool Finish(PuzzleFinishModel model);
        Task<Puzzle> GetNextRandomPuzzle(PuzzleType type, PlayerResponse playerResponse);
        Task<Puzzle> GetNextRandomPuzzle(PuzzleType type, long playerId);
        Task<Puzzle> GetCurrentPuzzle(PuzzleType type, long playerId);
        void MakePuzzleCurrent(Puzzle puzzle, long playerId);
        Task<Puzzle> GetCsvPuzzle();
    }
}