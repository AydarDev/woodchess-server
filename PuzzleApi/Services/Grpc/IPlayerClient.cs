﻿using System.Threading.Tasks;
using Grpc.Net.Client;
using GrpcService;

namespace PuzzleApi.Services.Grpc;

public interface IPlayerClient
{
    Task<PlayerResponse> GetPlayerAsync(long id);
}