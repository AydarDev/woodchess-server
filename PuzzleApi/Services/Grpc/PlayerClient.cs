﻿using System;
using System.Threading.Tasks;
using Grpc.Net.Client;
using GrpcService;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace PuzzleApi.Services.Grpc;

public class PlayerClient : IPlayerClient
{
    private readonly IConfiguration _conf;
    private readonly ILogger<PlayerClient> _logger;

    public PlayerClient(IConfiguration conf, ILogger<PlayerClient> logger)
    {
        _conf = conf;
        _logger = logger;
    }
    
    public async Task<PlayerResponse> GetPlayerAsync(long id)
    {
        try
        {
            var address = _conf["Grpc:WoodChessApi"]; 
            if (address == null) return new PlayerResponse { IsNull = true };
        
            using var channel = GrpcChannel.ForAddress(address);
            var client = new PlayerService.PlayerServiceClient(channel);
            var player = await client.GetPlayerAsync(new PlayerRequest {Id = id});
            _logger.LogDebug("Receive player with name {} data by GRPC", player);
            Console.WriteLine($"--> Receive player with name {player?.Name} data by GRPC");
            
            return player;
        }
        catch (Exception e)
        {
            _logger.LogError("{}", e.Message);
            _logger.LogDebug("{}", e.StackTrace);
            return new PlayerResponse { IsNull = true };
        }
    }
}