﻿using System;
using PuzzleApi.Models;
using MongoDB.Driver;
using MongoDB.Driver.Linq;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GameInstruments;
using GrpcService;
using WebSocketModels.Enums;
using WebSocketModels.Models.NullClasses;
using WebSocketModels.Models.Puzzles;
using PuzzleFinishModel = PuzzleApi.Models.PuzzleFinishModel;

namespace PuzzleApi.Services
{
    public class PuzzleService : IPuzzleService
    {
        private readonly IMongoCollection<Puzzle> _puzzles;
        private readonly PuzzleEngine _puzzleEngine;

        public PuzzleService(IPuzzleDbSettings settings)
        {
            var client = new MongoClient(settings.ConnectionString);
            var database = client.GetDatabase(settings.DatabaseName);

            _puzzles = database.GetCollection<Puzzle>(settings.PuzzlesCollectionName);
            _puzzleEngine = new PuzzleEngine();
        }

        public PuzzleService()
        {
        }

        public List<Puzzle> Get() =>
            _puzzles.Find(puzzle => true).ToList();

        public Puzzle Get(string id) =>
            _puzzles.Find(puzzle => puzzle.Id == id)
                .FirstOrDefault();

        public Puzzle Get(PuzzleType type, long id) =>
            _puzzles.Find(p => p.PuzzleType == type && !p.SolvedBy.Select(s => s.PlayerId).Contains(id))
                .FirstOrDefault();

        public Puzzle GetNextForPlayer(long playerId) =>
            _puzzles.Find(p => !p.SolvedBy.Select(s => s.PlayerId).Contains(playerId))
                .FirstOrDefault();

        public async Task<Puzzle> GetNextRandomPuzzle(PuzzleType type, PlayerResponse playerResponse)
        {
            var ratingRange = (Min: playerResponse.Rating - 100, Max: playerResponse.Rating + 100);
            var puzzles = await _puzzles.AsQueryable().Where(p =>
                p.PuzzleType == type
                && !p.SolvedBy.Select(s => s.PlayerId).Contains(playerResponse.Id)
                && p.Rating >= ratingRange.Min && p.Rating <= ratingRange.Max)
                .Take(10)
                .ToListAsync();

            return !puzzles.Any() ? null : puzzles[new Random().Next(puzzles.Count)];
        }

        public async Task<Puzzle> GetNextRandomPuzzle(PuzzleType type, long playerId)
        {
            var puzzles = await _puzzles.AsQueryable().Where(p =>
                    p.PuzzleType == type
                    && !p.SolvedBy.Select(s => s.PlayerId).Contains(playerId))
                .Take(10)
                .ToListAsync();

            return puzzles[new Random().Next(puzzles.Count)];
        }

        public async Task<Puzzle> GetCurrentPuzzle(PuzzleType type, long playerId) =>
            await _puzzles.AsQueryable()
                .FirstOrDefaultAsync(p => p.PuzzleType == type && p.CurrentOf.Contains(playerId));

        public async Task<Puzzle> GetCsvPuzzle() =>
            await _puzzles.AsQueryable().FirstOrDefaultAsync(p => p.DataType == DataType.Csv);

        public Puzzle Create(Puzzle puzzle)
        {
            _puzzles.InsertOne(puzzle);
            return puzzle;
        }

        public void Update(string id, Puzzle updPuzzle) =>
            _puzzles.ReplaceOne(p => p.Id == id, updPuzzle);

        public void Remove(Puzzle remPuzzle) =>
            _puzzles.DeleteOne(p => p.Id == remPuzzle.Id);

        public void Remove(string id) =>
            _puzzles.DeleteOne(p => p.Id == id);

        public bool Finish(PuzzleFinishModel model)
        {
            var puzzle = Get(model.PuzzleId);
            if (!puzzle.SolutionKey.Equals(model.SolutionKey) || !puzzle.CurrentOf.Contains(model.PlayerId))
                return false;
            
            puzzle.SolvedBy.Add(new SolvedBy { PlayerId = model.PlayerId, IsSolved = model.IsResolved });
            puzzle.CurrentOf.Remove(model.PlayerId);
            Update(puzzle.Id, puzzle);

            return true;
        }

        public void MakePuzzleCurrent(Puzzle puzzle, long playerId)
        {
            puzzle.CurrentOf.Add(playerId);
            Update(puzzle.Id, puzzle);
        }

        public void StorePuzzle(string data, string rating = "", List<string> tags = default)
        {
            //string name = _parser.GetNameFromStandardPuzzlePgn(pgn);
            Puzzle newPuzzle = new Puzzle
            {
                //Name = name,
                Data = data,
                DataType = DataType.Csv,
                PuzzleType = PuzzleType.Tactics,
                Rating = double.Parse(rating),
                HasPreMove = true,
                Tags = tags
            };

            var gameInfo = _puzzleEngine.LoadPuzzleFromCsv(data, true);
            newPuzzle.SolutionKey = SolutionKeyProvider.GetSolutionKey(gameInfo);

            Console.WriteLine($"Storing puzzle with pgn {newPuzzle.Data} with solution key {newPuzzle.SolutionKey}");

            _puzzles.InsertOne(newPuzzle);
        }
    }
}