﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using PuzzleApi.Models;
using PuzzleApi.Services;
using GameInstruments;
using Microsoft.Extensions.Configuration;

//using WebSocketModels.Enums;

namespace ConsoleApp1
{
    class Program
    {
        //private static readonly string filePath = "";
        private static PuzzleService _puzzleService;
        private static GameParser _parser;
        private static string _source;

        static void Main(string[] args)
        {
            try
            {
                _source = "lichess_db_puzzle.csv"; //args[0];
                _parser = new GameParser();
                _puzzleService = new PuzzleService(
                    new PuzzleDbSettings
                    (
                        "mongodb://puzzler:kestrel375@woodchess.art:32257/puzzleDb",
                        "puzzleDb",
                        "puzzles"
                    ));


                //StoreStringPuzzlesToDb();
                //StorePgnDb();
                StoreFromCsv();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }

        private static void StorePgnDb()
        {
            int counter = 0;

            StreamReader reader = new StreamReader(_source);
            //("/home/aydarbek/PgnExtractor/DataSource/");        //("../../../DataSource/small_lichess.pgn");

            _parser.Reader = reader;
            string pgn = "";
            do
            {
                try
                {
                    pgn = _parser.GetNextPgn();

                    if (!String.IsNullOrEmpty(pgn))
                    {
                        counter++;
                        _puzzleService.StorePuzzle(pgn);
                    }
                }
                catch (FormatException ex)
                {
                    Console.WriteLine($"{ex.Message}\n{ex.StackTrace}");
                }
                catch (Exception)
                {
                    Console.Write($"Error during puzzle save {pgn}");
                }
                finally
                {
                    Console.WriteLine($"Stored {counter} puzzles.");
                }
            } while (!string.IsNullOrEmpty(pgn));
        }


        private static void StoreStringPuzzlesToDb()
        {
            int counter = 0;

            using var reader = new StreamReader("../../../DataSource/tactics.txt");
            while (!reader.EndOfStream)
            {
                string nextLine;
                do
                {
                    nextLine = reader.ReadLine();
                } while (nextLine == string.Empty);

                string name = nextLine;
                string fen = reader.ReadLine();
                string line = reader.ReadLine();
                string pgn = BuildFenForPgn(fen, line);
                /*int key;
                try
                {
                    key = _parser.GetSolutionKey(pgn);

                }
                catch (FormatException e)
                {
                    Console.WriteLine(e.Message);
                    Console.WriteLine(e.StackTrace);
                    continue;
                }*/

                Console.WriteLine($"{++counter}. Storing puzzle {name}");
                /*Puzzle puzzle = new Puzzle
                {
                    Name = name,
                    Pgn = pgn,
                    Rating = 1600,
                    //PuzzleType = PuzzleType.Tactics,
                    SolutionKey = _parser.GetSolutionKey(pgn)
                };*/

                _puzzleService.StorePuzzle(pgn, name);
            }

            Console.WriteLine($"Saved {counter} puzzles");
        }


        //  r2qkb1r/pp2nppp/3p4/2pNN1B1/2BnP3/3P4/PPP2PPP/R2bK2R w KQkq - 1 0
        //  [FEN "1k6/8/3K4/8/8/7B/8/7Q w - - 0 1"]

        private static string BuildFenForPgn(string fen, string line)
        {
            StringBuilder resultPgn = new StringBuilder();
            resultPgn.Append("[FEN \"").Append(fen).Append("\"]").Append("\n\n").Append(line);

            return resultPgn.ToString();
        }

        private static void StoreFromCsv()
        {
            using var reader = new StreamReader(_source);
            var csvEntry = reader.ReadLine();
            StoreCsv(csvEntry);

            var counter = 0;
            while (!reader.EndOfStream)
            {
                csvEntry = reader.ReadLine();
                StoreCsv(csvEntry);
                Console.WriteLine($"Stored {++counter} puzzles");
            }
        }

        private static void StoreCsv(string csvEntry)
        {
            var csvElements = csvEntry.Split(',');
            var rating = csvElements[3].Trim(' ');
            var tags = csvElements[7].Split(' ').ToList();
            
            _puzzleService.StorePuzzle(csvEntry, rating, tags);
        }

        private static void UpdateSolutionKeyBulk()
        {
            var processed = new List<string>();
            IEnumerable<Puzzle> chunk;
            var count = 0;

            do
            {
                chunk = _puzzleService.Get().Where(p => p.SolutionKey == 0 && !processed.Contains(p.Id)).Take(1000);
                foreach (var puzzle in chunk)
                {
                    Console.WriteLine($"Processing puzzle {puzzle.Name}");
                    var key = _parser.GetSolutionKey(puzzle.Data, puzzle.HasPreMove);
                    processed.Add(puzzle.Id);
                    if (key != -1)
                    {
                        Console.WriteLine($"----Calculated key {key}");
                        puzzle.SolutionKey = key;
                        _puzzleService.Update(puzzle.Id, puzzle);
                        count++;
                        continue;
                    }

                    Console.WriteLine($"----Key empty, possibly no solution exists");
                }
            } while (chunk.Any());

            Console.WriteLine($"Processed {count} puzzles.");
        }
    }
}