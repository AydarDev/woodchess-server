using System;
using System.Text;
using ChessRules;
using Microsoft.Extensions.Primitives;
using WebSocketModels.Enums;
using WoodChessV1.Models;

namespace WoodChessV1.Tools;

public static class PgnBuilder
{
    public static void LoadInitialPgn(Game game)
    {
        var white = game[SideColor.White].Name;
        var black = game[SideColor.Black].Name;
        var builder = new StringBuilder();
        builder
            .Append($"[Event \"{game.RatingType} game on woodchess.art\"]\n")
            .Append($"[Date \"{DateTime.UtcNow.ToString("dd/MM/yyyy")}\"]\n")
            .Append($"[White \"{white}\"]\n")
            .Append($"[Black \"{black}\"]\n")
            .Append("[Result \"\"]\n\n");
        
        game.Pgn = builder.ToString();
    }

    public static void AddLastMove(Game game, Chess chess)
    {
        var builder = new StringBuilder(game.Pgn);
        if (game.CurrPly % 2 == 1)
            builder.Append($"{(game.CurrPly + 1) / 2}.");

        builder.Append(NotationTranslator.GetShortNotation(game.LastMove, chess)).Append(' ');
        
        game.Pgn = builder.ToString();
    }

    public static void SetResult(Game game)
    {
        var builder = new StringBuilder(game.Pgn);
        string result = GetGameResult(game.Result);
        builder.Replace("[Result \"\"]", $"[Result \"{result}\"]");
        builder.Append(result);

        game.Pgn = builder.ToString();
    }

    private static string GetGameResult(GameResult? gameResult)
    {
        if (gameResult == null) return string.Empty;
        return gameResult switch
        {
            GameResult.WhiteWin => "1-0",
            GameResult.BlackWin => "0-1",
            GameResult.Draw => "1/2-1/2",
            _ => string.Empty
        };
    }
}