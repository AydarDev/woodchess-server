﻿using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using ChessRules;
using WebSocketModels.Enums;
using WebSocketModels.Models.Events;
using WebSocketModels.Models.Ratings;
using WoodChessV1.Models;
using WoodChessV1.Models.Ratings;
using WoodChessV1.RatingCalculator;


namespace WoodChessV1.Tools
{
    public class RatingHelper : IRatingHelper
    {
        private readonly RatingCalculator.RatingCalculator _ratingCalculator = RatingCalculator.RatingCalculator.GetInstance();
        private readonly IMapper _mapper;

        public RatingHelper(IMapper mapper)
        {
            _mapper = mapper;
        }

        public void CalculatePlayerRatings(Game game)
        {
            var whiteRating = game?.GamePlayers?
                .FirstOrDefault(gp => gp.Color == SideColor.White)?.Player.Ratings
                .FirstOrDefault(r => r.RatingType == game.RatingType);
            
            var blackRating = game?.GamePlayers?
                .FirstOrDefault(gp => gp.Color == SideColor.Black)?.Player.Ratings
                .FirstOrDefault(r => r.RatingType == game.RatingType);

            if (whiteRating == null || blackRating == null) return;
            var inputRating = new InputRating(whiteRating.Value, blackRating.Value);

            var ratingDeviations = _ratingCalculator.GetRatingDeviation(inputRating, game.Result!.Value);

            whiteRating.Value += ratingDeviations[SideColor.White].Deviation;
            blackRating.Value += ratingDeviations[SideColor.Black].Deviation;


            game.GamePlayers.First(gp => gp.Color.Equals(SideColor.White)).RatingDeviation  = 
                _mapper.Map<RatingDeviation>(ratingDeviations[SideColor.White]);
            game.GamePlayers.First(gp => gp.Color.Equals(SideColor.Black)).RatingDeviation  = 
                _mapper.Map<RatingDeviation>(ratingDeviations[SideColor.Black]);
        }

        public Dictionary<SideColor, RatingDeviationDto> CalculatePuzzleDoneRatings(double playerRating, PuzzleDoneDto puzzleDone)
        {
            var result = puzzleDone.IsResolved
                ? GameResult.WhiteWin
                : GameResult.BlackWin;
            
            var ratingDeviation = _ratingCalculator.GetRatingDeviation(
                new InputRating(playerRating, puzzleDone.PuzzleRating), result);

            return ratingDeviation;
        }
    }
}