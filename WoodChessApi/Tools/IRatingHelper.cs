﻿using System.Collections.Generic;
using ChessRules;
using WebSocketModels.Models;
using WebSocketModels.Models.Events;
using WebSocketModels.Models.Ratings;
using WoodChessV1.Models;

namespace WoodChessV1.Tools
{
    public interface IRatingHelper
    {
        void CalculatePlayerRatings(Game game);
        Dictionary<SideColor, RatingDeviationDto> CalculatePuzzleDoneRatings(double playerRating, PuzzleDoneDto puzzleDone);
    }
}
