using System;
using System.Net;
using System.Runtime.CompilerServices;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Server.Kestrel.Core;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using WoodChessV1.Repository.Concrete;


[assembly: InternalsVisibleTo("WoodChessAPI.Tests")]
namespace WoodChessV1
{
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateHostBuilder(args).Build().Run();
            Console.WriteLine("--> WoodChess server started!");
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder
                    .UseKestrel()
                    .ConfigureKestrel(opt =>
                    {
                        opt.Listen(IPAddress.Any, 80, o => o.Protocols = HttpProtocols.Http1);
                        opt.Listen(IPAddress.Any, 5000, o => o.Protocols = HttpProtocols.Http2);
                    })
                    .UseStartup<Startup>();
                });
    }
}
