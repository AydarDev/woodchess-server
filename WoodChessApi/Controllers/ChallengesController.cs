using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using WoodChessV1.Misc;
using WoodChessV1.Services.ChallengeServices;

namespace WoodChessV1.Controllers;
public class ChallengesController : ControllerBase
{
    private readonly IChallengeService _challengeService;

    public ChallengesController(IChallengeService challengeService)
    {
        _challengeService = challengeService;
    }
    
    [HttpGet]
    public async Task<string> Get(long id)
    {
        var game = await _challengeService.Get(id);
        return JsonConvert.SerializeObject(game, JsonSettings.Settings);
    }
}