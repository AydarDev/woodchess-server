﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using System;
using Newtonsoft.Json;
using WoodChessV1.Models.UserManager;
using WoodChessV1.Services.UserServices;

namespace WoodChessV1.Controllers
{
    public class HomeController : Controller
    {
        private readonly IUserService _userService;

        public HomeController(IUserService userService)
        {
            _userService = userService;
        }

        [HttpPost]
        public async Task<ActionResult<string>> Login([FromBody] LoginViewModel model)
        {
            var ipAddress =  HttpContext.Connection.RemoteIpAddress;
            var result = await _userService.LoginUserAsync(model, ipAddress);
            string json = JsonConvert.SerializeObject(result);
            if (result.IsSuccess)
                return Ok(json);

            return BadRequest(json);
        }

        [HttpPost]
        public async Task<ActionResult<string>> Register([FromBody] RegisterViewModel model)
        {
            //TODO: Find real IP-address without proxies
            var ipAddress = HttpContext.Connection.RemoteIpAddress;
            var result = await _userService.RegisterUserAsync(model, ipAddress);
            string json = JsonConvert.SerializeObject(result);
            if (result.IsSuccess)
                return Ok(json);

            return BadRequest(json);
        }

        public ActionResult GetServerTime()
        {
            return Ok(DateTime.UtcNow);
        }
    }
}