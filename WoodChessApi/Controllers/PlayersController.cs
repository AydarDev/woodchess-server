﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using WoodChessV1.Misc;
using WoodChessV1.Services.PlayerServices;

namespace WoodChessV1.Controllers;
public class PlayersController : Controller
{
    private readonly IPlayerService _playerService;
    
    public PlayersController(IPlayerService playerService)
    {
        _playerService = playerService;
    }
       
    public async Task<string> GetAll()
    {
        var players = await _playerService.GetAll();
            
        return JsonConvert.SerializeObject(players, JsonSettings.Settings);
    }
    
    public async Task<string> GetOnline()
    {
        var players = await _playerService.GetOnline();
            
        return JsonConvert.SerializeObject(players, JsonSettings.Settings);
    }

    public async Task<string> Get(long id)
    {
        var player = await _playerService.Get(id);

        if (player == null || player.IsNull)
            return null;
            
        return JsonConvert.SerializeObject(player, JsonSettings.Settings);
    }
}