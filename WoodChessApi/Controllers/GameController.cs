using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using WoodChessV1.Services.GameServices;
using static WoodChessV1.Misc.JsonSettings;

namespace WoodChessV1.Controllers;
public class GamesController : Controller
{
    private readonly IGameService _gameService;

    public GamesController(IGameService gameService)
    {
        _gameService = gameService;
    }
    
    public async Task<string> Get(Guid id)
    {
        var game = await _gameService.Get(id);
        return JsonConvert.SerializeObject(game, Settings);
    }

    public async Task<string> GetCurrent(long id)
    {
        var game = await _gameService.GetCurrentGame(id);
        return JsonConvert.SerializeObject(game, Settings);
    }

    public async Task<string> GetGames([FromQuery] GameFilter filterBy)
    {
        var games = await _gameService.GetGames(filterBy);
        return JsonConvert.SerializeObject(games, Settings);
    }
}