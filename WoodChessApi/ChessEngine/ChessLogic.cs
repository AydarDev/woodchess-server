﻿using System;
using ChessRules;
using WebSocketModels.Enums;
using WoodChessV1.Models;
using WoodChessV1.Exceptions;
using WoodChessV1.Tools;

namespace WoodChessV1.ChessEngine
{
    public class ChessLogic : IChessLogic
    {
        private readonly IChessRepository _chessRepo;
        private const string StartFen = "rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1";

        public ChessLogic()
        {
            _chessRepo = ChessRepository.GetInstance();
        }

        public void StartGame(Guid gameId, string fen = StartFen)
        {
            _chessRepo.Create(gameId, new Chess(fen));
        }

        public void FinishGame(Guid gameId)
        {
            _chessRepo.Remove(gameId);
        }

        internal Chess GetChess(Game game)
        {
            return _chessRepo.Get(game);
        }

        private void UpdateChess(Guid id, Chess chess)
        {
            _chessRepo.Update(id, chess);
        }

        public Game MakeMove(Game game, string move)
        {
            var chess = GetChess(game);

            ValidateMove(move, chess);
            game.LastMove = move;
            PgnBuilder.AddLastMove(game, chess);
            chess = chess.Move(move);

            game.Fen = chess.Fen;
            game.LastMove = move;
            game.CurrPly++;

            CheckGameResult(game, chess);
            UpdateChess(game.Id, chess);

            return game;
        }

        private void ValidateMove(string move, Chess chess)
        {
            if (!chess.IsValidMove(move))
                throw new ChessException("Wrong move.");
        }

        private static void CheckGameResult(Game game, Chess chess)
        {
            if (chess.IsCheckmate)
            {
                game.DetailResult = GameDetailResult.WinByCheckmate;
                game.Status = GameStatus.Done;
            }

            else if (chess.IsStalemate)
            {
                game.Result = GameResult.Draw;
                game.DetailResult = GameDetailResult.DrawByStaleMate;
                game.Status = GameStatus.Done;
            }
        }

        private static GameResult GetWinner(Chess chess)
        {
            return chess.MoveColor == SideColor.White ? GameResult.BlackWin : GameResult.WhiteWin;
        }

    }
}
