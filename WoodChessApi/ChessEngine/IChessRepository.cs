﻿using System;
using ChessRules;
using WoodChessV1.Models;
using WoodChessV1.Repository.Abstract;

namespace WoodChessV1.ChessEngine;
public interface IChessRepository : IDictionaryBasic<Chess, Guid>, IDictionaryUpdate<Chess>
{
    Chess Get(Game game);
}