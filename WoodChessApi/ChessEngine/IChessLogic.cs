﻿using System;
using WoodChessV1.Models;

namespace WoodChessV1.ChessEngine
{
    public interface IChessLogic
    {
        void StartGame(Guid gameId, string fen = "rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1");
        Game MakeMove(Game game, string move);
        void FinishGame(Guid gameId);
    }
}
