﻿using System;
using ChessRules;
using System.Collections.Concurrent;
using WoodChessV1.Models;

namespace WoodChessV1.ChessEngine
{
    public class ChessRepository : IChessRepository
    {
        private static ChessRepository _chessRepository;

        private readonly ConcurrentDictionary<Guid, Chess> _chessBoards = new();

        private ChessRepository() { }

        public static ChessRepository GetInstance()
        {
            return _chessRepository ??= new ChessRepository();
        }

        public void Create(Guid id, Chess entity)
        {
            _chessBoards.TryAdd(id, entity);
        }

        public Chess Get(Game game)
        {
            return _chessBoards.GetOrAdd(game.Id, new Chess(game.Fen));
        }

        public bool Remove(Guid id)
        {
            return _chessBoards.TryRemove(id, out _);
        }

        public void Update(Guid id, Chess newChess)
        {
            if (_chessBoards.TryGetValue(id, out _))
                _chessBoards[id] = newChess;
        }
    }
}
