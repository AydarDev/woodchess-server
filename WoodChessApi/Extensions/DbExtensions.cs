using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace WoodChessV1.Extensions;

public static class DbExtensions
{
    public static IQueryable<T> Where<T>(this IQueryable<T> queryable, IEnumerable<Expression<Func<T, bool>>> filters) where T: class
    {
        return filters.Aggregate(queryable, (current, filter) => current.Where(filter));
    }
}