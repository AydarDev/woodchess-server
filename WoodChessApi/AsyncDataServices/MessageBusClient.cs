﻿using System;
using System.Text;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using RabbitMQ.Client;
using WebSocketModels.Models.Events;

namespace WoodChessV1.AsyncDataServices
{
    public class MessageBusClient : IMessageBusClient
    {
        private readonly IConnection _connection;
        private readonly IModel _channel;
        private readonly string _exchange;

        public MessageBusClient(IConfiguration config)
        {
            var connectionFactory = new ConnectionFactory()
            {
                HostName = config["RabbitMqConnection:Host"],
                Port = Int32.Parse(config["RabbitMqConnection:Port"])
            };
            _exchange = config["RabbitMqConnection:ExchangeTx"];

            try
            {
                _connection = connectionFactory.CreateConnection();
                _channel = _connection.CreateModel();
                _channel.ExchangeDeclare(exchange: _exchange, type: ExchangeType.Fanout);

                _connection.ConnectionShutdown += RabbitMqConnectionShutdown;
                Console.WriteLine("--> Connected to MessageBus");
            }
            catch (Exception e)
            {
                Console.WriteLine($"--> Could not connect to the Message bus: {e.Message}");
            }
        }

        public void SendRatingChangedMessage(RatingChangedDto ratingChanged)
        {
            if (_connection.IsOpen)
            {
                Console.WriteLine("--> RabbitMQ connection is open. Sending message");
                var ratingChangedMessage = JsonConvert.SerializeObject(ratingChanged);
                SendMessage(ratingChangedMessage);
            }
            else
                Console.WriteLine("--> RabbitMQ connection if closed. Not sending.");
        }


        public void Dispose()
        {
            Console.WriteLine("MessageBus Disposed.");
            if (_channel.IsOpen)
            {
                _channel.Close();
                _connection.Close();
            }
        }

        private void SendMessage(string message)
        {
            var body = Encoding.UTF8.GetBytes(message);
            _channel.BasicPublish(exchange: _exchange,
                routingKey: "",
                basicProperties: null,
                body: body);

            Console.WriteLine($"--> Send message done {message}");
        }

        private void RabbitMqConnectionShutdown(object sender, ShutdownEventArgs e)
        {
            Console.WriteLine("--> RabbitMQ Connection shutdown");
        }
    }
}