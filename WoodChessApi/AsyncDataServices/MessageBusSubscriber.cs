﻿using System;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using WoodChessV1.EventProcessing;

namespace WoodChessV1.AsyncDataServices
{
    public class MessageBusSubscriber : BackgroundService
    {
        private readonly IEventProcessor _eventProcessor;
        private readonly IConfiguration _config;
        private IConnection _connection;
        private IModel _channel;
        private string _queueName;
        private string _exchange =>  _config["RabbitMqConnection:ExchangeRx"];

        public MessageBusSubscriber(IConfiguration config, IEventProcessor eventProcessor)
        {
            _config = config;
            _eventProcessor = eventProcessor;
            InitRabbitMq();
        }
        
        private void InitRabbitMq()
        {
            try
            {
                var factory = new ConnectionFactory()
                {
                    HostName = _config["RabbitMqConnection:Host"],
                    Port = Int32.Parse(_config["RabbitMqConnection:Port"])
                };
                _connection = factory.CreateConnection();
                _channel = _connection.CreateModel();
                _channel.ExchangeDeclare(exchange: _exchange, type: ExchangeType.Fanout);
                _queueName = _channel.QueueDeclare("Name").QueueName;
                _channel.QueueBind(exchange: _exchange, queue: _queueName, routingKey: "");
                Console.WriteLine("--> Listening on the MessageBus");

                _connection.ConnectionShutdown += RabbitMqConnectionShutdown;
            }
            catch (Exception e)
            {
                Console.WriteLine($"--> Error during establish RabbitMQ connection: {e.Message}");
            }
            
        }
        
        
        protected override Task ExecuteAsync(CancellationToken stoppingToken)
        {
            try
            {
                stoppingToken.ThrowIfCancellationRequested();

                var consumer = new EventingBasicConsumer(_channel);
                consumer.Received += async (ModuleHandle, ea) =>
                {
                    var body = ea.Body;
                    var message = Encoding.UTF8.GetString(body.ToArray());

                    Console.WriteLine($"--> Event received with message {message}");
                    await _eventProcessor.ProcessEvent(message);
                };
                _channel.BasicConsume(queue: _queueName, autoAck: true, consumer: consumer);
            }
            catch (Exception e)
            {
                Console.WriteLine($"--> Could not subscribe to the Message bus: {e.Message}");
            }

            return Task.CompletedTask;
        }
        
        
        public override void Dispose()
        {
            Console.WriteLine("MessageBus Disposed.");
            if (_channel is { IsOpen: true })
            {
                _channel.Close();
                _connection.Close();
            }
        }

        private void RabbitMqConnectionShutdown(object sender, ShutdownEventArgs e)
        {
            Console.WriteLine("--> RabbitMQ Connection shutdown");
        }
    }
}