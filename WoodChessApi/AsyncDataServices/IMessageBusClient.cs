﻿using WebSocketModels.Models.Events;

namespace WoodChessV1.AsyncDataServices
{
    public interface IMessageBusClient
    {
        void SendRatingChangedMessage(RatingChangedDto puzzleSolvedDto);
    }
}