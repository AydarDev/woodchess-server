﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using WoodChessV1.Models;

namespace WoodChessV1.Repository.Abstract
{
    public interface IGameRepository : IRepository
    {
        Task<Game> Get(Guid id);
        Task<Game> GetCurrentGame(long playerId);
        Task<IEnumerable<Game>> GetGames(IEnumerable<Expression<Func<Game, bool>>> filters);
        Task Add(Game game);
    }
}
