﻿using System;
using System.Threading.Tasks;

namespace WoodChessV1.Repository.Abstract
{
    public interface IRepository : IDisposable
    {
        Task Add<T>(T entity);
        void Update<T>(T entity);
        Task Save();
    }
}