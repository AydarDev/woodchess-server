﻿namespace WoodChessV1.Repository.Abstract;

public interface IDictionaryBasic<TEntity, TKey> where TEntity : class
{
    void Create(TKey id, TEntity entity);
    bool Remove(TKey id);
}