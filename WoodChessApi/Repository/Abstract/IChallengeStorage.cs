﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using ChessRules;
using WebSocketModels.Models;
using WoodChessV1.Models;

namespace WoodChessV1.Repository.Abstract
{
    public interface IChallengeStorage : IRedisRepository<Guid, Challenge>
    {
        Task<Challenge> CreateChallenge(Player fromPlayer, Player toPlayer, TimeControl timeControl,
            SideColor sideColor);
        Task<List<Challenge>> GetPlayerChallenges(long playerId);
        Task<List<Challenge>> GetPrivateChallenges(long playerId);
        Task<List<Challenge>> GetPublicChallenges();
        Task RemoveCurrentChallenges(GameDto game); 
    }
}