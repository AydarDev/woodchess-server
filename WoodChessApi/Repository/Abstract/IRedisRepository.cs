using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace WoodChessV1.Repository.Abstract;

public interface IRedisRepository<TKey, TEntity> where TEntity : class
{
    Task Set(TKey id, TEntity entity);
    Task<TEntity> Get(TKey id);
    Task<IEnumerable<TEntity>> GetAll();
    Task<IEnumerable<TEntity>> Get(Func<TEntity, bool> predicate);
    Task Remove(TKey id);
    Task<bool> IsKeyExists(TKey id);
}