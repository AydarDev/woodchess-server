﻿using System.Collections.Generic;
using System.Threading.Tasks;
using WoodChessV1.Models;

namespace WoodChessV1.Repository.Abstract
{
    public interface IPlayerRepository : IRepository
    {
        Task<Player> Get(long id);
        Task<Player> Get(string name);
        Task<List<Player>> GetAll();
    }
}
