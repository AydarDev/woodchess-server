﻿using System;

namespace WoodChessV1.Repository.Abstract;
public interface IDictionaryUpdate<TEntity> where TEntity : class
{
    void Update(Guid id, TEntity entity);
}