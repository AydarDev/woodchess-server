﻿using Microsoft.EntityFrameworkCore;
using WebSocketModels.Models;
using WoodChessV1.Models;

namespace WoodChessV1.Repository.Contexts;
public class ChessContext : DbContext
{
    public virtual DbSet<Player> Players { get; set; }
    public virtual DbSet<Game> Games { get; set; }

    public ChessContext()
    {
    }

    public ChessContext(DbContextOptions<ChessContext> options) : base(options)
    {
    }

    protected override void OnModelCreating(ModelBuilder builder)
    {
        base.OnModelCreating(builder);

        builder.Entity<Player>().ToTable("players");
        builder.Entity<Player>().Property(p => p.Id).HasColumnName("id");
        builder.Entity<Player>().Property(p => p.Name).HasColumnName("name")
            .HasMaxLength(50)
            .IsRequired();
        builder.Entity<Player>().Property(p => p.RegisterTime).HasColumnName("register_time");
        builder.Entity<Player>().Property(p => p.LastLoginTime).HasColumnName("last_login_time");
        builder.Entity<Player>().Property(p => p.IpAddress).HasColumnName("ip_address");
        builder.Entity<Player>().Ignore(p => p.IsOnline);
        builder.Entity<Player>().Ignore(p => p.IsNull);
        builder.Entity<Player>().HasKey(p => p.Id);
        builder.Entity<Player>().HasIndex(i => i.Name).IsUnique(false);

        builder.Entity<Player>().OwnsMany(p => p.Ratings,
            r =>
            {
                r.ToTable("ratings");
                r.WithOwner().HasForeignKey(p => p.PlayerId);
                r.Property(p => p.PlayerId).HasColumnName("player_id");
                r.Property(p => p.RatingType).HasColumnName("rating_type").IsRequired();
                r.Property(p => p.Value).HasColumnName("value").IsRequired();
                r.Property(p => p.IsGame).HasColumnName("is_game").IsRequired();
                r.HasKey(p => new { p.PlayerId, p.RatingType });
            });

        builder.Entity<Game>().ToTable("games");
        builder.Entity<Game>().Property(g => g.Id).HasColumnName("id").ValueGeneratedNever();
        builder.Entity<Game>().Property(g => g.Fen).HasColumnName("fen")
            .HasMaxLength(100)
            .IsRequired();
        builder.Entity<Game>().Property(g => g.Pgn).HasColumnName("pgn").IsRequired();
        builder.Entity<Game>().Property(g => g.Status).HasColumnName("status").IsRequired();
        builder.Entity<Game>().Property(g => g.CurrPly).HasColumnName("curr_ply");
        builder.Entity<Game>().Property(g => g.LastMove).HasColumnName("last_move");
        builder.Entity<Game>().Property(g => g.StartedAt).HasColumnName("started_at");
        builder.Entity<Game>().Property(g => g.Result).HasColumnName("result");
        builder.Entity<Game>().Property(g => g.DetailResult).HasColumnName("detail_result");
        builder.Entity<Game>().Property(g => g.TextResult).HasColumnName("text_result");
        builder.Entity<Game>().Property(g => g.TimeControlId).HasColumnName("time_control_id");
        builder.Entity<Game>().Ignore(g => g.IsNull);

        builder.Entity<Game>().OwnsOne(g => g.TimeInfo, ti =>
        {
            ti.Property(g => g.WhiteTime).HasColumnName("white_time");
            ti.Property(g => g.BlackTime).HasColumnName("black_time");
            ti.Property(g => g.TimeStamp).HasColumnName("time_stamp");    
        });

        builder.Entity<Game>().OwnsOne(g => g.DrawInfo, dr =>
        {
            dr.WithOwner().HasForeignKey(p => p.GameId);
            dr.Ignore(d => d.GameId);
            dr.Property(d => d.FromPlayerId).HasColumnName("from_player_id");
            dr.Property(d => d.DrawFlag).HasColumnName("draw_flag");
            dr.Property(d => d.BlockCounter).HasColumnName("block_counter");
            dr.Ignore(d => d.IsNull);
        });

        builder.Entity<GamePlayer>().ToTable("game_players");
        builder.Entity<GamePlayer>().Property(gp => gp.Id).HasColumnName("id");
        builder.Entity<GamePlayer>().Property(gp => gp.GameId).HasColumnName("game_id");
        builder.Entity<GamePlayer>().Property(gp => gp.PlayerId).HasColumnName("player_id");
        builder.Entity<GamePlayer>().Property(gp => gp.Color).HasColumnName("color");
        builder.Entity<GamePlayer>().HasKey(gp =>  gp.Id);
        builder.Entity<GamePlayer>().HasIndex(gp => new { gp.GameId, gp.PlayerId }).IsUnique();
        builder.Entity<GamePlayer>().OwnsOne(gp => gp.RatingDeviation, rd =>
        {
            rd.WithOwner().HasForeignKey("player_id");
            rd.Property(r => r.Deviation).HasColumnName("rating_deviation");
            rd.Property(r => r.NewRating).HasColumnName("new_rating");
            rd.Ignore(r => r.IsNull);
        });
            
        builder.Entity<Game>()
            .HasMany(g => g.Players)
            .WithMany(p => p.Games)
            .UsingEntity<GamePlayer>(gamePlayer =>
                {
                    gamePlayer.HasOne(gp => gp.Game)
                        .WithMany(g => g.GamePlayers)
                        .HasForeignKey(g => g.GameId)
                        .OnDelete(DeleteBehavior.Cascade);
                    gamePlayer
                        .HasOne(gp => gp.Player)
                        .WithMany(p => p.GamePlayers)
                        .HasForeignKey(p => p.PlayerId)
                        .OnDelete(DeleteBehavior.Cascade);
                }
            );
        builder.Entity<Game>().HasOne(g => g.TimeControl).WithOne()
            .HasForeignKey<Game>(g => g.TimeControlId)
            .OnDelete(DeleteBehavior.SetNull);
        
        builder.Entity<TimeControl>().ToTable("time_controls");
        builder.Entity<TimeControl>().Property(tc => tc.Id).HasColumnName("id");
        builder.Entity<TimeControl>().Property(t => t.RatingType).HasColumnName("rating_type");
        builder.Entity<TimeControl>().Property(t => t.TimeRange).HasColumnName("time_range").IsRequired();
        builder.Entity<TimeControl>().Property(t => t.TimeIncrement).HasColumnName("increment").IsRequired()
            .HasDefaultValue(0);
        builder.Entity<TimeControl>().Property(t => t.ControlType).HasColumnName("control_type").IsRequired()
            .HasDefaultValue(TimeControlType.TimeOnGame);
        builder.Entity<TimeControl>().Ignore(t => t.IsNull);
    }
}