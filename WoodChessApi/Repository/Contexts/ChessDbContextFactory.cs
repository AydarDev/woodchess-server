﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;

namespace WoodChessV1.Repository.Contexts;
public class ChessDbContextFactory : IDesignTimeDbContextFactory<ChessContext>
{
    public ChessContext CreateDbContext(string[] args)
    {
        IConfigurationRoot configuration = new ConfigurationBuilder()
            .SetBasePath(AppDomain.CurrentDomain.BaseDirectory)
            .AddJsonFile("appsettings.Development.json")
            .Build();

        var optionsBuilder = new DbContextOptionsBuilder<ChessContext>().UseNpgsql(configuration.GetConnectionString("ChessDb"));
        return new ChessContext(optionsBuilder.Options);
    }
}