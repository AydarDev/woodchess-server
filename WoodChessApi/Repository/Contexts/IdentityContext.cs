﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using WoodChessV1.Models;

namespace WoodChessV1.Repository.Contexts;
public class IdentityContext : IdentityDbContext
{
    public DbSet<Player> Players;
        
    public IdentityContext(DbContextOptions options) : base(options)
    {
    }
        
    protected override void OnModelCreating(ModelBuilder modelbuilder)
    {
        base.OnModelCreating(modelbuilder);
    }
}