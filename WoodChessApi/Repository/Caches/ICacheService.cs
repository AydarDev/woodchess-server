using System.Threading.Tasks;

namespace WoodChessV1.Repository.Caches;

public interface ICacheService<TKey, TEntity>
{
    Task<TEntity> Get(TKey key);
    Task Set(TKey key, TEntity value);
    Task Remove(TKey key);
    Task<bool> IsExists(TKey key);
}