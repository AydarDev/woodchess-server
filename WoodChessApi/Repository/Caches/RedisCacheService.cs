using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using StackExchange.Redis;
using static WoodChessV1.Misc.JsonSettings;

namespace WoodChessV1.Repository.Caches;

public class RedisCacheService<TKey, TEntity> : ICacheService<TKey, TEntity>
{
    private readonly IConnectionMultiplexer _connectionMultiplexer;
    private readonly CacheSettings _settings;
    private IDatabase Db => _connectionMultiplexer.GetDatabase();

    public RedisCacheService(IConnectionMultiplexer connectionMultiplexer, IOptions<CacheSettings> settings)
    {
        _connectionMultiplexer = connectionMultiplexer;
        _settings = settings.Value;
    }

    public async Task<TEntity> Get(TKey key)
    {
        var textKey = GetStringKey(key);
        var value = await Db.StringGetAsync(textKey);
        return !string.IsNullOrEmpty(value) ? JsonConvert.DeserializeObject<TEntity>(value, Settings) : default;
    }

    public async Task Set(TKey key, TEntity value)
    {
        var json = JsonConvert.SerializeObject(value, Settings);
        var textKey = GetStringKey(key);
        await Db.StringSetAsync(textKey, json, _settings.ExpirationPeriod);
    }

    public async Task Remove(TKey key)
    {
        var textKey = GetStringKey(key);
        if (Db.KeyExists(textKey))
            await Db.KeyDeleteAsync(textKey);
    }

    public async Task<bool> IsExists(TKey key) => await Db.KeyExistsAsync(GetStringKey(key));

    private static string GetStringKey(TKey key) => $"{typeof(TEntity).Name}:{key.ToString()}";
}