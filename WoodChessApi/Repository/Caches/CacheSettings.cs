using System;

namespace WoodChessV1.Repository.Caches;

public class CacheSettings
{
    public TimeSpan ExpirationPeriod { get; set; }
}