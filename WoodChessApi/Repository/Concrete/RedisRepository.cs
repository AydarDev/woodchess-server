using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;
using StackExchange.Redis;
using WoodChessV1.Repository.Abstract;

namespace WoodChessV1.Repository.Concrete;
public class RedisRepository<TKey, TEntity>: IRedisRepository<TKey, TEntity> where TEntity : class
{
    private readonly IConnectionMultiplexer _connectionMultiplexer;
    private IDatabase Db => _connectionMultiplexer.GetDatabase();
    private static string HashName => $"{typeof(TEntity).Name}s";

    public RedisRepository(IConnectionMultiplexer connectionMultiplexer)
    {
        _connectionMultiplexer = connectionMultiplexer;
    }

    public async Task Set(TKey id, TEntity entity)
    {
        if (entity == null)
            throw new ArgumentException(null, HashName);

        var json = JsonConvert.SerializeObject(entity);
        await Db.HashSetAsync(HashName, new HashEntry[]
            {new(id.ToString(), json )});
    }

    public async Task<TEntity> Get(TKey id)
    {
        var json = await Db.HashGetAsync(HashName, id.ToString());
        if(!string.IsNullOrEmpty(json))
            return JsonConvert.DeserializeObject<TEntity>(json);

        return null;
    }

    public async Task<IEnumerable<TEntity>> GetAll()
    {
        var all = await Db.HashGetAllAsync(HashName);
        return all.Select(x => JsonConvert.DeserializeObject<TEntity>(x.Value));
    }

    public async Task<IEnumerable<TEntity>> Get(Func<TEntity, bool> predicate)
    {
        return (await GetAll()).Where(predicate);
    }
    
    public async Task Remove(TKey id)
    {
        await Db.HashDeleteAsync(HashName, id.ToString());
    }
    public async Task<bool> IsKeyExists(TKey id)
    {
        return await Db.HashExistsAsync(HashName, id.ToString());
    }
}