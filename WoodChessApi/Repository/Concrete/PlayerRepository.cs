﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using WoodChessV1.Models;
using WoodChessV1.Repository.Abstract;
using WoodChessV1.Repository.Contexts;

namespace WoodChessV1.Repository.Concrete
{
    public class PlayerRepository : BaseRepository, IPlayerRepository
    {
        public PlayerRepository(ChessContext context): base(context) { }

        public async Task<List<Player>> GetAll()
        {
            return await Context.Players.AsNoTracking()
                .AsSplitQuery()
                .Include(p => p.Ratings)
                .OrderByDescending(p => p.Ratings.Max(r => r.Value))
                .ToListAsync();
        }

        public async Task<Player> Get(long id)
        {
            return await Context.Players.AsNoTracking()
                       .AsSplitQuery()
                       .Include(p => p.Ratings)
                       .FirstOrDefaultAsync(p => p.Id == id)
                   ?? NullPlayer.GetInstance();
        }

        public async Task<Player> Get(string name)
        {
            return await Context.Players.AsNoTracking().AsSplitQuery()
                       .Include(p => p.Ratings)
                       .FirstOrDefaultAsync(p => p.Name.Equals(name))
                   ?? NullPlayer.GetInstance();
        }
    }
}