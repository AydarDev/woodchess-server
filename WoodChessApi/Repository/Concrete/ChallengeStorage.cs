using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ChessRules;
using StackExchange.Redis;
using WebSocketModels.Models;
using WoodChessV1.Exceptions;
using WoodChessV1.Models;
using WoodChessV1.Models.Factories;
using WoodChessV1.Repository.Abstract;

namespace WoodChessV1.Repository.Concrete;

public class ChallengeStorage : RedisRepository<Guid, Challenge>, IChallengeStorage
{
    public ChallengeStorage(IConnectionMultiplexer connectionMultiplexer) : base(connectionMultiplexer)
    {
    }
    
    public async Task<Challenge> CreateChallenge(Player fromPlayer, Player toPlayer, TimeControl timeControl,
        SideColor sideColor)
    {
        var challenge = EntitiesFactory.GetChallenge(fromPlayer, toPlayer, timeControl, sideColor);
        await CheckForExistingChallenges(fromPlayer.Id, challenge);
        await Set(challenge.Id, challenge);

        return challenge;
    }
    private async Task CheckForExistingChallenges(long playerId, Challenge challenge)
    {
        foreach (var currentChallenge in await GetPlayerChallenges(playerId))
            if (currentChallenge.Equals(challenge))
                throw new ChessException("Challenge with these parameters already exists");
    }

    public async Task<List<Challenge>> GetPlayerChallenges(long playerId) =>
        (await Get(ch =>
                ch.FromPlayer.Id == playerId ||
                (!ch.ToPlayer.IsNull && ch.ToPlayer?.Id == playerId)))
            .ToList();

    public async Task<List<Challenge>> GetPrivateChallenges(long playerId) =>
        (await Get(ch =>
            ch.ToPlayer != null && (ch.ToPlayer.Id == playerId || ch.FromPlayer.Id == playerId))).ToList();

    public async Task<List<Challenge>> GetPublicChallenges() =>
        (await Get(ch => ch.ToPlayer.IsNull)).ToList();

    public async Task RemoveCurrentChallenges(GameDto game)
    {
        await RemoveCurrentChallenges(game.GamePlayers.Select(p => p.PlayerId));
    }

    private async Task RemoveCurrentChallenges(IEnumerable<long> playerIds)
    {
        foreach (var playerId in playerIds)
        {
            var playerChallenges = await GetPlayerChallenges(playerId);
            foreach (var challenge in playerChallenges) await Remove(challenge.Id);
        }
    }
}