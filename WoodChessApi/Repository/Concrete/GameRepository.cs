﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using WebSocketModels.Enums;
using WoodChessV1.Models;
using WoodChessV1.Repository.Abstract;
using WoodChessV1.Extensions;
using WoodChessV1.Repository.Contexts;

namespace WoodChessV1.Repository.Concrete
{
    public class GameRepository : BaseRepository, IGameRepository
    {
        public GameRepository(ChessContext context) : base(context) { }
        public async Task<IEnumerable<Game>> GetGames(IEnumerable<Expression<Func<Game, bool>>> filters)
        {
            return await Context.Games
                .AsSplitQuery()
                .Include(g => g.Players)
                .Where(filters)
                .OrderByDescending(g => g.TimeInfo.TimeStamp)
                .ToListAsync();
        }

        public List<Game> GetCurrentGames()
        {
            return Context.Games
                .AsSplitQuery()
                .Include(g => g.GamePlayers)
                .ThenInclude(gp => gp.Player)
                .ThenInclude(g => g.Ratings)
                .Where(g => g.Status == GameStatus.Play).OrderByDescending(g => g.TimeInfo.TimeStamp).ToList();
        }

        public async Task<Game> Get(Guid id)
        {
            return await Context.Games
                       .AsSplitQuery()
                       .Include(g => g.GamePlayers)
                       .ThenInclude(g => g.Player)
                       .FirstOrDefaultAsync(p => p.Id.Equals(id))
                   ?? NullGame.GetInstance();
        }

        public async Task<Game> GetCurrentGame(long playerId)
        {
            return await Context.Games
                       .AsSplitQuery()
                       .Include(g => g.GamePlayers)
                       .ThenInclude(gp => gp.Player)
                       .ThenInclude(g => g.Ratings)
                       .FirstOrDefaultAsync(g => g.Players.Any(p => p.Id == playerId) && g.Status == GameStatus.Play)
                   ?? NullGame.GetInstance();
        }

        public async Task Add(Game game)
        {
            await Context.AddAsync(game);
            game.GamePlayers.ForEach(gamePlayer =>
            {
                Context.Entry(gamePlayer.Player).State = EntityState.Unchanged;
                gamePlayer.Player.Ratings.ForEach(rating => Context.Entry(rating).State = EntityState.Unchanged);
            });
        }
    }
}