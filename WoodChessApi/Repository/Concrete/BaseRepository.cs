using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using WoodChessV1.Repository.Abstract;
using WoodChessV1.Repository.Contexts;

namespace WoodChessV1.Repository.Concrete;

public abstract class BaseRepository : IRepository
{
    protected readonly ChessContext Context;

    protected BaseRepository(ChessContext context)
    {
        Context = context;
    }

    public virtual async Task Add<T>(T entity)
    {
        await Context.AddAsync(entity);
    }

    public void Update<T>(T entity)
    {
        Context.Entry(entity).State = EntityState.Modified;
    }

    public async Task Save()
    {
        await Context.SaveChangesAsync();
    }

    public void Dispose()
    {
        Context?.DisposeAsync();
    }
}