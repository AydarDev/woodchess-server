﻿using System;
using System.Threading.Tasks;
using ChessRules;
using Newtonsoft.Json;
using WebSocketModels.Enums;
using WebSocketModels.Models.Events;
using WebSocketModels.WsMessages.WsResponses;
using WoodChessV1.AsyncDataServices;
using WoodChessV1.Services;
using WoodChessV1.Services.PlayerServices;
using WoodChessV1.Tools;
using WoodChessV1.WebSocketClasses.WebSocketManager;

namespace WoodChessV1.EventProcessing;
public class EventProcessor : IEventProcessor
{
    private readonly IPlayerService _playerService;
    private readonly IWebSocketHandler _wsHandler;
    private readonly IRatingHelper _ratingHelper;
    private readonly IMessageBusClient _messageBusClient;

    public EventProcessor(IPlayerService playerService, IWebSocketHandler wsHandler,
        IRatingHelper ratingHelper, IMessageBusClient messageBusClient)
    {
        _playerService = playerService;
        _wsHandler = wsHandler;
        _ratingHelper = ratingHelper;
        _messageBusClient = messageBusClient;
    }
    public async Task ProcessEvent(string message)
    {
        var eventType = DetermineEventType(message);
        switch (eventType)
        {
            case(EventType.PuzzleDone):
                await MakeRatingsUpdate(message);
                break;
            case(EventType.RatingChanged):
                break;
        }
    }

    private async Task MakeRatingsUpdate(string message)
    {
        var receivedEvent = JsonConvert.DeserializeObject<PuzzleDoneDto>(message);
        var playerId = receivedEvent.PlayerId;
        var currentRating = await _playerService.GetPlayerRating(playerId, RatingType.Puzzle);
        Console.WriteLine($"--> Puzzle solved with event {message}");
        var ratingDeviation = _ratingHelper.CalculatePuzzleDoneRatings(currentRating, receivedEvent);
        Console.WriteLine($"--> Updating rating of player {playerId}, new value {ratingDeviation[SideColor.White].NewRating}");
        await _playerService.UpdateRating(playerId, RatingType.Puzzle, ratingDeviation[SideColor.White].NewRating);

        var ratingChanged = new RatingChangedDto()
        {
            PuzzleId = receivedEvent.PuzzleId,
            NewRating = ratingDeviation[SideColor.Black].NewRating,
            RatingDeviation = ratingDeviation[SideColor.Black].Deviation
        };
        _messageBusClient.SendRatingChangedMessage(ratingChanged);
        await _wsHandler.SendMessageAsync(playerId, new PlayerResponse(PlayerResponseType.RatingDeviation)
            { RatingDeviation =  ratingDeviation[SideColor.White]});
    }

    private EventType DetermineEventType(string message)
    {
        try
        {
            var genericEvent = JsonConvert.DeserializeObject<GenericEventDto>(message);
            return genericEvent.EventType;
        }
        catch (Exception e)
        {
            Console.WriteLine(e);
            return EventType.Undetermined;
        }
            
    }
}