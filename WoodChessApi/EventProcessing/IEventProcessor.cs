﻿using System.Threading.Tasks;

namespace WoodChessV1.EventProcessing
{
    public interface IEventProcessor
    {
        Task ProcessEvent(string message);
    }
}