using System;
using System.Text;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.HttpOverrides;
using Microsoft.AspNetCore.Identity;
using Microsoft.IdentityModel.Tokens;
using WoodChessV1.ChessClocks;
using WoodChessV1.ChessEngine;
using WebSocketModels.Enums;
using WoodChessV1.AsyncDataServices;
using WoodChessV1.EventProcessing;
using WoodChessV1.Repository.Abstract;
using WoodChessV1.Repository.Caches;
using WoodChessV1.Repository.Concrete;
using StackExchange.Redis;
using WoodChessV1.Models;
using WoodChessV1.Repository.Contexts;
using WoodChessV1.Services.ChallengeServices;
using WoodChessV1.Services.GameServices;
using WoodChessV1.Services.GroupServices;
using WoodChessV1.Services.PlayerServices;
using WoodChessV1.Services.UserServices;
using WoodChessV1.Tools;
using WoodChessV1.WebSocketClasses.WebSocketManager;
using WoodChessV1.WebSocketClasses.WsRequestHandlers;
using WoodChessV1.Services.Grpc;

namespace WoodChessV1
{
    public class Startup
    {
        public Startup(IConfiguration configuration, IWebHostEnvironment env)
        {
            Configuration = configuration;
            Env = env;
        }

        private IConfiguration Configuration { get; }
        private IWebHostEnvironment Env { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());

            var connectionString = Env.IsDevelopment() ? Configuration.GetConnectionString("ChessDb") : 
                    Environment.GetEnvironmentVariable("PG_CONNECTION");
            if (connectionString != null)
            {
                services.AddDbContext<ChessContext>(options =>
                    options.UseNpgsql (connectionString).EnableSensitiveDataLogging(), contextLifetime: ServiceLifetime.Transient,
                    optionsLifetime: ServiceLifetime.Singleton);
                services.AddDbContext<IdentityContext>(options =>
                    options.UseNpgsql (connectionString).EnableSensitiveDataLogging(), ServiceLifetime.Transient);
            }
            
            services.AddTransient<IConnectionMultiplexer>(_ => ConnectionMultiplexer.Connect(Configuration.GetConnectionString("Redis")));

            services.AddSession();
            
            services.AddIdentity<IdentityUser, IdentityRole>(options =>
                {
                    options.User.RequireUniqueEmail = true;
                    options.Password.RequireDigit = false;
                    options.Password.RequireLowercase = true;
                    options.Password.RequiredLength = 6;
                    options.Password.RequireUppercase = true;
                    options.Password.RequireNonAlphanumeric = false;
                }).AddEntityFrameworkStores<IdentityContext>()
                .AddDefaultTokenProviders();

            var authKey = Env.IsDevelopment()
                ? Configuration["AuthSettings:Key"]
                : Environment.GetEnvironmentVariable("AUTH_KEY");
            
            services.AddAuthentication(auth =>
            {
                auth.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                auth.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            }).AddJwtBearer(options =>
            {
                if (authKey != null)
                    options.TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidateIssuer = true,
                        ValidateAudience = true,
                        ValidAudience = Configuration["AuthSettings:Audience"],
                        ValidIssuer = Configuration["AuthSettings:Issuer"],
                        RequireExpirationTime = true,
                        IssuerSigningKey =
                            new SymmetricSecurityKey(
                                Encoding.UTF8.GetBytes(authKey)),
                        ValidateIssuerSigningKey = true,
                        ClockSkew = TimeSpan.FromSeconds(5)
                    };
            });

            services.AddCors();
            services.AddWebSocketManager();
            services.AddRazorPages();
            services.AddControllers();
            services.AddGrpc();
            
            services.AddScoped<IUserService, UserService>();
            services.AddSingleton<IChessLogic, ChessLogic>();
            services.AddSingleton<IChessClockManager, ChessClockManager>();
            services.AddSingleton<IGroupService, GroupService>();
            services.AddTransient<WsGameRequestHandler, WsGameRequestHandler>();
            services.AddTransient<WsChallengeRequestHandler, WsChallengeRequestHandler>();
            services.AddTransient<WsDataRequestHandler, WsDataRequestHandler>();
            services.AddTransient<IPlayerService, PlayerService>();
            services.AddTransient<IGameService, GameService>();
            
            services.AddTransient<IChallengeService, ChallengeService>();
            services.AddTransient<IPlayerRepository, PlayerRepository>();
            services.AddTransient<IGameRepository, GameRepository>();
            services.AddSingleton<IChallengeStorage, ChallengeStorage>();
            services.AddSingleton<IRedisRepository<Guid, GameGroup>, RedisRepository<Guid, GameGroup>>();
            
            services.AddTransient<ICacheService<Guid, Game>, RedisCacheService<Guid, Game>>();
            services.AddTransient<ICacheService<long, CurrentGame>, RedisCacheService<long, CurrentGame>>();
            services.AddTransient<ICacheService<long, Player>, RedisCacheService<long, Player>>();

            services.AddSingleton<WsHandlerResolver>(serviceProvider => messageType =>
            {
                switch (messageType)
                {
                    case (MessageType.Challenge):
                        return serviceProvider.GetService<WsChallengeRequestHandler>();
                    case (MessageType.Game):
                        return serviceProvider.GetService<WsGameRequestHandler>();
                    case (MessageType.Data):
                        return serviceProvider.GetService<WsDataRequestHandler>();
                    default:
                        return null;
                }
            });

            services.AddSingleton<IRatingHelper, RatingHelper>();
            services.AddSingleton<IWebSocketHandler, WebSocketHandler>();
            services.AddSingleton<IEventProcessor, EventProcessor>();
            services.AddSingleton<IMessageBusClient, MessageBusClient>();
            services.AddHostedService<MessageBusSubscriber>();
            services.Configure<CacheSettings>(opt => Configuration.GetSection("CacheSettings").Bind(opt));
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            AppContext.SetSwitch("Npgsql.EnableLegacyTimestampBehavior", true);
            var serviceScopeFactory = app.ApplicationServices.GetRequiredService<IServiceScopeFactory>();
            var serviceProvider = serviceScopeFactory.CreateScope().ServiceProvider;

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseCors(builder => builder
                .AllowAnyOrigin()
                .AllowAnyMethod()
                .AllowAnyHeader());
            app.UseRouting();
            app.UseSession();
            app.UseAuthorization();
            app.UseWebSockets();
            app.UseStaticFiles();
            
            
            app.MapWebSocketManager("/chess_api/ws", serviceProvider.GetService<IWebSocketHandler>());
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "chess_api/{controller}/{action}/{id?}");
                //endpoints.MapControllers();

                endpoints.MapGrpcService<GrpcPlayerService>();
            });
            
            
            app.UseForwardedHeaders(new ForwardedHeadersOptions { ForwardedHeaders = ForwardedHeaders.All });
        }
    }
}
