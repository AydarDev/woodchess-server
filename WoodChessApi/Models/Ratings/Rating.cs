﻿using System;
using WebSocketModels.Enums;

namespace WoodChessV1.Models.Ratings
{
    [Serializable]
    public class Rating
    {
        public Rating() {}
        public Rating(RatingType ratingType, double value, bool isGame)
        {
            RatingType = ratingType;
            Value = value;
            IsGame = isGame;
        }
        public long PlayerId { get; init; }
        public RatingType RatingType { get; set; }
        public double Value { get; set; }
        public bool IsGame { get; set; }
    }
}