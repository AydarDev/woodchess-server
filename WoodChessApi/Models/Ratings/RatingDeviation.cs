﻿using System;
using ChessRules;

namespace WoodChessV1.Models.Ratings;

[Serializable]
public class RatingDeviation
{
    public double Deviation { get; set; }
    public double NewRating { get; set; }
    public virtual bool IsNull { get; set; } = false;
}

public sealed class NullDeviation : RatingDeviation
{
    public override bool IsNull { get; set; } = true;
}