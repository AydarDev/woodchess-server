﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using Newtonsoft.Json;
using WebSocketModels.Enums;
using WoodChessV1.Models.Ratings;

namespace WoodChessV1.Models;

[Serializable]
public class Player
{
    public long Id { get; set; }
    
    public string Name { get; set; }

    [JsonIgnore]
    [System.Text.Json.Serialization.JsonIgnore]
    public DateTime? RegisterTime { get; set; }

    [JsonIgnore]
    [System.Text.Json.Serialization.JsonIgnore]
    public DateTime? LastLoginTime { get; set; }

    [JsonIgnore]
    [System.Text.Json.Serialization.JsonIgnore]
    public IPAddress IpAddress { get; set; }
    
    public List<Rating> Ratings { get; set; }

    public bool IsOnline { get; set; }

    public virtual bool IsNull { get; set; } = false;

    public List<Game> Games { get; set; } = new();
    public List<GamePlayer> GamePlayers { get; set; } = new();

    public double GetMaxRating()
    {
        return Ratings.Where(r => r.IsGame)
            .Select(r => r.Value)
            .Max();
    }

    public Rating GetRating(RatingType ratingType)
    {
        return Ratings.FirstOrDefault(r => r.RatingType == ratingType);
    }

    internal double this[RatingType ratingType]
    {
        get => Ratings.FirstOrDefault(r => r.RatingType == ratingType)!.Value;
        set => Ratings.FirstOrDefault(r => r.RatingType == ratingType)!.Value = value;
    }

    public override bool Equals(object obj)
    {
        if (obj is not Player p)
            return false;

        return (Id == p.Id) &&
               (Name == p.Name);
    }

    public override int GetHashCode()
    {
        return HashCode.Combine(Id, Name, Ratings, IsOnline);
    }
}

public sealed class NullPlayer : Player
{
    public override bool IsNull { get; set; } = true;
    private static NullPlayer _nullPlayer;

    private NullPlayer()
    {
    }

    public static NullPlayer GetInstance()
    {
        return _nullPlayer ??= new NullPlayer();
    }
}