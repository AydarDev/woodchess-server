﻿using System;
using ChessRules;
using WoodChessV1.Models.Ratings;

namespace WoodChessV1.Models;

public class GamePlayer
{
    public long Id { get; init; }
    public Guid GameId { get; init; }
    public Game Game { get; init; }
    public long PlayerId { get; init; }
    public Player Player { get; set; }
    public SideColor Color { get; init; }
    public RatingDeviation RatingDeviation { get; set; }
}