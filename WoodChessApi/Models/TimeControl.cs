﻿using System;
using WebSocketModels.Enums;
using WebSocketModels.Models;

namespace WoodChessV1.Models
{
    [Serializable]
    public class TimeControl
    {
        public long Id { get; init; }
        public int TimeRange { get; init; }
        public int TimeIncrement { get; init; }
        public TimeControlType ControlType { get; set; } = TimeControlType.TimeOnGame;

        public RatingType RatingType { get; set; } = RatingType.Blitz;

        public virtual bool IsNull { get; set; }

        public TimeControl() { }

        public TimeControl(int timeRange, int timeIncrement, TimeControlType controlType = TimeControlType.TimeOnGame)
        {
            TimeRange = timeRange;
            TimeIncrement = timeIncrement;
            ControlType = controlType;
        }
        
        public override bool Equals(object obj)
        {
            if (obj is not TimeControl tc)
                return false;

            return (TimeRange == tc.TimeRange) &&
                    (TimeIncrement == tc.TimeIncrement);
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(TimeRange, TimeIncrement);
        }
        public override string ToString()
        {
            return IsNull ? "No time limits" : $"{TimeRange}'+{TimeIncrement}''";
        }
    }
    
    public sealed class NullTimeControl : TimeControl
    {
        public override bool IsNull { get; set; } = true;
        private static NullTimeControl _nullTimeControl;
        private NullTimeControl() { }
        public static NullTimeControl GetInstance()
        {
            return _nullTimeControl ??= new NullTimeControl();
        }
    }
}
