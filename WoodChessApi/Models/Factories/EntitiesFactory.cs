﻿using System;
using System.Collections.Generic;
using System.Net;
using ChessRules;
using WebSocketModels.Enums;
using WoodChessV1.Models.Ratings;
using static WebSocketModels.Enums.RatingType;

namespace WoodChessV1.Models.Factories
{
    public static class EntitiesFactory
    {
        public static Player GetPlayer(string name, IPAddress ipAddress)
        {
            if (string.IsNullOrEmpty(name))
                throw new ArgumentNullException(name, "Name cannot be empty");
            return new Player()
            {
                Name = name,
                Ratings = new List<Rating>
                {
                    new(Blitz, 1600, true),
                    new(Bullet, 1600, true),
                    new(Rapid, 1600, true),
                    new(Classic, 1600, true),
                    new(Daily, 1600, true),
                    new(Puzzle, 1600, false)
                },
                LastLoginTime = DateTime.UtcNow,
                RegisterTime = DateTime.UtcNow,
                IpAddress = ipAddress
            };
        }
        public static Challenge GetChallenge(Player fromPlayer, Player toPlayer, TimeControl timeControl, SideColor sideColor)
        {
            var challenge = new Challenge
            {
                Id = Guid.NewGuid(),
                FromPlayer = fromPlayer,
                ToPlayer = toPlayer,
                TimeControl = timeControl,
                Color = sideColor
            };
            return challenge;
        }
        
        public static Game GetGame(Challenge challenge)
        {
            var game = new Game
            {
                Id = challenge.Id,
                Fen = new Chess().Fen,
                Status = GameStatus.Play,
                TimeControl = challenge.TimeControl,
                DrawInfo = new DrawInfo {GameId = challenge.Id},
                StartedAt = DateTime.UtcNow,
                CurrPly = 1,
                TimeInfo = new TimeInfo
                {
                    WhiteTime = TimeSpan.FromMinutes(challenge.TimeControl.TimeRange),
                    BlackTime = TimeSpan.FromMinutes(challenge.TimeControl.TimeRange),
                    TimeStamp = DateTime.UtcNow
                }
            };

            return game;
        }
        public static Move GetMove(Game game, long playerId, string value)
        {
            var move = new Move
            {
                GameId = game.Id,
                PlayerId = playerId,
                Value = value,
                Fen = game.Fen,
                TimeInfo = game.TimeInfo
            };

            return move;
        }
    }
}
