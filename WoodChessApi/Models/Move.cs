﻿using System;

namespace WoodChessV1.Models;

public class Move
{
    public Guid GameId { get; set; }
    public long PlayerId { get; set; }
    public string Value { get; set; }
    public string Fen { get; set; }
    public TimeInfo TimeInfo { get; set; }
    public virtual bool IsNull { get; set; } = false;
}

public sealed class NullMove : Move
{
    public override bool IsNull { get; set; } = true;

    private static NullMove _nullMove;
    private NullMove() { }

    public static NullMove GetInstance() => _nullMove ??= new NullMove();
}