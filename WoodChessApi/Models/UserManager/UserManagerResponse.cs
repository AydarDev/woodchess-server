﻿using System;
using System.Collections.Generic;
using WebSocketModels.Models;

namespace WoodChessV1.Models.UserManager
{
    [Serializable]
    public class UserManagerResponse
    {
        public string Message { get; set; }
        public bool IsSuccess { get; set; }
        public IEnumerable<string> Errors { get; set; }
        public DateTime? ExpireDate { get; set; }
        public PlayerDto CurrPlayer { get; set; }
        
        public UserManagerResponse() {}
    }
}