﻿using System;
using System.ComponentModel.DataAnnotations;

namespace WoodChessV1.Models.UserManager
{
    [Serializable]
    public class LoginViewModel
    {
        [Required]
        [StringLength(10, MinimumLength = 3)]
        public string UserName { get; set; }

        [Required] [StringLength(50, MinimumLength = 5)]
        public string Password { get; set; }

        public LoginViewModel() {}
    }
}
