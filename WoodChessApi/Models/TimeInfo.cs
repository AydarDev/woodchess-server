using System;

namespace WoodChessV1.Models;

public class TimeInfo
{
    public TimeSpan WhiteTime { get; set; }
    public TimeSpan BlackTime { get; set; }
    public DateTime TimeStamp { get; set; }
}