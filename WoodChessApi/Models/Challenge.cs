﻿using System;
using System.ComponentModel.DataAnnotations;
using ChessRules;
using WebSocketModels.Enums;

namespace WoodChessV1.Models;

public class Challenge
{
    [Required]
    public Guid Id { get; set; }
    public Player FromPlayer { get; init; }
    public SideColor Color { get; set; }       
    public Player ToPlayer { get; init; }
    public TimeControl TimeControl { get; init; }
    public RatingType RatingType => TimeControl.RatingType;
    public virtual bool IsNull { get; set; } = false;

    public override string ToString()
    {
        return ToPlayer.IsNull ? $"Public challenge from {FromPlayer.Name}, {TimeControl}" : $"Challenge from {FromPlayer.Name} to {ToPlayer.Name}, {TimeControl}";
    }
        
    public override bool Equals(object obj)
    {
        if (obj is not Challenge ch)
            return false;

        var result = FromPlayer.Equals(ch.FromPlayer) &&
                     Color.Equals(ch.Color) &&
                     TimeControl.Equals(ch.TimeControl);

        if (ToPlayer != null)
            result = result && ToPlayer.Equals(ch.ToPlayer);

        return result;                    
    }

    public override int GetHashCode()
    {
        return base.GetHashCode();
    }
}

public sealed class NullChallenge : Challenge
{
    public override bool IsNull { get; set; } = true;

    private static NullChallenge _nullChallenge;

    private NullChallenge() { }

    public static NullChallenge GetInstance()
    {
        return _nullChallenge ??= new NullChallenge();
    }
}