using System;
using System.Collections.Generic;

namespace WoodChessV1.Models;

[Serializable]
public class GameGroup
{
    public Guid Id { get; init; }
    public HashSet<long> Receivers { get; } = new();

    public GameGroup(){}
    public GameGroup(Guid id)
    {
        Id = id;
    }
}