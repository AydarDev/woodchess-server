﻿using ChessRules;
using System;
using WebSocketModels.Enums;
using WoodChessV1.WebSocketClasses.WebSocketManager;

namespace WoodChessV1.Models
{
    public static class ModelsExtensions
    {
        internal static Player GetOnlineStatus(this Player player)
        {
            player.IsOnline = WebSocketHandler.IsPlayerOnline(player.Id);
            return player;
        }

        internal static Game GetOnlineStatuses(this Game game)
        {
            game.Players.ForEach(p => p.GetOnlineStatus());

            return game;
        }

        internal static Game SetPlayersByColor(this Game game, long playerId, Challenge challenge)
        {
            if (challenge.Color == SideColor.None)
            {
                challenge.Color = GetRandomColor();
                SetPlayersByColor(game, playerId, challenge);
            }
            else
                game.GamePlayers.AddRange(
                    new[]
                    {
                        new GamePlayer { PlayerId = challenge.FromPlayer.Id, Color = challenge.Color },
                        new GamePlayer { PlayerId = playerId, Color = SwapColor(challenge.Color) }
                    });

            return game;
        }

        internal static Game SetDrawInfo(this Game game, DrawInfo drawInfo)
        {
            game.DrawInfo = drawInfo;
            return game;
        }

        internal static void SetDrawFlag(this DrawInfo drawInfo, DrawFlag drawFlag, long fromPlayer, int counter = -1)
        {
            drawInfo.DrawFlag = drawFlag;
            drawInfo.FromPlayerId = fromPlayer;
            drawInfo.BlockCounter = counter;
        }
       
        internal static void ResetDrawFlag(this DrawInfo drawInfo)
        {
            drawInfo.DrawFlag = DrawFlag.None;
            drawInfo.FromPlayerId = default;
            drawInfo.BlockCounter = -1;
        }

        private static SideColor GetRandomColor()
        {
            var rand = new Random();
            var x = rand.Next(1, 3);

            return x == 1 ? SideColor.White : SideColor.Black;
        }

        private static SideColor SwapColor(SideColor color)
        {
            return color switch
            {
                SideColor.White => SideColor.Black,
                SideColor.Black => SideColor.White,
                _ => SideColor.None
            };
        }
    }
}