﻿using System;
using System.Collections.Generic;
using System.Linq;
using ChessRules;
using Newtonsoft.Json;
using WebSocketModels.Enums;

namespace WoodChessV1.Models;
public class Game
{
    public Guid Id { get; set; }
    public string Fen { get; set; }
    public string Pgn { get; set; }
    public GameStatus Status { get; set; }
    public TimeControl TimeControl { get; set; }
    public long TimeControlId { get; init; }
    public int CurrPly { get; set; }
    public string LastMove { get; set; }
    public TimeInfo TimeInfo { get; set; }
    public DrawInfo DrawInfo { get; set; }
    public DateTime StartedAt { get; init; }
    public GameResult? Result { get; set; }
    public GameDetailResult? DetailResult { get; set; }
    public string TextResult { get; set; }
    public List<Player> Players { get; set; } = new();
    public List<GamePlayer> GamePlayers { get; set; } = new();
    public Player WhitePlayer => GamePlayers.FirstOrDefault(gp => gp.Color == SideColor.White)?.Player ??
                                 NullPlayer.GetInstance();
        
    public Player BlackPlayer => GamePlayers.FirstOrDefault(gp => gp.Color == SideColor.Black)?.Player ??
                                 NullPlayer.GetInstance();
    internal Player this[SideColor color] => GamePlayers.First(gp => gp.Color == color).Player;

    [JsonIgnore]
    public RatingType RatingType => TimeControl.RatingType;

    public virtual bool IsNull { get; set; }

    public override string ToString()
    {
        return $"Game {WhitePlayer.Name} VS {BlackPlayer.Name}";
    }
}
public sealed class NullGame : Game
{
    public override bool IsNull { get; set; } = true;

    private static NullGame _nullGame;

    private NullGame() { }

    public static NullGame GetInstance()
    {
        return _nullGame ??= new NullGame();
    }

}