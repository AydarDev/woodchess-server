﻿using System;
using WebSocketModels.Enums;

namespace WoodChessV1.Models;
public class DrawInfo
{
    public Guid GameId { get; init; }
    public DrawFlag DrawFlag { get; set; }
    public long FromPlayerId { get; set; }
    public int BlockCounter { get; set; } = -1;
    public virtual bool IsNull { get; set; } = false;
}

public class NullDrawInfo : DrawInfo
{
    private static NullDrawInfo _instance;
    public override bool IsNull { get; set; } = true;
    private NullDrawInfo(){}
    public static NullDrawInfo GetInstance() => _instance ??= new NullDrawInfo();
}