﻿using System;
using System.Collections.Generic;
using ChessRules;
using WebSocketModels.Enums;
using WebSocketModels.Models.Ratings;
using static System.Math;

namespace WoodChessV1.RatingCalculator;

public class RatingCalculator
{
    private const double Koef = 20;
    private static RatingCalculator _ratingCalculator;

    private RatingCalculator()
    {
    }

    public static RatingCalculator GetInstance()
    {
        return _ratingCalculator ??= new RatingCalculator();
    }

    public Dictionary<SideColor, RatingDeviationDto> GetRatingDeviation(InputRating inputRating, GameResult gameResult)
    {
        if (gameResult == GameResult.Cancel)
            return new Dictionary<SideColor, RatingDeviationDto>
            {
                { SideColor.White, new RatingDeviationDto(0) }, 
                { SideColor.Black, new RatingDeviationDto(0) }
            };

        GetPlayerPoints(gameResult);
        (double whitePoint, double blackPoint) points = GetPlayerPoints(gameResult);

        var whiteRatingDelta = Round(
            GetPlayerRatingDeviation(inputRating.WhiteRating, inputRating.BlackRating, points.whitePoint)
            , 0, MidpointRounding.AwayFromZero);
        var blackRatingDelta = Round(
            GetPlayerRatingDeviation(inputRating.BlackRating, inputRating.WhiteRating, points.blackPoint)
            , 0, MidpointRounding.AwayFromZero);

        var whiteDeviation = new RatingDeviationDto(whiteRatingDelta, inputRating.WhiteRating + whiteRatingDelta);
        var blackDeviation = new RatingDeviationDto(blackRatingDelta, inputRating.BlackRating + blackRatingDelta);

        return new Dictionary<SideColor, RatingDeviationDto>()
        {
            { SideColor.White, whiteDeviation }, {SideColor.Black, blackDeviation }
        };
    }

    private (double, double) GetPlayerPoints(GameResult gameResult)
    {
        double whitePoint = 0, blackPoint = 0;
        switch (gameResult)
        {
            case (GameResult.WhiteWin):
                whitePoint = 1;
                blackPoint = 0;
                break;
            case (GameResult.BlackWin):
                whitePoint = 0;
                blackPoint = 1;
                break;
            case (GameResult.Draw):
                whitePoint = 0.5;
                blackPoint = 0.5;
                break;
        }

        return (whitePoint, blackPoint);
    }

    private double GetPlayerRatingDeviation(double playerRating, double opponentRating, double point)
    {
        double mathExp = GetMathExpectation(playerRating, opponentRating);
        return Round(Koef * (point - mathExp), 2);
    }

    private double GetMathExpectation(double playerRating, double opponentRating)
    {
        return 1 / (1 + Pow(10, (opponentRating - playerRating) / 400));
    }
}