﻿namespace WoodChessV1.RatingCalculator;

public struct InputRating
{
    public readonly double WhiteRating;
    public readonly double BlackRating;

    public InputRating (double whiteRating, double blackRating)
    {
        this.WhiteRating = whiteRating;
        this.BlackRating = blackRating;
    }
}