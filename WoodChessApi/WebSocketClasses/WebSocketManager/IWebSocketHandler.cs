﻿using System;
using System.Collections.Generic;
using System.Net.WebSockets;
using System.Threading.Tasks;
using WebSocketModels.WsMessages.WsResponses;

namespace WoodChessV1.WebSocketClasses.WebSocketManager
{
    public interface IWebSocketHandler
    {
        Task OnConnected(long playerId, WebSocket socket);
        Task OnDisconnected(WebSocket socket);
        Task SendMessageAsync(long playerId, WsResponse message);
        Task SendMessageAsync(WebSocket socket, WsResponse message);
        Task SendMessageToAllAsync(WsResponse message);
        Task ReceiveBinary(WebSocket socket, WebSocketReceiveResult result, byte[] buffer);
        Task SendMessageToGroupAsync(Guid groupId, WsResponse message);
        Task ReceiveAsync(WebSocket socket, WebSocketReceiveResult result, byte[] buffer);
    }
}