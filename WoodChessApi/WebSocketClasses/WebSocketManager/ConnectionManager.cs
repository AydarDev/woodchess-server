﻿using System;
using System.Collections.Concurrent;
using System.Linq;
using System.Net.WebSockets;
using System.Threading;
using System.Threading.Tasks;

namespace WoodChessV1.WebSocketClasses.WebSocketManager
{
    public class ConnectionManager
    {
        private readonly ConcurrentDictionary<long, WebSocket> _sockets = new();

        public WebSocket GetSocketById (long id)
        {
            return _sockets.FirstOrDefault(p => p.Key == id).Value;
        }

        public ConcurrentDictionary<long, WebSocket> GetAll()
        {
            return _sockets;
        }

        public long GetId (WebSocket socket)
        {
            return _sockets.FirstOrDefault(w => w.Value == socket).Key;
        }

        public void AddSocket(long id, WebSocket socket)
        {
            _sockets.TryAdd(id, socket);
        }

        public async Task RemoveSocket(long id)
        {
            try
            {
                _sockets.TryRemove(id, out var socket);
                if (socket != null && socket.State != WebSocketState.Aborted)
                    await socket.CloseAsync(closeStatus: WebSocketCloseStatus.NormalClosure,
                        statusDescription: "Closed by the ConnectionManager",
                        cancellationToken: CancellationToken.None);
            }
            catch (WebSocketException e)
            {
                Console.WriteLine($"Error during socket closure {e.Message}.\nStackTrace {e.StackTrace}");
            }
        }
    }
}
