﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.WebSockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using WebSocketModels.Enums;
using WebSocketModels.WsMessages.WsRequests;
using WebSocketModels.WsMessages.WsResponses;
using WoodChessV1.Services.ChallengeServices;
using WoodChessV1.Services.GroupServices;
using WoodChessV1.Services.PlayerServices;
using WoodChessV1.WebSocketClasses.WsRequestHandlers;
using static Newtonsoft.Json.JsonConvert;
using static WoodChessV1.Misc.JsonSettings;

namespace WoodChessV1.WebSocketClasses.WebSocketManager
{
    public delegate IWsRequestHandler WsHandlerResolver(MessageType messageType);

    public class WebSocketHandler : IWebSocketHandler
    {
        private static ConnectionManager WsConnectionManager { get; set; }
        private readonly ArraySegment<byte> _pongArraySegment = new(new byte[] { 7, 5 });
        private readonly IGroupService _groupService;
        private readonly IServiceScopeFactory _scopeFactory;
        private readonly ILogger<WebSocketHandler> _logger;
        private readonly IPlayerService _players;
        private readonly WsHandlerResolver _requestHandlerResolver;
        private readonly WsRequestConverter _wsRequestConverter = new();

        public WebSocketHandler(ConnectionManager wsConnectionManager, IGroupService groupService,
            IServiceScopeFactory scopeFactory, IPlayerService players,
            WsHandlerResolver wsHandlerResolver, ILogger<WebSocketHandler> logger)
        {
            WsConnectionManager = wsConnectionManager;
            _groupService = groupService;
            _scopeFactory = scopeFactory;
            _requestHandlerResolver = wsHandlerResolver;
            _logger = logger;
            _players = players;
        }

        public async Task OnConnected(long playerId, WebSocket socket)
        {
            var existingSocket = WsConnectionManager.GetSocketById(playerId);
            if (existingSocket != null)
                await OnDisconnected(existingSocket);
            WsConnectionManager.AddSocket(playerId, socket);
            var player = await _players.GetPlayer(playerId);
            player.IsOnline = true;
            await _players.Cache(player);
            await SendNotifications(playerId);
        }

        private async Task SendNotifications(long playerId)
        {
            var player = await _players.Get(playerId);
            await SendMessageAsync(playerId,
                new DataResponse(DataResponseType.System, $"Welcome, {player.Name}!"));
            await _groupService.RestorePlayerInGroup(playerId);
            await SendPlayerOnlineUpdate(playerId);
            await SendMessageAsync(playerId,
                new DataResponse(DataResponseType.Clock)
                    { ServerTime = DateTime.UtcNow });
        }

        public async Task OnDisconnected(WebSocket socket)
        {
            using var scope = _scopeFactory.CreateScope();
            var challengeService = scope.ServiceProvider.GetService<IChallengeService>();
            var privateChallenges = await challengeService.GetPrivate(GetPlayerId(socket));
            var tasks = privateChallenges.Select(ch => SendMessageToGroupAsync(ch.Id,
                new ChallengeResponse(ChallengeResponseType.ChallengeRemove) { ChallengeId = ch.Id }));
            await Task.WhenAll(tasks);

            await challengeService.RemoveCurrentChallenges(GetPlayerId(socket));
            await SendMessageToAllAsync(new ChallengeResponse(ChallengeResponseType.Challenges)
                { Challenges = await challengeService.GetPublic() });

            var socketId = WsConnectionManager.GetId(socket);
            var playerId = WsConnectionManager.GetId(socket);
            await WsConnectionManager.RemoveSocket(socketId);
            _logger.LogWarning("Player {} has logout", playerId);
            var player = await _players.GetPlayer(playerId);
            player.IsOnline = false;
            await _players.Cache(player);
            await SendPlayerOnlineUpdate(playerId);
            await _groupService.RemovePlayerFromGroups(playerId);
            _logger.LogDebug("Socket {} is disconnected", socketId);
        }

        private async Task SendMessageAsync(WebSocket socket, string message)
        {
            if (socket == null || socket.State != WebSocketState.Open)
                return;

            ArraySegment<byte> arraySegment = new ArraySegment<byte>(array: Encoding.UTF8.GetBytes(message),
                offset: 0,
                count: message.Length);

            await socket.SendAsync(buffer: arraySegment,
                messageType: WebSocketMessageType.Text,
                endOfMessage: true,
                cancellationToken: CancellationToken.None);
        }

        private async Task SendMessageAsync(long playerId, string message)
        {
            await SendMessageAsync(WsConnectionManager.GetSocketById(playerId), message);
        }

        public async Task SendMessageAsync(long playerId, WsResponse message)
        {
            var convertedMessage = SerializeObject(message, Settings);
            await SendMessageAsync(playerId, convertedMessage);
        }

        public async Task SendMessageAsync(WebSocket socket, WsResponse message)
        {
            var convertedMessage = SerializeObject(message, Settings);
            await SendMessageAsync(socket, convertedMessage);
        }

        public async Task ReceiveBinary(WebSocket socket, WebSocketReceiveResult result, byte[] buffer)
        {
            if (IsMatchPingPattern(buffer))
                await SendPong(socket);
        }

        private static bool IsMatchPingPattern(byte[] buffer)
        {
            return buffer[0] == 5 && buffer[1] == 7;
        }

        private async Task SendPong(WebSocket socket)
        {
            if (socket is not { State: WebSocketState.Open })
                return;

            await socket.SendAsync(buffer: _pongArraySegment,
                messageType: WebSocketMessageType.Binary,
                endOfMessage: true,
                cancellationToken: CancellationToken.None);
        }


        public async Task SendMessageToGroupAsync(Guid groupId, WsResponse message)
        {
            var group = await _groupService.GetGroup(groupId);
            if(group != null)
                await SendMessageToPlayers(message, group.Receivers);
        }
        
        private async Task SendMessageToPlayers(WsResponse message, IEnumerable<long> players)
        {
            var convertedMessage = SerializeObject(message, Settings);
            var tasks = players
                .Select(player => SendMessageAsync(player, convertedMessage));

            await Task.WhenAll(tasks);
        }


        public async Task SendMessageToAllAsync(WsResponse message)
        {
            var convertedMessage = SerializeObject(message, Settings);
            var tasks = WsConnectionManager.GetAll().Values
                .Select(socket => SendMessageAsync(socket, convertedMessage));

            await Task.WhenAll(tasks);
        }


        public async Task ReceiveAsync(WebSocket socket, WebSocketReceiveResult result, byte[] buffer)
        {
            try
            {
                var inputMessage = Encoding.UTF8.GetString(buffer, 0, result.Count);
                _logger.LogDebug("{}", inputMessage);
                var wsRequest = DeserializeObject<WsRequest>(inputMessage, _wsRequestConverter);
                await GetWsRequestHandler(wsRequest).HandleWsRequest(wsRequest);
            }
            catch (Exception e)
            {
                _logger.LogError("Error during receive Websocket message: {Message}", e.Message);
            }
        }

        private IWsRequestHandler GetWsRequestHandler(WsRequest wsRequest)
        {
            return _requestHandlerResolver.Invoke(wsRequest.MessageType);
        }

        private long GetPlayerId(WebSocket socket)
        {
            return WsConnectionManager.GetId(socket);
        }

        public static bool IsPlayerOnline(long playerId)
        {
            var socket = WsConnectionManager.GetSocketById(playerId);

            if (socket != null)
                return (socket.State == WebSocketState.Open);

            return false;
        }

        private async Task SendPlayerOnlineUpdate(long playerId)
        {
            var player = await _players.Get(playerId);
            if (player.IsNull) return;
            await SendMessageToAllAsync(new PlayerResponse(PlayerResponseType.Player)
                { Player = player });
        }
    }
}