﻿using System;
using System.IdentityModel.Tokens.Jwt;
using System.Net.WebSockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;

namespace WoodChessV1.WebSocketClasses.WebSocketManager
{
    public class WebSocketManagerMiddleware
    {
        RequestDelegate _next;
        private readonly ILogger<WebSocketManagerMiddleware> _logger;
        private readonly IWebSocketHandler _webSocketHandler;
        private readonly IConfiguration _config;
        private readonly IHostEnvironment _env;

        public WebSocketManagerMiddleware(RequestDelegate next, IWebSocketHandler webSocketHandler, ILogger<WebSocketManagerMiddleware> logger,
            IConfiguration config, IHostEnvironment env)
        {
            _next = next;
            _logger = logger;
            _webSocketHandler = webSocketHandler;
            _config = config;
            _env = env;
        }

        public async Task Invoke(HttpContext context)
        {            
            var token =  context.WebSockets.WebSocketRequestedProtocols[0];
            var playerId = context.WebSockets.WebSocketRequestedProtocols[1];
            if (!IsValidToken(token))
                return;
            var socket = await context.WebSockets.AcceptWebSocketAsync(token);
            try
            {
                if (!context.WebSockets.IsWebSocketRequest)
                    return;

                await _webSocketHandler.OnConnected(long.Parse(playerId), socket);

                await Receive(socket, async (result, buffer) =>
                {
                    switch (result.MessageType)
                    {
                        case WebSocketMessageType.Binary:
                            await _webSocketHandler.ReceiveBinary(socket, result, buffer);
                            break;
                        case WebSocketMessageType.Text:
                            await _webSocketHandler.ReceiveAsync(socket, result, buffer);
                            break;
                        case WebSocketMessageType.Close:
                            await _webSocketHandler.OnDisconnected(socket);
                            break;
                    }
                });
            }
            catch (WebSocketException e)
            {
                _logger.LogError("Websocket error: {EMessage}", e.Message);
                await _webSocketHandler.OnDisconnected(socket);
            }

            catch (Exception e)
            {
                _logger.LogError("Error while invoking websocket middleware: {EStackTrace}", e.StackTrace);
            }                
        }

        private bool IsValidToken(string token)
        {
            try
            {
                JwtSecurityTokenHandler handler = new JwtSecurityTokenHandler();
                //TODO: Make one-point for providing environment-dependent parameters
                var authKey = _env.IsDevelopment()
                    ? _config["AuthSettings:Key"]
                    : Environment.GetEnvironmentVariable("AUTH_KEY");
                if (authKey == null) return false;
                var tokenValidationParams = new TokenValidationParameters
                {
                    ValidIssuer = _config["AuthSettings:Issuer"],
                    ValidAudience = _config["AuthSettings:Audience"],
                    IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(authKey))
                };

                handler.ValidateToken(token, tokenValidationParams, out _);
                    
                return true;

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return false;
            }
        }

        private async Task Receive(WebSocket socket, Action<WebSocketReceiveResult, byte[]> handleMessage)
        {
            var buffer = new byte[1024 * 4];
            while (socket.State == WebSocketState.Open)
            {
                var result = await socket.ReceiveAsync(buffer: new ArraySegment<byte>(buffer),
                                                       cancellationToken: CancellationToken.None);

                handleMessage(result, buffer);
            }
        }
    }
}
