﻿using System.Threading.Tasks;
using AutoMapper;
using WebSocketModels.Enums;
using WebSocketModels.WsMessages.WsRequests;
using WebSocketModels.WsMessages.WsResponses;
using WoodChessV1.Services.ChallengeServices;
using WoodChessV1.WebSocketClasses.WebSocketManager;

namespace WoodChessV1.WebSocketClasses.WsRequestHandlers
{
    public class WsChallengeRequestHandler : WsRequestHandler<ChallengeRequest>
    {
        private readonly IChallengeService _challenges;

        public WsChallengeRequestHandler(IWebSocketHandler chessMessageHandler, IChallengeService challenges, IMapper mapper)
            :base(chessMessageHandler, mapper)
        {
            _challenges = challenges;
        }
        public async Task CreateChallenge(ChallengeRequest request) =>
            await _challenges.Create(Mapper.Map<CreateChallengeModel>(request));

        public async Task AcceptChallenge(ChallengeRequest request) =>
            await _challenges.Accept(request.FromPlayerId, request.ChallengeId);

        public async Task RemoveChallenge(ChallengeRequest request) =>
            await _challenges.Remove(request.FromPlayerId, request.ChallengeId);

        public async Task DeclineChallenge(ChallengeRequest request) => 
            await _challenges.Decline(request.FromPlayerId, request.ChallengeId);
    }
}