﻿using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using WebSocketModels.Enums;
using WebSocketModels.WsMessages.WsRequests;
using WebSocketModels.WsMessages.WsResponses;
using WoodChessV1.Services.PlayerServices;
using WoodChessV1.WebSocketClasses.WebSocketManager;

namespace WoodChessV1.WebSocketClasses.WsRequestHandlers
{
    public class WsDataRequestHandler : WsRequestHandler<DataRequest>
    {
        private readonly IPlayerService _playerService;

        public WsDataRequestHandler(IWebSocketHandler messageHandler, ILogger<WsDataRequestHandler> logger, IPlayerService playerService) :base(messageHandler)
        {
            _playerService = playerService;
        }

        public async Task Chat(DataRequest dataRequest)
        {
            var sender = await _playerService.Get(dataRequest.FromPlayerId);
            if (dataRequest.ToPlayerId == 0)
                await ChessMessageHandler.SendMessageToAllAsync(
                    new DataResponse(DataResponseType.Chat, dataRequest.Data)
                        {FromPlayerName = sender.Name});
            else
                await ChessMessageHandler.SendMessageAsync(dataRequest.ToPlayerId,
                    new DataResponse(DataResponseType.Chat, dataRequest.Data)
                        {FromPlayerName = sender.Name});
        }
    }
}