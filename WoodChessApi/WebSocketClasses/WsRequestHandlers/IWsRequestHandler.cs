﻿using System.Net.WebSockets;
using System.Threading.Tasks;
using WebSocketModels.WsMessages.WsRequests;

namespace WoodChessV1.WebSocketClasses.WsRequestHandlers
{
    public interface IWsRequestHandler
    {
        Task HandleWsRequest(WsRequest wsRequest);
    }
}