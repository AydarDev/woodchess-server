﻿using System;
using System.Threading.Tasks;
using AutoMapper;
using WebSocketModels.Enums;
using WebSocketModels.WsMessages.WsRequests;
using WebSocketModels.WsMessages.WsResponses;
using WoodChessV1.Services.GameServices;
using WoodChessV1.WebSocketClasses.WebSocketManager;

namespace WoodChessV1.WebSocketClasses.WsRequestHandlers
{
    public class WsGameRequestHandler : WsRequestHandler<GameRequest>
    {
        private readonly IGameService _gameService;
        public WsGameRequestHandler(IWebSocketHandler chessMessageHandler, IMapper mapper, IGameService gameService) :base(chessMessageHandler, mapper)
        {
            _gameService = gameService;
        }
        
        public async Task OfferDraw(GameRequest gameRequest) => 
            await _gameService.OfferDraw(gameRequest.FromPlayerId, gameRequest.GameId);

        public async Task DeclineDraw(GameRequest gameRequest) => 
            await _gameService.DeclineDraw(gameRequest.FromPlayerId, gameRequest.GameId);

        public async Task AcceptDraw(GameRequest gameRequest) =>
            await _gameService.AcceptDraw(gameRequest.FromPlayerId, gameRequest.GameId);

        public async Task ResignGame(GameRequest gameRequest) =>
            await _gameService.Resign(gameRequest.FromPlayerId, gameRequest.GameId);

        public async Task MakeMove(GameRequest gameRequest)=>
            await _gameService.MakeMove(gameRequest.FromPlayerId, gameRequest.GameId, gameRequest.Move);
        
        public async Task ExitGame(GameRequest gameRequest) =>
            await _gameService.Exit(gameRequest.FromPlayerId, gameRequest.GameId);
    }
}