﻿using System;
using System.Reflection;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.Extensions.Logging;
using WebSocketModels.WsMessages.WsRequests;
using WoodChessV1.WebSocketClasses.WebSocketManager;

namespace WoodChessV1.WebSocketClasses.WsRequestHandlers
{
    public class WsRequestHandler<TRequest> : IWsRequestHandler where TRequest : WsRequest
    {
        protected readonly IWebSocketHandler ChessMessageHandler;
        protected readonly IMapper Mapper;

        protected WsRequestHandler(IWebSocketHandler chessMessageHandler, IMapper mapper = null)
        {
            ChessMessageHandler = chessMessageHandler;
            Mapper = mapper;
        }

        public virtual async Task HandleWsRequest(WsRequest request)
        {
            if (request is TRequest concreteRequest)
                await Task.Run(() => GetType()
                    .GetMethod(concreteRequest.DetailType, BindingFlags.Instance | BindingFlags.Public)
                    ?.Invoke(obj: this, parameters: new object[] { concreteRequest }));
        }
    }
}