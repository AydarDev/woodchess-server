﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WoodChessV1.Exceptions
{
    public class ChessException : Exception
    {
        public ChessException(string message): base(message)
        {

        }
       
    }
}
