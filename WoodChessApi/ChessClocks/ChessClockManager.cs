﻿using ChessRules;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Concurrent;
using System.Threading;
using System.Threading.Tasks;
using WoodChessV1.Models;
using WoodChessV1.Services.GameServices;

namespace WoodChessV1.ChessClocks;
public class ChessClockManager : IChessClockManager
{
    readonly ConcurrentDictionary<Guid, ChessClock> _clocks = new();
    readonly IServiceScopeFactory _scopeFactory;

    public ChessClockManager(IServiceScopeFactory scopeFactory)
    {
        _scopeFactory = scopeFactory;
        Task.Run(TimeForward);
    }

    public ChessClock GetClock(Game game)
    {
        return _clocks.GetOrAdd(game.Id, new ChessClock(game));
    }

    public void Create(Guid gameId, ChessClock chessClock)
    {
        chessClock.GameId = gameId;            
        _clocks.TryAdd(gameId, chessClock);
    }

    public bool Remove(Guid gameId)
    {
        return _clocks.TryRemove(gameId, out _);
    }

    public Game SetUpClock(Challenge challenge, Game game)
    {
        //TODO: Change this suspicious part
        if (!challenge.TimeControl.IsNull) GetClock(game);

        return game;
    }

    public void SetTimeValues(Game game)
    {
        if (game.IsNull) return;
        game.TimeInfo.TimeStamp = DateTime.UtcNow;
        game.TimeInfo.WhiteTime = GetClock(game).GetTime(SideColor.White);
        game.TimeInfo.BlackTime = GetClock(game).GetTime(SideColor.Black);
    }

    public async Task FlagDrop(ChessClock chessClock)
    {
        using var scope = _scopeFactory.CreateScope();
        await scope.ServiceProvider.GetService<IGameService>().InitTimeOver(chessClock);
    }

    //TODO: Background worker
    private async Task TimeForward()
    {
        while (true)
        {
            foreach (var clock in _clocks.Values)
            {
                clock.ClockRun();
                if (!clock.IsGameRun)
                    await FlagDrop(clock);
            }
            Thread.Sleep(100);
        }
    }
}