﻿using System;
using System.Threading.Tasks;
using WoodChessV1.Models;
using WoodChessV1.Repository.Abstract;

namespace WoodChessV1.ChessClocks
{
    public interface IChessClockManager : IDictionaryBasic<ChessClock, Guid>
    {
        ChessClock GetClock(Game game);
        Game SetUpClock(Challenge challenge, Game game);
        void SetTimeValues(Game game);
    }
}