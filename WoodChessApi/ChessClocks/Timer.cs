﻿using System;

namespace WoodChessV1.ChessClocks;
public class Timer
{
    internal TimeSpan CurrTime { get; set; }
    internal bool IsExpired => CurrTime <= TimeSpan.Zero;
    private readonly TimeSpan _timeIncrement;
    private TimeSpan _timerSpan;
    private bool _isRun;
    private DateTime _timeStamp;


    internal Timer(TimeSpan timeRange, TimeSpan timeIncrement)
    {
        _timeIncrement = timeIncrement;
        _timerSpan = timeRange;
        CurrTime = timeRange;
        _isRun = false;
    }

    internal void Start(DateTime timeStamp)
    {
        _timeStamp = timeStamp;
        CurrTime = _timerSpan - (DateTime.UtcNow - timeStamp);
        _isRun = true;
    }

    internal void Pause()
    {
        CurrTime += _timeIncrement;
        Stop();
    }
        
    internal void Resume(DateTime timeStamp)
    {
        _timeStamp = timeStamp;
        _isRun = true;
    }

    internal void Stop()
    {
        _timerSpan = CurrTime;
        _isRun = false;
    }

    internal void Update()
    {
        if (_isRun)
            CurrTime = _timerSpan - (DateTime.UtcNow - _timeStamp);
    }
}