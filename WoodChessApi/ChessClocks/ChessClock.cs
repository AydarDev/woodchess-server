﻿using System;
using WoodChessV1.Models;
using ChessRules;
using System.Collections.Concurrent;

namespace WoodChessV1.ChessClocks;
public class ChessClock
{
    internal Guid GameId { get; set; }
    internal SideColor ToMoveSide;
    internal bool IsGameRun { get; private set; }
    private readonly ConcurrentDictionary<SideColor, Timer> _timers = new();
    private DateTime _timeStamp;

    public ChessClock(Game game)
    {
        GameId = game.Id;
        var whiteTimeRange = game.TimeInfo.WhiteTime;
        var blackTimeRange = game.TimeInfo.BlackTime;
        _timeStamp = game.TimeInfo.TimeStamp;

        var timeIncrement = TimeSpan.FromSeconds(game.TimeControl.TimeIncrement);

        _timers.TryAdd(SideColor.White, new Timer(whiteTimeRange, timeIncrement));
        _timers.TryAdd(SideColor.Black, new Timer(blackTimeRange, timeIncrement));
        ToMoveSide = game.CurrPly % 2 == 1 ? SideColor.White : SideColor.Black;
        StartClock();
    }

    private void StartClock()
    {
        IsGameRun = true;
        _timers[ToMoveSide].Start(_timeStamp);
    }

    internal void Swap(DateTime timeStamp)
    {
        _timeStamp = timeStamp;
        _timers[ToMoveSide].Pause();
        SwitchSide();
        _timers[ToMoveSide].Resume(timeStamp);
    }
        
    private void SwitchSide()
    {
        ToMoveSide = (ToMoveSide == SideColor.White) ? SideColor.Black : SideColor.White;
    }
        
    internal void ClockRun()
    {
        foreach (Timer timer in _timers.Values)
        {
            timer.Update();
            if (timer.IsExpired)
            {
                StopClock();
                timer.CurrTime = TimeSpan.Zero;
            }
        }
    }

    internal void StopClock()
    {
        IsGameRun = false;
        _timeStamp = DateTime.UtcNow;
            
        foreach (Timer timer in _timers.Values)
            timer.Stop();
    }
        
    internal TimeSpan GetTime(SideColor side)
    {
        return _timers[side].CurrTime;
    }
}