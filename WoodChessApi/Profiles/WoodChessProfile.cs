﻿using AutoMapper;
using WebSocketModels.Models;
using WebSocketModels.Models.Events;
using WebSocketModels.Models.Ratings;
using WebSocketModels.WsMessages.WsRequests;
using WoodChessV1.Models;
using WoodChessV1.Models.Ratings;
using WoodChessV1.Services.ChallengeServices;

namespace WoodChessV1.Profiles
{
    public class WoodChessProfile : Profile
    {
        public WoodChessProfile()
        {
            CreateMap<Player, PlayerDto>();
            CreateMap<PlayerDto, Player>();
            CreateMap<Game, GameDto>();
            CreateMap<GameDto, Game>();
            CreateMap<GamePlayer, GamePlayerDto>();
            CreateMap<GamePlayerDto, GamePlayer>();
            CreateMap<TimeControl, TimeControlDto>();
            CreateMap<TimeControlDto, TimeControl>();
            CreateMap<DrawInfo, DrawInfoDto>();
            CreateMap<DrawInfoDto, DrawInfo>();
            CreateMap<Challenge, ChallengeDto>();
            CreateMap<Move, MoveDto>();
            CreateMap<TimeInfo, TimeInfoDto>();
            CreateMap<TimeInfoDto, TimeInfo>();
            CreateMap<Rating, RatingDto>();
            CreateMap<RatingDto, Rating>();
            CreateMap<RatingDeviation, RatingDeviationDto>();
            CreateMap<RatingDeviationDto, RatingDeviation>();
            CreateMap<RatingDeviationDto, RatingChangedDto>();
            CreateMap<ChallengeRequest, CreateChallengeModel>();
        }
    }
}