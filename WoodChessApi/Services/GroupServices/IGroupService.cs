﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using WoodChessV1.Models;

namespace WoodChessV1.Services.GroupServices;
public interface IGroupService
{
    Task AddPlayerToGroup(Guid groupId, long playerId);
    Task<GameGroup> GetGroup(Guid groupId);
    Task<List<Guid>> GetPlayerGroupIds(long playerId);
    Task Remove(Guid groupId);
    Task RemovePlayerFromGroup(Guid groupId, long playerId);
    Task RemovePlayerFromGroups(long playerId);
    Task RestorePlayerInGroup(long playerId);
}