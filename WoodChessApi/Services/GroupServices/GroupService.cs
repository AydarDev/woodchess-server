﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using WoodChessV1.Models;
using WoodChessV1.Repository.Abstract;
using WoodChessV1.Services.GameServices;

namespace WoodChessV1.Services.GroupServices;

public class GroupService : IGroupService
{
    private readonly IServiceScopeFactory _scopeFactory;
    private readonly IRedisRepository<Guid, GameGroup> _groupsRepository;

    public GroupService(IServiceScopeFactory scopeFactory, IRedisRepository<Guid, GameGroup> groupsRepository)
    {
        _scopeFactory = scopeFactory;
        _groupsRepository = groupsRepository;
    }

    private async Task<GameGroup> GetOrAddGroup(Guid groupId)
    {
        if (await _groupsRepository.IsKeyExists(groupId))
            return await _groupsRepository.Get(groupId);
            
        var group = new GameGroup(groupId);
        await Save(group);

        return group;
    }
    public async Task<GameGroup> GetGroup(Guid groupId) => await _groupsRepository.Get(groupId);

    public async Task AddPlayerToGroup(Guid groupId, long playerId)
    {
        var group = await GetOrAddGroup(groupId);
        group.Receivers.Add(playerId);
        await Save(group);
    }
    public async Task Remove(Guid groupId) => await _groupsRepository.Remove(groupId);

    public async Task RemovePlayerFromGroup(Guid groupId, long playerId)
    {
        if (! await _groupsRepository.IsKeyExists(groupId)) return;
        var group = await GetGroup(groupId);
        await RemovePlayerFromGroup(group, playerId);
    }
        
    private async Task RemovePlayerFromGroup(GameGroup group, long playerId)
    {
        group.Receivers.Remove(playerId);
        if(group.Receivers.Any())
            await Save(group);
        else
            await Remove(group.Id);
    }
        
    public async Task RestorePlayerInGroup(long playerId)
    {
        using var scope = _scopeFactory.CreateScope();
        var gameService = scope.ServiceProvider.GetService<IGameService>();
        var game = await gameService.GetCurrentGame(playerId);
        if(!game.IsNull)
            await AddPlayerToGroup(game.Id, playerId);
    }
    public async Task RemovePlayerFromGroups(long playerId)
    {
        var groups = await _groupsRepository.Get(group => group.Receivers.Contains(playerId));
        var tasks = groups.Select(group => RemovePlayerFromGroup(group, playerId));
        await Task.WhenAll(tasks);
    }
        
    public async Task<List<Guid>> GetPlayerGroupIds(long playerId) => 
        (await _groupsRepository.Get(group => group.Receivers.Contains(playerId))).Select(g => g.Id).ToList();

    private async Task Save(GameGroup group)
    {
        await _groupsRepository.Set(group.Id, group);
    }
}