using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using WebSocketModels.Models;

namespace WoodChessV1.Services.ChallengeServices;

public interface IChallengeService
{
    Task<List<ChallengeDto>> GetPublic();
    Task<List<ChallengeDto>> Get(long playerId);
    Task<List<ChallengeDto>> GetPrivate(long playerId);
    Task Create(CreateChallengeModel model);
    Task Accept(long playerId, Guid challengeId);
    Task Remove(long playerId, Guid challengeId);
    Task RemoveCurrentChallenges(long playerId);
    Task Decline(long playerId, Guid challengeId);
}