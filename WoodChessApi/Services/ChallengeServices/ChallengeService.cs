using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.Extensions.Logging;
using WebSocketModels.Enums;
using WebSocketModels.Models;
using WebSocketModels.WsMessages.WsResponses;
using WoodChessV1.ChessClocks;
using WoodChessV1.ChessEngine;
using WoodChessV1.Exceptions;
using WoodChessV1.Models;
using WoodChessV1.Models.Factories;
using WoodChessV1.Repository.Abstract;
using WoodChessV1.Services.GameServices;
using WoodChessV1.Services.GroupServices;
using WoodChessV1.Services.PlayerServices;
using WoodChessV1.Tools;
using WoodChessV1.WebSocketClasses.WebSocketManager;

namespace WoodChessV1.Services.ChallengeServices;

public class ChallengeService : IChallengeService
{
    private readonly IChallengeStorage _challengeStorage;
    private readonly IGroupService _groupService;
    private readonly IChessClockManager _clockManager;
    private readonly IChessLogic _chessLogic;
    private readonly IMapper _mapper;
    private readonly IWebSocketHandler _messageHandler;
    private readonly ILogger<ChallengeService> _logger;
    private readonly IGameService _gameService;
    private readonly IPlayerService _playerService;

    public ChallengeService(IChallengeStorage challengeStorage, IGroupService groupService,
        IChessClockManager clockManager, IChessLogic chessLogic, IMapper mapper, IWebSocketHandler webSocketHandler,
        ILogger<ChallengeService> logger, IGameService gameService,
        IPlayerService playerService)
    {
        _challengeStorage = challengeStorage;
        _groupService = groupService;
        _clockManager = clockManager;
        _chessLogic = chessLogic;
        _mapper = mapper;
        _messageHandler = webSocketHandler;
        _logger = logger;
        _gameService = gameService;
        _playerService = playerService;
    }

    public async Task<List<ChallengeDto>> GetPublic()
        => _mapper.Map<List<ChallengeDto>>(await _challengeStorage.GetPublicChallenges());

    public async Task<List<ChallengeDto>> Get(long playerId)
    {
        var allChallenges = (await _challengeStorage.GetPublicChallenges())
        .Union(await _challengeStorage.GetPlayerChallenges(playerId));
            
        return _mapper.Map<List<ChallengeDto>>(allChallenges);
    }
        

    public async Task<List<ChallengeDto>> GetPrivate(long playerId)
        => _mapper.Map<List<ChallengeDto>>(await _challengeStorage.GetPrivateChallenges(playerId));

    public async Task Create(CreateChallengeModel model)
    {
        try
        {
            var fromPlayer = await _playerService.GetPlayer(model.FromPlayerId);
            var toPlayer = model.ToPlayerId != 0 ? await _playerService.GetPlayer(model.ToPlayerId) : NullPlayer.GetInstance();
            var timeControl = _mapper.Map<TimeControl>(model.TimeControl);
            await ValidateChallengeCreate(fromPlayer, toPlayer);
            var newChallenge =
                await _challengeStorage.CreateChallenge(fromPlayer, toPlayer, timeControl, model.SideColor);
            await CreateReceiversGroup(newChallenge);
            await SendResultChallenge(_mapper.Map<ChallengeDto>(newChallenge));

            _logger.LogDebug("{} created", newChallenge.ToString());
        }
        catch (ChessException e)
        {
            await HandleChessException(model.FromPlayerId, e);
        }
    }

    private async Task ValidateChallengeCreate(Player fromPlayer, Player toPlayer)
    {
        if (fromPlayer.IsNull)
            throw new UnauthorizedAccessException();
        if (fromPlayer.Equals(toPlayer))
            throw new ChessException("You cannot make challenge to yourself");
        if (!(await GeCurrentGame(fromPlayer.Id)).IsNull)
            throw new ChessException("Please finish current game.");
        if (!toPlayer.IsNull && !(await GeCurrentGame(toPlayer.Id)).IsNull)
            throw new ChessException($"Player {toPlayer.Name} is playing now");
    }

    private async Task SendResultChallenge(ChallengeDto challenge)
    {
        if (!challenge.IsNull)
        {
            await _messageHandler.SendMessageAsync(challenge.FromPlayer.Id,
                new DataResponse(DataResponseType.System, "Challenge created!"));
            if (challenge.ToPlayer is { IsNull: false })
                await _messageHandler.SendMessageToGroupAsync(challenge.Id,
                    new ChallengeResponse(ChallengeResponseType.Challenge)
                        { Challenge = challenge });
            else
                await SendChallengesUpdate();
        }
        else
            throw new ChessException("Challenge create error.");
    }

    private async Task CreateReceiversGroup(Challenge challenge)
    {
        await _groupService.AddPlayerToGroup(challenge.Id, challenge.FromPlayer.Id);
        if (!challenge.ToPlayer.IsNull)
            await _groupService.AddPlayerToGroup(challenge.Id, challenge.ToPlayer.Id);
    }

    public async Task Accept(long playerId, Guid challengeId)
    {
        try
        {
            var challenge = await _challengeStorage.Get(challengeId);
            await ValidateJoinGame(playerId, challenge);
            var game = EntitiesFactory.GetGame(challenge).SetPlayersByColor(playerId, challenge);
            game = _clockManager.SetUpClock(challenge, game);
            _chessLogic.StartGame(game.Id);
            await LoadPlayersDetails(game);
            PgnBuilder.LoadInitialPgn(game);
            if (challenge.ToPlayer.IsNull)
                await _groupService.AddPlayerToGroup(challenge.Id, playerId);

            await _gameService.Cache(game);
            var gameDto = _mapper.Map<GameDto>(game);
            await SendChallengeAccept(gameDto);
            await _gameService.Save(game);
            await _challengeStorage.RemoveCurrentChallenges(gameDto);
            await SendChallengesUpdate();
            _logger.LogDebug("{} has been started!", game);
        }
        catch (ChessException e)
        {
            await HandleChessException(playerId, e);
        }
    }

    private async Task ValidateJoinGame(long playerId, Challenge challenge)
    {
        if (!(await GeCurrentGame(playerId)).IsNull)
            throw new ChessException("Only one game can be play at the moment");
        if (challenge.IsNull || playerId.Equals(challenge.FromPlayer.Id))
            throw new ChessException("Incorrect challenge.");
        if (!challenge.ToPlayer.IsNull && !playerId.Equals(challenge.ToPlayer.Id))
            throw new ChessException("Incorrect challenge.");
    }

    private async Task LoadPlayersDetails(Game game)
    {
        foreach (var gamePlayer in game.GamePlayers)
            gamePlayer.Player = await _playerService.GetPlayer(gamePlayer.PlayerId);
    }

    private async Task SendChallengeAccept(GameDto game)
    {
        await _messageHandler.SendMessageToGroupAsync(game.Id,
            new ChallengeResponse(ChallengeResponseType.ChallengeAccept)
                { Game = game, ServerTime = DateTime.UtcNow });
    }

    public async Task Remove(long playerId, Guid challengeId)
    {
        try
        {
            var challenge = await _challengeStorage.Get(challengeId);

            ValidateChallengeRemove(challenge, playerId);
            await _challengeStorage.Remove(challengeId);
            await SendRemoveResult(playerId, challenge);
            await _groupService.Remove(challenge.Id);
            _logger.LogDebug("Player {} removed {}", playerId, challenge);
        }
        catch (ChessException e)
        {
            await HandleChessException(playerId, e);
        }
    }

    public async Task RemoveCurrentChallenges(long playerId)
    {
        var playerChallenges = await _challengeStorage.GetPlayerChallenges(playerId);
        var tasks = playerChallenges.Select(ch => _challengeStorage.Remove(ch.Id));
        await Task.WhenAll(tasks);
    }

    private static void ValidateChallengeRemove(Challenge challenge, long playerId)
    {
        if (challenge.IsNull)
            throw new ChessException("Challenge is not exists.");
        if (challenge.FromPlayer.Id != playerId)
            throw new ChessException("Error. Not owner of challenge.");
    }

    private async Task SendRemoveResult(long playerId, Challenge challenge)
    {
        if (challenge.ToPlayer.IsNull)
            await _messageHandler.SendMessageToAllAsync(
                new ChallengeResponse(ChallengeResponseType.Challenges)
                {
                    Challenges =
                        _mapper.Map<List<ChallengeDto>>(await _challengeStorage.GetPublicChallenges())
                });
        else
            await _messageHandler.SendMessageToGroupAsync(challenge.Id,
                new ChallengeResponse(ChallengeResponseType.ChallengeRemove) { ChallengeId = challenge.Id });

        await _messageHandler.SendMessageAsync(playerId,
            new DataResponse(DataResponseType.System, "Challenge removed."));
    }

    public async Task Decline(long playerId, Guid challengeId)
    {
        try
        {
            var challenge = await _challengeStorage.Get(challengeId);
            ValidateChallengeDecline(challenge, playerId);

            await _challengeStorage.Remove(challengeId);
            await _groupService.Remove(challengeId);
            await _messageHandler.SendMessageAsync(challenge.FromPlayer.Id,
                new ChallengeResponse(ChallengeResponseType.ChallengeDecline)
                    { ChallengeId = challengeId });
            _logger.LogDebug("Player {} declined {}", playerId, challenge.ToString());
        }
        catch (ChessException e)
        {
            await HandleChessException(playerId, e);
        }
    }

    private async Task<GameDto> GeCurrentGame(long playerId) => await _gameService.GetCurrentGame(playerId);

    private static void ValidateChallengeDecline(Challenge challenge, long playerId)
    {
        if (challenge.IsNull)
            throw new ChessException("Challenge is not exists.");
        if (challenge.ToPlayer.Id != playerId)
            throw new ChessException("Error. Not owner of challenge.");
    }

    private async Task SendChallengesUpdate()
    {
        await _messageHandler.SendMessageToAllAsync(new ChallengeResponse(ChallengeResponseType.Challenges)
            { Challenges = await GetPublic() });
    }

    private async Task HandleChessException(long playerId, ChessException e)
    {
        await _messageHandler.SendMessageAsync(playerId, new DataResponse(DataResponseType.Exception, e.Message));
        await SendChallengesUpdate();
        _logger.LogError("{}", e.Message);
    }
}