using ChessRules;
using WebSocketModels.Models;

namespace WoodChessV1.Services.ChallengeServices;

public class CreateChallengeModel
{
    public long FromPlayerId { get; set; }
    public long ToPlayerId { get; set; }
    public SideColor SideColor { get; set; }
    public TimeControlDto TimeControl { get; set; }
}