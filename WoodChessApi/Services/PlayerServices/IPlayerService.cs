using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using WebSocketModels.Enums;
using WebSocketModels.Models;
using WoodChessV1.Models;

namespace WoodChessV1.Services.PlayerServices;

public interface IPlayerService
{
    Task Create(string name, IPAddress ipAddress);
    Task<List<PlayerDto>> GetAll();
    Task<List<PlayerDto>> GetOnline();
    Task<PlayerDto> Get(long id);
    Task<PlayerDto> Get(string name);
    Task<Player> GetPlayer(long id);
    Task<double> GetPlayerRating(long id, RatingType ratingType);
    Task Cache(Player player);
    Task UpdateRating(long id, RatingType ratingType, double value);
    Task UpdateRatings(GameDto game);
    Task UpdateLoginTrack(PlayerDto player, IPAddress ipAddress);
}