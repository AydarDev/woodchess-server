using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.Extensions.DependencyInjection;
using WebSocketModels.Enums;
using WebSocketModels.Models;
using WoodChessV1.Models;
using WoodChessV1.Models.Factories;
using WoodChessV1.Repository.Abstract;
using WoodChessV1.Repository.Caches;

namespace WoodChessV1.Services.PlayerServices;

public class PlayerService : IPlayerService
{
    private readonly IMapper _mapper;
    private readonly IServiceScopeFactory _scopeFactory;
    private readonly ICacheService<long, Player> _cache;

    public PlayerService(IMapper mapper, IServiceScopeFactory scopeFactory, ICacheService<long, Player> cache)
    {
        _mapper = mapper;
        _scopeFactory = scopeFactory;
        _cache = cache;
    }

    public async Task Create(string name, IPAddress ipAddress)
    {
        using var repo = GetRepository();
        await repo.Add(EntitiesFactory.GetPlayer(name, ipAddress));
        await repo.Save();
    }
    public async Task<List<PlayerDto>> GetAll()
    {
        using var repo = GetRepository();
        var players = await repo.GetAll();
        foreach (var player in players) player.GetOnlineStatus();
        return _mapper.Map<List<PlayerDto>>(players);
    }

    public async Task<List<PlayerDto>> GetOnline()
    {
        using var repo = GetRepository();
        var players = (await repo.GetAll())
            .Select(p => p.GetOnlineStatus())
            .Where(p => p.IsOnline);
        
        return _mapper.Map<List<PlayerDto>>(players);
    }

    public async Task<PlayerDto> Get(long id)
    {
        var player = await GetPlayer(id);
        return _mapper.Map<PlayerDto>(player);
    }

    public async Task<Player> GetPlayer(long id)
    {
        var cachedPlayer = await _cache.Get(id);
        if (cachedPlayer != null)
            return cachedPlayer;
        
        using var repo = GetRepository();
        var player = (await repo.Get(id)).GetOnlineStatus();
        if(!player.IsNull)
            await _cache.Set(id, player);

        return player;
    }

    public async Task<PlayerDto> Get(string name)
    {
        using var repo = GetRepository();
        var player = (await repo.Get(name)).GetOnlineStatus();
        return _mapper.Map<PlayerDto>(player);
    }

    public async Task<double> GetPlayerRating(long id, RatingType ratingType)
    {
        var player = await GetPlayer(id);
        return !player.IsNull ? player[ratingType] : -1;
    }
    private async Task Update(Player player)
    {
        await _cache.Set(player.Id, player);
        using var repo = GetRepository();
        repo.Update(player);
        await repo.Save();
    }

    public async Task Cache(Player player)
    {
        await _cache.Set(player.Id, player);
    }
    public async Task UpdateRating(long id, RatingType ratingType, double value)
    {
        using var repo = GetRepository();
        var player = await GetPlayer(id);
        var rating = player.Ratings.First(r => r.RatingType == ratingType);
        rating.Value = value;
        repo.Update(rating);
        await _cache.Set(id, player);
        await repo.Save();
    }

    public async Task UpdateRatings(GameDto game)
    {
        using var repo = GetRepository();
        foreach (var gamePlayer in game.GamePlayers)
        {
            var player = await repo.Get(gamePlayer.Player.Id);
            var rating = player.Ratings.First(r => r.RatingType == game.RatingType);
            rating.Value = gamePlayer.RatingDeviation.NewRating;
            repo.Update(rating);
            await _cache.Set(player.Id, player);
        }
        await repo.Save();
    }

    public async Task UpdateLoginTrack(PlayerDto playerDto, IPAddress ipAddress)
    {
        var player = _mapper.Map<Player>(playerDto);
        player.LastLoginTime = DateTime.UtcNow;
        player.IpAddress = ipAddress;
        await Update(player);
    }

    private IPlayerRepository GetRepository()
    {
        return _scopeFactory.CreateScope().ServiceProvider.GetService<IPlayerRepository>();
    }
}