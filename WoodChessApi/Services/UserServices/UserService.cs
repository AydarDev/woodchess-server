﻿using System;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Net;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.IdentityModel.Tokens;
using WebSocketModels.Models;
using WoodChessV1.Models.UserManager;
using WoodChessV1.Services.PlayerServices;
using static WebSocketModels.EncryptionHelper;

namespace WoodChessV1.Services.UserServices
{
    public class UserService : IUserService
    {
        private readonly UserManager<IdentityUser> _userManager;
        private readonly IConfiguration _configuration;
        private readonly IPlayerService _playerService;
        private readonly IWebHostEnvironment _env;
        private readonly IMapper _mapper;
        

        public UserService(UserManager<IdentityUser> userManager, IConfiguration configuration,
            IPlayerService playerService, IWebHostEnvironment env, IMapper mapper)
        {
            _userManager = userManager;
            _configuration = configuration;
            _playerService = playerService;
            _env = env;
            _mapper = mapper;
        }

        public async Task<UserManagerResponse> LoginUserAsync(LoginViewModel model, IPAddress ipAddress)
        {
            var plainUserName = Decrypt(model.UserName, networkKey);
            var plainPassword = Decrypt(model.Password, networkKey);
            
            var user = await _userManager.FindByNameAsync(plainUserName);
            var currPlayer = await _playerService.Get(plainUserName);
            var success = user != null && currPlayer != null &&
                          await _userManager.CheckPasswordAsync(user, plainPassword);

            if (!success)
                return new UserManagerResponse
                {
                    Message = "UserName or password is incorrect",
                    IsSuccess = false
                };
            
            await _playerService.UpdateLoginTrack(currPlayer, ipAddress);
            var claims = new[]
            {
                new Claim(ClaimTypes.NameIdentifier, user.Id),
                new Claim(ClaimTypes.Name, user.UserName),
                new Claim(ClaimTypes.Email, user.Email)
            };

            var authKey = _env.IsDevelopment()
                ? _configuration["AuthSettings:Key"]
                : Environment.GetEnvironmentVariable("AUTH_KEY");
            
            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(authKey));
            var token = new JwtSecurityToken(
                issuer: _configuration["AuthSettings:Issuer"],
                audience: _configuration["AuthSettings:Audience"],
                claims: claims,
                expires: DateTime.Now.AddDays(30),
                signingCredentials: new SigningCredentials(key, SecurityAlgorithms.HmacSha256)
            );

            string tokenString = new JwtSecurityTokenHandler().WriteToken(token);

            return new UserManagerResponse
            {
                CurrPlayer = _mapper.Map<PlayerDto>(currPlayer),
                Message = tokenString,
                IsSuccess = true,
                ExpireDate = token.ValidTo
            };
        }

        public async Task<UserManagerResponse> RegisterUserAsync(RegisterViewModel model, IPAddress ipAddress)
        {
            var plainUserName = Decrypt(model.UserName, networkKey);
            var plainPassword = Decrypt(model.Password, networkKey);
            var plainPasswordConfirm = Decrypt(model.ConfirmPassword, networkKey);
            var plainEmail = Decrypt(model.Email, networkKey);
            
            if (model == null)
                throw new NullReferenceException("Register model is null");

            IdentityUser user;

            user = await _userManager.FindByNameAsync(plainUserName);
            if (user != null)
                return new UserManagerResponse()
                {
                    Message = "User with this name already exists",
                    IsSuccess = false
                };
            
            if (plainPassword != plainPasswordConfirm)
                return new UserManagerResponse
                {
                    Message = "Confirm password not match",
                    IsSuccess = false
                };

            var identityUser = new IdentityUser
            {
                UserName = plainUserName,
                Email = plainEmail,
            };

            var result = await _userManager.CreateAsync(identityUser, plainPassword);
            

            if (result.Succeeded)
            {
                await _playerService.Create(plainUserName, ipAddress);
                //TODO: Send confirmation email
                return new UserManagerResponse()
                {
                    Message = "User created successfully",
                    IsSuccess = true
                };
            }

            return new UserManagerResponse
            {
                Message = "User did not created",
                IsSuccess = false,
                Errors = result.Errors.Select(er => er.Description)
            };
        }
    }
}