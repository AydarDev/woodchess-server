﻿using System.Net;
using System.Threading.Tasks;
using WoodChessV1.Models.UserManager;

namespace WoodChessV1.Services.UserServices
{
    public interface IUserService
    {
        Task<UserManagerResponse> RegisterUserAsync(RegisterViewModel model, IPAddress ipAddress);
        Task<UserManagerResponse> LoginUserAsync(LoginViewModel model, IPAddress ipAddress);
    }
}