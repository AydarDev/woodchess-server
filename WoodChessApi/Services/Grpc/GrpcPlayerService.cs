using System;
using System.Threading.Tasks;
using Grpc.Core;
using GrpcService;
using Microsoft.Extensions.Logging;
using WebSocketModels.Enums;
using WoodChessV1.Services.PlayerServices;
using PlayerService = GrpcService.PlayerService;

namespace WoodChessV1.Services.Grpc;

public class GrpcPlayerService : PlayerService.PlayerServiceBase
{
    private readonly IPlayerService _players;
    private readonly ILogger<GrpcPlayerService> _logger;

    public GrpcPlayerService(IPlayerService players, ILogger<GrpcPlayerService> logger)
    {
        _players = players;
        _logger = logger;
    }

    public override async Task<PlayerResponse > GetPlayer(PlayerRequest request, ServerCallContext context)
    {
        try
        {
            var player = await _players.GetPlayer(request.Id);
            _logger.LogDebug("[GRPC] Return player data {}", player.Name);
            
            if (!player.IsNull)
                return new PlayerResponse
                {
                    Id = player.Id,
                    Name = player.Name,
                    Rating = player[RatingType.Puzzle]
                };

            return new PlayerResponse { IsNull = true };
        }
        catch (Exception e)
        {
            _logger.LogError("{}", e.Message);
            _logger.LogDebug("{}", e.StackTrace);
            return new PlayerResponse{ IsNull = true };
        }
    }
}