using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using WebSocketModels.Models;
using WoodChessV1.ChessClocks;
using WoodChessV1.Models;

namespace WoodChessV1.Services.GameServices;

public interface IGameService
{
    Task<GameDto> Get(Guid gameId);
    Task<GameDto> GetCurrentGame(long playerId);
    Task<List<GameDto>> GetGames(GameFilter gameFilter);
    Task Cache(Game game);
    Task Save(Game game);
    Task MakeMove(long playerId, Guid gameId, string move);
    Task OfferDraw(long playerId, Guid gameId);
    Task AcceptDraw(long playerId, Guid gameId);
    Task DeclineDraw(long playerId, Guid gameId);
    Task Resign(long playerId, Guid gameId);
    Task InitTimeOver(ChessClock chessClock);
    Task Exit(long playerId, Guid gameId);
}