using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using AutoMapper;
using ChessRules;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using WebSocketModels.Enums;
using WebSocketModels.Models;
using WebSocketModels.WsMessages.WsResponses;
using WoodChessV1.ChessClocks;
using WoodChessV1.ChessEngine;
using WoodChessV1.Exceptions;
using WoodChessV1.Models;
using WoodChessV1.Models.Factories;
using WoodChessV1.Repository.Abstract;
using WoodChessV1.Repository.Caches;
using WoodChessV1.Services.GroupServices;
using WoodChessV1.Services.PlayerServices;
using WoodChessV1.Tools;
using WoodChessV1.WebSocketClasses.WebSocketManager;

namespace WoodChessV1.Services.GameServices;

public class GameService : IGameService
{
    private readonly IWebSocketHandler _chessMessageHandler;
    private readonly IChessLogic _chessLogic;
    private readonly IGroupService _groupService;
    private readonly IMapper _mapper;
    private readonly ICacheService<Guid, Game> _gamesCache;
    private readonly ICacheService<long, CurrentGame> _currGameCache;
    private readonly IServiceScopeFactory _scopeFactory;
    private readonly IRatingHelper _ratingHelper;
    private readonly ILogger<GameService> _logger;
    private readonly IPlayerService _playerService;
    private readonly IChessClockManager _clockManager;
    private const int InitialBlockPeriod = 3;

    private readonly ImmutableDictionary<GameResult, string> _gameResults = new Dictionary<GameResult, string>()
    {
        { GameResult.WhiteWin, "White win" },
        { GameResult.BlackWin, "Black win" },
        { GameResult.Draw, "Draw" }
    }.ToImmutableDictionary();

    private readonly ImmutableDictionary<GameDetailResult, string> _gameDetailResults =
        new Dictionary<GameDetailResult, string>()
        {
            { GameDetailResult.WinByResign, "by resign" },
            { GameDetailResult.WinByCheckmate, "by checkmate" },
            { GameDetailResult.DrawByAgree, "by agree" },
            { GameDetailResult.WinByTimeOver, "by time over" },
            { GameDetailResult.DrawByFiftyMoves, "by fifty moves rule" },
            { GameDetailResult.DrawByStaleMate, "stalemate" },
            { GameDetailResult.DrawByTripleRepetition, "by triple position repetition" }
        }.ToImmutableDictionary();

    public GameService(IWebSocketHandler chessMessageHandler, IChessLogic chessLogic, IPlayerService playerService,
        IChessClockManager clockManager, IGroupService groupService, IServiceScopeFactory scopeFactory,
        IRatingHelper ratingHelper,
        ILogger<GameService> logger, IMapper mapper, ICacheService<Guid, Game> gamesCache, ICacheService<long, CurrentGame> currGameCache)
    {
        _chessMessageHandler = chessMessageHandler;
        _chessLogic = chessLogic;
        _groupService = groupService;
        _mapper = mapper;
        _gamesCache = gamesCache;
        _currGameCache = currGameCache;
        _scopeFactory = scopeFactory;
        _ratingHelper = ratingHelper;
        _logger = logger;
        _playerService = playerService;
        _clockManager = clockManager;
    }

    public async Task<GameDto> Get(Guid gameId)
    {
        var game = await GetGame(gameId);
        return _mapper.Map<GameDto>(game);
    }
    private async Task<Game> GetGame(Guid gameId)
    {
        Game game;
        if (await _gamesCache.IsExists(gameId))
            game = await _gamesCache.Get(gameId);
        else
        {
            using var repo = GetGameRepository();
            game = await repo.Get(gameId);
            await _gamesCache.Set(gameId, game);
        }

        if(!game.IsNull && game.Status == GameStatus.Play)
            _clockManager.GetClock(game);
        return game;
    }
    public async Task<GameDto> GetCurrentGame(long playerId)
    {
        if (await _currGameCache.IsExists(playerId))
        {
            var currGame = await _currGameCache.Get(playerId);
            return await Get(currGame.Id);
        }
            
        using var repo = GetGameRepository();
        var game = await repo.GetCurrentGame(playerId);
        await _currGameCache.Set(playerId, new CurrentGame(game.Id));

        return _mapper.Map<GameDto>(game);
    }

    public async Task Cache(Game game)
    {
        await _gamesCache.Set(game.Id, game);
    }
    public async Task Save(Game game)
    {
        using var repo = GetGameRepository();
        await repo.Add(game);
        await repo.Save();
        await Cache(game);
        var currGame = new CurrentGame(game.Id);
        foreach (var gamePlayer in game.GamePlayers) 
            await _currGameCache.Set(gamePlayer.PlayerId, currGame);
    }

    private async Task UpdateGame(params object[] gameEntities)
    {
        using var repo = GetGameRepository();
        foreach (var entity in gameEntities) repo.Update(entity);
        await repo.Save();
    }


    public async Task<List<GameDto>> GetGames(GameFilter gameFilter)
    {
        var predicates = BuildPredicates(gameFilter);
        using var repo = GetGameRepository();

        var games = await repo.GetGames(predicates);
        return _mapper.Map<List<GameDto>>(games);
    }

    private IEnumerable<Expression<Func<Game, bool>>> BuildPredicates(GameFilter filter)
    {
        if (filter.PlayerId != null)
        {
            if (filter.Color != null)
                yield return g =>
                    g.GamePlayers.Any(gp => gp.PlayerId == filter.PlayerId && gp.Color == filter.Color);
            else
                yield return g => g.Players.Any(p => p.Id == filter.PlayerId);
        }
        if (filter.Result != null)
            yield return g => g.Result == filter.Result;
        if (filter.Status != null)
            yield return g => g.Status == filter.Status;
    }

    public async Task MakeMove(long playerId, Guid gameId, string moveValue)
    {
        try
        {
            var timeStamp = DateTime.UtcNow;
            Console.WriteLine("Trying to get game");
            var game = await GetGame(gameId);

            ValidateMove(playerId, game);
            game = _chessLogic.MakeMove(game, moveValue);

            _clockManager.GetClock(game).Swap(timeStamp);
            _clockManager.SetTimeValues(game);

            ProcessDrawInfo(playerId, game);
            var newMove = EntitiesFactory.GetMove(game, playerId, moveValue);
            await _chessMessageHandler.SendMessageToGroupAsync(game.Id,
                new GameResponse(GameResponseType.Move) { Move = _mapper.Map<MoveDto>(newMove) });

            if (game.Status.Equals(GameStatus.Done))
            {
                await FinalizeGame(game);
                return;
            }

            await Cache(game);
            await UpdateGame(game, game.TimeInfo);
        }
        catch (ChessException e)
        {
            await HandleChessException(playerId, e);
        }
    }

    private static void ValidateMove(long playerId, Game game)
    {
        if (game.GamePlayers.All(p => p.PlayerId != playerId))
            throw new ChessException("Not game owner!");
        if (game.Status != GameStatus.Play)
            throw new ChessException("Game has been finished.");
        if (!CanPlayerMove(game, playerId))
            throw new ChessException("Not your turn!");
    }

    private void ProcessDrawInfo(long playerId, Game game)
    {
        if (game.DrawInfo.IsNull) return;
        if (game.DrawInfo.DrawFlag == DrawFlag.Offer && game.DrawInfo.FromPlayerId != playerId)
        {
            game.DrawInfo.SetDrawFlag(DrawFlag.Decline, playerId, InitialBlockPeriod);
            SendDrawResponse(game);
            _logger.LogDebug("Draw was declined in game {}", game);
        }

        if (game.DrawInfo.BlockCounter < 0) return;
        game.DrawInfo.BlockCounter--;
        if (game.DrawInfo.BlockCounter == 0)
        {
            game.DrawInfo.ResetDrawFlag();
            SendDrawResponse(game);
        }
    }

    private static bool CanPlayerMove(Game game, long playerId)
    {
        return game.CurrPly % 2 == 1 && game.GamePlayers.FirstOrDefault(gp => gp.Color == SideColor.White)!.PlayerId ==
               playerId ||
               game.CurrPly % 2 == 0 && game.GamePlayers.FirstOrDefault(gp => gp.Color == SideColor.Black)!.PlayerId ==
               playerId;
    }

    private void SetGameTextResult(Game game)
    {
        var gameDetailResult = game.DetailResult!;
        game.TextResult = $"{_gameResults[game.Result!.Value]} {_gameDetailResults[gameDetailResult!.Value]}";
    }

    public async Task OfferDraw(long playerId, Guid gameId)
    {
        var game = await GetGame(gameId);

        if (game.Status != GameStatus.Play)
            return;
        game.DrawInfo.SetDrawFlag(DrawFlag.Offer, playerId);
        await Cache(game);
        SendDrawResponse(game);
        await UpdateGame(game.DrawInfo);
    }

    public async Task AcceptDraw(long playerId, Guid gameId)
    {
        try
        {
            var game = await GetGame(gameId);
            await ValidateDrawOperation(playerId, game);
            game.Result = GameResult.Draw;
            game.DetailResult = GameDetailResult.DrawByAgree;
            await FinalizeGame(game);
            _logger.LogDebug("Opponents agreed to draw in game {}", game);
        }
        catch (ChessException e)
        {
            await HandleChessException(playerId, e);
        }
    }

    public async Task DeclineDraw(long playerId, Guid gameId)
    {
        try
        {
            var game = await GetGame(gameId);
            await ValidateDrawOperation(playerId, game);
            game.DrawInfo.SetDrawFlag(DrawFlag.Decline, playerId, InitialBlockPeriod);
            await Cache(game);
            SendDrawResponse(game);
            await UpdateGame(game.DrawInfo);
            _logger.LogDebug("Draw was declined in game {}", game);
        }
        catch (ChessException e)
        {
            await HandleChessException(playerId, e);
        }
    }

    private async Task ValidateDrawOperation(long playerId, Game game)
    {
        if (game.GamePlayers.All(p => p.PlayerId != playerId))
        {
            var player = await _playerService.Get(playerId);
            throw new ChessException($"{player.Name} not owner in {game}!");
        }

        if (game.IsNull)
            throw new Exception("Game not found.");
        if (game.DrawInfo.DrawFlag != DrawFlag.Offer)
            throw new ChessException($"Draw was not offered in {game}");
        if (game.DrawInfo.FromPlayerId == playerId)
        {
            var player = await _playerService.Get(playerId);
            throw new ChessException($"Player {player.Name} offered a draw and cannot accept or decline a draw");
        }
    }

    private void SendDrawResponse(Game game)
    {
        _chessMessageHandler.SendMessageToGroupAsync(game.Id, new GameResponse
        {
            GameResponseType = GameResponseType.Draw,
            DrawInfo = _mapper.Map<DrawInfoDto>(game.DrawInfo)
        });
    }

    public async Task Resign(long playerId, Guid gameId)
    {
        try
        {
            var game = await GetGame(gameId);
            if (game.Status != GameStatus.Play)
                return;
            await ValidateResignGame(playerId, game);
            SetGameResults(playerId, game);
            await FinalizeGame(game);
            _logger.LogDebug("Player {} resigned in game {}", playerId, game);
        }
        catch (ChessException e)
        {
            await HandleChessException(playerId, e);
        }
    }

    private async Task ValidateResignGame(long playerId, Game game)
    {
        if (game.IsNull)
            throw new Exception("game not found");
        if (game.GamePlayers.All(p => p.PlayerId != playerId))
        {
            var player = await _playerService.Get(playerId);
            throw new ChessException($"{player.Name} not owner in {game}!");
        }
    }

    private static void SetGameResults(long playerId, Game game)
    {
        var gameResult = GameResult.None;
        if (game.GamePlayers
                .FirstOrDefault(gp => gp.Color == SideColor.White)!.PlayerId == (playerId))
            gameResult = GameResult.BlackWin;
        else if (game.GamePlayers
                     .FirstOrDefault(gp => gp.Color == SideColor.Black)!.PlayerId == (playerId))
            gameResult = GameResult.WhiteWin;
        game.Result = gameResult;
        game.DetailResult = GameDetailResult.WinByResign;
    }

    public async Task InitTimeOver(ChessClock chessClock)
    {
        var game = await GetGame(chessClock.GameId);
        try
        {
            ValidateTimeOverGame(game);
            var result = chessClock.ToMoveSide == SideColor.White ? GameResult.BlackWin : GameResult.WhiteWin;
            game.Result = result;
            game.DetailResult = GameDetailResult.WinByTimeOver;
            await FinalizeGame(game);
            _logger.LogDebug("Time expired in game {}", game);
        }
        catch (ChessException e)
        {
            await HandleChessException(game, e);
        }
        catch (Exception e)
        {
            _logger.LogError("{}", e.Message);
        }
    }

    private async Task FinalizeGame(Game game)
    {
        try
        {
            game.Status = GameStatus.Done;
            _clockManager.SetTimeValues(game);
            _clockManager.GetClock(game).StopClock();
            _clockManager.Remove(game.Id);
            _chessLogic.FinishGame(game.Id);
            _ratingHelper.CalculatePlayerRatings(game);
            SetGameTextResult(game);
            PgnBuilder.SetResult(game);
            await _chessMessageHandler.SendMessageToGroupAsync(game.Id,
                new GameResponse(GameResponseType.Game) { Game = _mapper.Map<GameDto>(game) });

            await _groupService.Remove(game.Id);
            await UpdateGame(game, game.TimeInfo, game.DrawInfo);
            await _playerService.UpdateRatings(_mapper.Map<GameDto>(game));
            await _gamesCache.Remove(game.Id);
            foreach (var gamePlayer in game.GamePlayers)
                await _currGameCache.Set(gamePlayer.PlayerId, CurrentGame.GetNull());
        }
        catch (Exception e)
        {
            Console.WriteLine(e);
            await _gamesCache.Remove(game.Id);
        }
    }

    private static void ValidateTimeOverGame(Game game)
    {
        if (game.IsNull)
            throw new Exception("Time over. But Game not found");
        if (game.Status != GameStatus.Play)
            throw new ChessException($"Time over. But game is not playing. Game {game.Id}");
    }

    public async Task Exit(long playerId, Guid gameId)
    {
        var player = await _playerService.Get(playerId);
        await _groupService.RemovePlayerFromGroup(gameId, player.Id);
        var exitMessage = $"Player {player.Name} left the game.";
        _logger.LogDebug("Player {} left the game", player.Name);
        await _chessMessageHandler.SendMessageToGroupAsync(gameId,
            new DataResponse(DataResponseType.System, exitMessage));
    }
    private IGameRepository GetGameRepository() => 
        _scopeFactory.CreateScope().ServiceProvider.GetService<IGameRepository>();
    private async Task HandleChessException(long playerId, ChessException e)
    {
        await _chessMessageHandler.SendMessageAsync(playerId,
            new DataResponse(DataResponseType.Exception) { Data = e.Message });
        _logger.LogError("{}", e.Message);
    }

    private async Task HandleChessException(Game game, ChessException e)
    {
        await _chessMessageHandler.SendMessageToGroupAsync(game.Id,
            new DataResponse(DataResponseType.System, e.Message));
        _logger.LogError("{}", e.Message);
    }
}