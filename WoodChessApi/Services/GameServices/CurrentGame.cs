using System;
namespace WoodChessV1.Services.GameServices;

[Serializable]
public class CurrentGame
{
    public Guid Id { get; set;}
    public CurrentGame(Guid gameId)
    {
        Id = gameId;
    }

    public static NullCurrentGame GetNull() => NullCurrentGame.GetInstance();
}

public class NullCurrentGame : CurrentGame
{
    private static NullCurrentGame _game;
    private NullCurrentGame() : base(Guid.Empty) { }
    public static NullCurrentGame GetInstance() => _game ??= new NullCurrentGame();
}