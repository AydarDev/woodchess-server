using ChessRules;
using WebSocketModels.Enums;

namespace WoodChessV1.Services.GameServices;

public class GameFilter
{
    public GameStatus? Status { get; set; } 
    public long? PlayerId { get; set; }
    public SideColor? Color { get; set; }
    public GameResult? Result { get; set; }
}