using Newtonsoft.Json;

namespace WoodChessV1.Misc;

public class JsonSettings
{
    public static readonly JsonSerializerSettings Settings = new()
    {
        PreserveReferencesHandling = PreserveReferencesHandling.Objects,
        Formatting = Formatting.Indented
    };
}