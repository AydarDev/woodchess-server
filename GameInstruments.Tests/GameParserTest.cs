using System;
using System.Collections.Generic;
using GameInstruments;
using GameInstruments.Models;
using GameInstruments.TreeBuilders;
using GameInstruments.Utils;
using WebSocketModels.Enums;
using Xunit;
using static GameInstruments.Tests.TestInfo;

namespace GameInstruments.Tests
{
    public class GameParserTest
    {
        [Fact]
        public void ParseSimpleGameTest()
        {
            var fen = "8/8/3K4/8/3k4/5Q2/2R5/8 w - - 0 1";
            var gameLine = "1. Qh3 Ke4 2. Rc4# 1-0";
            var builder = TreeBuilder.GetTreeBuilder(DataType.Pgn).Init(fen, gameLine);
            if (!builder.IsNull) builder.CreateGameTree();
        }

        [Fact]
        public void ParseGameWithVariationTest()
        {
            var fen = "1k6/8/3K4/8/8/7B/8/7Q w - - 0 1";
            var gameLine = "1.Bc8 Ka7 ( 1...Kxc8 2.Qa8# ) 2.Qb7# 1-0";
            var builder = TreeBuilder.GetTreeBuilder(DataType.Pgn).Init(fen, gameLine);
            if (!builder.IsNull) builder.CreateGameTree();
        }

        [Fact]
        public void ParseSimplePgnTest()
        {
            var source = $"{TestDataDir}/Puzzle2.pgn";
            var pgn = IoHelper.GetPgnFromFile(source);
            var parser = new GameParser();

            parser.ParsePgn(pgn);
        }

        [Fact]
        public void ParsePgnWithoutFen()
        {
            string pgn = "[Variant \"From Position\"]\n1. e4 e5 2. Nf3 Nc6 3. Bb5"; 
            GameParser parser = new GameParser();
            GameInfo resultGameInfo = parser.ParsePgn(pgn);
        }

        [Fact]
        public void ParsePgnWithManyLines()
        {
            string source = $"{TestDataDir}/tree.pgn";
            string pgn = IoHelper.GetPgnFromFile(source);
            GameParser parser = new GameParser();

            GameInfo resultGameInfo = parser.ParsePgn(pgn);
        }

        [Fact]
        public void ParseSmallPgnDb()
        {
            string file = $"{TestDataDir}/smallDbFile.txt";
            GameParser parser = new GameParser();
            List<string> pgnList = parser.GetPgnList(file);
            
            Assert.Equal(10, pgnList.Count);
        }

        [Fact]
        public void BuildPuzzleNameFromStandardPgn()
        {
            string pgn = IoHelper.GetPgnFromFile($"{TestDataDir}/StandartPgn1.pgn");
            GameParser parser = new GameParser();
            string name = parser.GetNameFromStandardPuzzlePgn(pgn);
            
            Assert.Equal("Deutsche Schachzeitung, 9 AUTHORS, 1968", name);
        }

        [Fact]
        public void ParsePgnWithPreMove()
        {
            string source = $"{TestDataDir}/PremovePgn.pgn";
            string pgn = IoHelper.GetPgnFromFile(source);
            GameParser parser = new GameParser();

            GameInfo resultGameInfo = parser.ParsePgn(pgn, true);
        }
        
        [Fact]
        public void ParsePgnWithPreMove1()
        {
            string source = $"{TestDataDir}/PremovePgn1.pgn";
            string pgn = IoHelper.GetPgnFromFile(source);
            GameParser parser = new GameParser();

            GameInfo resultGameInfo = parser.ParsePgn(pgn, true);
        }

        [Fact]
        public void ParseSimpleCsv()
        {
            var parser = new GameParser();
            parser.ParseCsv(Csv, true);
        }
    }
}