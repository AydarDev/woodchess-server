﻿namespace GameInstruments.Tests
{
    public class TestInfo
    {
        internal const string TestDataDir = "../../../TestData";
        internal const string  Csv =
            @"00008,r6k/pp2r2p/4Rp1Q/3p4/8/1N1P2R1/PqP2bPP/7K b - - 0 24,f2g3 e6e7 b2b1 b3c1 b1c1 h6c1,1855,75,97,744,crushing hangingPiece long middlegame,https://lichess.org/787zsVup/black#48";
    }
}