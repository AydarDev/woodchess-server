﻿using GameInstruments;
using GameInstruments.Models;
using GameInstruments.Utils;
using Xunit;
using static GameInstruments.Tests.TestInfo;
using static GameInstruments.Models.ResultType;

namespace GameInstruments.Tests
{
    public class PuzzleEngineTests
    {
        [Fact]
        public void ResolveSimplePuzzle()
        {
            PuzzleEngine puzzleEngine = new PuzzleEngine();
            string pgn = IoHelper.GetPgnFromFile($"{TestDataDir}/Puzzle1.pgn");
            puzzleEngine.LoadPuzzle(pgn);

            string wrongMove = "Qf3g3";
            GuessMoveResponse response = puzzleEngine.GuessMove(wrongMove);
            Assert.Equal(ResultType.Fail, response.Result);
            Assert.True(response.ResponsePly.IsNull);
                
            string firstMove = "Qf3h3";
            response = puzzleEngine.GuessMove(firstMove);
            Assert.Equal("kd4e4", response.Move);
            Assert.Equal(ResultType.MoveSolved, response.Result);

            string secondMove = "Rc2c4";
            response = puzzleEngine.GuessMove(secondMove);
            Assert.Equal(ResultType.PuzzleSolved, response.Result);
            Assert.True(response.ResponsePly.IsNull);
        }

        [Fact]
        public void ResolveMateInThreeWithOneVariation()
        {
            PuzzleEngine puzzleEngine = new PuzzleEngine();
            puzzleEngine.LoadPuzzle(IoHelper.GetPgnFromFile($"{TestDataDir}/Puzzle2.pgn"));
            GuessMoveResponse response = puzzleEngine.GuessMove("Bh3c8");
            Assert.Equal("kb8c8", response.Move);

            response = puzzleEngine.GuessMove("Qh1a8");
            Assert.Equal("kb8a7", response.Move);

            response = puzzleEngine.GuessMove("Qh1b7");
            Assert.Equal(PuzzleSolved, response.Result);
        }

        [Fact]
        public void ResolveMateInThreeWithManyVariations()
        {
            PuzzleEngine puzzleEngine = new PuzzleEngine();
            puzzleEngine.LoadPuzzle(IoHelper.GetPgnFromFile($"{TestDataDir}/Puzzle3.pgn"));

            GuessMoveResponse response = puzzleEngine.GuessMove("Qh5h1");
            Assert.Equal("rf8f7", response.Move);

            response = puzzleEngine.GuessMove("Qh1h8");
            Assert.Equal("rf7f8", response.Move);

            response = puzzleEngine.GuessMove("Qh8f8");
            Assert.Equal("rf8b8", response.Move);

            response = puzzleEngine.GuessMove("Rb4b6");
            Assert.Equal("rb8c8", response.Move);

            response = puzzleEngine.GuessMove("Qh1b7");
            Assert.Equal("pa7a5", response.Move);

            response = puzzleEngine.GuessMove("Rb6a6");
            Assert.Equal("pa7a6", response.Move);

            response = puzzleEngine.GuessMove("Rb6a6");
            Assert.Equal(PuzzleSolved, response.Result);

        }

        [Fact]
        public void SolvePuzzleWithPreMove()
        {
            PuzzleEngine puzzleEngine = new PuzzleEngine();
            puzzleEngine.LoadPuzzle(IoHelper.GetPgnFromFile($"{TestDataDir}/PremovePgn.pgn"), true);
            
            var response = puzzleEngine.GuessMove("rd7d1");
            Assert.Equal("Ra1d1", response.Move);

            response = puzzleEngine.GuessMove("rd8d1");
            Assert.Equal(ResultType.PuzzleSolved, response.Result);
        }
        
        [Fact]
        public void SolvePuzzleWithAdditionalOpponentMoveInTheEnd()
        {
            PuzzleEngine puzzleEngine = new PuzzleEngine();
            puzzleEngine.LoadPuzzle(IoHelper.GetPgnFromFile($"{TestDataDir}/PremovePgn1.pgn"), true);
            
            var response = puzzleEngine.GuessMove("Qf3h5");
            Assert.Equal("re8e1", response.Move);

            response = puzzleEngine.GuessMove("Kf1e1");
            Assert.Equal(ResultType.PuzzleSolved, response.Result);
        }

        [Fact]
        public void SolveSimplePuzzleFromCsv()
        {
            PuzzleEngine puzzleEngine = new PuzzleEngine();
            puzzleEngine.LoadPuzzleFromCsv(Csv, true);

            var response = puzzleEngine.GuessMove("Re6e7");
            Assert.Equal("qb2b1", response.Move);

            response = puzzleEngine.GuessMove("Nb3c1");
            Assert.Equal("qb1c1", response.Move);

            response = puzzleEngine.GuessMove("Qh6c1");
            Assert.Equal(PuzzleSolved, response.Result);
        }
    }
}