[Event "Deutsche Schachzeitung"]
[Site "?"]
[Date "1968.??.??"]
[Round "-"]
[White "9 AUTHORS"]
[Black "#2"]
[Result "1-0"]
[FEN "8/8/8/8/1Q6/1K6/8/2Nk4 w - - 0 1"]
[SetUp "1"]

1. Qa5 Kxc1 2. Qe1# 1-0

[Event "Illustrated London News"]
[Site "?"]
[Date "1849.??.??"]
[Round "-"]
[White "A. B. S."]
[Black "#2"]
[Result "1-0"]
[FEN "8/8/5R2/8/2P1k3/2K5/5P2/2B5 w - - 0 1"]
[SetUp "1"]

1. Bb2 Ke5 2. Kd3# 1-0

[Event "The British Chess Magazine"]
[Site "?"]
[Date "1882.??.??"]
[Round "-"]
[White "A. L. S."]
[Black "#2"]
[Result "1-0"]
[FEN "2b4Q/4p2K/4pr2/2P1k3/2P2R2/3P3p/2n4B/8 w - - 0 1"]
[SetUp "1"]

1. Kg7 Nd4 1-0

[Event "Thema Danicum"]
[Site "?"]
[Date "1980.??.??"]
[Round "?"]
[White "AAHOLST Svend"]
[Black "#2"]
[Result "1-0"]
[FEN "2B5/2p1B1nn/2PR2p1/1K2k3/1p2p3/2b1P3/5R2/8 w - - 0 1"]
[SetUp "1"]

1. Kc5 1-0

[Event "Thema Danicum"]
[Site "?"]
[Date "1981.??.??"]
[Round "?"]
[White "AAHOLST Svend"]
[Black "#2"]
[Result "1-0"]
[FEN "8/2R5/8/B2N4/p3B3/Q1pP4/1pqk1K2/br1b3R w - - 0 1"]
[SetUp "1"]

1. Nxc3 1-0

[Event "Thema Danicum"]
[Site "?"]
[Date "1981.??.??"]
[Round "?"]
[White "AAHOLST Svend & LARSEN Lars Aksglaede"]
[Black "#2"]
[Result "1-0"]
[FEN "6B1/3K2pN/4N3/1Rp2k1p/2Q3br/5p2/1r1b4/B1n3R1 w - - 0 1"]
[SetUp "1"]

1. Nxc5 1-0

[Event "Probleemblad"]
[Site "?"]
[Date "1977.??.??"]
[Round "-"]
[White "AARZEN Hans"]
[Black "#2"]
[Result "1-0"]
[FEN "b2R1B2/3p3r/nN6/1R6/r2kp3/P3p3/nPQ5/K2BN3 w - - 0 1"]
[SetUp "1"]

1. Bf3 N2b4 1-0

[Event "Probleemblad"]
[Site "?"]
[Date "1978.??.??"]
[Round "-"]
[White "AARZEN Hans"]
[Black "#2"]
[Result "1-0"]
[FEN "4n3/2rR2B1/3b1q2/1pP3N1/1P1kpQ2/5BK1/N1pnP3/3R1b2 w - - 0 1"]
[SetUp "1"]

1. Nxe4 Bg2 1-0

[Event "Probleemblad"]
[Site "?"]
[Date "1978.??.??"]
[Round "-"]
[White "AARZEN Hans"]
[Black "#2"]
[Result "1-0"]
[FEN "3Rn3/1np3B1/b2b1qP1/p6R/1P1kN1K1/r2p2P1/1P3P2/2Q5 w - - 0 1"]
[SetUp "1"]

1. Qf4 Ra4 1-0

[Event "Probleemblad"]
[Site "?"]
[Date "1979.??.??"]
[Round "-"]
[White "AARZEN Hans"]
[Black "#2"]
[Result "1-0"]
[FEN "6Q1/2p1kr2/3p4/3R4/1R2Kp2/B3N2B/8/8 w - - 0 1"]
[SetUp "1"]

1. Rxd6 fxe3 1-0