﻿using System;
using System.Collections.Generic;
using WebSocketModels.Models;

namespace WebSocketModels.UserManagerModels
{
    [Serializable]
    public class UserManagerResponse
    {
        public string Message;
        public bool IsSuccess;
        public IEnumerable<string> Errors;
        public DateTime? ExpireDate;
        public PlayerDto CurrPlayer;
        
        public UserManagerResponse() {}
    }
}