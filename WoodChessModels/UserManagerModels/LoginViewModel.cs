﻿using System;
using System.ComponentModel.DataAnnotations;

namespace WebSocketModels.UserManagerModels
{
    [Serializable]
    public class LoginViewModel
    {
        [Required] [StringLength(10, MinimumLength = 3)]
        public string UserName;

        [Required] [StringLength(50, MinimumLength = 5)]
        public string Password;

        public LoginViewModel() {}
    }
}
