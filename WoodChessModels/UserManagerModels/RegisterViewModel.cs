﻿using System;
using System.ComponentModel.DataAnnotations;

namespace WebSocketModels.UserManagerModels
{
    [Serializable]
    public class RegisterViewModel
    {
        [Required] [StringLength(10, MinimumLength = 3)]
        public string UserName;

        [Required] [StringLength(50)] [EmailAddress]
        public string Email;

        [Required] [StringLength(50, MinimumLength = 5)]
        public string Password;

        [Required] [StringLength(50, MinimumLength = 5)]
        public string ConfirmPassword;

        public RegisterViewModel() {}
    }
}