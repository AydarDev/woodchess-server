﻿using WebSocketModels.WsMessages.WsResponses;

namespace WebSocketModels.WsReceivers
{
    public interface IChallReceiver : IReceiver
    {
        void HandleChallenge(ChallengeResponse challengeResponse);
        void HandleChallenges(ChallengeResponse challResponse);
        void HandleChallengeAccept(ChallengeResponse challResponse);
        void HandleChallengeDecline(ChallengeResponse challResponse);
        void HandleChallengeRemove(ChallengeResponse challResponse);
    }
}
