﻿using WebSocketModels.WsMessages.WsResponses;

namespace WebSocketModels.WsReceivers
{
    public interface IPlayerReceiver : IReceiver
    {
        void HandlePlayer(PlayerResponse playerResponse);
        void HandleRatingDeviation(PlayerResponse playerResponse);
    }
}