﻿using WebSocketModels.WsMessages.WsResponses;

namespace WebSocketModels.WsReceivers
{
    public interface IGameReceiver : IReceiver
    {
        void HandleGame(GameResponse gameResponse);
        void HandleMove(GameResponse gameResponse);
        void HandleDraw(GameResponse gameResponse);
    }
}
