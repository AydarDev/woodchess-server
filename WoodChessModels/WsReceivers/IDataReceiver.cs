﻿using WebSocketModels.WsMessages.WsResponses;

namespace WebSocketModels.WsReceivers
{
    public interface IDataReceiver : IReceiver
    {
        void HandleSystem(DataResponse dataResponse);
        void HandleException(DataResponse dataResponse);
        void HandleClock(DataResponse dataResponse);
    }
}
