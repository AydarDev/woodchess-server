﻿using WebSocketModels.WsMessages.WsResponses;

namespace WebSocketModels.WsReceivers
{
    public interface IReceiver
    {
        void ShowMessage(string message);
    }
}
