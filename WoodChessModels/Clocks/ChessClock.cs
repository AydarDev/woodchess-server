﻿using System;
using System.Collections.Generic;
using Assets.Scripts;
using ChessRules;
using WebSocketModels.Models;

namespace WebSocketModels.Clocks
{
    public class ChessClock
    {
        public Dictionary<SideColor, Timer> Timers { get; } = new Dictionary<SideColor, Timer>();
        public SideColor toMoveSide;
        public virtual TimeSpan CorrectionSpan { get; } = TimeSpan.Zero;
        private DateTime _timeStamp;
        public bool IsRun { get; set; } = true;
        public virtual bool IsNull { get; set; } = false;

        protected ChessClock()
        {
        }

        public ChessClock(GameDto game, Chess chess)
        {
            TimeSpan whiteTime;
            TimeSpan blackTime;

            whiteTime = game.TimeInfo.WhiteTime;
            blackTime = game.TimeInfo.BlackTime;
            _timeStamp = game.TimeInfo.TimeStamp;

            Timers.Add(SideColor.White, new Timer(this, whiteTime));
            Timers.Add(SideColor.Black, new Timer(this, blackTime));
            toMoveSide = chess.MoveColor;
        }

        public void StartClock()
        {
            Timers[toMoveSide].Start(_timeStamp);
        }

        public void Swap(TimeInfoDto timeInfo)
        {
            var timeStamp = timeInfo.TimeStamp;
            TimeSpan syncTime = TimeSpan.Zero;

            if (toMoveSide.Equals(SideColor.White))
                syncTime = timeInfo.WhiteTime;
            else if (toMoveSide.Equals(SideColor.Black))
                syncTime = timeInfo.BlackTime;

            Timers[toMoveSide].Stop(syncTime);
            SwapSide();
            Timers[toMoveSide].Start(timeStamp);
        }

        public void StopClock()
        {
            Timers[SideColor.White].Stop(TimeSpan.Zero);
            Timers[SideColor.Black].Stop(TimeSpan.Zero);
            IsRun = false;
        }

        public virtual void ClockRun()
        {
        }

        private void SwapSide()
        {
            if (toMoveSide == SideColor.White) toMoveSide = SideColor.Black;
            else if (toMoveSide == SideColor.Black) toMoveSide = SideColor.White;
        }

        public void Synchronize(TimeInfoDto timeInfo)
        {
            Timers[SideColor.White].CurrTime = timeInfo.WhiteTime;
            Timers[SideColor.Black].CurrTime = timeInfo.BlackTime;
        }
    }
}