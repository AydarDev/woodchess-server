﻿using System;
using WebSocketModels.Clocks;

namespace Assets.Scripts
{
    public class Timer
    {
        private ChessClock chessClock;
        internal TimeSpan timeControl { get; }
        public TimeSpan CurrTime { get; set; }
        internal TimeSpan TimerSpan { get; private set; }
        public bool IsRun { get; private set; }
        internal DateTime TimeStamp { get; set; }
        internal TimeSpan CorrectionSpan => chessClock.CorrectionSpan;
        internal Action<Timer> ShowTime = timer => {};


        internal Timer(ChessClock chessClock, TimeSpan timeControl)
        {
            this.chessClock = chessClock;
            this.timeControl = timeControl;
            TimerSpan = timeControl;
            CurrTime = timeControl;
            IsRun = false;
        }

        internal void Start(DateTime timeStamp)
        {
            this.TimeStamp = timeStamp;
            IsRun = true;
        }


        internal void Stop(TimeSpan serverTime)
        {
            if (serverTime != null && serverTime != TimeSpan.Zero)
                CurrTime = serverTime;
            TimerSpan = CurrTime;
            IsRun = false;
        }


        public void Update()
        {
            ShowTime(this);
            if (IsRun)
            {
                DateTime currentUtcNow = DateTime.UtcNow;
                CurrTime = TimerSpan - (currentUtcNow - (TimeStamp - CorrectionSpan));
            }

            if (CurrTime <= TimeSpan.Zero)
                CurrTime = TimeSpan.Zero;
        }
    }
}
