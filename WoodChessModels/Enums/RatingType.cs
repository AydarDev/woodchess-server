﻿namespace WebSocketModels.Enums
{
    public enum RatingType
    {
        Bullet, Blitz, Rapid, Classic, Daily, Puzzle, None
    }
}