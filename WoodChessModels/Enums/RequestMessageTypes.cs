﻿namespace WebSocketModels.Enums
{
    public enum ChallengeRequestType { GetChallenges, GetMyChallenges, CreateChallenge, RemoveChallenge, AcceptChallenge, DeclineChallenge }
    public enum GameRequestType { EnterGame, ExitGame, MakeMove, ResignGame, OfferDraw, AcceptDraw, DeclineDraw }
    public enum PlayerRequestType { GetPlayer, GetAllPlayers, GetOnlinePlayers }
    public enum DataRequestType { System, Exception, Chat, Clock }
}
