namespace WebSocketModels.Enums
{
    public enum GameDetailResult
    {
        WinByResign, WinByTimeOver, WinByCheckmate, DrawByAgree, DrawByStaleMate, DrawByFiftyMoves, DrawByTripleRepetition
    }
}