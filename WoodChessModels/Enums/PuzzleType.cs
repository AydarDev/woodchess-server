﻿namespace WebSocketModels.Enums
{
    public enum PuzzleType { SimpleMates, Tactics, MateInTwo, MateInThree, Etude }
}