namespace WebSocketModels.Enums
{
    public enum GameResult
    {
        WhiteWin, BlackWin, Draw, Cancel, None
    }
}