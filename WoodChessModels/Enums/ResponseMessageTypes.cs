﻿namespace WebSocketModels.Enums
{
    public enum PlayerResponseType { Player, RatingDeviation }
    public enum GameResponseType { Game, Move, Draw }
    public enum ChallengeResponseType { Challenge, Challenges, ChallengeDecline, ChallengeAccept, ChallengeRemove }
    public enum DataResponseType { System, Exception, Chat, Clock }
}
