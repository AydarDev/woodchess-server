﻿namespace WebSocketModels.Enums
{
    public enum EventType
    {
        PuzzleDone, RatingChanged, Undetermined
    }
}