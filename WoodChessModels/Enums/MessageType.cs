﻿namespace WebSocketModels.Enums
{
    public enum MessageType 
    { 
        Player, Challenge, Game, Data
    }
}
