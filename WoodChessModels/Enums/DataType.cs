﻿namespace WebSocketModels.Enums
{
    public enum DataType
    {
        Pgn, Csv
    }
}