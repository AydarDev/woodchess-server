﻿using System;
using WebSocketModels.Enums;
using WebSocketModels.WsMessages.WsResponses;
using WebSocketModels.WsReceivers;

namespace WebSocketModels.WsHandlers
{
    public class WsChallengeHandler : IWsHandler
    {
        public void ProcessWsMessage(IReceiver receiver, WsResponse message)
        {
            if (!(message is ChallengeResponse challengeResponse) ||
                !(receiver is IChallReceiver challengeReceiver)) return;
            switch (challengeResponse.ChallengeResponseType)
            {
                case (ChallengeResponseType.Challenge):
                    challengeReceiver.HandleChallenge(challengeResponse);
                    break;
                case (ChallengeResponseType.Challenges):
                    challengeReceiver.HandleChallenges(challengeResponse);
                    break;
                case (ChallengeResponseType.ChallengeAccept):
                    challengeReceiver.HandleChallengeAccept(challengeResponse);
                    break;
                case (ChallengeResponseType.ChallengeDecline):
                    challengeReceiver.HandleChallengeDecline(challengeResponse);
                    break;
                case (ChallengeResponseType.ChallengeRemove):
                    challengeReceiver.HandleChallengeRemove(challengeResponse);
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }
    }
}