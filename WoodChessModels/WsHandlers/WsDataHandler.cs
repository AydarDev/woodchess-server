﻿using System;
using WebSocketModels.Enums;
using WebSocketModels.WsMessages.WsResponses;
using WebSocketModels.WsReceivers;

namespace WebSocketModels.WsHandlers
{
    public class WsDataHandler : IWsHandler
    {
        public void ProcessWsMessage(IReceiver receiver, WsResponse message)
        {
            if (!(receiver is IDataReceiver dataReceiver) || !(message is DataResponse dataResponse)) return;
            switch (dataResponse.DataResponseType)
            {
                case(DataResponseType.Clock):
                    dataReceiver.HandleClock(dataResponse);
                    break;
                case(DataResponseType.Exception):
                    dataReceiver.HandleException(dataResponse);
                    break;
                case(DataResponseType.System):
                    dataReceiver.HandleSystem(dataResponse);
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }
    }
}