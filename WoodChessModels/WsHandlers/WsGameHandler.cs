﻿using System;
using WebSocketModels.Enums;
using WebSocketModels.WsMessages.WsResponses;
using WebSocketModels.WsReceivers;

namespace WebSocketModels.WsHandlers
{
    public class WsGameHandler : IWsHandler
    {
        public void ProcessWsMessage(IReceiver receiver, WsResponse message)
        {
            if (!(receiver is IGameReceiver gameReceiver) || !(message is GameResponse gameResponse)) return;
            switch (gameResponse.GameResponseType)
            {
                case(GameResponseType.Game):
                    gameReceiver.HandleGame(gameResponse);
                    break;
                case(GameResponseType.Move):
                    gameReceiver.HandleMove(gameResponse);
                    break;
                case(GameResponseType.Draw):
                    gameReceiver.HandleDraw(gameResponse);
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }
    }
}