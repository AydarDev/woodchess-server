﻿using System;
using WebSocketModels.Enums;
using WebSocketModels.WsMessages.WsResponses;
using WebSocketModels.WsReceivers;

namespace WebSocketModels.WsHandlers
{
    public class WsPlayerHandler : IWsHandler
    {
        public void ProcessWsMessage(IReceiver receiver, WsResponse message)
        {
            if (!(receiver is IPlayerReceiver playerReceiver) || !(message is PlayerResponse playerResponse)) return;
            switch (playerResponse.PlayerResponseType)
            {
                case(PlayerResponseType.Player):
                    playerReceiver.HandlePlayer(playerResponse);
                    break;
                case(PlayerResponseType.RatingDeviation):
                    playerReceiver.HandleRatingDeviation(playerResponse);
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }
    }
}