﻿using System.Collections.Concurrent;
using System.Collections.Generic;
using WebSocketModels.Enums;
using WebSocketModels.WsMessages.WsResponses;
using WebSocketModels.WsReceivers;

namespace WebSocketModels.WsHandlers
{
    public class WsHandlerFactory
    {
        private static readonly ConcurrentDictionary<MessageType, IWsHandler> Handlers =
            new ConcurrentDictionary<MessageType, IWsHandler>();

        public static IWsHandler GetWsHandler(WsResponse wsResponse)
        {
            MessageType wsMessageType = wsResponse.MessageType;
            if (Handlers.TryGetValue(wsMessageType, out IWsHandler wsHandler))
                return wsHandler;
            switch (wsMessageType)
            {
                case (MessageType.Challenge):
                    wsHandler = new WsChallengeHandler();
                    break;
                case (MessageType.Game):
                    wsHandler = new WsGameHandler();
                    break;
                case (MessageType.Player):
                    wsHandler = new WsPlayerHandler();
                    break;
                case (MessageType.Data):
                    wsHandler = new WsDataHandler();
                    break;
                default:
                    return null;
            }

            Handlers.TryAdd(wsMessageType, wsHandler);
            return wsHandler;
        }
    }
}