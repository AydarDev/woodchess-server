﻿using WebSocketModels.WsMessages.WsResponses;
using WebSocketModels.WsReceivers;

namespace WebSocketModels.WsHandlers
{
    public interface IWsHandler
    {
        void ProcessWsMessage(IReceiver receiver, WsResponse message);
    }
}