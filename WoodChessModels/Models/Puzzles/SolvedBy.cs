﻿namespace WebSocketModels.Models.Puzzles;

public class SolvedBy
{
    public long PlayerId { get; set; }
    public bool IsSolved { get; set; }
}