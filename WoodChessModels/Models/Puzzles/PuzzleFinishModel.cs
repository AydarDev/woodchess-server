﻿using System;

namespace WebSocketModels.Models.Puzzles
{
    [Serializable]
    public class PuzzleFinishModel
    {
        public long PlayerId;
        public string PuzzleId;
        public int SolutionKey;
        public bool IsResolved;
    }
}