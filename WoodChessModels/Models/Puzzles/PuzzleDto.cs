﻿using System;
using System.Collections.Generic;
using WebSocketModels.Enums;

namespace WebSocketModels.Models.Puzzles
{
    [Serializable]
    public class PuzzleDto
    {
        public string Id;
        public string Name;
        public PuzzleType PuzzleType;
        public DataType DataType;
        public string Data;
        public double Rating;
        public HashSet<long> CurrentOf = new HashSet<long>();
        public HashSet<SolvedBy> SolvedBy { get; set; } = new();
        public HashSet<long> LikedBy   = new HashSet<long>();
        public int SolutionKey;
        public bool HasPreMove;
        public List<string> Tags;
        public bool IsNull = false;

        public PuzzleDto()
        {
        }


        public override string ToString()
        {
            return Name;
        }

        public override bool Equals(object obj)
        {
            if (obj is PuzzleDto puzzleObj)
            {
                return Id == puzzleObj.Id &&
                       Name == puzzleObj.Name &&
                       Data == puzzleObj.Data;
            }

            return false;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}