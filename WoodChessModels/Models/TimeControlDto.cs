﻿using System;
using WebSocketModels.Enums;

namespace WebSocketModels.Models
{
    public enum TimeControlType { TimeOnGame, TimeOnMove, Null}

    [Serializable]
    public class TimeControlDto
    {
        public int TimeRange;
        public int TimeIncrement;
        public TimeControlType ControlType = TimeControlType.TimeOnGame;
        public RatingType RatingType = RatingType.Blitz;
        public bool IsNull = false;

        public TimeControlDto() { }

        public TimeControlDto(int timeRange, int timeIncrement)
        {
            TimeRange = timeRange;
            TimeIncrement = timeIncrement;
        }

        public override bool Equals(object obj)
        {
            if (obj == null)
                return false;

            TimeControlDto tc = obj as TimeControlDto;
            if ((System.Object)tc == null)
                return false;

            return (TimeRange == tc.TimeRange) &&
                   (TimeIncrement == tc.TimeIncrement) &&
                   (ControlType == tc.ControlType);
        }
       

        public override string ToString()
        {
            if (IsNull)
                return ("No time limits");
            else
                return String.Format("{0}'+{1}''", TimeRange, TimeIncrement);
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}
