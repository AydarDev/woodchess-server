﻿using ChessRules;
using System;
using RatingType = WebSocketModels.Enums.RatingType;

namespace WebSocketModels.Models
{
    [Serializable]
    public class ChallengeDto
    {
        public Guid Id;
        public PlayerDto FromPlayer;
        public SideColor Color;
        public TimeControlDto TimeControl;
        public PlayerDto ToPlayer;
        public RatingType RatingType => TimeControl.RatingType;
        public bool IsNull = false;

        public override bool Equals(object obj)
        {
            bool result = false;
            if (obj == null)
                return false;


            ChallengeDto ch = obj as ChallengeDto;
            if ((System.Object)ch == null)
                return false;

            result = FromPlayer.Equals(ch.FromPlayer) &&
                    Color.Equals(ch.Color) &&
                    TimeControl.Equals(ch.TimeControl);

            if (ToPlayer != null)
                result = result && ToPlayer.Equals(ch.ToPlayer);

            return result;                    
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public override string ToString()
        {
            if (ToPlayer.IsNull)
                return $"Public challenge from {FromPlayer.Name}, {TimeControl.ToString()}";
            else
                return $"Challenge from {FromPlayer.Name} to {ToPlayer.Name}, {TimeControl.ToString()}";
        }
    }
}
