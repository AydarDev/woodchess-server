﻿using System;
using ChessRules;
using WebSocketModels.Models.Ratings;

namespace WebSocketModels.Models
{
    [Serializable]
    public class GamePlayerDto
    {
        public long PlayerId;
        public PlayerDto Player;
        public SideColor Color;
        public RatingDeviationDto RatingDeviation;
    }
}