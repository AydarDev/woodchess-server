﻿
using System;
using ChessRules;

namespace WebSocketModels.Models.Ratings
{
    [Serializable]
    public class RatingDeviationDto
    {
        public double Deviation;
        public double NewRating;
        public bool IsNull = false;

        public RatingDeviationDto (double deviation)
        {
            Deviation = deviation;
        }
        
        public RatingDeviationDto (double deviation, double newRating)
        {
            Deviation = deviation;
            NewRating = newRating;
        }

        public RatingDeviationDto()
        {
        }

        public override bool Equals(object obj)
        {
            if (obj == null)
                return false;

            var ratingDeviation = (RatingDeviationDto)obj;

            return ratingDeviation.Deviation.Equals(Deviation);
        }
        
        public override int GetHashCode()
        {
            return (int)Deviation;
        }
    }

    public class NullDeviation : RatingDeviationDto
    {
        public NullDeviation()
        {
            IsNull = true;
        }
    }
}
