﻿using System;
using WebSocketModels.Enums;

namespace WebSocketModels.Models.Ratings
{
    [Serializable]
    public class UpdatePlayerRatingModel
    {
        public long PlayerId { get; set; }
        public RatingType RatingType { get; set; }
        public double RatingValue { get; set; }
    }
}