﻿using System;
using Newtonsoft.Json;
using WebSocketModels.Enums;

namespace WebSocketModels.Models.Ratings
{
    [Serializable]
    public class RatingDto
    {
        [JsonIgnore]
        public long PlayerId;
        public RatingType RatingType;
        public double Value;
        [JsonIgnore]
        public bool IsGame;
    }
}