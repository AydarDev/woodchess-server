﻿using System;
using WebSocketModels.Enums;

namespace WebSocketModels.Models
{
    [Serializable]
    public class DrawInfoDto
    {
        public DrawFlag DrawFlag;
        public int FromPlayerId;
        public int BlockCounter;
    }
}