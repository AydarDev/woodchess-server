﻿using WebSocketModels.Enums;

namespace WebSocketModels.Models.Events
{
    public class RatingChangedDto : GenericEventDto
    {
        public string PuzzleId { get; set; }
        public double RatingDeviation { get; set; }
        public double NewRating { get; set; }

        public RatingChangedDto()
        {
            EventType = EventType.RatingChanged;
        }
    }
}