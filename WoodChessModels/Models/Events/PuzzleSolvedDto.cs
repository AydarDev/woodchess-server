﻿using WebSocketModels.Enums;

namespace WebSocketModels.Models.Events
{
    public class PuzzleDoneDto : GenericEventDto
    {
        public long PlayerId { get; set; }
        public string PuzzleId { get; set; }
        public double PuzzleRating { get; set; }
        public bool IsResolved { get; set; }

        public PuzzleDoneDto()
        {
            EventType = EventType.PuzzleDone;
        }
    }
}