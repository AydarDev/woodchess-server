﻿using WebSocketModels.Enums;

namespace WebSocketModels.Models.Events
{
    public class GenericEventDto
    {
        public EventType EventType { get; set; }
    }
}