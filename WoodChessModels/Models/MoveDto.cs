﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using WebSocketModels;
using WebSocketModels.Enums;

namespace WebSocketModels.Models
{
    [Serializable]
    public class MoveDto
    {
        public Guid GameId;
        public long PlayerId;
        public string Value;
        public string Fen;
        public TimeInfoDto TimeInfo;
        public bool IsNull = false;
    }
}
