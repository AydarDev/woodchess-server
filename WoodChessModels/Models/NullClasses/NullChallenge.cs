﻿using ChessRules;

namespace WebSocketModels.Models.NullClasses
{
    public class NullChallenge : ChallengeDto
    {
        private static NullChallenge _nullChallenge;
        private NullChallenge()
        {
            IsNull = true;
        }

        public static NullChallenge GetInstance()
        {
            return _nullChallenge ?? (_nullChallenge = new NullChallenge());
        }
    }
}
