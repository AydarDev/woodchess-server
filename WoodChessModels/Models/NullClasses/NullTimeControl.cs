﻿namespace WebSocketModels.Models.NullClasses
{
    public sealed class NullTimeControl : TimeControlDto
    {
        private static NullTimeControl _nullTimeControl;
        private NullTimeControl()
        {
            IsNull = true;
        }
        
        public static NullTimeControl GetInstance()
        {
            return _nullTimeControl ?? (_nullTimeControl = new NullTimeControl());
        }
    }
}
