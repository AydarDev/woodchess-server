﻿namespace WebSocketModels.Models.NullClasses
{
    public sealed class NullPlayer : PlayerDto
    {
        private static NullPlayer _nullPlayer;
        private NullPlayer()
        {
            IsNull = true;
        }

        public static NullPlayer GetInstance()
        {
            return _nullPlayer ?? (_nullPlayer = new NullPlayer());
        }
    }
}
