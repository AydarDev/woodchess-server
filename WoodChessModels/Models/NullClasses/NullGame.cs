﻿namespace WebSocketModels.Models.NullClasses
{
    public sealed class NullGame : GameDto
    {
        private static NullGame _nullGame;
        private NullGame()
        {
            IsNull = true;
        }

        public static NullGame GetInstance()
        {
            return _nullGame ?? (_nullGame = new NullGame());
        }
    }
}
