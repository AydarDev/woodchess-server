﻿using WebSocketModels.Models.Puzzles;

namespace WebSocketModels.Models.NullClasses
{
    public class NullPuzzle : PuzzleDto
    {
        private static NullPuzzle _instance;
        private NullPuzzle()
        {
            IsNull = true;
        }

        public static NullPuzzle GetInstance()
        {
            return _instance ?? (_instance = new NullPuzzle());
        }
    }
}