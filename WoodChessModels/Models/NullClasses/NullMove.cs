﻿namespace WebSocketModels.Models.NullClasses
{
    public sealed class NullMove : MoveDto
    {
        private static NullMove _nullMove;
        private NullMove()
        {
            IsNull = true;
        }

        public static NullMove GetInstance()
        {
            return _nullMove ?? (_nullMove = new NullMove());
        }
    }        
}
