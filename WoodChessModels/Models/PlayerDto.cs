﻿using System;
using System.Collections.Generic;
using System.Linq;
using WebSocketModels.Models.Ratings;
using RatingType = WebSocketModels.Enums.RatingType;

namespace WebSocketModels.Models
{
    [Serializable]
    public class PlayerDto
    {
        public long Id;
        public string Name;
        public List<RatingDto> Ratings;
        public bool IsOnline;
        public bool IsNull = false;

        public override bool Equals(object obj)
        {
            if (obj == null)
                return false;

            PlayerDto p = obj as PlayerDto;
            if ((System.Object)p == null) return false;

            return (Id == p.Id) &&
                    (Name == p.Name);
        }

        public override int GetHashCode()
        {
            return (int)Id / 1024 + Name.Length;
        }

        public double GetMaximumRating()
        {
            return Ratings
                .Where(r => r.IsGame)
                .Select(r => r.Value)
                .Max();
        }

        public double this[RatingType ratingType]
        {
            get => Ratings.FirstOrDefault(r => r.RatingType == ratingType).Value;
            set => Ratings.FirstOrDefault(r => r.RatingType == ratingType).Value = value;
        }

        public void SetRating(RatingType ratingType, double value)
        {
            Ratings.First(r => r.RatingType == ratingType).Value = value;
        }
    }
}
