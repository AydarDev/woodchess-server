﻿using System;
using System.Collections.Generic;
using System.Linq;
using ChessRules;
using Newtonsoft.Json;
using WebSocketModels.Enums;
using WebSocketModels.Models.NullClasses;
using WebSocketModels.Models.Ratings;
using RatingType = WebSocketModels.Enums.RatingType;

namespace WebSocketModels.Models
{
    [Serializable]
    public class GameDto
    {
        public Guid Id;
        public string Fen;
        public string Pgn;
        public GameStatus Status;
        public TimeControlDto TimeControl;
        public int CurrPly;
        public string LastMove;
        public TimeInfoDto TimeInfo;
        public DrawInfoDto DrawInfo;
        public DateTime StartedAt;
        public GameResult Result;
        public GameDetailResult DetailResult;
        public string TextResult;
        public List<GamePlayerDto> GamePlayers;
        public bool IsNull = false;
        
        [JsonIgnore]
        public PlayerDto WhitePlayer => GamePlayers.FirstOrDefault(gp => gp.Color == SideColor.White)?.Player ??
                                        NullPlayer.GetInstance();
        [JsonIgnore]
        public PlayerDto BlackPlayer => GamePlayers.FirstOrDefault(gp => gp.Color == SideColor.Black)?.Player ??
                                        NullPlayer.GetInstance();

        [JsonIgnore]
        public RatingType RatingType => TimeControl?.RatingType ?? RatingType.None;
        

        public override string ToString()
        {
            return $"Game {WhitePlayer.Name} VS {BlackPlayer.Name} {StartedAt}";
        }
    }
}
