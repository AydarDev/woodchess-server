using System;

namespace WebSocketModels.Models
{
    [Serializable]
    public class TimeInfoDto
    {
        public TimeSpan WhiteTime;
        public TimeSpan BlackTime;
        public DateTime TimeStamp;
    }
}