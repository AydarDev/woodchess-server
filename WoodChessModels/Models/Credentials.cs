﻿using System;

namespace WebSocketModels.Models
{
    [Serializable]
    public class Credentials
    {
        public string UserName;
        public string Password;

        public Credentials()
        {
        }

        public Credentials(string userName, string password)
        {
            UserName = userName;
            Password = password;
        }

        public Credentials Encrypt(string key)
        {
            UserName = EncryptionHelper.Encrypt(UserName, key);
            Password = EncryptionHelper.Encrypt(Password, key);
            return this;
        }

        public Credentials Decrypt(string key)
        {
            UserName = EncryptionHelper.Decrypt(UserName, key);
            Password = EncryptionHelper.Decrypt(Password, key);
            return this;
        }
    }
}