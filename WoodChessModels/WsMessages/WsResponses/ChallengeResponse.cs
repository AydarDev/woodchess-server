﻿using System;
using System.Collections.Generic;
using WebSocketModels.Enums;
using WebSocketModels.Models;

namespace WebSocketModels.WsMessages.WsResponses
{
    [Serializable]
    public class ChallengeResponse : WsResponse
    {
        public ChallengeResponseType ChallengeResponseType;
        public ChallengeDto Challenge;
        public Guid ChallengeId;
        public List<ChallengeDto> Challenges;
        public List<ChallengeDto> PrivateChallenges;
        public GameDto Game;

        public override string DetailType => ChallengeResponseType.ToString();

        public ChallengeResponse() { }

        public ChallengeResponse(ChallengeResponseType challengeResponseType) : base(MessageType.Challenge)
        {
            ChallengeResponseType = challengeResponseType;
        }
    }
}
