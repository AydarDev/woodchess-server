﻿using System;
using WebSocketModels.Enums;

namespace WebSocketModels.WsMessages.WsResponses
{
    [Serializable]
    public abstract class WsResponse : WsMessage
    {
        public string Data;
        public abstract string DetailType { get; }
        public DateTime ServerTime;
        
        public WsResponse() { }

        public WsResponse(MessageType messageType, string data)
        {
            MessageType = messageType;
            Data = data;
        }

        public WsResponse(MessageType messageType)
        {
            MessageType = messageType;
        }
    }
}
