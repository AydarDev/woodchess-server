﻿using System;
using System.Collections.Generic;
using WebSocketModels.Enums;
using WebSocketModels.Models;
using WebSocketModels.Models.Ratings;

namespace WebSocketModels.WsMessages.WsResponses
{
    [Serializable]
    public class PlayerResponse : WsResponse
    {
        public PlayerResponseType PlayerResponseType;
        public PlayerDto Player;
        public IEnumerable<PlayerDto> Players;
        public RatingDeviationDto RatingDeviation;
        public override string DetailType => PlayerResponseType.ToString();

        public PlayerResponse() { }

        public PlayerResponse(PlayerResponseType playerResponseType) : base(MessageType.Player)
        {
            PlayerResponseType = playerResponseType;
        }
    }
}