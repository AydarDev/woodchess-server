﻿using System;
using WebSocketModels.Enums;

namespace WebSocketModels.WsMessages.WsResponses
{
    [Serializable]
    public class DataResponse : WsResponse
    {
        public DataResponseType DataResponseType;
        public long ToPlayerId;
        public string FromPlayerName;

        public override string DetailType => DataResponseType.ToString();

        public DataResponse() { }

        public DataResponse(DataResponseType dataResponseType) : base(MessageType.Data)
        {
            DataResponseType = dataResponseType;
        }

        public DataResponse(DataResponseType dataResponseType, string data) : base(MessageType.Data, data)
        {
            DataResponseType = dataResponseType;
        }
    }
}
