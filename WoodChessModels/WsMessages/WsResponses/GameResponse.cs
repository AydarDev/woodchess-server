﻿using System;
using System.Collections.Generic;
using WebSocketModels.Enums;
using WebSocketModels.Models;

namespace WebSocketModels.WsMessages.WsResponses
{
    [Serializable]
    public class GameResponse : WsResponse
    {
        public GameResponseType GameResponseType;
        public GameDto Game;
        public MoveDto Move;
        public List<GameDto> Games;
        public DrawInfoDto DrawInfo;


        public override string DetailType => GameResponseType.ToString();

        public GameResponse() : base(MessageType.Game)
        {
            
        }

        public GameResponse(GameResponseType gameResponseType) : base(MessageType.Game)
        {
            this.GameResponseType = gameResponseType;
        }
    }
}
