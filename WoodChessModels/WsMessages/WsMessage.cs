﻿using System;
using WebSocketModels.Enums;

namespace WebSocketModels.WsMessages
{    
    [Serializable]
    public class WsMessage
    {
        public MessageType MessageType;
    }
}
