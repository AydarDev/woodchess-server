﻿using System;
using ChessRules;
using WebSocketModels.Enums;

namespace WebSocketModels.WsMessages.WsRequests
{
    [Serializable]
    public class GameRequest : WsRequest
    {
        public GameRequestType GameRequestType;
        public Guid GameId;
        public string Move;
        public GameStatus? Status;
        public SideColor? Color;
        public long? PlayerId;
        public GameResult? Result;
        public override string DetailType => GameRequestType.ToString();

        public GameRequest() { }

        public GameRequest (long playerId, GameRequestType gameRequestType) : base(playerId, MessageType.Game)
        {
            GameRequestType = gameRequestType;
        }
    }
}
