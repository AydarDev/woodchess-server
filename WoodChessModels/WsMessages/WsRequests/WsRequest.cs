﻿using System;
using WebSocketModels.Enums;

namespace WebSocketModels.WsMessages.WsRequests
{
    [Serializable]
    public abstract class WsRequest : WsMessage
    {
        public long FromPlayerId;
        public abstract string DetailType { get; }

        public WsRequest() { }

        public WsRequest(long fromPlayerId, MessageType messageType)
        {
            FromPlayerId = fromPlayerId;
            MessageType = messageType;
        }

        public WsRequest(MessageType messageType)
        {
            base.MessageType = messageType;
        }
    }
}
