﻿using System;
using ChessRules;
using WebSocketModels.Enums;
using WebSocketModels.Models;

namespace WebSocketModels.WsMessages.WsRequests
{
    [Serializable]
    public class ChallengeRequest : WsRequest
    {
        public ChallengeRequestType ChallengeRequestType;
        public Guid ChallengeId;
        public TimeControlDto TimeControl;
        public SideColor SideColor;
        public long ToPlayerId;
        public override string DetailType => ChallengeRequestType.ToString();

        public ChallengeRequest() { }

        public ChallengeRequest (long playerId, ChallengeRequestType challengeRequestType) : base(playerId, MessageType.Challenge)
        {
            this.ChallengeRequestType = challengeRequestType;
        }
    }
}
