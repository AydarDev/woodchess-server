﻿using System;
using WebSocketModels.Enums;

namespace WebSocketModels.WsMessages.WsRequests
{
    [Serializable]
    public class DataRequest : WsRequest
    {
        public DataRequestType DataRequestType;
        public override string DetailType => DataRequestType.ToString();
        public long ToPlayerId;
        public string Data;
        public bool IsGroup;

        public DataRequest()
        {
            MessageType = MessageType.Data;
        }

        public DataRequest(long playerId, DataRequestType dataRequestType) : base(playerId, MessageType.Data)
        {
            this.DataRequestType = dataRequestType;
        }

        public DataRequest(DataRequestType dataRequestType) : base(MessageType.Data)
        {
            this.DataRequestType = dataRequestType;
        }
    }
}