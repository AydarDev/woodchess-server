﻿using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using WebSocketModels.Enums;

namespace WebSocketModels.WsMessages.WsRequests
{
    public class WsRequestConverter : JsonConverter
    {
        public override bool CanConvert(Type objectType)
        {
            return typeof(WsRequest).IsAssignableFrom(objectType);
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            JObject jo = JObject.Load(reader);

            WsRequest item;
            MessageType requestType = (MessageType)(int)jo["MessageType"];

            switch (requestType)
            {
                case (MessageType.Challenge):
                    item = new ChallengeRequest();
                    break;
                case (MessageType.Game):
                    item = new GameRequest();
                    break;
                case (MessageType.Player):
                    item = new PlayerRequest();
                    break;
                case (MessageType.Data):
                    item = new DataRequest();
                    break;
                default:
                    item = null;
                    break;
            }

            serializer.Populate(jo.CreateReader(), item);

            return item;            
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            throw new NotImplementedException();
        }
    }
}
