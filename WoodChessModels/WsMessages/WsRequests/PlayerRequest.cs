﻿using System;
using WebSocketModels.Enums;

namespace WebSocketModels.WsMessages.WsRequests
{
    [Serializable]
    public class PlayerRequest : WsRequest
    {
        public PlayerRequestType PlayerRequestType;
        public long PlayerId;
        public override string DetailType => PlayerRequestType.ToString();


        public PlayerRequest() { }

        public PlayerRequest(long fromPlayerId, PlayerRequestType playerRequestType) : base(fromPlayerId, MessageType.Player)
        {
            this.PlayerRequestType = playerRequestType;
        }
    }
}
