﻿namespace WebSocketModels.Utils
{
    public static class WoodChessHelpers
    {
        public static string Normalize(this string text)
        {
            text = text.ToLower();
            text = text.Replace(text[0].ToString(), text[0].ToString().ToUpper());

            return text;
        }
    }
}