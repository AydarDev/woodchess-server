﻿using Newtonsoft.Json;
using System;
using WebSocketModels.Enums;
using ChessRules;
using WebSocketModels.Models;
using WebSocketModels.WsMessages;
using WebSocketModels.WsMessages.WsRequests;

namespace WebSocketModels
{
    public class NetworkClient
    {
        private static NetworkClient _webSocketClient;
        public long PlayerId { get; set; }
        public Action<string> SendRequestAction { get; set; }

        private NetworkClient() { }

        public static NetworkClient GetInstance()
        {
            if (_webSocketClient == null)
                _webSocketClient = new NetworkClient();

            return _webSocketClient;
        }
        
        public void CreateChallenge(TimeControlDto timeControl, SideColor sideColor = SideColor.None, long toPlayerId = 0)
        {
            var createChallenge = new ChallengeRequest(PlayerId, ChallengeRequestType.CreateChallenge);
            createChallenge.TimeControl = timeControl;
            createChallenge.SideColor = sideColor;
            createChallenge.ToPlayerId = toPlayerId;

            SendRequest(createChallenge);
        }

        public void RemoveChallenge(Guid challengeId)
        {
            var removeChallenge = new ChallengeRequest(PlayerId, ChallengeRequestType.RemoveChallenge);
            removeChallenge.ChallengeId = challengeId;
            SendRequest(removeChallenge);
        }

        public void AcceptChallenge(Guid challengeId)
        {
            var joinGame = new ChallengeRequest(PlayerId, ChallengeRequestType.AcceptChallenge);
            joinGame.ChallengeId = challengeId;
            SendRequest(joinGame);
        }

        public void DeclineChallenge(Guid challengeId)
        {
            var declineChallenge = new ChallengeRequest(PlayerId, ChallengeRequestType.DeclineChallenge);
            declineChallenge.ChallengeId = challengeId;
            SendRequest(declineChallenge);
        }

        public void ExitGame(Guid gameId)
        {
            var exitGame = new GameRequest(PlayerId, GameRequestType.ExitGame);
            exitGame.GameId = gameId;
            SendRequest(exitGame);
        }

        public void MakeMove(Guid gameId, string move)
        {
            var makeMove = new GameRequest(PlayerId, GameRequestType.MakeMove);
            makeMove.GameId = gameId;
            makeMove.Move = move;
            SendRequest(makeMove);
        }

        public void ResignGame(Guid gameId)
        {
            var resignGame = new GameRequest(PlayerId, GameRequestType.ResignGame);
            resignGame.GameId = gameId;
            SendRequest(resignGame);
        }
        
        public void OfferDraw(Guid gameId)
        {
            var offerDraw = new GameRequest(PlayerId, GameRequestType.OfferDraw);
            offerDraw.GameId = gameId;

            SendRequest(offerDraw);
        }

        public void AcceptDraw(Guid gameId)
        {
            var acceptDraw = new GameRequest(PlayerId, GameRequestType.AcceptDraw);
            acceptDraw.GameId = gameId;

            SendRequest(acceptDraw);
        }

        public void DeclineDraw(Guid gameId)
        {
            var declineDraw = new GameRequest(PlayerId, GameRequestType.DeclineDraw);
            declineDraw.GameId = gameId;
            SendRequest(declineDraw);
        }

        public void SendChatMessage(long toPlayerId, string message)
        {
            SendRequest(new DataRequest(toPlayerId, DataRequestType.Chat) { Data = message });
        }

        internal void SendRequest(WsMessage wsRequest)
        {
            SendRequestAction?.Invoke(JsonConvert.SerializeObject(wsRequest));
        }
    }
}
