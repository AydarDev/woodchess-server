﻿using ChessRules;

namespace WebSocketModels.Boards
{
    public interface IBoardConsumer
    {
        bool IsMyMove();
        void Rotate();
        void SendMove(string move);
        bool IsValidPick(SideColor figureColor);
    }
}