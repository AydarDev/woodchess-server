﻿namespace WebSocketModels.Boards.PromotionWizards.WizardStates
{
    internal class PromotionRotateEdgeState : PromotionWizardState
    {
        private static  PromotionRotateEdgeState _promotionRotateEdgeState;

        public static PromotionRotateEdgeState GetInstance(PromotionWizard promotionWizard)
        {
            if (_promotionRotateEdgeState == null)
                _promotionRotateEdgeState = new PromotionRotateEdgeState(promotionWizard);
            _promotionRotateEdgeState.promotionWizard = promotionWizard;
            return _promotionRotateEdgeState;
        }
        private PromotionRotateEdgeState(PromotionWizard promotionWizard) : base(promotionWizard)
        {
            
        }
        internal override void InitPromotionCoords()
        {
            X2 = baseX - 1;
            Y1 = 0;
            Y2 = 1;
        }

        internal override void FillPromotesInfo()
        {
            int index = 0;
            for (int y = 0; y <= 1; y++)
                for (int x = baseX; x >= baseX - 1; x--)
                    PromotesInfo.Add(PromoteObjectNames[index++], (x, y));
        }

        public override float[] GetLayoutPosition()
        {
            return new float[] { (float) (baseX - 0.5), (float) 0.5 };
        }
    }
}