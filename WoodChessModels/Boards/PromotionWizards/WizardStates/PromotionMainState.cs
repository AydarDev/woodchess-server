﻿namespace WebSocketModels.Boards.PromotionWizards.WizardStates
{
    internal class PromotionMainState : PromotionWizardState
    {
        private static PromotionMainState _promotionMainState;
        
        public static PromotionMainState GetInstance(PromotionWizard promotionWizard)
        {
            if (_promotionMainState == null)
                _promotionMainState = new PromotionMainState(promotionWizard);
            _promotionMainState.promotionWizard = promotionWizard;
            return _promotionMainState;
        }
        private PromotionMainState(PromotionWizard promotionWizard) : base(promotionWizard)
        {
            
        }
        internal override void InitPromotionCoords()
        {
            X2 = baseX + 1;
            Y1 = 7;
            Y2 = 6;
        }

        internal override void FillPromotesInfo()
        {
            int index = 0;
            for (int y = 7; y >= 6; y--)
                for (int x = baseX; x <= baseX + 1; x++)
                    PromotesInfo.Add(PromoteObjectNames[index++], (x, y));
        }

        public override float[] GetLayoutPosition()
        {
            return new float[] {baseX + (float) 0.5, (float) 6.5};
        }
    }
}