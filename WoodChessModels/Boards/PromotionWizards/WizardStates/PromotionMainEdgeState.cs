﻿namespace WebSocketModels.Boards.PromotionWizards.WizardStates
{
    class PromotionMainEdgeState : PromotionWizardState
    {
        private static PromotionMainEdgeState _promotionMainEdgeState;
        public static PromotionMainEdgeState GetInstance(PromotionWizard promotionWizard)
        {
            if (_promotionMainEdgeState == null)
                _promotionMainEdgeState = new PromotionMainEdgeState(promotionWizard);
            _promotionMainEdgeState.promotionWizard = promotionWizard;
            return _promotionMainEdgeState;
        }
        private PromotionMainEdgeState(PromotionWizard promotionWizard) : base(promotionWizard)
        {
            
        }
        
        internal override void InitPromotionCoords()
        {
            X2 = baseX - 1;
            Y1 = 7;
            Y2 = 6;
        }

        internal override void FillPromotesInfo()
        {
            int index = 0;
            for (int y = 7; y >= 6; y--)
            for (int x = baseX; x >= baseX - 1; x--)
                PromotesInfo.Add(PromoteObjectNames[index++], (x, y));
        }

        public override float[] GetLayoutPosition()
        {
            return new float[] { (float)(baseX - 0.5), (float)6.5 };
        }

    }

}
