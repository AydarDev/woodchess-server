﻿using System.Collections.Generic;

namespace WebSocketModels.Boards.PromotionWizards.WizardStates
{
    public abstract class PromotionWizardState
    {
        internal PromotionWizard promotionWizard { get; set; }

        internal PromotionWizardState(PromotionWizard promotionWizard)
        {
            this.promotionWizard = promotionWizard;
        }
        protected int baseX => promotionWizard.BaseX;

        internal abstract void FillPromotesInfo();
        public abstract float[] GetLayoutPosition();
        internal abstract void InitPromotionCoords();
        protected string[] PromoteObjectNames => promotionWizard.PromoteObjectNames[promotionWizard.Color];
        protected Dictionary<string, (int x, int y)> PromotesInfo => promotionWizard.PromotesInfo;
        protected int X2;
        protected int Y1;
        protected int Y2;
        
        internal static PromotionWizardState GetWizardState(PromotionWizard promotionWizard)
        {
            PromotionWizardState state = null;
            switch (promotionWizard.IsRotate)
            {
                case true:
                    if (promotionWizard.BaseX == 7)
                        state = PromotionRotateEdgeState.GetInstance(promotionWizard);
                    else
                        state = PromotionRotateState.GetInstance(promotionWizard);
                    break;
                case (false):
                    if (promotionWizard.BaseX == 7)
                        state = PromotionMainEdgeState.GetInstance(promotionWizard);
                    else
                        state = PromotionMainState.GetInstance(promotionWizard);
                    break;
            }

            return state;
        }
    }
}
