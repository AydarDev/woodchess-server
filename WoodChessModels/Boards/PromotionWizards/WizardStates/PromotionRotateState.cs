﻿namespace WebSocketModels.Boards.PromotionWizards.WizardStates
{
    class PromotionRotateState : PromotionWizardState
    {
        private static PromotionRotateState _promotionRotateState;

        public static PromotionRotateState GetInstance(PromotionWizard promotionWizard)
        {
            if (_promotionRotateState == null)
                _promotionRotateState = new PromotionRotateState(promotionWizard);
            _promotionRotateState.promotionWizard = promotionWizard;
            return _promotionRotateState;
        }
        private PromotionRotateState(PromotionWizard promotionWizard) : base(promotionWizard)
        {
            
        }
        internal override void InitPromotionCoords()
        {
            X2 = baseX + 1;
            Y1 = 0;
            Y2 = 1;
        }

        internal override void FillPromotesInfo()
        {
            int index = 0;
            for (int y = 0; y <= 1; y++)
                for (int x = baseX; x <= baseX + 1; x++)                    
                    PromotesInfo.Add(PromoteObjectNames[index++], (x, y));
        }

        public override float[] GetLayoutPosition()
        {
            return new float[] {(float) (baseX + 0.5), (float) 0.5};
        }
    }
}