﻿using System;
using System.Collections.Generic;
using ChessRules;
using WebSocketModels.Boards.PromotionWizards.WizardStates;

namespace WebSocketModels.Boards.PromotionWizards
{
    public class PromotionWizard
    {
        private readonly BoardState _boardState;
        
        private static PromotionWizardState _promotionWizardState;
        internal bool IsRotate => !Color.Equals(_boardState.Color);
        private char _rank;
        public Dictionary<SideColor, string[]> PromoteObjectNames => _boardState.PromoteObjectNames;
        internal SideColor Color { get; private set; }

        public String OnPromotionMove { get; set; } = string.Empty;
        internal int BaseX { get; private set; } = -1;
        public readonly Dictionary<string, (int x, int y)> PromotesInfo = new Dictionary<string, (int x, int y)>();

        internal PromotionWizard(BoardState boardState)
        {
            _boardState = boardState;
        }
        
        public void InitPromotion(string move)
        {
            Color = move[0].Equals('p') ? SideColor.Black : SideColor.White;
            if (!move.ToUpper().Contains("P")) return;
            PromotesInfo.Clear();
            OnPromotionMove = move;
            _rank = OnPromotionMove[3];
            BaseX = _boardState.RankToX(_rank);
            _promotionWizardState = PromotionWizardState.GetWizardState(this);
            _promotionWizardState.InitPromotionCoords();
            _promotionWizardState.FillPromotesInfo();
        }
        
        public float[] GetLayoutPosition()
        {
            return _promotionWizardState.GetLayoutPosition();
        }
    }
}
