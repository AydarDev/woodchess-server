﻿using System;
using ChessRules;

namespace WebSocketModels.Boards
{
    public class WhiteBoard : BoardState
    {
        //public override string[] PromoteObjectNames => IsRotate ? BlackPromoteNames : WhitePromoteNames;
        public override SideColor Color => SideColor.White;
        public WhiteBoard() : base()
        {
            
        }

        public override string CoordsToSquare((int x, int y)coords)
        {
            return ((char) ('a' + coords.x)).ToString() + (coords.y + 1);
        }

        public override (int, int) SquareToDigitCoords(string square)
        {
            var letter = square[0];
            var digit = int.Parse(square[1].ToString());


            return (letter - 'a', digit - 1);
        }
        public override string GetFigureAt(Chess chess, int x, int y)
        {
            return chess.GetFigureAt(x, y).ToString();
        }

        public override int RankToX(char rank)
        {
            return rank - 'a';
        }

        public override string XToRank(int rank)
        {
            return ((char)('A' + rank)).ToString();
        }

        public override string YToRow(int row)
        {
            return (row + 1).ToString();
        }
    }
}