﻿using System;
using System.Collections.Generic;
using System.Text;
using ChessRules;
using WebSocketModels.Boards.PromotionWizards;

namespace WebSocketModels.Boards
{
    public abstract class BaseBoard<TSquare, TFigure>
    {
        public IBoardConsumer BoardConsumer { get; set; }
        public BoardState BoardState { get; set; }
        public string LastMove { get; set; }
        public bool IsPromote { get; set; }

        public PromotionWizard PromotionWizard => BoardState.PromotionWizard;
        public Chess Chess;

        public Dictionary<string, TSquare> Squares { get; } = new Dictionary<string, TSquare>();
        public List<TFigure> Promotes { get; } = new List<TFigure>();
        public Action<string> Log;
        public TFigure SelectedFigure { get; set; }
        public bool IsFigureSelected => SelectedFigure != null;
        public bool IsRotate { get; set; }
        //private BoardSettings boardSettings => woodChess.GameConfiguration.BoardSettings;

        public readonly Dictionary<string, string> FigureNames = new Dictionary<string, string>()
        {
            {"P", "WhitePawn"},
            {"N", "WhiteKnight"},
            {"B", "WhiteBishop"},
            {"R", "WhiteRook"},
            {"Q", "WhiteQueen"},
            {"K", "WhiteKing"},
            {"p", "BlackPawn"},
            {"n", "BlackKnight"},
            {"b", "BlackBishop"},
            {"r", "BlackRook"},
            {"q", "BlackQueen"},
            {"k", "BlackKing"},
            {".", "Empty"}
        };
        public abstract void PickFigure(TFigure figure);
        public abstract bool IsValidPick(TFigure figure);
        public abstract void ShowSquare(int x, int y, bool marked = false);
        public abstract void MakeCaptions();
        public abstract void ShowPromotionFigures(string move);
        public abstract void HidePromotionFigures();
        public abstract void ResetObject(TFigure figure);
        public abstract void SetUpBoardImpl();
        public abstract void MakeMove(string move);

        public BaseBoard(){}
        public void SetUpBoard(string fen, SideColor sideColor = SideColor.White, string lastMove = "")
        {
            Chess = new Chess(fen);
            LastMove = lastMove;
            BoardState = BoardState.GetPlayBoard(sideColor, IsRotate);
            IsPromote = false;
            MakeCaptions();
            SetUpBoardImpl();
            UnmarkSquares();
            MarkLastMove();
        }

        public void Rotate()
        {
            if (IsPromote)
                return;
            IsRotate = !IsRotate;
            SetUpBoard(Chess.Fen, BoardState.Color.Flip(), LastMove);
            BoardConsumer.Rotate();
        }

        public void GoToPosition(string fen, string move = "")
        {
            Chess = new Chess(fen);
            LastMove = move;
            SetUpBoardImpl();
            MarkLastMove();
        }

        public void UpdateBoardState(string move)
        {
            Chess = Chess.Move(move);
        }

        public bool IsPromotionMove(string figureAt, string e4)
        {
            return (figureAt == "P" && e4[1] == '8') || // e4 = "d8"
                   (figureAt == "p" && e4[1] == '1');
        }
        
        public bool IsPromotionMove(string move)
        {
            return IsPromotionMove(move.Substring(0, 1), move.Substring(3, 2));
        }

        public bool IsValidMove(string move)
        {
            return BoardConsumer.IsMyMove() && Chess.IsValidMove(move);
        }

        public void MarkSquaresTo(string figureSquare)
        {
            /*if (!boardSettings.IsMarkPossibleMoves)
                return;*/

            UnmarkSquares();
            if (!string.IsNullOrEmpty(LastMove))
                MarkLastMove();
            foreach (string move in Chess.YieldValidMoves())
                if (move.StartsWith(figureSquare))
                {
                    (int x, int y) coords = BoardState.SquareToDigitCoords(move.Substring(3, 2));
                    ShowSquare(coords, true); //Nf3e5
                }
        }

        public void UnmarkSquares()
        {
            for (int y = 0; y < 8; y++)
            for (int x = 0; x < 8; x++)
                ShowSquare(x, y);
        }

        public void ShowSquare((int x, int y) coords, bool marked = false)
        {
            ShowSquare(coords.x, coords.y, marked);
        }

        public void MarkLastMove()
        {
            //TODO Generalize boardSettings
            if (LastMove == null || LastMove == String.Empty)
                return;
            //Pe2e4
            string fromSquare = LastMove.Substring(1, 2);
            string toSquare = LastMove.Substring(3, 2);
            (int x, int y) fromCoords = BoardState.SquareToDigitCoords(fromSquare);
            (int x, int y) toCoords = BoardState.SquareToDigitCoords(toSquare);
            ShowSquare(fromCoords.x, fromCoords.y, true);
            ShowSquare(toCoords.x, toCoords.y, true);
        }

        public string GetKey(int x, int y)
        {
            StringBuilder key = new StringBuilder();
            key.Append(x).Append(y);
            return key.ToString();
        }
    }
}