﻿using System.Collections.Generic;
using ChessRules;
using WebSocketModels.Boards.PromotionWizards;

namespace WebSocketModels.Boards
{
    public abstract class BoardState
    {
        public Dictionary<SideColor, string[]> PromoteObjectNames { get; } = new Dictionary<SideColor, string[]>
        {
            { SideColor.White, WhitePromoteNames },
            { SideColor.Black, BlackPromoteNames }
        };
        public abstract SideColor Color { get; }
        public abstract string GetFigureAt(Chess chess, int x, int y);
        public abstract string CoordsToSquare((int x, int y) coords);
        //public abstract string VectorToSquare(Vector2 point);
        public abstract (int x, int y) SquareToDigitCoords(string square);
        public abstract int RankToX(char rank);
        public abstract string YToRow(int row);
        public abstract string XToRank(int rank);
        public bool IsRotate { get; set; }

        protected static readonly string[] WhitePromoteNames = new string[] {"Q", "R", "B", "N"};
        protected static readonly string[] BlackPromoteNames = new string[] {"q", "r", "b", "n"};
        public static readonly Dictionary<SideColor, string[]> AllPromotes = new Dictionary<SideColor, string[]>
        {
            {SideColor.White, WhitePromoteNames},
            {SideColor.Black, BlackPromoteNames}
        };
        public PromotionWizard PromotionWizard { get; }

        public static BoardState GetPlayBoard(SideColor sideColor, bool rotate = false)
        {
            BoardState boardState;
            if (sideColor == SideColor.Black)
                boardState = new BlackBoard();
            else
                boardState = new WhiteBoard();
            boardState.IsRotate = rotate;
            return boardState;
        }

        internal BoardState()
        {
            PromotionWizard = new PromotionWizard(this);
        }
    }
}