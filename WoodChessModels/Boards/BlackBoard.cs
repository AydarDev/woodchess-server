﻿using System;
using ChessRules;

namespace WebSocketModels.Boards
{
    public class BlackBoard : BoardState
    {
        //public override string[] PromoteObjectNames => IsRotate ? WhitePromoteNames : BlackPromoteNames;
        public override SideColor Color => SideColor.Black;

        public BlackBoard() : base()
        {
            
        }

        public override string CoordsToSquare((int x, int y) coords)
        {
            return ((char)('h' - coords.x)).ToString() + (8 - coords.y);
        }

        public override (int, int) SquareToDigitCoords(string square)
        {
            char letter = square[0];
            int digit = int.Parse(square[1].ToString());

            return ('h' - letter, 8 - digit);
        }
        public override string GetFigureAt(Chess chess, int x, int y)
        {
            return chess.GetFigureAt(7 - x, 7 - y).ToString();
        }

        public override int RankToX(char rank)
        {
            return 'h' - rank;
        }
        
        public override string XToRank(int rank)
        {
            return ((char)('H' - rank)).ToString();
        }

        public override string YToRow(int row)
        {
            return (8 - row).ToString();
        }
    }
}
